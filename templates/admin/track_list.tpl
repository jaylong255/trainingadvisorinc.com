{include file='admin/admin_header.tpl'}


<BODY bgcolor="#FFFCED" onLoad="parent.HideTrackTabs(); MM_preloadImages('../themes/{$uiTheme}/admin/check_mark.gif', '../themes/{$uiTheme}/admin/btn_go_up.gif','../themes/{$uiTheme}/admin/btn_go_down.gif','../themes/{$uiTheme}/admin/btn_reset_up.gif','../themes/{$uiTheme}/admin/btn_reset_down.gif','../themes/{$uiTheme}/admin/btn_back_up.gif','../themes/{$uiTheme}/admin/btn_back_down.gif','../themes/{$uiTheme}/admin/btn_update_up.gif','../themes/{$uiTheme}/admin/btn_update_down.gif')">


<!--
{if isset($returnLink) && $returnLink}
<a href="{$decodedReturnURL}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Back','','../themes/{$uiTheme}/admin/btn_back_down.gif',1)">
<img name="Back" border="0" align="absbottom" width="23" height="21" src="../themes/{$uiTheme}/admin/btn_back_up.gif">
&nbsp;&nbsp;<b>Return to Class List</b></a><br><hr>
{/if}
-->


<!-- display search info -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td height="50">
    <form method="get" id="search_form" onSubmit="return DoResetSearch('search_form', '', false)">
    Search <select size="1" class="formvalue" name="get_search_by">
    {html_options options=$search_by_Opts selected=$get_search_by}
    </select>
    <!--select style="visibility:hidden; position:absolute; left:0px; top:0px; z-index:0" size="1" name="search_key">
    <option %s>t.Name</option>
    <option %s>t.Track_ID</option>
    <option %s>u.Full_Name</option>
    </select-->
    &nbsp;for&nbsp;<input type="text" class="formvalue" name="get_search" size="25" value="{$get_search}">&nbsp;
    <a href="javascript:DoResetSearch('search_form', '{$href}', false);" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('GoSearch','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)"><img name="GoSearch" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a>
    <a href="javascript:DoResetSearch('search_form', '{$href}', true);" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ResetSearch','','../themes/{$uiTheme}/admin/btn_reset_down.gif',1)"><img name="ResetSearch" border="0" align="absbottom" width="49" height="22" src="../themes/{$uiTheme}/admin/btn_reset_up.gif"></a>
    </form>
    </td>
    <td align="right" height="50"><form method="get" id="row_form" onSubmit="return ChangeMaxRows('row_form', '{$href1}', true)">
    Show&nbsp;<input type="text" class="formvalue" name="row_input" size="3" value="{$get_max_rows}">
    &nbsp;items per page&nbsp;
    <a href="javascript:ChangeMaxRows('row_form', '{$href1}', false);" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('GoMaxRows','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)"><img name="GoMaxRows" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a>
    </form>
    </td>
  </tr>
</table>
<hr>


<!-- links for creating and displaying tracks -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td>
      <a id="ColumnLink" href="javascript:ShowPopupMenu('ColumnMenu', 'ColumnLink', true);">Select columns</a>
      {if isset($get_assigned) && $get_assigned && ($isAdmin || $superUser)}
       | <a href="{$smarty.server.PHP_SELF}?$options{if $get_show_assigned}&get_show_assigned=1{/if}">Show all classes</a>
      {elseif $sessionIsAdmin}
       | <a href="track_edit.php?get_track=000000&{$return}">Create new class</a>
      {/if}
    </td>
  </tr>
</table>


<!-- Menu where user selects the columns they would like to see -->
<div id="ColumnMenu" style="visibility:hidden; position:absolute; left:0px; top:0px; z-index:10">
<table border="0" class="table_popup" cellpadding="2" cellspacing="0">

  <tr>
    <td>{$colImage1}</td>
    <td><a class="popup" href="{$colMenuHref1}">Class ID</a></td>
  </tr>
  <tr>
    <td>{$colImage2}</td>
    <td><a class="popup" href="{$colMenuHref2}">Name</a></td>
  </tr>
  <tr>
    <td>{$colImage3}</td>
    <td><a class="popup" href="{$colMenuHref3}">Supervisor</a></td>
  </tr>
</table>
</div>


<!-- Pagination information -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td width="50%">&nbsp;</td>
    <td width="50%">&nbsp;</td>
  </tr>
  <tr>
    <td width="50%">
      {if $numRows}
        Viewing {$startRow} - {$lastRow} of {$numRows}
      {else}
        <font color="#FF0000">No matching records found</font>
      {/if}
    </td>
    <td width="50%" align="right">
      {if $numPages}
        Page {$numPage} of {$numPages}
      {else}
        &nbsp;
      {/if}
    </td>
  </tr>
  <tr>
    <td width="50%">&nbsp;</td>
    <td width="50%">&nbsp;</td>
  </tr>
</table>


<!-- Start data display table -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="table_list">
  <tr>
    <td class="table_header" width="20">&nbsp;</td>
    {if isset($showIDColumn) && $showIDColumn}
    <td class="table_header" width="70">
      &nbsp;&nbsp;<a style="color:#FFFFFF" href="{$idRef}{if $sortBy == 't.Track_ID' && !$sortDescending}&get_descend=true{/if}">ID
      {if $sortBy == 't.Track_ID'}
	&nbsp;<img border="0" src="../themes/{$uiTheme}/admin/{if $sortDescending}order_descend.gif{else}order_ascend.gif{/if}">
      {/if}
      </a>
    </td>
    {/if}
    {if isset($showNameColumn) && $showNameColumn}
    <td class="table_header">
      &nbsp;&nbsp;<a style="color:#FFFFFF" href="{$nameRef}{if $sortBy == 't.Name' && !$sortDescending}&get_descend=true{/if}">Name
      {if $sortBy == 't.Name'}
	&nbsp;<img border="0" src="../themes/{$uiTheme}/admin/{if $sortDescending}order_descend.gif{else}order_ascend.gif{/if}">
      {/if}
      </a>
    </td>
    {/if}
    {if isset($showSupervisorColumn) && $showSupervisorColumn}
    <td class="table_header">
      &nbsp;&nbsp;<a style="color:#FFFFFF" href="{$superRef}{if $sortBy == 'u.Full_Name' && !$sortDescending}&get_descend=true{/if}">Supervisor
      {if $sortBy == 'u.Full_Name'}
	&nbsp;<img border="0" src="../themes/{$uiTheme}/admin/{if $sortDescending}order_descend.gif{else}order_ascend.gif{/if}">
      {/if}
      </a>
    </td>
    {/if}
    <td class="table_header" width="100" align="center">
      Actions
    </td>
  </tr>

  <!-- This section loops through the organization list -->
  {section name=trackListIndex loop=$trackID}
  <tr>
    <td class="{cycle name="colSelected" values="tdDefault,tdDefaultAlt"}" width="20">&nbsp;</td>
    {if isset($showIDColumn) && $showIDColumn}
    <td class="{cycle name="colID" values="tdDefault,tdDefaultAlt"}" width="70">
      {$trackID[trackListIndex]}
    </td>
    {/if}
    {if isset($showNameColumn) && $showNameColumn}
    <td class="{cycle name="colName" values="tdDefault,tdDefaultAlt"}">
      {$trackName[trackListIndex]}
    </td>
    {/if}
    {if isset($showSupervisorColumn) && $showSupervisorColumn}
    <td class="{cycle name="colSupervisor" values="tdDefault,tdDefaultAlt"}">
      &nbsp;{$trackSuper[trackListIndex]}
    </td>
    {/if}
    <td class="{cycle name="colAction" values="tdDefault,tdDefaultAlt"}" width="100" align="center">
      <a href="{$editRefs[trackListIndex]}">Edit</a>
      &nbsp;&nbsp;&nbsp;&nbsp;
      <a href="{$delRefs[trackListIndex]}">Delete</a>
    </td>
  </tr>
  {/section}
</table>


<!-- Pagination at the bottom of the page -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td width="50%">&nbsp;</td><td width="50%">&nbsp;</td>
  </tr>
  <tr>
    <td width="50%">&nbsp;</td>
    <td width="50%" align="right">
      {if $startRow > 1}
        <a href="{$prev_ref}">Previous</a>
      {/if}
      &nbsp;&nbsp;&nbsp;
      {if $numPage < $numPages}
        <a href="{$next_ref}">Next</a>
      {/if}
    </td>
  </tr>
  <tr>
    <td width="50%">&nbsp;</td>
    <td width="50%">&nbsp;</td>
  </tr>
</table>


</body>
</html>
