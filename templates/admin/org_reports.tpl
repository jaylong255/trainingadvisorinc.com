{include file='admin/admin_header.tpl'}
{include file='admin/org_header.tpl'}

<BODY>

<table border="0">
  <tr>
    <td class="OrgInfoText" height="20">
      <ul>
        <li><a href="org_report_accuracy_by_track.php">Question Accuracy By Class or Date Range</a></li>
	{if $showAb1825Reports}
        <li><a href="org_report_ab1825_completion_by_track.php">Completion of AB1825 Training By Class</a></li>
	{/if}
	{if $showIndividualReport}
	<li><a href="org_report_individual_by_track.php">Individual Assignment Report</a></li>
	{/if}
      </ul>
    </td>
    <td class="OrgInfoText" height="20">&nbsp;</td>
  </tr>
</table>

</body>
</html>
