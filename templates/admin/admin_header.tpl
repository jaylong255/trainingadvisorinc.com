<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
  <TITLE>Training Advisor Administration :: {$smarty.server.PHP_SELF}</TITLE>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">  
  <LINK rel='stylesheet' href='../themes/{$uiTheme}/desktop.css' type='text/css'>
  <SCRIPT language=JavaScript src="../javascript/rpc.js" type="text/javascript"></SCRIPT>
  <SCRIPT language=JavaScript src="../javascript/PopupMenu.js" type="text/javascript"></SCRIPT>
  <SCRIPT language=JavaScript src="../javascript/desktop.js" type="text/javascript"></SCRIPT>
  <SCRIPT language=JavaScript src="../javascript/admin.js" type="text/javascript"></SCRIPT>
  <SCRIPT language="JavaScript" type="text/javascript" src="../javascript/calendar.js"></SCRIPT>
{if isset($currentTab) && $currentTab == 5 && isset($currentSubTab) && $currentSubTab == 1}
  <!--LINK rel='stylesheet' href='../themes/{$uiTheme}/dtree.css' type='text/css'-->
  <LINK rel='stylesheet' href='../css/dtree.css' type='text/css'>
  <SCRIPT language="JavaScript" type="text/javascript" src="../javascript/dtree.js"></SCRIPT>
{/if}
</HEAD>
