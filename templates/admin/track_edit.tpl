{include file='admin/admin_header.tpl'}
{include file='admin/track_header.tpl'}


<form name="AdminForm" class="orgForm" method="POST" action="{$smarty.server.PHP_SELF}">
  <!--div id="Layer1" style="position:absolute; left:15px; top:55px; width:363px; height:31px; z-index:5"-->
  <!-- TODO test to see if this hidden field is even necessary.  Now that the track is stored in seession I do not think so -->
  <!--input type="hidden" name="trackId" value="{$track->trackId}"-->
  <table width="100%" border="0">
  <tr>
    <td class="OrgInfoText" width="90">Class ID:</td>
    <td width="150">
      <input class="FormDisabled" readonly type="text" name="trackId"
	     onClick="alert('This field is automatically generated and can not be changed.');"
	     value ="{if !$track->trackId}To Be Assigned{else}{$track->trackId}{/if}">
    </td>
    <td width="20">&nbsp;</td>
    <td valign="top" rowspan="6">
      <p><input class="FormValue" type="checkbox" name="isManagement" id="isManagement"
	    value="1" onchange="javascript:SetFlag();"{if $track->isManagement} checked{/if}>
	    Management Class - (Make all questions available to class.)</p>
      </p>
      {if !$track->trackId}
      <p>
        <input class="FormValue" type="checkbox" name="groupAssignments" id="groupAssignments" value="1"
	    {if $licenseAb1825 && !$track->trackId} checked disabled{/if}>
	    Group Assignments - (Automatically add all assignments.)
      </p>
      {/if}
      <p>
	<input class="FormValue" type="checkbox" name="isBinding" id="isBinding" value="1"
	    onchange="javascript:SetFlag();"
	    {if $track->isBinding || !$track->trackId} checked{/if}
	    {if $track->trackId || ($licenseAb1825 && !$track->trackId)} disabled{/if}>
            Bind Class - (Synchronize questions for all participants.)
      </p>
      <p>
	<input class="FormValue" type="checkbox" name="isLooping" id="isLooping"
	    value="1" onchange="javascript:SetFlag();" {if $track->isLooping} checked{/if}
	    {if $licenseAb1825 && !$track->trackId} disabled{/if}>
	    Loop Class - (Keep repeating questions in this class.)
      </p>
      <p>
      {if $licenseAb1825}
	<input class="FormValue" type="checkbox" name="isAb1825" id="isAb1825" value="1"
	    onchange="javascript:SetFlag(); ToggleAb1825Track();"
	    {if $track->isAb1825 || !$track->trackId} checked{/if}
	    {if is_numeric($track->trackId) && $track->trackId > 0} disabled{/if}>
	    AB1825 Class - (Meets California AB1825 training requirements.)
      {/if}
      </p>
    </td>
  </tr>
  <tr>
    <td class="OrgInfoText" width="90"> Name: &nbsp;&nbsp;</td>
    <td width="150">
      <input class="FormValue" type="text" name="trackName" id="trackName" maxlength="45"
	     size="30" value ="{$track->trackName}" onchange="javascript:SetFlag();">
    </td>
    <td width="20">&nbsp;</td>
  </tr>
  <tr>
    <td class="OrgInfoText" valign="middle" width="90">Supervisor: &nbsp;&nbsp;</td>
    <td width="150">
      <select name="supervisorId" class="FormValue" onchange="javascript:SetFlag();">
	<option></option>
        {html_options options=$track->supervisors selected=$track->supervisorId}
      </select>
    </td>
    <td width="20">&nbsp;</td>
  </tr>
  <tr>
    <td class="OrgInfoText" width="90">E-Mail Reply:</td>
    <td width="150">
      <input class="FormValue" type="text" name="emailReplyTo" maxlength="50" size="30" value ="{$track->emailReplyTo}"
	     onchange="javascript:SetFlag();">
    </td>
    <td width="20">&nbsp;</td>
  </tr>
  <tr>
    <td class="OrgInfoText" width="90">Start Date:</td>
    <td valign="center" width="150">
      <input class="FormValue" type="text" name="startDate" id="startDate" value ="{$track->startDate}"
	     onchange="javascript:SetFlag();" title="Schedule when questions will be delivered">
        <a href="javascript:doNothing()"
	   onClick="setDateField(document.getElementById('startDate'));top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')">
	  <img id="cal" src="../themes/{$uiTheme}/admin/calendar.gif" border="0" width="16" height="16"></a>
    </td>
    <td width="20">&nbsp;</td>
  </tr>
  <tr>
    <td class="OrgInfoText" width="90">Recurrence:</td>
    <td width="150">
      <textarea class="FormValue" rows="2" cols="25" name="strRecurrence" id="strRecurrence"
		onchange="javascript:SetFlag();" readonly>{$track->strRecurrence}</textarea>
      <input class="FormValue" type="hidden" name="recurrence" id="recurrence" value ="{$track->recurrence}"
	     onchange="javascript:SetFlag();">
    </td>
    <td valign="center" align="left" width="20" style="width:20px;">
      <a href="javascript:doNothing();"
	 onClick="window.open('recurrence.php?recurrence='+document.getElementById('recurrence').value,'recurrence','dependent=yes,width=515,height=200,screenX=200,screenY=300,titlebar=yes')" align="left">
      <img src="../themes/{$uiTheme}/admin/calendar.gif" border="0" width="17" height="16" align="left"></a>
    </td>
  </tr>
  <!--tr>
    <td class="OrgInfoText">Class News URL:</td>
    <td colspan="4">
      <input class="FormValue" type="text" name="trackNewsUrl" id="trackNewsUrl"
	     maxlength="150" size="85" value ="{$track->trackNewsUrl}" onchange="javascript:SetFlag();">
    </td>
  </tr-->
  <tr>
    <td class="OrgInfoText" width="90">Delinquency:</td>
    <td colspan="3" valign="center">
      <div style="float:left; vertical-align:middle"><input class="FormValue" style="vertical-align:middle;" type="text" name="delinquencyNotification" id="delinquencyNotification" size="4"
	     maxlength="3" value ="{if $track->delinquencyNotification > 0}{$track->delinquencyNotification}{/if}"
	     onchange="javascript:SetFlag();"
	     title="Notify class organizer when student has X or more questions assigned." align="left"></div>
	The number of questions a participant can fall behind before a delinquency report is sent by email. Leave blank if no report is desired.
    </td>
  </tr>
</table>
</form>
</body>
</html>
