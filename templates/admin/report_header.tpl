<table border="0" width="680" align="top" cellpadding="2" cellspacing="3">
  <tr>
    <td width="23">
      <a href="org_reports.php" onMouseOut="MM_swapImgRestore()"
	 onMouseOver="MM_swapImage('Back','','../themes/{$uiTheme}/admin/btn_back_down.gif',1)">
        <img name="Back" border="0" src="../themes/{$uiTheme}/admin/btn_back_up.gif" width="23" height="21" align="center"></a>
    </td>
    <td align="left">
      <a class="OrgInfoText" href="org_reports.php" alt="Return to Reports List">
	 Return to Reports List</a>
    </td>
    <td align="right">
	&nbsp;
    </td>
  </tr>
  {if isset($reportsErrMsg) && $reportsErrMsg}
  <tr>
    <td colspan="3" class="error">
      {$reportsErrMsg}
    </td>
  </tr>
  {/if}
  <tr>
    <td colspan="3"><hr width="100%"></td>
  </tr>
</table>
