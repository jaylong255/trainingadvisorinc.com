<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
  <TITLE>Class List</TITLE>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <LINK rel='stylesheet' href='../themes/{$uiTheme}/desktop.css' type='text/css'>
  <SCRIPT language="JavaScript" type="text/javascript" src="../javascript/PopupMenu.js"></SCRIPT>
  <SCRIPT language="javascript" type="text/javascript" src="../javascript/desktop.js"></SCRIPT>
  <SCRIPT language="javascript" type="text/javascript" src="../javascript/admin.js"></SCRIPT>
</HEAD>
<BODY bgcolor="#FFFCED" onLoad="parent.HideTrackTabs(); MM_preloadImages('../themes/{$uiTheme}/admin/check_mark.gif', '../themes/{$uiTheme}/admin/btn_go_up.gif','../themes/{$uiTheme}/admin/btn_go_down.gif','../themes/{$uiTheme}/admin/btn_reset_up.gif','../themes/{$uiTheme}/admin/btn_reset_down.gif','../themes/{$uiTheme}/admin/btn_back_up.gif','../themes/{$uiTheme}/admin/btn_back_down.gif','../themes/{$uiTheme}/admin/btn_update_up.gif','../themes/{$uiTheme}/admin/btn_update_down.gif')">

<a href="{$hrefReassign}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('CancelDelete','','../themes/{$uiTheme}/admin/btn_back_down.gif',1)">
  <img name="CancelDelete" border="0" align="absbottom" width="23" height="21" src="../themes/{$uiTheme}/admin/btn_back_up.gif">&nbsp;&nbsp;<b>Cancel delete and return to class list</b></a>
<br>

<hr>
  
  {if $numAssigned == 1}
    <br>There is currently $numAssigned employee assigned to the '$trackName' class.<br>
  {else}
    <br>There are currently $numAssigned employees assigned to the '$trackName' class.<br>
  {/if}
  <br><br>What would you like to do with these assignments?<br><br>
  
  <script language="JavaScript" type="text/javascript">
{literal}  
  function DoReAssignTrack(bReturn) {
{/literal}
  	var temp = "{$hrefJscript1}";
{literal}
  	form = document.getElementById("reassign_form");
  	if (form.reassign_option1.checked)
  		temp += '&get_action_reassign=000000';
  	else {
  		i = form.track.selectedIndex;
  		temp += '&get_action_reassign=' + form.trackid[i].text;
  	}
  	window.location.href=temp;
  	if (bReturn)
  		return false;
  }
  
  function OnReassignOption() {
  	form = document.getElementById("reassign_form");
  	if (form.reassign_option1.checked)
  		form.track.disabled = true;
  	else
  		form.track.disabled = false;
  }
{/literal}
  </script>
  
<form method="get" id="reassign_form" onSubmit="return DoReAssignTrack(true)">
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="25" height="25"></td>
    <td>
      <input type="radio" id="reassign_option1" value="remove" name="reassign_option" OnClick="OnReassignOption()" checked>
      <label for="reassign_option1">Remove class assignments for these employees</label>
    </td>
  </tr>
  <tr>
    <td width="25" height="25"></td>
    <td>
      <input type="radio" id="reassign_option2" value="reassign" name="reassign_option" OnClick="OnReassignOption()">
      <label for="reassign_option2">Assign employees to another class</label>&nbsp;&nbsp;
  
      <select size="1" class="formvalue" name="track" disabled>
      {if $delTrack}
        {html_options values=$trackNames}
      {else}
        <option>none</option>
        <script language="JavaScript" type="text/javascript">
    	  form = document.getElementById("reassign_form");
    	  if (form)
    		form.reassign_option2.disabled = true
        </script>
      {/if}
      </select>
      {if $trackIDs}
      <!--  if ($trackid_array && $count) { -->
      <select size="1" name="trackid" style="visibility:hidden">
      <!-- for ($i=0; $i < $count; $i++) -->
      {html_options values=$trackIDs}
      </select>
    </td>
  <tr>
    <td width="25"></td>
    <td>&nbsp;</td></tr>
  <tr>
    <td width="25"></td>
    <td><a href="javascript:DoReAssignTrack(false)" onMouseOut="MM_swapImgRestore()"
           onMouseOver="MM_swapImage('Update','','../themes/{$uiTheme}/admin/btn_update_down.gif',1)">
	<img name="Update" border="0" align="absbottom" src="../themes/{$uiTheme}/admin/btn_update_up.gif"></a>
    </td>
  </tr>
</table>
</form>
</body>
</html>
