{include file='admin/admin_header.tpl'}
{include file='admin/org_header.tpl'}
{include file='admin/report_header.tpl'}
<!--SCRIPT language=JavaScript src="../javascript/ajax.js" type="text/javascript"></SCRIPT-->

{if !isset($reportsErrMsg)}
<table border="0" width="680" align="top" cellpadding="2" cellspacing="3">
  <tr>
    <td width="23">
      <a href="org_reports.php" onMouseOut="MM_swapImgRestore()"
	 onMouseOver="MM_swapImage('Back','','../themes/{$uiTheme}/admin/btn_back_down.gif',1)">
        <img name="Back" border="0" src="../themes/{$uiTheme}/admin/btn_back_up.gif" width="23" height="21" align="center"></a>
    </td>
    <td align="left">
      <a class="OrgInfoText"
	href="org_report_accuracy_by_track.php?trackIdsSelected[]={$trackIdsSelected}{if $dateFrom}&dateFrom={$dateFrom}{/if}{if $dateTo}&dateTo={$dateTo}{/if}{if $questionId}&questionId={$questionId}{/if}{if $outputFormat}&outputFormat={$outputFormat}{/if}{if $resultFormatNumbers}&resultFormatNumbers=1{/if}{if $resultFormatPercentages}&resultFormatPercentages=1{/if}"
	alt="Return to Report Parameters">Return to Report Parameters</a>
    </td>
    <td align="right">
	&nbsp;
    </td>
  </tr>
  <tr>
    <td colspan="3"><hr width="100%"></td>
  </tr>
  {if empty($trackList)}
  <tr>
    <td colspan="2">
      There is not data in this report.  Please return the report parameters and ensure that the parameters you have selected should actually contain data.
    </td>
  </tr>
  {/if}
</table>



{section name=trackListIdx loop=$trackList}
<div class="Stacking">
  <span class="OrgInfoText">Report for Track: {$trackList[trackListIdx][0][1]}</span>
</div>

<div class="Stacking">
<table border="0" width="100%" align="center" bgcolor="#FFFFFF">
  <tr bgcolor="#DDDDDD">
    <th class="OrgInfoText">Question #</th>
    <th class="OrgInfoText">Topic</th>
    <th class="OrgInfoText">Delivery Date</th>
    <th class="OrgInfoText">Assigned</th>
    <th class="OrgInfoText">Completed</th>
    <th class="OrgInfoText">Understood</th>
    <th class="OrgInfoText">Correct</th>
  </tr>
  {section name=trackListDataIdx loop=$trackList[trackListIdx] start=1}
  <tr bgcolor="#FFFFFF">
    <td>{$trackList[trackListIdx][trackListDataIdx][0]}</td>
    <td>{$trackList[trackListIdx][trackListDataIdx][1]}</td>
    <td>{$trackList[trackListIdx][trackListDataIdx][2]}</td>
    <td align="center">{$trackList[trackListIdx][trackListDataIdx][3]}</td>
    <td align="center">{if $resultFormatNumbers}{$trackList[trackListIdx][trackListDataIdx][4]}{/if}
	{if $resultFormatPercentages}{if $resultFormatNumbers} ({/if}{math equation="$trackList[trackListIdx][trackListDataIdx][4]/$trackList[trackListIdx][trackListDataIdx][3]*100.0" format="%.1f"}%{if $resultFormatNumbers}){/if}{/if}</td>
    <td align="center">{if $resultFormatNumbers}{$trackList[trackListIdx][trackListDataIdx][5]}{/if}
	{if $resultFormatPercentages}{if $resultFormatNumbers} ({/if}{math equation="$trackList[trackListIdx][trackListDataIdx][5]/$trackList[trackListIdx][trackListDataIdx][4]*100.0" format="%.1f"}%{if $resultFormatNumbers}){/if}{/if}</td>
    <td align="center">{if $resultFormatNumbers}{$trackList[trackListIdx][trackListDataIdx][6]}{/if}
	{if $resultFormatPercentages}{if $resultFormatNumbers} ({/if}{math equation="$trackList[trackListIdx][trackListDataIdx][6]/$trackList[trackListIdx][trackListDataIdx][4]*100.0" format="%.1f"}%{if $resultFormatNumbers}){/if}{/if}</td>
  </tr>
  {/section}
  <tr bgcolor="#DDDDDD">
    <td colspan="3"><b>Summary Totals:</b></td>
    <td align="center"><b>{$trackList[trackListIdx][0][2]}</b></td>
    <td align="center"><b>{if $resultFormatNumbers}{$trackList[trackListIdx][0][3]}{/if}
    	{if $resultFormatPercentages}{if $resultFormatNumbers} ({/if}{math equation="$trackList[trackListIdx][0][3]/$trackList[trackListIdx][0][2]*100.0" format="%.1f"}%{if $resultFormatNumbers}){/if}{/if}</b></td>
    <td align="center"><b>{if $resultFormatNumbers}{$trackList[trackListIdx][0][4]}{/if}
    	{if $resultFormatPercentages}{if $resultFormatNumbers} ({/if}{math equation="$trackList[trackListIdx][0][4]/$trackList[trackListIdx][0][3]*100.0" format="%.1f"}%{if $resultFormatNumbers}){/if}{/if}</b></td>
    <td align="center"><b>{if $resultFormatNumbers}{$trackList[trackListIdx][0][5]}{/if}
    	{if $resultFormatPercentages}{if $resultFormatNumbers} ({/if}{math equation="$trackList[trackListIdx][0][5]/$trackList[trackListIdx][0][3]*100.0" format="%.1f"}%{if $resultFormatNumbers}){/if}{/if}</b></td>
  </tr>
</table>
</div>
<div class="Stacking">
  <hr width="100%">
</div>
{/section}
{/if}
<!--BR>
{include file='admin/report_footer.tpl'} -->

</body>
</html>
