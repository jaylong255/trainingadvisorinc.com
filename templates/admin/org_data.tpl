{include file='admin/admin_header.tpl'}
{include file='admin/org_header.tpl'}

<div id="DivOrgForm" class="orgForm">
<p>
<div id="Page1" class="OrgInfoText">Select an item below to add, modify, or delete values for that item.</div>
</p>

<!-- Table for displaying the menu selection on the left and the edit subwindow on the right -->
<table border="0" width="575">
  {section name=label loop=$editLabels}
    <tr>
      <td width="15" align="left">
        <img id="pointer{$smarty.section.label.iteration-1}" src="../themes/{$uiTheme}/admin/pointer.gif"
	     width="12" height="11" style="visibility: {if $smarty.section.label.first}visible{else}hidden{/if}">
      </td>
      <td width="185" align="left">
        <a class="OrgInfoText" href="org_data_edit.php?form={$smarty.section.label.iteration-1}"
	   target="DataEditFrame"
	   onclick="javascript:ChangePointer({$smarty.section.label.iteration-1});">{$editLabels[label]}:</a>
      </td>
      {if $smarty.section.label.first}
      <td rowspan="8" width="375">
        <div id="IFrameDiv">
          <IFRAME NAME="DataEditFrame" ID="DataEditFrameId" SCROLLING="auto" WIDTH="100%" HEIGHT="100%"
		 FRAMEBORDER="{$frameBoarderSize}" SRC="org_data_edit.php?form={$currentDataEditFrame}"></IFRAME>
        </div>
      </td>
      {/if}
    </tr>
  {/section}
    <tr>
      <td width="15" align="left">
        &nbsp;
      </td>
      <td width="185" align="left">
        &nbsp;
      </td>
    </tr>
</table>
  
<!--p>

<div id="Text2" class="PageText">
 * Optional fields that give you the ability to further categorize your employees. <BR>
 These categories might include location (other than division or department), job title, shift, etc.
</div>

</p-->

</div>
</BODY>
</HTML>
