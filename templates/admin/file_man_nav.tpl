{include file='admin/admin_header.tpl'}

<BODY class="ContentFrameBody">

  <SCRIPT language="javascript">
    t = new dTree('t');
    t.config.useStatusText=true;
    t.config.closeSameLevel=true;
    t.config.useCookies=false;
    t.config.inOrder=true;
    t.add(0, -1, '{$orgName} Files', 'file_man_edit.php?path=', '{$orgName} Files', 'file_man_edit');
    {section name=nodeIndex loop=$nodes}
     t.add({$smarty.section.nodeIndex.index+1}, {$nodes[nodeIndex].parent},
	'{$nodes[nodeIndex].name}', '{$nodes[nodeIndex].url}', '{$nodes[nodeIndex].name}', 'file_man_edit');
    {/section}
    document.write(t);
  </SCRIPT>

</body>
</html>
