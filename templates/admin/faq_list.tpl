{debug}
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
  <TITLE>FAQ List</TITLE>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <LINK rel="stylesheet" type="text/css" href="../themes/{$uiTheme}/desktop.css">
  <SCRIPT language="JavaScript" type="text/javascript" src="../javascript/PopupMenu.js"></SCRIPT>
  <SCRIPT language="javascript" type="text/javascript" src="../javascript/desktop.js"></SCRIPT>
  <SCRIPT language="javascript" type="text/javascript" src="../javascript/admin.js"></SCRIPT>
  <SCRIPT language="JavaScript" type="text/javascript" src="../javascript/calendar.js"></SCRIPT>

{literal}
	<script language="JavaScript" type="text/javascript">
  	function DoSearch() {
  		var temp = "$href";
  		form = document.getElementById("searchForm");
  		if (form.searchInput.value) {
  			temp += "&get_search=" + form.searchInput.value;
  			i = form.search_by.selectedIndex;
  			temp += "&get_search_by=" + form.searchKey[i].text;
  		}
  		window.location.href=temp;
  		return false;
  	}
  	function ResetSearch() {
  		form = document.getElementById("searchForm");
  		form.searchInput.value = '';
  		DoSearch();
  	}
  	function ChangeMaxRows(bReturn) {
  		form = document.getElementById("rowForm");
  		if (form.rowInput.value > 0 && form.rowInput.value < 1000) {
  			var temp = "$href" + "&get_max_rows=" + form.rowInput.value;
  			window.location.href=temp;
  		}
  		if (bReturn)
  			return false;
  	}
  	
	</script>
{/literal}
</HEAD>

<BODY bgcolor="#FFFCED" onLoad="MM_preloadImages('../themes/{$uiTheme}/admin/check_mark.gif', '../themes/{$uiTheme}/admin/btn_go_up.gif','../themes/{$uiTheme}/admin/btn_go_down.gif','../themes/{$uiTheme}/admin/btn_reset_up.gif','../themes/{$uiTheme}/admin/btn_reset_down.gif','../themes/{$uiTheme}/admin/btn_back_up.gif','../themes/{$uiTheme}/admin/btn_back_down.gif','../themes/{$uiTheme}/admin/btn_update_up.gif','../themes/{$uiTheme}/admin/btn_update_down.gif')">

<!-- Back link -->
  <a href="{$returnURL}"
      onMouseOut="MM_swapImgRestore()"
      onMouseOver="MM_swapImage('Back','','../themes/{$uiTheme}/admin/btn_back_down.gif',1)">
    <img name="Back" border="0" align="absbottom" width="23" height="21" src="../themes/{$uiTheme}/admin/btn_back_up.gif">
    &nbsp;&nbsp;
    <b>Return to {$returnName}</b>
  </a>
  <br>
  <hr>

<!-- Search bar -->
  <table>
    <tr>
     	<td height="50">
        <form method="get" id="searchForm" onSubmit="return DoSearch()">
        	Search
          <select size="1" class="formvalue" name="searchBy">
            <option>Question</option>
    	      <option>Answer</option>
          </select>
          	&nbsp;for&nbsp;
            <input type="text" class="formvalue" name="searchInput" size="25" value="{$search}">
            &nbsp;
          	<a href="javascript:DoSearch();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('GoSearch','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)"><img name="GoSearch" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a>
          	<a href="javascript:ResetSearch();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ResetSearch','','../themes/{$uiTheme}/admin/btn_reset_down.gif',1)"><img name="ResetSearch" border="0" align="absbottom" width="49" height="22" src="../themes/{$uiTheme}/admin/btn_reset_up.gif"></a>
        </form>
      </td>

<!-- Items per page -->
    	<td align="right" height="50">
        <form method="get" id="rowForm" onSubmit="return ChangeMaxRows(true)">
          Show&nbsp;<input type="text" class="formvalue" name="rowInput" size="3" value="{$faqsPerPage}">
          &nbsp;items per page&nbsp;
          <a href="javascript:ChangeMaxRows(false);" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('GoMaxRows','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)"><img name="GoMaxRows" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a>
        </form>
      </td>
    </tr>
	</table>
	<hr>

<!-- Links to page functionality -->
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr><td>
  	 <a id="ColumnLink" href="javascript:ShowPopupMenu('ColumnMenu', 'ColumnLink', true);">Select columns</a> |
     <a href="faq_edit.php?get_track={$assigned}&{$returnParameters}">Create new FAQ</a>
  	</td></tr>
  </table>

 	<div id="ColumnMenu" style="visibility:hidden; position:absolute; left:0px; top:0px; z-index:10">
  	<table border="0" class="table_popup" cellpadding="2" cellspacing="0">
      {section name=menuIndex loop=$columnMenuItem}
        <tr>
          <td>{$columnMenuItem.image}</td>
          <td>
            <a class="popup"
                 href="$PHP_SELF?$CurrentOptions&get_columns=$temp">
                 {$columnMenuItem.itemName}
            </a>
          </td>
        </tr>
      {/section}
  	</table>
  </div>


  <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td width="50%">&nbsp;</td>
      <td width="50%">&nbsp;</td>
    </tr>
    
    <tr>
      <td width="50%">
		    Viewing {$startFaq} - {$lastFaq} of {$nMaxRows}
      </td>
      <td width="50%" align="right">
        Page {$pageNumber} of {$maxPages}
    </tr>
    
    <tr>
      <td width="50%">&nbsp;</td>
      <td width="50%" align="right">
        {if ($startFaq <= 1)}
          Previous
        {else}
          <a href={$PHP_SELF}?get_start_row={$prevFaq}>Previous</a>
        {/if}

        &nbsp;&nbsp;&nbsp;

      	{if (($startFaq + $faqsPerPage) >= $maxFaqs)}
      		Next
      	{else}
      		{assign var="nextFaq" value="$startFaq + $faqsPerPage"}
      		<a href="{$PHP_SELF}"?get_start_row={$nextFaq}>Next</a>
        {/if}
        </td>
      </tr>
    <tr>
      <td width="50%">&nbsp;</td>
      <td width="50%">&nbsp;</td>
    </tr>
	</table>



<!-- column headings -->
	<table border="0" cellpadding="0" cellspacing="0" width="100%" id="columnHeading">
  	<tr>
      <td width="20" class="table_header" >&nbsp;&nbsp;</td>
      {section name=columnIndex loop=$columnHeading}
        {$columnHeading[columnIndex]}
      {/section}
    </tr>
  </table>


	<table border="0" cellpadding="0" cellspacing="0" width="100%">
  {section name=rowIndex loop=$rows}
    {cycle name="rowStyle" values="trDefault,trDefaultAlt" assign="rowStyle"}
    <tr class="{$rowStyle}">
      <td>&nbsp;</td>
      {section name=colIndex loop=$rows[rowIndex]}
        <td {if $colHdrWidth[colIndex] > 0} width="{$colHdrWidth[colIndex]}"{/if}>
          <font color="#C0C0C0">
            &nbsp;&nbsp;{$rows[rowIndex][colIndex]}
          </font>
        </td>
      {/section}
    </tr>
  {/section}
    <tr>
      <td width="20">&nbsp;</td>
      <td width="100" align="center">{$strLinks}</td>
    </tr>
  </table>

</BODY>
</HTML>

