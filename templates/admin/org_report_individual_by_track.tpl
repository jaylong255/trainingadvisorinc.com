{include file='admin/admin_header.tpl'}
{include file='admin/org_header.tpl'}
{include file='admin/report_header.tpl'}


<!--SCRIPT language=JavaScript src="../javascript/ajax.js" type="text/javascript"></SCRIPT-->
{if !isset($reportsErrMsg)}
<span class="OrgInfoText"><b>Individual Assignment by Class and Date Range Report</b><BR><BR>
	Description: This report provides a detailed list of individual assignment data for each person who has received<BR> an assignment in this class.  It can be filtered to include only specific question numbers.  The output will include<br> the individuals name, the assignment, question number, title, and information about how effectively the<BR> correct answer was provided.<BR><br>
	<b>Report Parameters:</b></span>
<form name="AdminForm" method="post" action="{$smarty.server.PHP_SELF}">
<table border="0">
  <tr>
    <td class="OrgInfoText" height="20" colspan="2">
      Select Classe(s):
    </td>
  </tr>
  {section name=trackIdx loop=$trackIds}
  {if $smarty.section.trackIdx.index%2 == 0}
  <tr>
  {/if}
    <td class="OrgInfoText" height="20"{if $smarty.section.trackIdx.index%2 == 0 && $smarty.section.trackIdx.last} colspan="2"{/if}>
      <input type="checkbox" name="trackIdsSelected[]" value="{$trackIds[trackIdx]}" alt="{$trackNames[trackIdx]}"
	     id="trackCheck{$smarty.section.trackIdx.index}"
	     onChange="javascript:loadDistinctValues(document.AdminForm.trackIdsSelected[]);"
	     {if isset($trackIdsSelected) && in_array($trackIds[trackIdx], $trackIdsSelected)} checked{/if}>{$trackNames[trackIdx]}
    </td>
  {if $smarty.section.trackIdx.index%2 != 0}
  </tr>
  {/if}
  {/section}
  {if $smarty.section.trackIdx.total}
  <tr>
    <td class="OrgInfoText" height="20">
      <a href="javascript:reportAccuracyToggleTracks('trackCheck', {$smarty.section.trackIdx.total});"
	 alt="Select all">Toggle All Classes</a>
    </td>
  </tr>
  {/if}
  <tr>
    <td class="OrgInfoText" height="20" colspan="2"><BR>
      Select the desired delivery date range for the selected question(s)
    </td>
  </tr>
  <tr>
    <td height="20"><span class="OrgInfoText">From</span>&nbsp;
      <select name="dateFrom" class="FormValue">
        <option lable="&lt;Any Date&gt;" value="0">&lt;Any Date&gt;</option>
        {html_options values=$deliveryDates output=$deliveryDates selected=$dateFrom}
      </select>
    </td>
    <td height="20"><span class="OrgInfoText">To</span>&nbsp;
      <select name="dateTo" class="FormValue">
        <option lable="&lt;Any Date&gt;" value="0">&lt;Any Date&gt;</option>
        {html_options values=$deliveryDates output=$deliveryDates selected=$dateTo}
      </select>
    </td>
  </tr>

<tr>

	<td>

		<label for="includeDivisionWithResults">
			<input type="checkbox" id="includeDivisionWithResults" name="includeDivisionWithResults">
			<span class="OrgInfoText">Include Division</span>&nbsp;
		</label>

	</td>

</tr>


  <tr>
    <td class="OrgInfoText" height="20" colspan="2"><BR>
      Select the output format for the report:<BR>
      <input type="radio" name="outputFormat" value="html"{if !isset($outputFormat) || $outputFormat == 'html'} checked{/if}>
      	     HTML (display as web page)<BR>
      <input type="radio" name="outputFormat" value="pdf"{if isset($outputFormat) && $outputFormat == 'pdf'} checked{/if}>
      	     PDF (used for hard copy, email attachments, and presentation)<BR>
      <input type="radio" name="outputFormat" value="csv"{if isset($outputFormat) && $outputFormat == 'csv'} checked{/if}>
      	     CSV (used for spreadsheets and charts)
    </td>
  </tr>
</table>
{/if}
<BR>
{include file='admin/report_footer.tpl'}
</form>
</body>
</html>
