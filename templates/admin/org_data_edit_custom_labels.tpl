{include file='admin/admin_header.tpl'}

<BODY bgcolor="#FFFCED" class="noscroll">

<form name="AdminForm" id="AdminForm" method="POST" action="org_data_edit.php">
<input type="hidden" name="elementValue" value="dummy">
<table border="0" class="PageText" width="350" align="left" valign="top">

  {if isset($dataElementValues) && $dataElementValues}
  {section name=dataElementIndex loop=$dataElementValues}
  <tr>
    <td align="left">
      <div class="OrgInfoText">Custom{$smarty.section.dataElementIndex.iteration} Label:</div>
    </td>
    <td>
      <input type="text" name="dataElementValues[]" value="{$dataElementValues[dataElementIndex]}"
	 class="FormValue" size="35" maxlengh="35" onKeyPress="OrgDataEditKeyPress(event);">
    </td>
  </tr>
  {/section}

  <tr>
    <td align="left">
      <div class="OrgInfoText">&nbsp;</div>
    </td>
    <td>
      <!-- The OrgDataEditSubmit method called below updates the value of this hidden field before submitting form -->
      <input type="hidden" name="operation" value="update">
      {if $dataElementValues}<a href="javascript:OrgDataEditSubmit('update');" class="OrgInfoText">Update</a>{/if}
    </td>
  </tr>
  {else}
  <tr>
    <td>You must first define some custom fields before you may define labels for them.</td>
  </tr>
  {/if}
</table>
</form>
</BODY>
</HTML>
