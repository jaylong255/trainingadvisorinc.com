<BODY class="ContentFrameBody"
      onLoad="MM_preloadImages('../themes/{$uiTheme}/admin/btn_update_up.gif','../themes/{$uiTheme}/admin/btn_update_down.gif',
			       '../themes/{$uiTheme}/admin/btn_back_down.gif');
	     {if isset($track) && $track->trackId != 0}parent.SetTab({$currentTab},{$currentSubTab});{/if}">

{if $currentSubTab > 0}

<table border="0" width="680" align="top" cellpadding="2" cellspacing="3" style="z-index:99">
  <tr>
    <td width="23">
      <a href="track_list.php" onClick="parent.SetTab({$currentTab},0); return CheckSave();"
	 onMouseOut="MM_swapImgRestore();" onMouseOver="MM_swapImage('Back','','../themes/{$uiTheme}/admin/btn_back_down.gif',1);">
        <img name="Back" border="0" src="../themes/{$uiTheme}/admin/btn_back_up.gif" width="23" height="21"></a>
    </td>
    <td class="OrgInfoText" align="left">
      <a href="track_list.php" onClick="javascript:parent.SetTab({$currentTab},0);">Return to Class List</a><BR>
    </td>
    <td align="right">
      {if isset($currentSubTab) && $currentSubTab == 1}
      {if $track->trackId == 0}
	<a href="javascript:AddRecordTrackEdit();" onMouseOut="MM_swapImgRestore()"
	   onMouseOver="MM_swapImage('AddBtn','','../themes/{$uiTheme}/admin/btn_add_down.gif',1)">
	  <img name="AddBtn" border="0" src="../themes/{$uiTheme}/admin/btn_add_up.gif" width="56" height="22"></a>
      {else}
	<a href="javascript:UpdateRecordTrackEdit();" onMouseOut="MM_swapImgRestore()"
	   onMouseOver="MM_swapImage('Update','','../themes/{$uiTheme}/admin/btn_update_down.gif',1)">
	  <img name="Update" border="0" src="../themes/{$uiTheme}/admin/btn_update_up.gif" width="56" height="22"></a>
      {/if}
      {else}
        &nbsp;
      {/if}
    </td>
  </tr>
  {if isset($errMsg) && $errMsg}
  <tr>
    <td colspan="3" class="error">
      {$errMsg}
    </td>
  </tr>
  {/if}
</table>

  {if isset($currentTab) && $currentTab == 1}
    <hr width="680" align="left">
  {else}
    <hr width="99%" align="left">
  {/if}


{/if}
