{include file='admin/admin_header.tpl'}
{include file='admin/org_header.tpl'}
{include file='admin/report_header.tpl'}
<!--SCRIPT language=JavaScript src="../javascript/ajax.js" type="text/javascript"></SCRIPT-->
{if !isset($reportsErrMsg)}

<table border="0" width="680" align="top" cellpadding="2" cellspacing="3">
  <tr>
    <td width="23">
      <a href="org_reports.php" onMouseOut="MM_swapImgRestore()"
	 onMouseOver="MM_swapImage('Back','','../themes/{$uiTheme}/admin/btn_back_down.gif',1)">
        <img name="Back" border="0" src="../themes/{$uiTheme}/admin/btn_back_up.gif" width="23" height="21" align="center"></a>
    </td>
    <td align="left">
      <a class="OrgInfoText"
	href="org_report_individual_by_track.php?trackIdsSelected[]={$trackIdsSelected}{if $dateFrom}&dateFrom={$dateFrom}{/if}{if $dateTo}&dateTo={$dateTo}{/if}{if $outputFormat}&outputFormat={$outputFormat}{/if}"
	alt="Return to Report Parameters">Return to Report Parameters</a>
    </td>
    <td align="right">
	&nbsp;
    </td>
  </tr>
  {if isset($reportsErrMsg) && $reportsErrMsg}
  <tr>
    <td colspan="3" class="error">
      {$reportsErrMsg}
    </td>
  </tr>
  {/if}
  <tr>
    <td colspan="3"><hr width="100%"></td>
  </tr>
</table>



{section name=trackListIdx loop=$trackList}
<div class="Stacking">
  <span class="OrgInfoText">Report for Track: {$trackList[trackListIdx][0][1]}</span>
</div>

<div class="Stacking">
<table border="0" width="100%" align="center" bgcolor="#FFFFFF">
  <tr bgcolor="#DDDDDD">
    <th class="OrgInfoText">Login Name ({$trackList[trackListIdx][0][2]})</th>
    {if isset($includeDivisionWithResults)}
    <th class="OrgInfoText">Division</th>
    {/if}
    <th class="OrgInfoText">Assigned Questions ({$trackList[trackListIdx][0][3]})</th>
    <th class="OrgInfoText">Answered ({$trackList[trackListIdx][0][4]})</th>
    <th class="OrgInfoText">Unanswered ({$trackList[trackListIdx][0][5]})</th>
    <th class="OrgInfoText">Correct ({$trackList[trackListIdx][0][6]})</th>
  </tr>
  {section name=trackListDataIdx loop=$trackList[trackListIdx] start=1}
  <tr bgcolor="#FFFFFF">
    <td>{$trackList[trackListIdx][trackListDataIdx][0]}</td>
    {if isset($includeDivisionWithResults)}
    <td>{$trackList[trackListIdx][trackListDataIdx][5]}</td>
    {/if}
    <td align="center">{$trackList[trackListIdx][trackListDataIdx][1]}</td>
    <td align="center">{$trackList[trackListIdx][trackListDataIdx][2]}</td>
    <td align="center">{$trackList[trackListIdx][trackListDataIdx][3]}</td>
    <td align="center">{$trackList[trackListIdx][trackListDataIdx][4]}</td>
  </tr>
  {/section}
</table>
</div>
<div class="Stacking">
  <hr width="100%">
</div>
{/section}
{/if}
<!--BR>
{include file='admin/report_footer.tpl'} -->

</body>
</html>
