{include file='admin/admin_header.tpl'}

<BODY class="ContentFrameBody" onLoad="{if $reloadFrame}parent.frames[0].location.reload();{/if}
   {if isset($refreshMotif) && $refreshMotif}
        ChangeOrg('{$orgName}', '{$topLeftBack}', '{$topLeftFront}', '{$topLeftBackRepeat}', '{$topMiddleBack}',
		  '{$topMiddleFront}', '{$topMiddleBackRepeat}', '{$topRightBack}', '{$topRightFront}', '{$topRightBackRepeat}', parent.parent);
   {/if}">

<form enctype="multipart/form-data" class="CustomizeForm" name="FileEditForm" method="POST" action="{$smarty.server.PHP_SELF}">

{if isset($errMsg) && $errMsg}
  <span class="error">{$errMsg}</span>
{/if}


{if isset($path)}
  <input type="hidden" name="path" value="{$path}">
  <p class="highlighted" style="width: 90%;"><span class="Large">File/Folder Selected: {$path}</span></p>
{/if}

{if isset($isDir) && $isDir}
<!--  If this is a directory -->
<span class="OrgInfoText" align="left">Upload File to Selected Folder</span><BR>
  <input type="hidden" name="MAX_FILE_SIZE" value="20971520">
  <input type="file" name="sendFile" size="85" maxlen="128">
  <input type="submit" name="sendSubmit" value="Upload File"><BR>
  <hr width="90%" align="left">
<span class="OrgInfoText" align="left">Create New Folder Named</span><BR>
  <input type="text" name="createFolder" size="85" maxlen="128">
  <input type="submit" name="createFolderSubmit" value="Create Folder"><BR>
  <hr width="90%" align="left">
{/if}

{if isset($isRoot) && !$isRoot}
<span class="OrgInfoText" align="left">Rename {if isset($isDir) && $isDir}Folder{else}File{/if}</span><BR>
  <input type="text" name="renameFile" size="85" maxlen="128" value="{$basepath}">
  <input type="submit" name="renameSubmit" value="Rename"><BR>
  <hr width="90%" align="left">
{/if}

{if isset($path) && !empty($path) && !$isRoot}
  <table>
    <tr>
      <td>
        <input type="checkbox" name="deleteFile" value="1" align="left">
      </td>
      <td valign="top">
        <span class="OrgInfoText">
	  Delete Selected {if isset($isDir) && $isDir}Folder And All Files Or Folders Within This Folder{else}File{/if}
	</span>
	<input type="submit" name="deleteSubmit" value="Confirm Delete">
      </td>
    </tr>
  </table>
  <hr width="90%" align="left">

  <table>
    <tr>
      <td valign="top">
        <span class="OrgInfoText">
	  <input type="radio" name="copyMove" value="copy">Copy OR
	  <input type="radio" name="copyMove" value="move">Move Selected {if isset($isDir) && $isDir}Folder{else}File{/if} To:
	</span>
        <select name="copyMoveDest">
           <option value="/">/</option>
        {section name=nodeIndex loop=$nodes}
           {if $nodes[nodeIndex].isDir && $nodes[nodeIndex].path != $path}
	     <option value="{$nodes[nodeIndex].path}">{$nodes[nodeIndex].path}</option>
	   {/if}
        {/section}
	</select>
	<input type="submit" name="copyMoveSubmit" value="Execute">
      </td>
    </tr>
  </table>
  <hr width="90%" align="left">

{/if}
  <!--hr width="90%" align="left"-->
{if isset($isEmail) && $isEmail}
<span class="OrgInfoText" align="left">
  Send test email message to the following recipient(s):</span><BR>
  Select contact: <select name="testEmailUser" class="FormValue">
	<option></option>
	{html_options options=$contactList}
	</select> and/or enter an email address: <input type="text" name="testEmailAddr" size="35">
  <input type="submit" name="testEmailSubmit" value="Send"><BR>
  <hr width="90%" align="left">
{/if}

{if isset($isEditable) && $isEditable && !(isset($isDir) || $isDir)}
<span class="OrgInfoText" align="left">
  Edit File</span><BR>
  <textarea name="editFile" rows="10" cols="50">{$fileContent}</textarea><BR>
  <input type="submit" name="editSubmit" value="Rename"><BR>
  <hr width="90%" align="left">
{/if}

{if isset($showNewsOptions) && $showNewsOptions}
  <table>
    <tr>
      <td>
        <input type="checkbox" name="makeNewsFile" value="1" align="left"{if $publishSelected} checked{/if}>
      </td>
      <td valign="top">
        <span class="OrgInfoText">
	  Publish contents of selected file to news
	</span>
	<input type="submit" name="submitNewsFile" value="Confirm Publish">
      </td>
    </tr>
  </table>
  <hr width="90%" align="left">
{/if}

{if isset($showEdit) && $showEdit}
  <span class="OrgInfoText">Edit File: {$path}</span><BR>
  <textarea name="fileContents" rows="15" cols="80">{$fileContents}</textarea><BR>
  <input type="submit" name="editSubmit" value="Save File">
  <hr width="90%" align="left">
{/if}

{if isset($imageUrlPath)}
  <span class="OrgInfoText">Below is the image file you selected (images restricted to a height of 97 pixels):</span><BR>
  <img src="{$imageUrlPath}" border="0" name="imageUrlPath"><BR>
  <span class="OrgInfoText">
    If you would like to use this image as part of the motif, select the region(s) in which you would like it to appear:<BR>
    <input type="checkbox" name="topLeftBack" value="1"{if $configTopLeftBack==$path} checked{/if}>Left Background Image<BR>
    &nbsp;&nbsp;&nbsp;<input type="checkbox" name="topLeftBackRepeat" id="topLeftBackRepeat" value="1"{if $topLeftBackRepeat} checked{/if}>Repeat left background image across the display area<BR>
    <input type="checkbox" name="topLeftFront" value="1"{if $configTopLeftFront==$path} checked{/if}>Left Foreground Image<BR>
    <input type="checkbox" name="topMiddleBack" value="1"{if $configTopMiddleBack==$path} checked{/if}>Middle Background Image<BR>
    &nbsp;&nbsp;&nbsp;<input type="checkbox" name="topMiddleBackRepeat" value="1"{if $topMiddleBackRepeat} checked{/if}>Repeat middle background image across the display area<BR>
    <input type="checkbox" name="topMiddleFront" value="1"{if $configTopMiddleFront==$path} checked{/if}>Middle Foreground Image<BR>
    <input type="checkbox" name="topRightBack" value="1"{if $configTopRightBack==$path} checked{/if}>Right Background Image<BR>
    &nbsp;&nbsp;&nbsp;<input type="checkbox" name="topRightBackRepeat" value="1"{if $topRightBackRepeat} checked{/if}>Repeat right background image across the display area<BR>
    <input type="checkbox" name="topRightFront" value="1"{if $configTopRightFront==$path} checked{/if}>Right Foreground Image<BR>
    <input type="checkbox" name="useDefaultMotif" value="1"{if isset($useDefaultMotif) && $useDefaultMotif} checked{/if}>Use the default Motif<BR>
	<input type="submit" name="publishMotif" value="Publish Changes"><BR>
  </span>

  <hr width="90%" align="left">
{elseif isset($motifFolderSelected) && $motifFolderSelected}
  {if isset($useDefaultMotif) && $useDefaultMotif}
    <input type="submit" name="publishMotif" value="Disable Default Motif">
  {else}
    <input type="submit" name="publishMotif" value="Publish Default Motif">
  {/if}
  <BR>
  <hr width="90%" align="left">
{/if}


</form>

</body>
</html>
