{include file='admin/admin_header.tpl'}
{include file="admin/$localHeader.tpl"}

<!-- Search Bar -->
<table border="0" cellpadding="0" cellspacing="0" width="99%">
  <tr>
    <form method="POST" id="search_form" name="search_form" action="{$smarty.server.PHP_SELF}">
    <td>
      Search
      <select size="1" class="formvalue" name="searchBy">
	 {html_options options=$columnLabels selected=$searchBy}
      </select> for <input type="text" class="formvalue" name="searchFor" size="25">
      <a href="javascript:document.search_form.submit();" onMouseOut="MM_swapImgRestore()"
       	 onMouseOver="MM_swapImage('GoSearch','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)">
      <img name="GoSearch" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a>
    </td>
    </form>
    <form method="POST" id="row_form" name="row_form" action="../login/change_prefs.php">
    <td align="right">
      Show&nbsp;<input type="text" class="formvalue" name="showRows" size="3" value="{$rowsPerPage}">
      &nbsp;items per page&nbsp;
      <a href="javascript:document.row_form.submit();" onMouseOut="MM_swapImgRestore()"
         onMouseOver="MM_swapImage('GoMaxRows','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)">
      <img name="GoMaxRows" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a>
      <input type="hidden" name="returnUrl" value="{$smarty.server.PHP_SELF}">
    </td>
      </form>
  </tr>
  {if FALSE && $currentTab == 4 && (isset($currentSubTab) && ($currentSubTab == 1 || $currentSubTab == 2))}<!--questions tab-->
  <tr>
    <td>
      Category: <select size="1" class="formvalue" name="displayCategories">
	{html_options options=$displayCategories selected=$displayCategory}
      </select>
    </td>
    <td align="right">
      {if $currentSubTab == 1}
      <input type="button" value="Release Questions" OnClick="BranchTo('question_release.php')">
      {else}
      &nbsp;
      {/if}
    </td>
  </tr>
  {/if}
</table>
<hr align="left" width="99%">



<!-- Links -->
<table border="0" cellpadding="0" cellspacing="0" width="99%">
  <tr>
    <td>
      <a id="ColumnLink" href="javascript:ShowPopupMenu('ColumnMenu', 'ColumnLink', true);">Select columns</a>
      {if isset($topLinkList)}
	{if gettype($topLinkList) == 'array'}
          {section name=topLinkIdx loop=$topLinkList}
         | <a href="{$topLinkList[topLinkIdx]->href}{if $topLinkList[topLinkIdx]->params}?{$topLinkList[topLinkIdx]->params}{/if}"
	      {if $topLinkList[topLinkIdx]->target}target="{$topLinkList[topLinkIdx]->target}"{/if}
	      {if $topLinkList[topLinkIdx]->class}class="{$topLinkList[topLinkIdx]->class}"{/if}
	      {if $topLinkList[topLinkIdx]->style}style="{$topLinkList[topLinkIdx]->style}"{/if}
	      {if $topLinkList[topLinkIdx]->onClick}onClick="javascript:{$topLinkList[topLinkIdx]->onClick};"{/if}
	      {if $topLinkList[topLinkIdx]->alt}alt="{$topLinkList[topLinkIdx]->alt}"{/if}>{$topLinkList[topLinkIdx]->caption}</a>
          {/section}
        {elseif $topLinkList}
         | <a href="{$topLinkList->href}{if $topLinkList->params}?{$topLinkList->params}{/if}"
	      {if $topLinkList->target}target="{$topLinkList->target}"{/if}
	      {if $topLinkList->class}class="{$topLinkList->class}"{/if}
	      {if $topLinkList->style}style="{$topLinkList->style}"{/if}
	      {if $topLinkList->onClick}onClick="javascript:{$topLinkList->onClick}"{/if}
	      {if $topLinkList->alt}alt="{$topLinkList->alt}"{/if}>{$topLinkList->caption}</a>
        {/if}
      {/if}
    </td>
    <td width="50%" align="right">
      {if $pageNumber > 1}
        <a href="../login/change_prefs.php?{$pageNumberParamName}={$pageNumber-1}&returnUrl={$smarty.server.PHP_SELF}">Previous</a>
      {/if}
      &nbsp;&nbsp;&nbsp;
      {if $pageNumber < $numPages}
        <a href="../login/change_prefs.php?{$pageNumberParamName}={$pageNumber+1}&returnUrl={$smarty.server.PHP_SELF}">Next</a>
      {/if}
    </td>
  </tr>
</table>


<div id="ColumnMenu" style="visibility:hidden; position:absolute; left:0px; top:0px; z-index:10">
<table border="0" class="table_popup" cellpadding="2" cellspacing="0">
{foreach name=selectColumns key=columnId item=columnLabel from=$columnLabels}
  <tr>
    <td>{if $columnId & $showColumns}
	<img src="../themes/{$uiTheme}/admin/check_mark.gif">{else}&nbsp;{/if}</td>
    <td><a class="popup" href="../login/change_prefs.php?returnUrl={$smarty.server.PHP_SELF}&{$toggleParamName}={$columnId}">
	{$columnLabel}</a></td>
  </tr>
{/foreach}
</table>
</div>


<!-- Formerly DisplayPageInfo() Includes page viewing and display info -->
<table border="0" cellpadding="0" cellspacing="0" width="99%">
  <tr>
    <td width="50%">&nbsp;</td><td width="50%">&nbsp;</td>
  </tr>
  <tr>
    <td width="50%">
      {if $numRows}
        Viewing <span id="first_row" name="first_row">{$firstRow+1}</span> - <span id="last_row" name="last_row">{$lastRow+1}</span> of <span id="num_rows" name="num_rows">{$numRows}</span>
      {else}
        <font color="#FF0000">No matching records found</font>
      {/if}
    </td>
    <td width="50%" align="right">
      {if $numRows}
        Page {$pageNumber} of {$numPages}
      {else}
        &nbsp;
      {/if}
    </td>
  </tr>
  <tr>
    <td width="50%">&nbsp;</td>
    <td width="50%">&nbsp;</td>
  </tr>
</table>

<!-- end of DisplayPageInfo -->






{if $numRows}
<!-- heders and data -->
<table id="dataTable" name="dataTable" border="0" cellpadding="3" cellspacing="0" width="99%" class="table_list">
  <tr>
    {if isset($showSelectedColumn) && $showSelectedColumn}
      <td class="table_header" width="20">&nbsp;</td>
    {/if}
    {foreach name=columnHeaders key=columnId item=columnLabel from=$columnLabels}
      {if $columnId & $showColumns}
        <td class="table_header">
        {if $columnId == $sortColumn}
	  {if $sortDescending}
           <a style="color:#FFFFFF"
	      href="../login/change_prefs.php?returnUrl={$smarty.server.PHP_SELF}&{$orderParamName}=0">
	      {$columnLabel}</a>
	   <a href="../login/change_prefs.php?returnUrl={$smarty.server.PHP_SELF}&{$orderParamName}=0">
	      <img border="0" src="../themes/{$uiTheme}/admin/order_descend.gif"></a>
	  {else}
           <a style="color:#FFFFFF"
	      href="../login/change_prefs.php?returnUrl={$smarty.server.PHP_SELF}&{$orderParamName}=1">
	      {$columnLabel}</a>
	   <a href="../login/change_prefs.php?returnUrl={$smarty.server.PHP_SELF}&{$orderParamName}=1">
	      <img border="0" src="../themes/{$uiTheme}/admin/order_ascend.gif"</a>
	  {/if}
        {else}
         <a style="color:#FFFFFF" href="../login/change_prefs.php?returnUrl={$smarty.server.PHP_SELF}&{$sortParamName}={$columnId}">
	    {$columnLabel}</a>
        {/if}
        </td>
      {/if}
    {/foreach}
    {if !isset($hideActionHeader)}
    <td class="table_header" width="100">
      Actions
    </td>
    {/if}
  </tr>



  <!-- This section loops through the list of data provided-->
  {section name=recordListIndex loop=$recordList}
  {cycle name="rowClass" assign="trStyle" values="trDefault,trDefaultAlt"}
  <tr{if isset($recordList[recordListIndex].rowClass)} class="{$recordList[recordListIndex].rowClass}"{else} class="{$trStyle}"{/if}>
    {if isset($showSelectedColumn) && $showSelectedColumn}
    <td width="20">
      {if isset($recordId) && $recordIds[recordListIndex] == $recordId}
        <img id="{$recordId}" border="0" src="../themes/{$uiTheme}/admin/check_mark.gif">
      {else}
        &nbsp;
      {/if}
    </td>
    {/if}
    {foreach name=org key=orgColumnName item=orgColumnValue from=$recordList[recordListIndex]}
    {if is_numeric($orgColumnName)}
    <td>
      {if gettype($orgColumnValue) == 'array'}
        {section name=actionIdx loop=$orgColumnValue}
	  {if !$smarty.section.actionIdx.first}&nbsp;&nbsp;&nbsp;{/if}
	  {if gettype($orgColumnValue[actionIdx]) == 'object'}
          <a href="{$orgColumnValue[actionIdx]->href}{if (strpos($orgColumnValue[actionIdx]->href, 'javascript') !== 0 && strpos($orgColumnValue[actionIdx]->href, '#') !== 0 && $orgColumnValue[actionIdx]->isService == FALSE)}?{if isset($idParamName)}{$idParamName}={$recordIds[recordListIndex]}{/if}{/if}{if $orgColumnValue[actionIdx]->params}{if isset($idParamName)}&{/if}{$orgColumnValue[actionIdx]->params}{/if}"
	    {if $orgColumnValue[actionIdx]->id}id="{$orgColumnValue[actionIdx]->id}"{else}id="action{$smarty.section.actionIdx.index}-{$smarty.section.recordListIndex.index}"{/if}
	    {if $orgColumnValue[actionIdx]->target}target="{$orgColumnValue[actionIdx]->target}"{/if}
	    {if $orgColumnValue[actionIdx]->class}class="{$orgColumnValue[actionIdx]->class}"{/if}
	    {if $orgColumnValue[actionIdx]->style}style="{$orgColumnValue[actionIdx]->style}"{/if}
	    {if $orgColumnValue[actionIdx]->onClick}onClick="return {$orgColumnValue[actionIdx]->onClick};"{/if}
	    {if $orgColumnValue[actionIdx]->alt}
	    alt="{$orgColumnValue[actionIdx]->alt}"{/if}>{$orgColumnValue[actionIdx]->caption}</a>
          {else}
	    {if $orgColumnValue[actionIdx]}{$orgColumnValue[actionIdx]}{else}<span class="DisabledText">Not Set</span>{/if}
	  {/if}
	{/section}
      {else}
	{if gettype($orgColumnValue) == 'object'}
          <a href="{$orgColumnValue->href}{if (strpos($orgColumnValue->href, 'javascript') !== 0 && strpos($orgColumnValue->href, '#') !== 0 && $orgColumnValue->isService == FALSE)}?{if isset($idParamName)}{$idParamName}={$recordIds[recordListIndex]}{/if}{/if}{if $orgColumnValue->params}{if isset($idParamName)}&{/if}{$orgColumnValue->params}{/if}"
	  {if $orgColumnValue->id}id="{$orgColumnValue->id}"{else}id="action0-{$smarty.section.recordListIndex.index}"{/if}
	  {if $orgColumnValue->target}target="{$orgColumnValue->target}"{/if}
	  {if $orgColumnValue->class}class="{$orgColumnValue->class}"{/if}
	  {if $orgColumnValue->style}style="{$orgColumnValue->style}"{/if}
	  {if $orgColumnValue->onClick}onClick="return {$orgColumnValue->onClick};"{/if}
	  {if $orgColumnValue->alt}
	      alt="{$orgColumnValue.alt}"{/if}>{$orgColumnValue->caption}</a>
        {else}
	  {if $orgColumnValue}{$orgColumnValue}{else}<span class="DisabledText">Not Set</span>{/if}
	{/if}
      {/if}
    </td>
    {/if}
    {/foreach}
  </tr>
  {/section}
</table>
      

<!-- Display page footer with previous and next links at bottom -->
<table border="0" cellpadding="0" cellspacing="0" width="99%">
  <tr>
    <td width="50%">&nbsp;</td><td width="50%">&nbsp;</td>
  </tr>
  <tr>
    <td width="50%">&nbsp;</td>
    <td width="50%" align="right">
      {if $pageNumber > 1}
        <a href="../login/change_prefs.php?returnUrl={$smarty.server.PHP_SELF}&{$pageNumberParamName}={$pageNumber-1}">Previous</a>
      {/if}
      &nbsp;&nbsp;&nbsp;
      {if $pageNumber < $numPages}
        <a href="../login/change_prefs.php?returnUrl={$smarty.server.PHP_SELF}&{$pageNumberParamName}={$pageNumber+1}">Next</a>
      {/if}
    </td>
  </tr>
  <tr>
    <td width="50%">&nbsp;</td>
    <td width="50%">&nbsp;</td>
  </tr>
</table>
{/if}

{if isset($includeSequenceMenu) && $includeSequenceMenu}{include file='admin/sequence_menu.tpl'}{/if}
{if isset($includeGroupMenu) && $includeGroupMenu}{include file='admin/group_menu.tpl'}{/if}

</body>
</html>
