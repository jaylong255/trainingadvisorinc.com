{include file='admin/admin_header.tpl'}

<BODY class="ContentFrameBody">


{if isset($searchResults)}

<!--div id="Title" style="position:absolute; left:11px; top:11px; width:100%; height:18px; z-index:2"-->
<p><b>Analysis Results:</b></p>
<!--/div-->

<p>
<!--div class="OrgInfoText" id="Results" style="position:absolute; left:60px; top:37px; width:548px; height:102px; z-index:6"-->
 <table border="0" cellpadding="0" cellspacing="0" width="700" class="table_list">
 <tr>
 <td class="table_header" width="20">&nbsp;</td>
 <td class="table_header" >&nbsp;&nbsp;Total</td>
 <td class="table_header" >&nbsp;&nbsp;Correct</td>
 <td class="table_header" >&nbsp;&nbsp;Incorrect</td>
 <td class="table_header" >&nbsp;&nbsp;Incomplete</td>
 <td class="table_header" >&nbsp;&nbsp;Understood</td>
 <td class="table_header" >&nbsp;&nbsp;Contact</td>
 <td class="table_header" >&nbsp;&nbsp;A</td>
 <td class="table_header" >&nbsp;&nbsp;B</td>
 <td class="table_header" >&nbsp;&nbsp;C</td>
 <td class="table_header" >&nbsp;&nbsp;D</td>
 <td class="table_header" >&nbsp;&nbsp;E</td>
 </tr>
 <tr>
 <td width="20">&nbsp;</td>
 <td class='sm_font'>&nbsp;&nbsp;{$searchResults.numResults}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$searchResults.numCorrect}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$searchResults.numIncorrect}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$searchResults.numIncomplete}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$searchResults.numUnderstood}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$searchResults.numContact}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$searchResults.numAnswerA}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$searchResults.numAnswerB}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$searchResults.numAnswerC}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$searchResults.numAnswerD}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$searchResults.numAnswerE}</td>
 </tr>
 <tr>
   <td colspan="11" align="center">'X' indicates that the column is part of the search criteria.<br>
   '--' indicates that the column is excluded from the search.</td>
 </tr>
 </table>
</p>
<!--/DIV-->
{/if}




<p><b>Analysis Criteria:</b></p>

<p>
<form name="AdminForm" method="post" action="{$smarty.server.PHP_SELF}">
    <table border="0" width="100%">
      <tr>
        <td class="OrgInfoText" width="21%">Question Number:</td>
        <td width="79%">
          <select class="FormValue" name="questionNumber" id="questionNumber">
          <option></option>
          {html_options values=$questionNumbers output=$questionNumbers selected=$selectedQuestionNumber}
          </select>
      </td>
	</tr>
	<tr>
        <td class="OrgInfoText" width="21%">Version Date:</td>
        <td width="79%"><span class="OrgInfoText">from</span>
          <select class="FormValue" name="versionDateFrom" id="versionDateFrom">
            <option></option>
            {html_options values=$versionDates output=$versionDates selected=$selectedVersionDateFrom}
          </select>
          <span class="OrgInfoText">to</span>
          <select class="FormValue" name="versionDateTo" id="versionDateTo">
            <option></option>
            {html_options values=$versionDates output=$versionDates selected=$selectedVersionDateTo}
	  </select>
        </td>
    </tr>
	<tr>
        <td class="OrgInfoText" width="21%">Completion Date:</td>
        <td width="79%"><span class="OrgInfoText">from</span>
          <select class="FormValue" name="completionDateFrom" id="completionDateFrom">
            <option></option>
            {html_options values=$completionDates output=$completionDates selected=$selectedCompletionDateFrom}
          </select>
          <span class="OrgInfoText">to</span>
          <select class="FormValue" name="completionDateTo" id="completionDateTo">
            <option></option>
            {html_options values=$completionDates output=$completionDates selected=$selectedCompletionDateTo}
          </select>
        </td>
    </tr>
    <tr>
      <td class="OrgInfoText" height="20" width="21%">Domain: &nbsp;&nbsp;</td>
      <td height="20" width="79%">
        <select class="FormValue" name="domainId" id="domainId">
          <option></option>
          {html_options options=$domainIds selected=$selectedDomainId}
        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" width="21%">Class:</td>
      <td width="79%">
        <select class="FormValue" name="trackId" id="trackId">
          <option></option>
          {html_options options=$trackIds selected=$selectedTrackId}
        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" width="21%">Department:</td>
      <td width="79%">
        <select class="FormValue" name="departmentId" id="departmentId">
          <option></option>
          {html_options options=$departmentIds selected=$selectedDepartmentId}
        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" width="21%">Result:</td>

      <td width="79%">
        <select class="FormValue" name="resultId" id="resultId">
	  <option></option>
	  {html_options options=$resultIds selected=$selectedResultId}
	</select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" width="21%">Answer Selected:</td>

      <td width="79%">
        <select class="FormValue" name="multipleChoiceAnswerSelected" id="multipleChoiceAnswerSelected">
	  <option></option>
	  {html_options options=$multipleChoiceAnswerSelected selected=$selectedMultipleChoiceAnswerSelected}
	</select>
      </td>
    <tr>
      <td class="OrgInfoText" width="21%">Exit Information:</td>
      <td width="79%">
        <select class="FormValue" name="exitInfo" id="exitInfo">
          <option></option>
          {html_options options=$exitInfos selected=$selectedExitInfo}
        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" width="21%">&nbsp;</td>
      <td width="79%">
        <input type="image" name="execute" id="execute" value="1" src="../themes/{$uiTheme}/admin/btn_search_up.gif"  onMouseOut="MM_swapImgRestore()"
               onMouseOver="MM_swapImage('Update','','../themes/{$uiTheme}/admin/btn_search_down.gif',1)">
      </td>
    </tr>
    <!--tr><td><input type="hidden" name="Insert" value="TRUE"></td></tr>
    <tr><td><input type="hidden" name="get_return_name" value=""></td></tr>
    <tr><td><input type="hidden" name="get_return_URL" value=""></td></tr-->
  </table>

<!--div id="UpdateDiv" style="position:absolute; left:500px; top:160px; width:67px; height:25px; z-index:10"-->
    <!--img name="Update" border="0" src="../themes/{$uiTheme}/admin/btn_search_up.gif" width="60" height="22"></a>
</div-->

</form>
</p>

</BODY>
</HTML>
