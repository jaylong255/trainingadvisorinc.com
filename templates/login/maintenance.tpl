<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
  <TITLE>Training Advisor Portal Login</TITLE>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <LINK rel="stylesheet" href="../themes/{$uiTheme}/desktop.css" type="text/css">
  <SCRIPT language="javascript" type="text/javascript" src="../javascript/rpc.js"></script>
  <SCRIPT language="javascript" type="text/javascript" src="../javascript/desktop.js"></script>
</HEAD>
<BODY bgcolor="#FFFFFF" onLoad="MM_preloadImages('../themes/{$uiTheme}/login/btn_logon_dn.gif'); SetFocus(document.AdminForm.User_ID);{if !isset($fromEmail)} svcGetUserOrgs('User_ID');{/if}">
	<div id="Layer1" style="position:absolute; left:0px; top:0px; z-index:1"><img src="../themes/{$uiTheme}/login/logon_header.gif"></div>
	{if isset($showTaLogo) && $showTaLogo}
	  <div id="Layer2" style="position:absolute; left:629px; top:0px; z-index:2"><img src="../organizations/hr_tools/motif/hrtools_logo.gif"></div>
	{/if}
	<div id="Layer5" class="FormText" style="position:absolute; left:80px; top:150px; width:6290px; height:25px; z-index:5">The system is currently unavailable for maintenance.<BR>
	  Any assignments that were made today will be available for you tomorrow morning.<BR>
	  Please excuse us for the inconvience as we work to improve the service provided.</div>
	{if isset($showPoweredBy) && $showPoweredBy}
	<div id="Layer11" style="position:absolute; left:342px; top:267px; width:193px; height:21px; z-index:11">
	  <a href="http://www.trainingadvisorinc.com" class="CRText">Powered by Training Advisor, Inc.</a></div>
	{/if}
	<div id="Layer12" class="CRText" style="position:absolute; left:342px; top:287px; width:131px; height:23px; z-index:12">Copyright {$copyrightYear}</div>
	<div id="Layer6" style="position:absolute; left:297px; top:245px; width:77px; height:26px; z-index:6">
	</div>

<div style="position:absolute; left:119px; top:375px; width:627px; height:32px; z-index:12">
	<font face="Arial, Helvetica, sans-serif">
	  <span class='red'>{$ErrMsg}</span>
	</font>
	</div>

</body>
</html>
