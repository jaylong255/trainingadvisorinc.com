{include file='presentation/presentation_header.tpl'}
<body bgcolor="#FFFCED" onLoad="javascript:window.print(); history.back();">

{section name="questionIdx" loop=$questions}
<div style="width:auto; overflow:visible; display:block;
     {if !$smarty.section.questionIdx.last}page-break-after:always;{/if}">
{include file='admin/question_view.tpl' q=$questions[questionIdx] hideAction=true contentOnly=true}
</div>
<BR>
{/section}

</body>
</html>
