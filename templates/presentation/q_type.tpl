<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <Title>Question</Title>
  <link rel="stylesheet" href="../themes/{$uiTheme}/desktop.css" type="text/css">
  <link rel="stylesheet" href="../css/jt_DialogBox.css" type="text/css">
  <link rel="stylesheet" href="../css/jt_Veils.css" type="text/css">
  <SCRIPT LANGUAGE="JavaScript">
    var qType = '{$a->frameTypeId}';
    {if !$preview}
    var assignmentId = {$a->assignmentId}; //$nResult;
    {/if}
    var firstAnswer = 0; //$iFirstChoice;
    var finalAnswer = 0; //$iStudentAnswer;
    var currentPage = 0; //$iCurrentPage;
    var review = {$review};
    var preview = {$preview};
    var exitInfo = 0;
    var lastPage = 0;
    var correctAnswer = 0;
    var requireCorrectAnswer = {if $a->requireCorrectAnswer}1{else}0{/if};
    // Page numbers are:
    // 0=Question Frame 1=Why It's Important 2=Key Learning Points 3=Additional HR Comments 4=Thank You
    var strStem = "{$a->question}";
    var strFoil1 = "{$a->multipleChoice1}";
    var strFoil2 = "{$a->multipleChoice2}";
    var strFoil3 = "{$a->multipleChoice3}";
    var strFoil4 = "{$a->multipleChoice4}";
    var strFoil5 = "{$a->multipleChoice5}";
    var strCorrect = {$a->correctAnswer};
    var strPurpose = "{$a->purpose}";
    var strFoil1_Fdbk = "{$a->feedbackChoice1}";
    var strFoil2_Fdbk = "{$a->feedbackChoice2}";
    var strFoil3_Fdbk = "{$a->feedbackChoice3}";
    var strFoil4_Fdbk = "{$a->feedbackChoice4}";
    var strFoil5_Fdbk = "{$a->feedbackChoice5}";
    var strLearnPoints = "{$a->learningPoints}";
    var strMedia = "{$a->mediaFilePath}";
    var strSupport_Info = "{$a->supportInfo}";
    var strNotes = "{$a->notes}";
    var strMediaPath='../organizations/{$orgDirectory}/motif/' + strMedia;
    var nextDelay = {$a->mediaDurationSeconds};
    var isAb1825 = {if $a->categoryId == 118}1{else}0{/if};
    var pageTimer;
    {if !$preview}
    var progress = {$a->progress};
    var progStep = {$progStep};
    {/if}
    var nextChild = {$a->nextAssignmentId};
    var startTime = {$startTime};
    var assignmentSetSize = {$a->questionSetSize}.0;
    var incorrectInSet = {$incorrectInSet}.0;
    var nextButtonActive = 1;
    var uiTheme = '{$uiTheme}';
  </SCRIPT>
  {if !$preview && ($a->progress || $a->nextAssignmentId)}
  <!-- jsProgressBarHandler prerequisites : prototype.js -->
  <SCRIPT TYPE="text/javascript" SRC="../javascript/prototype.js"></script>
  <!-- jsProgressBarHandler core -->
  <SCRIPT TYPE="text/javascript" SRC="../javascript/jsProgressBarHandler.js"></script>
  {/if}
  <SCRIPT LANGUAGE="Javascript" type="text/javascript" src="../javascript/fade.js"></script>
  <SCRIPT LANGUAGE="Javascript" type="text'javascript" src="../javascript/AC_QuickTime.js"></script>
  <SCRIPT LANGUAGE="Javascript" type="text/javascript" src="../javascript/desktop.js"></script>
</HEAD>
<BODY bgcolor="#FFFFFF"
      onLoad="MM_preloadImages('../themes/{$uiTheme}/presentation/qbar_left_long.png', '../themes/{$uiTheme}/presentation/btn_FAQ_back_dim.png', '../themes/{$uiTheme}/presentation/btn_FAQ_back_up.png',
			       '../themes/{$uiTheme}/presentation/btn_continue_dim_greenbg.png', '../themes/{$uiTheme}/presentation/correct_check.jpg',
			       '../themes/{$uiTheme}/presentation/incorrect_x.jpg', '../themes/{$uiTheme}/presentation/contact_header.jpg', '../themes/{$uiTheme}/presentation/send_msg_up.jpg',
			       '../themes/{$uiTheme}/presentation/send_msg_down.jpg', '../themes/{$uiTheme}/presentation/btn_continue_down_greenbg.png',
			       '../themes/{$uiTheme}/presentation/btn_continue_up_greenbg.png', '../themes/{$uiTheme}/presentation/btn_understood_orange.png',
			       '../themes/{$uiTheme}/presentation/qbar_left_long_empty_with_seal.png', '../themes/{$uiTheme}/presentation/btn_understood_orange_over.png',
			       '../themes/{$uiTheme}/presentation/feedback_header.jpg', '../themes/{$uiTheme}/presentation/qbar_left_long_empty.png',
			       '../themes/{$uiTheme}/presentation/btn_continue_up_whitebg.png', '../themes/{$uiTheme}/presentation/btn_continue_down_whitebg.png'); GoMove(0, uiTheme);">


{if !$preview && ($a->progress || $a->nextAssignmentId)}
<div style="position:absolute; left:150px; top:190px;">
  <span style="color:#006600;font-weight:bold;">Question Progress:</span>
  <span id="element6">[ Loading Progress Bar ]</span>
</div>
{/if}

<div id="presentationWindow" name="presentationWindow" class="PresentationWindow">
  
  <!-- Fairly benign table at top of page displaying pretty stuff in template/header -->
  {include file='common/motif.tpl'}

  <!-- Shows the question answer header with back/next images -->
  <TABLE width="95%" cellpadding="0" cellspacing="0" align="center" border="0">
    <tr>
      <td id="qbarLeft" name="qbarLeft" width="223" height="105" valign="top"
  	style="background-image:url(../themes/{$uiTheme}/presentation/qbar_left_long.png); background-color:#FFFFFF;">
        &nbsp;
      </td>
      <td id="qbarMid" name="qbarMid" height="105" align="center" valign="center"
          style="background-image:url(../themes/{$uiTheme}/presentation/qbar_mid.png); background-repeat:repeat-x; background-color:#FFFFFF;">
        <div id="textHeader" class="HeaderText" name="textHeader">The Question</div>
      </td>
      <td id="qbarBack" name="qbarBack" align="right" valign="center" width="79" height="105"
	style="background-image:url(../themes/{$uiTheme}/presentation/qbar_mid.png); background-repeat:repeat-x; background-color:#FFFFFF;">
	<!-- BACK BUTTON -->
	<div id="backButtonDiv" class="HideByDefault">
        <a id="btnBackLink" name="btnBackLink" href="javascript: GoMove(-1, uiTheme);" alt="Back Button"
           onmouseout="SetImage('btnBackImage', '../themes/{$uiTheme}/presentation/btn_FAQ_back_up.png', finalAnswer, strCorrect, requireCorrectAnswer);"
           onmouseover="SetImage('btnBackImage', '../themes/{$uiTheme}/presentation/btn_FAQ_back_down.png', finalAnswer, strCorrect, requireCorrectAnswer);">
        <img name="btnBackImage" id="btnBackImage" src="../themes/{$uiTheme}/presentation/btn_FAQ_back_up.png" alt="" border="0"></a>
        </div>
      </td>
      <td id="qbarNext" name="qbarNext" align="center" width="144" height="105"
          style="background-image:url(../themes/{$uiTheme}/presentation/qbar_mid.png); background-repeat:repeat-x; background-color:#FFFFFF;">
	<!-- NEXT BUTTON -->
	<div id="nextButtonDiv" class="HideByDefault">
        <a id="btnNextLink" name="btnNextLink" href="javascript:alert('Please wait...still loading');"  alt="Next Button"
           onmouseout="SetImage('btnNextImage', '../themes/{$uiTheme}/presentation/btn_continue_up_greenbg.png', finalAnswer, strCorrect, requireCorrectAnswer);"
           onmouseover="SetImage('btnNextImage', '../themes/{$uiTheme}/presentation/btn_continue_down_greenbg.png', finalAnswer, strCorrect, requireCorrectAnswer);">
        <img name="btnNextImage" id="btnNextImage" src="../themes/{$uiTheme}/presentation/btn_continue_dim_greenbg.png" alt="" border="0"></a>
	</div>
      </td>
      <td id="qbarRight" name="qbarRight" width="22" height="105" style="background-image:url(../themes/{$uiTheme}/presentation/qbar_right.png); background-color:#FFFFFF;">
        &nbsp;
      </td>
    </tr>
  </TABLE>

  <!-- QUESTION PAGES, EACH INCLUDE IS YET ANOTHER PAGE -->

  <!-- Actual question presentation -->
  <div id="page_0" name="page_0" class="presentationBodyStyle">
    {if isset($contentErrorMessage)}
	<span class="Error">{$contentErrorMessage}</span>
    {else}
        {include file='presentation/question.tpl'}
    {/if}
  </div>

  <!-- Why it's important -->
  <div id="page_1" name="page_1" class="presentationBodyStyle">
    <div style="width:100%; height:20px;">&nbsp;</div>
    <script language="JavaScript">
      document.writeln('<span class="Instruction">' + strPurpose + '</span>');
    </script>
  </div>

  <!-- Key Learning points -->
  <div id="page_2" name="page_2" class="presentationBodyStyle">
    <script language="JavaScript">
      document.writeln('<span class="Instruction">' + strLearnPoints + '</span>');
    </script>
  </div>

  <!-- Supplimental inforomation -->
  <div id="page_3" name="page_3" class="presentationBodyStyle">
    <script language="JavaScript">
      document.writeln('<span class="Instruction">' + strSupport_Info + '</span>');
    </script>
  </div>

  <!-- additional info -->
  <div id="page_4" name="page_4" class="presentationBodyStyle">
    <script language="JavaScript">
      document.writeln('<span class="Instruction">' + strNotes + '</span>');
    </script>
  </div>

  <!-- confirmation page -->
  <div id="page_5" name="page_5" class="presentationBodyStyle">
    <p class="Instruction" style="text-align:center;">{$a->capConfirmThanks}</p>

    <div style="width:100%; height:20px;">&nbsp;</div>
    <div class="Instruction" style="font-color:#325100; text-align:center; width:auto; height:auto;">
	  NOTE: You must select one of the options below before completion of training will be recorded.</div>
    <div style="width:100%; height:20px;">&nbsp;</div>

    <p class="Instruction" style="text-align:center;">If you understand the material and have no need
        for further information, please click the button below to return to the desktop.</p>

    <p style="text-align:center;">
      {if isset($genAb1825Cert) && $genAb1825Cert}
      <a target="_blank" href="gen_ab1825_cert.php?assignmentId={$a->assignmentId}"
	 onmouseout="document.images.btnCloseImage.src='../themes/{$uiTheme}/presentation/btn_understood_orange.png'"
	 onmouseover="document.images.btnCloseImage.src='../themes/{$uiTheme}/presentation/btn_understood_orange_over.png'"
	 onclick="setTimeout('{if $preview}window.close();{else}exitInfo=1; SubmitAnswer();{/if}', 5000);">
	<img name="btnCloseImage" id="btnCloseImage" src="../themes/{$uiTheme}/presentation/btn_understood_orange.png" alt="Understood" border="0">
      </a>
      {else}
      <a href="javascript:{if $preview}window.close();{else}exitInfo=1; SubmitAnswer();{/if}"
	 onmouseout="document.images.btnCloseImage.src='../themes/{$uiTheme}/presentation/btn_understood_orange.png'"
	 onmouseover="document.images.btnCloseImage.src='../themes/{$uiTheme}/presentation/btn_understood_orange_over.png'">
	<img name="btnCloseImage" id="btnCloseImage" src="../themes/{$uiTheme}/presentation/btn_understood_orange.png" alt="Understood" border="0">
      </a>
      {/if}
    </p>

    <p class="Instruction" style="text-align:center;">If you have some questions or would like additional information, please
	 click the button below to contact someone for assistance.</p>

    <p style="text-align:center;">
      <a href="javascript:exitInfo=2; DisplayContacts();"
	 onmouseout="document.images.btnHRImage.src='../themes/{$uiTheme}/presentation/btn_contact.png'"
	 onmouseover="document.images.btnHRImage.src='../themes/{$uiTheme}/presentation/btn_contact-mo.png'">
	<img name="btnHRImage" id="btnHRImage" src="../themes/{$uiTheme}/presentation/btn_contact.png" alt="Contact" border="0">
      </a>
    </p>
  </div>

<!--/div-->


<!-- Content for feedback window displayed when a user selects an answer to a question -->
{include file='presentation/feedback.tpl'}

<!-- Content for contacts window displayed when a user selects the Contact button on the thank you page -->
{include file='presentation/contacts.tpl'}

</BODY>
</HTML>
