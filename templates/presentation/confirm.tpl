<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <Title>Question Confirmation</Title>
  <link rel='stylesheet' href='../themes/{$uiTheme}/desktop.css' type='text/css'>
  <script language='javascript' type='text/javascript' src='../javascript/desktop.js'></script>
</HEAD>
<BODY bgcolor="#FFFFFF" onLoad="javascript:ShowParentBackButton();">
<center>
<br>
  <span class="Instruction"><strong>{$confirmCapThanks}</strong></span>
<!--div id="theText2" style="position:absolute; left:100px; top:40px; width:592px; height:30px; z-index:3"-->
  <p><span class="Instruction"><em>If you understand the material and have no need
  for further information, please click the button below to return to the desktop.</em></span></p> <!--/div-->

<!--div id="btnClose" style="position:absolute; left:275px; top:112px; width:98px; height:19px; z-index:5;"-->
<p><a target="_parent" href="thanks.php?exit_info=1" onmouseout="document.images.btnCloseImage.src='../themes/{$uiTheme}/presentation/btn_desktop.png' " onmouseover="document.images.btnCloseImage.src='../themes/{$uiTheme}/presentation/btn_desktop-mo.png' "><img name="btnCloseImage" id="btnCloseImage" src="../themes/{$uiTheme}/presentation/btn_desktop.png" alt="" border="0"></a></p><!--/div-->

<!--div id="theText3" style="position:absolute; left:100px; top:190px; width:592px; height:30px; z-index:4"-->
  <p><span class="Instruction"><em>If you have some questions or would like additional
  information, please click the button below to contact someone for assistance.</em></span></p> <!--/div-->

<!--div id="btnHR" style="position:absolute; left:275px; top:237px; width:98px; height:19px; z-index:6;"-->
<p><a href="javascript:parent.window.BodyInfo.location.href='contacts.php'" onmouseout="document.images.btnHRImage.src='../themes/{$uiTheme}/presentation/btn_contact.png' " onmouseover="document.images.btnHRImage.src='../themes/{$uiTheme}/presentation/btn_contact-mo.png' "><img name="btnHRImage" id="btnHRImage" src="../themes/{$uiTheme}/presentation/btn_contact.png" alt="" border="0" /></a></p><!--/div-->
</center>
</BODY>
</HTML>
