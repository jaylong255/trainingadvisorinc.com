{include file='presentation/presentation_header.tpl'}

<body class="faq">

<p>
{section name="faqIdx" loop=$faqs}
  <a href="javascript:parent.DisplayFaq(document.getElementById('faq{$smarty.section.faqIdx.iteration}').innerHTML);">
    Q: {$faqs[faqIdx].Question}</a><BR>
{/section}
{section name="faqIdx" loop=$faqs}
  <div name="faq{$smarty.section.faqIdx.iteration}" id="faq{$smarty.section.faqIdx.iteration}" class="translucent">
    {$faqs[faqIdx].Answer}
  </div>
{/section}
</p>

</body>
</html>
