{include file='presentation/presentation_header.tpl'}

<BODY class="noscroll" marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'>

<table border='0' cellpadding='0' cellspacing='0' width='100%'>
  <tr>
    <td valign='top'>
      <table border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tr>
	  <td background='../themes/{$uiTheme}/presentation/header_middle.gif' style='background-repeat:repeat-x;'>
	    <img src='../themes/{$uiTheme}/presentation/wtp_logo.gif' border='0' alt=''>
	  </td>
	  <td align='right' valign='top' width='350' background='../themes/{$uiTheme}/presentation/header_right.gif' style='background-repeat:no-repeat;'>
	    <img id="Logo" src="/desktop/organizations/hr_tools/motif/hrtools_logo.gif">
	  </td>
	</tr>
      </table>		
    </td>
  </tr>
</table>

<table border='0' cellpadding='0' cellspacing='25' width='80%'>
  <tr>
    <td valign='middle' width='15%' nowrap>
      <a class="Links" href="portal.php">Return to desktop</a>
    </td>
  </tr>
  <tr>
    <td>
      <p>Disclaimers & Limited Liability Terms</p>

      <p>The information and content provided herein, as incorporated into Training Advisor's products and services, is based on summaries of certain FEDERAL employment laws.  It is important to note that the laws of individual states are not covered here, and may differ from federal law in certain areas, depending on the laws of the state in question.  THE INFORMATION AND CONTATINED HEREIN IS NOT LEGAL ADVICE.  Receipt of, or access to, Training Advisor's products and services, and the information contained therein, does not create or constitute an attorney-client relationship.  If legal advice or other expert assistance is required, the services of a competent professional should be sought.</p>

       <p>This product is designed to provide accurate and authoritative information in regard to the subject matter covered.  In publishing these materials, neither Training Advisor nor its trusted employment law content provider, Jones Waldo Holbrook & McDonough PC, are engaged in rendering legal or other professional services.</p>

       <p>Due to the constanly changing legal and regulatory framework of federal and state laws  and regulations in the human resources and employment areas, no published work can be entirely current.  As a result, all information and content contained within Training Advisor's products and services should be reviewed by a qualified professional for conformity to the most recent regulatory and legal changes.  While every attempt has been made to provide accurate information, Training Advisor and Jones Waldo Holbrook & McDonough PC shall not be held accountable for any error or omission resulting in nonconformity to the most recent laws and regulations.</p>

        <p>Additionally, neither Training Advisor nor Jones Waldo Holbrook & McDonough PC are liable for any claims made by an employee of the company using these materials or for fines or damages resulting from violation of federal or state law.</p>
    </td>
  </tr>
</table>



</BODY>
</HTML>
