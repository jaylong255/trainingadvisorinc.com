#!/bin/sh

# Refresh all the organization directories in test
#tar -C /var/www/c13690/trainingadvisorinc.com/htdocs/desktop/organizations --exclude-vcs --exclude=hr_tools --exclude=ORG2 --exclude=HRT-P -Ocp . | tar -C /var/www/c13690/test.trainingadvisorinc.com/htdocs/desktop/organizations -mx --overwrite

LOG_DATE=`date +%Y-%m-%d`
LOG_TIME=`date +%H%M`
AGENT_PATH="/var/www/c13690/trainingadvisorinc.com/htdocs/desktop/agent"
LOG_FILE="$AGENT_PATH/logs/$LOG_DATE_cron.log"
ERR_FILE="$AGENT_PATH/logs/$LOG_DATE_err.log"
OUT_FILE="$AGENT_PATH/logs/$LOG_DATE.htm"
EXE_FILE="$AGENT_PATH/update.php"
#URL="http://www.hrtoolsonline.com/desktop/agent/update.php"

# -O is set to /dev/null because the output of the request will be written to
# OUT_FILE anyway by the web server or script itself.
#/usr/bin/wget -a $LOG_FILE -O /dev/null $URL
cd "$AGENT_PATH"
/usr/bin/php $EXE_FILE >> $LOG_FILE 2>> $ERR_FILE

tail -2 $LOG_FILE | grep "Run Completed"
if [ $? -ne 0 ]; then
    mv $LOG_FILE "$LOG_FILE.$LOG_TIME"
    /usr/bin/php $EXE_FILE >> $LOG_FILE 2>> $ERR_FILE
fi
