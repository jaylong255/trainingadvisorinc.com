﻿fixMozillaZIndex=true; //Fixes Z-Index problem  with Mozilla browsers but causes odd scrolling problem, toggle to see if it helps
_menuCloseDelay=500;
_menuOpenDelay=150;
_subOffsetTop=2;
_subOffsetLeft=-2;




with(menuStyle=new mm_style()){

fontfamily="Helvetica, Verdana, Tahoma, Arial";
fontsize="12px";
fontstyle="normal";
headerbgcolor="#ffffff";
headercolor="#000000";
offbgcolor="#f4f4f4";
offcolor="#000000";
onbgcolor="#8ec640";
oncolor="#444444";
outfilter="randomdissolve(duration=0.3)";
overfilter="Fade(duration=0.2);Alpha(opacity=90);Shadow(color=#777777', Direction=135, Strength=3)";
padding=6;
pagebgcolor="#e3e3e3";
pagecolor="black";
separatorcolor="#685f50";
separatorsize=1;
subimage="http://img.milonic.com/arrow.gif";
subimagepadding=6;
}

with(milonic=new menuname("Main Menu")){ 
alwaysvisible=1; 
orientation="horizontal"; 
align="right"; 
position="relative" 
style=menuStyle; 
aI("text=Home;url=http://www.trainingadvisorinc.com/;"); 
aI("showmenu=Products;text=Products;url=http://www.trainingadvisorinc.com/products/index.html"); 
aI("showmenu=Free Tour;text=Free Tour;url=http://www.trainingadvisorinc.com/tour/index.html"); 
aI("showmenu=Clients;text=Clients;url=http://www.trainingadvisorinc.com/clients/index.html"); 
aI("showmenu=Request Quote;text=Request Quote;url=http://www.trainingadvisorinc.com/contact/quote.html"); 
aI("showmenu=Company;text=Company;url=http://www.trainingadvisorinc.com/company/index.html");
} 


with(milonic=new menuname("Products")){
style=menuStyle;
aI("text=Training Advisor;align=left;url=http://www.trainingadvisorinc.com/products/trainingadvisor.html;");
aI("text=AB1825 Advisor;align=left;url=http://www.trainingadvisorinc.com/products/ab1825advisor.html;");
}

with(milonic=new menuname("Free Tour")){
style=menuStyle;

}

with(milonic=new menuname("Clients")){
style=menuStyle;
aI("text=Success Stories;align=left;url=http://www.trainingadvisorinc.com/clients/successstories.html;");
aI("text=Partners;align=left;url=http://www.trainingadvisorinc.com/clients/partners.html;");
aI("text=Testimonials;align=left;url=http://www.trainingadvisorinc.com/clients/testimonials.html;");
}

with(milonic=new menuname("Request Quote")){
style=menuStyle;

}


with(milonic=new menuname("Company")){
style=menuStyle;
aI("text=Contact Us;align=left;url=http://www.trainingadvisorinc.com/contact/index.html;");
}


drawMenus();

