<?php

// Included files and libs

require_once '../includes/common.php';
require_once '../includes/Config.php';
require_once '../includes/EmailQueue.php';
require_once '../includes/Organization.php';
require_once '../includes/Track.php';

$logFile = NULL;


  
/////////////////////////////////////////////////////////////////////////////
//	output
//		Write the following string to the document and logfile.
function logEntry($string)
{
  global 		$logFile;

  // Output text to document
  //  echo $string;
  
  if (!$logFile)
    return;

    
  // Create new log file name based on todays date
  $objToday = new DateObject();
  $date = $objToday->strDate;
  
  if ($fp != NULL && $fp != FALSE)
    fwrite($fp, $string);
}


function openLogFile() {

  $dt = date('Y-m-d');
  $dirPath = APPLICATION_ROOT.'/agent/logs';
  $logFileName = "$dirPath/sendmail_$dt.log";

  // Create log file path if needed
  if (!is_dir($dirPath)) {
    
    if (!mkdir($dirPath, 0777)) {
      echo "<br>NOTICE: Unable to create directory $dirPath, check path and parent directory permissions.<br>\n";
      return NULL;
    }
    if (!is_writable($dirPath)) {
      echo "<br>NOTICE: Directory $dirPath not writable so no long file will be created.<br>\n";
      return NULL;
    }
  }

  $logFp = fopen($logFileName, 'a+');

  return $logFp;
}


$logfile = openLogFile();
logEntry("********** STARTING NEW RUN OF $0 ***********");

$org = new Organization();
$orgIds = $org->GetOrganizationIds();

for($i = 0; $i < 5; $i++) {
  echo "step $i";
  sleep(2);
}


foreach ($orgIds as $orgId) {
  logEntry("Processing organization $orgId");
  $org->SetOrgId($orgId);
  logEntry("\tOrg ID $orgId has name ".$org->orgName);
  $trk = new Track($orgId);
  $tracks = $trk->GetTracks($orgId);
  foreach ($tracks as $tId => $tName) {
    $trk->SetTrackId($tId);
    logEntry("\tGetting track participants for track $tId");
    $participants = $trk->GetTrackParticipants();
    if (count($participants) > 0) {
      logEntry("\tGetting Queue List for org $orgId and track $tId ($tName)");
      $queueEntries = EmailQueue::GetQueueList($orgId, $tId);
      if (count($queueEntries) > 0) {
	$queueIds = array_keys($queueEntries);
	foreach ($queueIds as $queueId) {
	  $eq = new EmailQueue($orgId, $queueId);
	  $eq->SetRecipientCount(count($participants));
	  //$eq->Dump();
	  foreach ($participants as $participant) {
	    if (!empty($eq->lastRecipient)) {
	      if ($participant[2] == $eq->lastRecipient) {
		$eq->lastRecipient = '';
		continue;
	      }
	      continue;
	    }
	    $to = $participant[0].' '.$participant[1]."<$participant[2]";
	    mail($to, $eq->subject, $eq->body, 'From: '.$eq->fromAddress);
	    $eq->UpdateLastRecipient($participant[2]);
	    logEntry('\tSent message with queueID='.$eq->emailQueueId." to $to from ".
		     $eq->fromAddress.' and subject '.$eq->subject);
	    sleep(10);
	  }
	  $eq->DeliveriesCompleted();
	}
      } else
	logEntry("\tNo email queue messages pending participants of track($tId:$tName)");
    } else
      logEntry("\tNo track participants in track $tId, skipped processing of mail queue.");
  }
}
logEntry("*********** ENDED NEW RUN OF $0 ****************");

?>
