<?php

require_once('../includes/common.php');
require_once('../includes/Report.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER &&
      $_SESSION['role'] != ROLE_CLASS_CONTACT_SUPERVISOR)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}


// Specify this as the current tab
$_SESSION['currentTab'] = TAB_QUESTION;
$_SESSION['currentSubTab'] = SUBTAB_QUESTION_ANALYSIS;


$errMsg = '';
$searchParams = array();
$searchResults = array();
$r = new Report($_SESSION['orgId']);

if ((isset($_REQUEST['execute']) && $_REQUEST['execute'] == '1') ||
    (isset($_REQUEST['execute_x']) && isset($_REQUEST['execute_y']))) {
  if ((isset($_REQUEST['questionNumber']) && !is_numeric($_REQUEST['questionNumber']) &&
       $_REQUEST['questionNumber'] != '') ||
      (isset($_REQUEST['versionDateFrom']) && !preg_match("/^\d{4}-\d{2}-\d{2}/", $_REQUEST['versionDateFrom']) &&
       $_REQUEST['versionDateFrom'] != '') ||
      (isset($_REQUEST['versionDateTo']) && !preg_match("/^\d{4}-\d{2}-\d{2}/", $_REQUEST['versionDateTo']) &&
       $_REQUEST['versionDateTo'] != '') ||
      (isset($_REQUEST['completionDateFrom']) && !preg_match("/^\d{4}-\d{2}-\d{2}/", $_REQUEST['completionDateFrom']) &&
       $_REQUEST['completionDateFrom'] != '') ||
      (isset($_REQUEST['completionDateTo']) && !preg_match("/^\d{4}-\d{2}-\d{2}/", $_REQUEST['completionDateTo']) &&
       $_REQUEST['completionDateTo'] != '') ||
      (isset($_REQUEST['domainId']) && !is_numeric($_REQUEST['domainId']) && $_REQUEST['domainId'] != '') ||
      (isset($_REQUEST['trackId']) && !is_numeric($_REQUEST['trackId']) && $_REQUEST['trackId'] != '') ||
      (isset($_REQUEST['departmentId']) && !is_numeric($_REQUEST['departmentId']) && $_REQUEST['departmentId'] != '') ||
      (isset($_REQUEST['resultId']) && !is_numeric($_REQUEST['resultId']) && $_REQUEST['resultId'] != '') ||
      (isset($_REQUEST['multipleChoiceAnswerSelected']) && !is_numeric($_REQUEST['multipleChoiceAnswerSelected']) &&
       $_REQUEST['multipleChoiceAnswerSelected'] != '') ||
      (isset($_REQUEST['exitInfo']) && !is_numeric($_REQUEST['exitInfo']) && $_REQUEST['exitInfo'] != '')) {
    error_log('question_analysis.php: search parameter validation checks failed.');
  } else {
    $searchParams['questionNumber']               = $_REQUEST['questionNumber'];
    $searchParams['versionDateFrom']              = $_REQUEST['versionDateFrom'];
    $searchParams['versionDateTo']                = $_REQUEST['versionDateTo'];
    $searchParams['completionDateFrom']           = $_REQUEST['completionDateFrom'];
    $searchParams['completionDateTo']             = $_REQUEST['completionDateTo'];
    $searchParams['domainId']                     = $_REQUEST['domainId'];
    $searchParams['trackId']                      = $_REQUEST['trackId'];
    $searchParams['departmentId']                 = $_REQUEST['departmentId'];
    $searchParams['resultId']                     = $_REQUEST['resultId'];
    $searchParams['multipleChoiceAnswerSelected'] = $_REQUEST['multipleChoiceAnswerSelected'];
    $searchParams['exitInfo']                     = $_REQUEST['exitInfo'];
  }
} else {
  $searchParams['questionNumber']               = FALSE;
  $searchParams['versionDateFrom']              = FALSE;
  $searchParams['versionDateTo']                = FALSE;
  $searchParams['completionDateFrom']           = FALSE;
  $searchParams['completionDateTo']             = FALSE;
  $searchParams['domainId']                     = FALSE;
  $searchParams['trackId']                      = FALSE;
  $searchParams['departmentId']                 = FALSE;
  $searchParams['resultId']                     = FALSE;
  $searchParams['multipleChoiceAnswerSelected'] = FALSE;
  $searchParams['exitInfo']                     = FALSE;
}

// Data validation
$searchResults = $r->ExecuteSearch($searchParams);
$smarty->assign('searchResults', $searchResults);

// Setup the list of options for correct response selection dropdown
$smarty->assign('errMsg', $errMsg);
//$smarty->assign('retLink', $retLink);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('superUser', $_SESSION['superUser']);
$smarty->assign('questionNumbers', $r->GetDistinctQuestionIds());
$smarty->assign('selectedQuestionNumber', '');
$smarty->assign('versionDates', $r->GetDistinctVersionDates());
$smarty->assign('selectedVersionDateFrom', '');
$smarty->assign('selectedVersionDateTo', '');
$smarty->assign('completionDates', $r->GetDistinctCompletionDates());
$smarty->assign('selectedCompletionDateFrom', '');
$smarty->assign('selectedCompletionDateTo', '');
$smarty->assign('domainIds', $r->GetDistinctDomains());
$smarty->assign('selectedDomainId', '');
$smarty->assign('trackIds', $r->GetDistinctTracks());
$smarty->assign('selectedTrackId', '');
$smarty->assign('departmentIds', $r->GetDistinctDepartments());
$smarty->assign('selectedDepartmentId', '');
$smarty->assign('resultIds', unserialize(RESULT_OPTIONS));
$smarty->assign('selectedResultId', '');
$smarty->assign('multipleChoiceAnswerSelected', unserialize(MULTIPLE_CHOICE_OPTIONS));
$smarty->assign('selectedMultipleChoiceAnswerSelected', '');
$smarty->assign('exitInfos', unserialize(EXIT_INFO_OPTIONS));
$smarty->assign('selectedExitInfo', '');
$smarty->assign('searchParams', $searchParams);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/question_analysis.tpl');

$r->Close();

?>
