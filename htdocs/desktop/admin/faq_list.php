<?php
/////////////////////////////////////////////////////////////////////////////
//	faq_list.php
//		Page used for administration of tracks for the current user and company.
//



// Include libs which starts session and creates smarty objects
require_once ('../includes/common.php');
require_once ('../includes/Faq.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or an org admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}


// Make sure if this is only a track admin this is the track they are super for
if ($_SESSION['role'] == ROLE_CLASS_CONTACT_SUPERVISOR) {
  $track = new Track($_SESSION['orgId'], $_SESSION['trackId']);
  if ($_SESSION['userId'] != $track->supervisorId) {
    error_log("Redirecting to track denied for trackId(".$track->trackId);
    header("Location: /desktop/login/perm_denied.php?trackId=".$track->trackId);
    exit;
  }
}


// Specify this as the current tab
$_SESSION['currentTab'] = TAB_TRACK;
$_SESSION['currentSubTab'] = SUBTAB_TRACK_FAQS;

if (isset($_REQUEST['searchBy'])) {
  $searchBy = $_REQUEST['searchBy'];
} else {
  $searchBy = FAQ_LIST_COLUMN_QUESTION;
}

if (isset($_REQUEST['searchFor'])) {
  $searchFor = $_REQUEST['searchFor'];
  $_SESSION['faqPageNumber'] = 1;
} else {
  $searchFor = '';
}

// create new employee list
$errMsg = '';
$rc = 0;
$firstRow = 0;
$lastRow = 0;
$numRows = 0;
$numPages = 0;
$faqIds = array();
$faqs = array();
$faqList = new Faq($_SESSION['orgId'], $_SESSION['trackId']);

// Delete faq if faqId passed into us
if (isset($_REQUEST['faqId']) && is_numeric($_REQUEST['faqId']) && $_REQUEST['faqId']) {
  $faqList->DeleteFaq($_REQUEST['faqId']);
}

$rc = $faqList->GetFaqList($_SESSION['faqShowColumns'], $searchBy, $searchFor,
			   $_SESSION['faqSortColumn'], $_SESSION['faqSortDescending'],
			   $_SESSION['faqPageNumber'], $_SESSION['rowsPerPage']);
if ($rc === FALSE) {
  echo "Failed to get employee list\n";
  exit(1);
}

if (DEBUG & DEBUG_FORM) {
  $smarty->assign('GetFaqList', $rc);
}

if (!empty($rc)) {
  list($faqIds, $faqs) = $rc;
  $faqList->GetRowCounts($firstRow, $lastRow, $numRows);
  $numPages = $faqList->GetPageCount($_SESSION['rowsPerPage']);
}


$createLink = new TplCaption('Create New FAQ');
$createLink->href='faq_edit.php';


// setup links across the top of the page
$smarty->assign('topLinkList', $createLink);

// This alone is enough information to identify a search column
$smarty->assign('columnLabels', $faqList->GetColumnLabels($_SESSION['faqShowColumns']));
$smarty->assign('showColumns', $_SESSION['faqShowColumns']);

// Keep the selected search by field
$smarty->assign('searchBy', $searchBy);

// Set the one to be selected
$smarty->assign('sortColumn', $_SESSION['faqSortColumn']);
$smarty->assign('sortDescending', $_SESSION['faqSortDescending']);


// TODO: make this dynamic by adding an organization column to the Organization table
// and a widget in the admin to control display of logo
$smarty->assign('displayLogo', TRUE);


$smarty->assign('firstRow', $firstRow);
$smarty->assign('lastRow', $lastRow);
$smarty->assign('numRows', $numRows);
$smarty->assign('pageNumber', $_SESSION['faqPageNumber']);
$smarty->assign('numPages', $numPages);
$smarty->assign('recordList', $faqs);
$smarty->assign('recordIds', $faqIds);

if (DEBUG) {
  $smarty->assign('ErrMsg', $faqList->GetErrorMessage());
}

$faqList->Close();

$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('toggleParamName', 'toggleFaqColumn');
$smarty->assign('sortParamName', 'faqSortColumn');
$smarty->assign('idParamName', 'faqId');
$smarty->assign('pageNumberParamName', 'faqPageNumber');
$smarty->assign('orderParamName', 'faqSortDescending');
$smarty->assign('showSelectedColumn', FALSE);
$smarty->assign('rowsPerPage', $_SESSION['rowsPerPage']);
$smarty->assign('localHeader', 'track_header');
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/generic_list.tpl');

?>
