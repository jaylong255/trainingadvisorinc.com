<?php

require_once ('../includes/common.php');
require_once ('../includes/Config.php');
require_once ('../includes/Question.php');
require_once ('../includes/CustomCsv.php');
require_once ('../includes/Organization.php');
require_once ('../includes/Classes/PHPExcel/IOFactory.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] == ROLE_ORG_ADMIN)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}

// Make sure we have a current org id and if not, send to security
if (!isset($_SESSION['orgId'])) {
  header("Location: /desktop/login/security.php");
  exit(0);
}


// Create session and local/(script global) variables
$msg = '';
$csvArr = array();
$dupUserRef = array();
$pids = array();
$tmppath = '/tmp/hca_import';
$hcaEmplStatus = array('', 'Active', 'Active', 'Active', 'Active', 'Leave',
		       'Active', 'Terminated', 'Terminated', 'Active');

// Specify this as the current tab
$_SESSION['currentTab'] = TAB_ORG;
$_SESSION['currentSubTab'] = SUBTAB_ORG_UTILITIES;


// common smarty assigns for all templates in org edit
$smarty->assign('create', FALSE);
$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('superUser', $_SESSION['superUser']);


// Init some variables if resubmitted
if (isset($_REQUEST['termStatusDate'])) {
  $smarty->assign('termStatusDate', $_REQUEST['termStatusDate']);
} else {
  $smarty->assign('termStatusDate', '');
}


$smarty->assign('userStatusMsg', '');
$smarty->assign('questionStatusMsg', '');
$smarty->assign('deleteStatusMsg', '');
$smarty->assign('agentStatusMsg', '');
$smarty->assign('updateStatusMsg', '');


// user file processing
if (isset($_REQUEST['userSubmit'])) {

  $rc = CheckFileUpload('userFile');

  if ($rc != RC_OK) {
    $msg = RcToText($rc);
    error_log("org_utilities.php: upload error ".$msg);
  } else {
    $fileName = $_FILES['userFile']['tmp_name'];
    if (DEBUG & DEBUG_FORM)
      $smarty->assign('userFile', $fileName);
    $pageNumber = 1;
    if (isset($_REQUEST['sync']) && $_REQUEST['sync'] == 1)
      $sync = 1;
    else
      $sync = 0;    
    $users = new User($_SESSION['orgId']);

    // As of 2015-05-14 HCA changed their import file format.  We are
    // now presented with a zip file containing four Excel spreadsheets
    // that we must now process and convert into a valid import file
    // which can be used the ImportFile function below
    if ($_SESSION['orgId'] == 262) {
      $currentTrack = Track::GetMostRecentTrack($_SESSION['orgId']);
      error_log("Most recen track is: $currentTrack\n");
      //$cwd = getcwd();
      if (!file_exists($tmppath))
	mkdir($tmppath);
      //chdir('/tmp/hca_import');
      error_log("Removing old xls* and csv files...\n");
      system("rm -f $tmppath/*.csv $tmppath/*.xls*");
      error_log("Unzipping the upload file...\n");
      system("unzip $fileName -d $tmppath >/tmp/zip.out 2>/tmp/zip.err");
      $files = glob("$tmppath/*.xls*");
      error_log('Unzipped the following xls* files: '.print_r($files, TRUE)."\n");
      error_log("Converting xls files to csv...\n");
      //chdir($cwd);
      foreach($files as $file) {
	error_log('Calling: '.APPLICATION_ROOT."/tools/xl2csv.php \"$file\" > /tmp/conv.out_err 2>&1 & echo $!\n");
	$pids[] = trim(shell_exec(APPLICATION_ROOT."/tools/xl2csv.php \"$file\" > /tmp/conv.out_err 2>&1 & echo $!"));
      }
      $pidsstr = implode(',', $pids);
      error_log("pidsstr is $pidsstr, waiting for external processes started at ".date('H:i:s')."\n");
      do {
	usleep(250000);
	$execStat = shell_exec("ps -p $pidsstr --no-headers");
      }	while (!empty($execStat));
      error_log("done waiting for external processes, all finished at ".date('H:i:s')."\n");
      $files = glob("$tmppath/*.csv");
      //error_log("FILES: ".print_r($files, TRUE));
      $csvFilename = "$tmppath/hca.csv";
      if (!$csvFile = fopen($csvFilename, 'w'))
	error_log("Failed to create csv file $csvFilename");
      else {
	foreach($files as $file) {
	  $i = 0;
	  $csvInFh = fopen($file, 'r');
	  while (($rawRow = fgetcsv($csvInFh)) !== FALSE) {
	    $row = array_map('trim', $rawRow);
	    // Eliminate empty leading columns
	    while(count($row) > 0 && empty($row[0])) array_shift($row);
	    if (count($row) == 0) continue;
	    // Make sure we are dealing with a 34ID indicating a row of real data
	    if (empty($row[0]) || !preg_match('/[a-z0-9]{3}[0-9]{4}/i', $row[0])) {
	      error_log("Row $i was empty or didn't contain 34ID, skipping...".print_r($row, TRUE)."\n");
	      continue;
	    }
	    // Check for duplicates
	    if (!is_numeric($row[0]) && array_key_exists($row[0], $dupUserRef))
	      continue;
	    $dupUserRef[$row[0]] = 1;
	    if (count($row) == 7) array_splice($row, 5, 0, '');
	    // Fix up the status
	    if (is_numeric($row[7])) {
	      if ($row[7] == 6) $row[7] = 4;
	      if ($row[7] == 99) $row[7] = 4;
	      $row[7] = $hcaEmplStatus[(int)$row[7]];
	    }
	    // Fill CSV array the way we need it formatted
	    $csvArr = array($row[0], $row[0], trim($row[3]), trim($row[2]), $row[1], trim($row[5]),
			    trim($row[6]), 'no', 'no', 'English', $row[7], '', $currentTrack,
			    '000-000-0000', 'Regular Employee/User');
	    fputcsv($csvFile, $csvArr);
	    //	    error_log("row: ".print_r($row, TRUE));
	    //	    if ($i++ > 20)
	    //	      break;
	  }
	}
	if (!isset($dupUserRef['CORPER'])) {
	  $dupUserRef['CORPER'] = 1;
	  $csvArr = array('CORPER', 'CORPER', 'Corporate Employee', 'Relations',
			  'Corp.TrainingAdvisor@HCAhealthcare.com', '', '', 'no', 'no',
			  'English', 'Active', '', '', '', '', 'yes');
	  fputcsv($csvFile, $csvArr);
	}
	fclose($csvFile);
	$fileName = $csvFilename;
      }
    }
    $rc = $users->ImportFile($fileName, $sync, $msg, $_SESSION['userId']);
  }

  $smarty->assign('userStatusMsg', $msg);
}


// question import file processing
if (isset($_REQUEST['questionSubmit'])) {

  if (isset($_FILES['questionFile']['tmp_name'])) {
    $fileName = $_FILES['questionFile']['tmp_name'];
    if (DEBUG & DEBUG_FORM) {
      $smarty->assign('questionFile', $fileName);
    }
    
    $config = new Config($_SESSION['orgId']);
    $questions = new Question($_SESSION['orgId']);
    // Import for this org only at this point even if importing for all orgs so that if there are
    // any issues with the import they can be identified before wasting time doing it for all of them.
    if (($rc = $questions->ImportFile($fileName, $msg, $_SESSION['userId'], '',
				      APPLICATION_ROOT.'/organizations/'.$config->GetValue(CONFIG_ORG_DIRECTORY).'/motif')) == RC_OK) {
      if (isset($_REQUEST['importToAllOrgs']) && $_REQUEST['importToAllOrgs']) {
	$linkToDir = '../../'.$config->GetValue(CONFIG_ORG_DIRECTORY).'/motif';
	$org = new Organization();
	$orgIds = $org->GetOrganizationIds();
	foreach ($orgIds as $orgId) {
	  // Skip the one we already imported
	  if ($orgId == $_SESSION['orgId'])
	    continue;
	  $config = new Config($orgId);
	  unset($quesitons);
	  $questions = false;
	  $questions = new Question($orgId);
	  if (($rc = $questions->ImportFile($fileName, $msg, $_SESSION['userId'], $linkToDir,
					    APPLICATION_ROOT.'/organizations/'.$config->GetValue(CONFIG_ORG_DIRECTORY).'/motif')) != RC_OK) {
	    $smarty->assign('questionStatusMsg', RcToText($rc).
			    "<BR>\nProblem with import in org($orgId), aborting all org import...");
	    break;
	  }
	}
      }
      if ($rc == RC_OK)
	$smarty->assign('questionStatusMsg', 'Questions successfully imported');
    } else {
      $smarty->assign('questionStatusMsg', $msg);
    }
  } else {
    error_log("org_utilities.php: questionfile parameter not found");
  }
}


// question export processing
if (isset($_REQUEST['exportQuestions'])) {
  
  // Get all question Numbers
  $i = 0;
  $csv = new CustomCsv();
  $questionColumns = unserialize(QUESTION_SQL_COLUMNS);
  $choices = unserialize(MULTIPLE_CHOICE_OPTIONS);
  $csvHeader = array('Question_Number', 'Parent_Question_Number', 'Language_ID', 'Domain_ID', 'Version_Date', 'Category_ID',
		     'Frametype_ID', 'Is_Management', 'Author', 'Notes', 'Title', 'Support_Info', 'Question', 'Answer_A',
		     'Answer_B', 'Answer_C', 'Answer_D', 'Answer_E', 'Correct_Answer', 'Purpose', 'Feedback_A', 'Feedback_B',
		     'Feedback_C', 'Feedback_D', 'Feedback_E', 'Learning_Point1', 'Learning_Point2', 'Learning_Point3',
		     'Learning_Point4', 'Learning_Point5', 'Learning_Point6', 'Learning_Point7', 'Learning_Point8',
		     'Learning_Point9', 'Learning_Point10', 'Media', 'Media Duration', 'Cap_Confirm_Thanks');
  //  $csvLine = $csvHeader."\n";
  $fields = array();

  $csv->WriteHeaders('questions.csv');
  $csv->WriteCsv($csvHeader);

  $db = new DatabaseObject($_SESSION['dbName']);
  $db->Select('*', QUESTION_TABLE);
  $db->Where($questionColumns[QUESTION_COLUMN_ID].'>=400'); // AND '.$questionColumns[QUESTION_COLUMN_ID].'<100000');
  $db->SortBy($questionColumns[QUESTION_COLUMN_ID]);
  $results = $db->Query(MANY_ROWS, KEYS_FORMAT);

  if (is_array($results)) {
    
    foreach ($results as $questionArray) {

      $fields = array();

      // Pick apart the learning points
      $questionArray[$questionColumns[QUESTION_COLUMN_LEARNING_POINTS]] =
	str_replace('<ol>', '', $questionArray[$questionColumns[QUESTION_COLUMN_LEARNING_POINTS]]);
      $questionArray[$questionColumns[QUESTION_COLUMN_LEARNING_POINTS]] =
	str_replace('</ol>', '', $questionArray[$questionColumns[QUESTION_COLUMN_LEARNING_POINTS]]);
      $questionArray[$questionColumns[QUESTION_COLUMN_LEARNING_POINTS]] =
	str_replace('<ul>', '', $questionArray[$questionColumns[QUESTION_COLUMN_LEARNING_POINTS]]);
      $questionArray[$questionColumns[QUESTION_COLUMN_LEARNING_POINTS]] =
	str_replace('</ul>', '', $questionArray[$questionColumns[QUESTION_COLUMN_LEARNING_POINTS]]);

      $learningPoints = explode('<li>', $questionArray[$questionColumns[QUESTION_COLUMN_LEARNING_POINTS]]);
      $versionDate = explode(' ', $questionArray[$questionColumns[QUESTION_COLUMN_VERSION_DATE]]);
      $versionDate = $versionDate[0];

      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_ID]];
      if (is_numeric($questionArray[$questionColumns[QUESTION_COLUMN_PARENT_ID]]) &&
	  $questionArray[$questionColumns[QUESTION_COLUMN_PARENT_ID]] > 0)
	$fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_PARENT_ID]];
      else
	$fields[] = '';
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_LANGUAGE_ID]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_DOMAIN_ID]];
      $fields[] = $versionDate;
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_CATEGORY_ID]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_FRAMETYPE_ID]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_IS_MANAGEMENT]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_AUTHOR]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_NOTES]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_TITLE]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_SUPPORT_INFO]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_QUESTION]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_MULTIPLE_CHOICE1]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_MULTIPLE_CHOICE2]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_MULTIPLE_CHOICE3]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_MULTIPLE_CHOICE4]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_MULTIPLE_CHOICE5]];
      $fields[] = $choices['0'.$questionArray[$questionColumns[QUESTION_COLUMN_CORRECT_ANSWER]]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_PURPOSE]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_FEEDBACK_CHOICE1]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_FEEDBACK_CHOICE2]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_FEEDBACK_CHOICE3]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_FEEDBACK_CHOICE4]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_FEEDBACK_CHOICE5]];

      foreach($learningPoints as $learningPoint) {
	//if ($questionArray[$questionColumns[QUESTION_COLUMN_ID]] == 449)
	//  error_log("LEARNING POINTS:-".print_r($learningPoints, TRUE).'-');
	$learningPoint = trim(preg_replace("/<.*>/", '', $learningPoint));
	if ($learningPoint)
	  $fields[] = $learningPoint;
	else
	  array_shift($learningPoints);
      }

      // Add the additional fields in the event there are fewer than 10 learning points
      for ($i = 0; $i < 10-count($learningPoints); $i++) {
	$fields[] = ''; //$csvLine .= ',';
      }

      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_MEDIA_FILE_PATH]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_MEDIA_DURATION_SECONDS]];
      $fields[] = $questionArray[$questionColumns[QUESTION_COLUMN_CAP_CONFIRM_THANKS]];
      
      //sputcsv($csvLine, $fields);
      $csv->WriteCsv($fields);
    }
    
  }
  //echo $csvLine;
  //  print_r($results, FALSE);
  $csv->CloseCsv();

  exit(0);
}


if (isset($_REQUEST['deleteSubmit'])) {

  $smarty->assign('deleteStatusMsg', 'Feature still under development');
}


if (isset($_REQUEST['agentSubmit'])) {
  
  $smarty->assign('agentStatusMsg', 'Feature under development');
}


if (isset($_REQUEST['updateSubmit'])) {
  
  $smarty->assign('updateStatusMsg', 'Feature under development');
}


$smarty->assign('dbName', $_SESSION['dbName']);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/org_utilities.tpl');

exit(0);
?>
