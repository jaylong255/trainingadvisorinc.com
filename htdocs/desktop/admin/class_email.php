<?php

// Libs
require_once("../includes/common.php");
require_once('../includes/EmailQueue.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit;
}

// Ensure user is superuser or an org admin or track supervisor
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER)) {
  header('Location: /desktop/login/perm_denied.php');
  exit;
}

// Set tab navigation options
$_SESSION['currentSubTab'] = SUBTAB_TRACK_EMAIL;

// Global variables
$statusMsg = '';
//error_log('Queue rows: '.print_r($emailQueueRows, TRUE));
$config = new Config($_SESSION['orgId']);
$eQueue = new EmailQueue($_SESSION['orgId']);


// Class/Track email queue submit being performed
if (isset($_REQUEST['submit'])) {

  //$dateTime = date('Y-m-d_H-i-s');
  // Save properties to the database
  if (isset($_REQUEST['eQueueId']) && is_numeric($_REQUEST['eQueueId']) &&
      $_REQUEST['eQueueId'] > 0)
    $eQueue->setEmailQueueId($_REQUEST['eQueueId']);
  $eQueue->trackId = esql($_SESSION['trackId']);
  $eQueue->userId = esql($_SESSION['userId']);
  $eQueue->fromAddress = esql($_REQUEST['mailFrom']);
  $eQueue->subject = esql($_REQUEST['subject']);
  $eQueue->body = esql($_REQUEST['body']);
  $eQueue->scheduledDate = esql($_REQUEST['schedDate']);
  
  if ($eQueue->UpdateEmailQueue() != RC_OK) {
    error_log("Failed to insert or update email queue record for id ".$eQueue->emailQueueId);
    $statusMsg = "Add/Update Failed<BR>\n";
  } else {
    $statusMsg = "Email successfully updated.<BR>\n";
    $eQueue = new EmailQueue($_SESSION['orgId']);
    $user = new User($_SESSION['orgId'], $_SESSION['userId']);
    $eQueue->fromAddress = "$user->firstName $user->lastName <$user->email>";
  }

} else { // Show blank form since no queue ID passed in

  if (isset($_REQUEST['queueId']) && is_numeric($_REQUEST['queueId'])) {
    $eQueue->setEmailQueueId($_REQUEST['queueId']);
    if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'delete') {
      $eQueue->DeleteEmailQueue();
      $statusMsg = "Queued message was successfully deleted.<BR>\n";
      $eQueue = new EmailQueue($_SESSION['orgId']);
      $user = new User($_SESSION['orgId'], $_SESSION['userId']);
      $eQueue->fromAddress = "$user->firstName $user->lastName <$user->email>";
    }
  } else {
    $user = new User($_SESSION['orgId'], $_SESSION['userId']);
    $eQueue->fromAddress = "$user->firstName $user->lastName <$user->email>";
  }
}

$tmp = EmailQueue::GetQueueList($_SESSION['orgId'], $_SESSION['trackId']);
// Post processing of data

foreach($tmp as $k => &$v) {
  // If the delivery date is 0 show not yet
  if ($v[3] == '0000-00-00')
    $v[3] = 'Not Yet';
}

//error_log("ROWS: ".print_r(array_values($emailQueueRows), TRUE));
$smarty->assign('superUser', $_SESSION['superUser']);
$smarty->assign('queueListLbls', array_values(unserialize(EMAIL_QUEUE_LABELS)));
$smarty->assign('queueIds', array_keys($tmp));
$smarty->assign('queueRows', array_values($tmp));
$smarty->assign_by_ref('queueEntry', $eQueue);
$smarty->assign('statusMsg', $statusMsg);
$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/class_email.tpl');


?>