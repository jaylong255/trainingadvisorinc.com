<?php

require_once ('../includes/common.php');
//require_once ('../includes/Organization.php');


$config = new Config($_SESSION['orgId']);


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}

// Make sure we have a current org id and if not, send to security
if (!isset($_SESSION['orgId'])) {
    header("Location: /desktop/login/security.php");
    exit(0);
}


// Create session and local/(script global) variables


// Specify this as the current tab
$_SESSION['currentTab'] = TAB_ORG;
$_SESSION['currentSubTab'] = SUBTAB_ORG_REPORTS;


if ($config->GetValue(CONFIG_LICENSED_AB1825_CONTENT)) {
  $smarty->assign('showAb1825Reports', TRUE);
} else {
  $smarty->assign('showAb1825Reports', FALSE);
}  


if ($config->GetValue(CONFIG_SHOW_USER_THAT_ANSWERED)) {
  $smarty->assign('showIndividualReport', TRUE);
} else {
  $smarty->assign('showIndividualReport', FALSE);
}


$smarty->assign('superUser', $_SESSION['superUser']);
$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/org_reports.tpl');

exit(0);
?>
