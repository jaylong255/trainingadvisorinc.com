<?php
	session_start();
?>

<html>
<head>
<title>Release</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<?php

	$Link;
	$Action = "question_release.php";
	include '../includes/ReturnURL.php';

	//////////////////////////////////////////////
	//Open Database
	//////////////////////////////////////////////
	function OpenDatabase()
	{
		global $session_Host;
		global $session_User;
		global $session_Password;
		global $Link;

		$Link = mysql_connect($session_Host,$session_User,$session_Password);
	}

	//////////////////////////////////////////////
	//Close Database
	//////////////////////////////////////////////
	function CloseDatabase()
	{
		global $Link;

		mysql_close($Link);
	}


	/////////////////////////////////////////////////////////////////////////////
	//	UpdateQuestionsFile 			//Write the following question file to the Organization Directory
	////////////////////////////////////////////////////////////////////////////
	function UpdateQuestionsFile($database)
	{
		global $session_Host;
		global $session_User;
		global $session_Password;
		global $strLogFile;

		$db = mysql_connect($session_Host, $session_User, $session_Password);

					// Attempt to open the companys database
		if (!mysql_select_db($database, $db)) {
			print("An error occurred while attempting to open database '$database'.<br>\n");
			mysql_close($db);
			return;
		}
					// Get and display the company name
		$result = mysql_query("SELECT Directory FROM Organization LIMIT 1", $db);
		if ($result && mysql_num_rows($result) != 0) {
			$Organization_Directory = mysql_result($result, 0, "Directory");

			$source=$strLogFile;
			$dest="../organizations/$Organization_Directory/reports/question_import.txt";
			if(!copy($source,$dest))
				print("failed to copy $file...<br>\n");
			else
			{
				print("<br>Copied '$source' into '$dest'");
				return TRUE;
			}

		}
		return FALSE;
	}

	/////////////////////////////////////////////////////////////////////////////
	//	UpdateOrganizations 			//Get all the Organizations in the system
	////////////////////////////////////////////////////////////////////////////
	function UpdateOrganizations()
	{

		global $session_Host;
		global $session_User;
		global $session_Password;
		$db = mysql_connect($session_Host, $session_User, $session_Password);

					// Attempt to open the authentication database
		if (mysql_select_db("HR_Tools_Authentication",$db))
		{
					// Enumerate through list of organization databases
			$result = mysql_query("SELECT Database_Name FROM Organization", $db);
			mysql_close($db);

			print("<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><b>Updating Organizations:</b>");

			$var=TRUE;
			while ($row = mysql_fetch_array($result))
			{
				$database = $row["Database_Name"];
				if(!UpdateQuestionsFile($database)) $var=FALSE;
			}
		}
		return $var;
	}



	/////////////////////////////////////////////////////////////////////////////
	//	output 			//Write the following string to the document and logfile.
	////////////////////////////////////////////////////////////////////////////
	function CheckFile($dir,$file)
	{


		$strLogFile = "../organizations/$dir/$file";

		if(!file_exists($strLogFile))
		{
											// See if the log directory exists, create if needed
				if (!is_dir("../organizations/$dir"))
				{
					$ErrMsg="<br>Directory Does Not Exist";

					if (!is_writable("../organizations/$dir")) {
						$ErrMsg="NOTICE: Unable to create log directory, no log file will be saved.<br>";
					}

					$oldumask = umask(0);
					if (!mkdir("../organizations/$dir", 0777)) {
						$ErrMsg="NOTICE: Unable to create log directory, no log file will be saved.<br>";
					}
					umask($oldumask);
				}
													// Ensure log directory is writable
				if (!is_writable("../organizations/$dir")) {
					$ErrMsg="NOTICE: File access to log folder not allowed on server, no log file will be saved.<br>";
				}

						// Create a new empty file
				$fp = fopen($strLogFile, "w");
				if ($fp != FALSE)
				{
					fclose($fp);
					return TRUE;
				}
				else
				{
					return FALSE;
				}
		}

		if (file_exists($strLogFile))
		{

			if (!is_writable("../organizations/$dir/$File"))
			{
				$ErrMsg = "NOTICE: File access to report folder not allowed on server, no log report will be created.";
				return FALSE;
			}
			return TRUE;
		}


	}

	//////////////////////////////////////////////
	//Escape Characters
	//////////////////////////////////////////////
	function EscapeCharacters($str)
	{
		$str=str_replace(",","\,",$str);  //Replace commas with \comma
		$str=str_replace("`","\`",$str);  //Replace apostrophes with \apostrophe

		$str=str_replace("\\'","'",$str);  //Replace single quote with \single quote
		$str=str_replace("'","\'",$str);  //Replace single quote with \single quote

		$str=str_replace("\"","\\\"",$str);  //Replace double quotes with \double quotes

		$str=str_replace("\n","\\n",$str);  //Replace double quotes with \double quotes
		$str=str_replace("\r","\\r",$str);  //Replace double quotes with \double quotes

		return $str;
	}



	//////////////////////////////////////////////
	//Validate the Form
	//////////////////////////////////////////////
	function Validate_Form()
	{

		OpenDatabase();

		global $session_DBName;
		global $Link;
		global $FormAction;
		global $F_Date;
		global $Msg;
		global $strLogFile;


		$Query = "SELECT * FROM Organization";
		if($Result=mysql_db_query($session_DBName,$Query,$Link))
		{
			$Row = mysql_fetch_array($Result);
			$Directory = $Row['Directory']."/reports";
		}

		if (strcmp($FormAction,"Release")==0)
		{

			$New_Date=$F_Date." 00:00:00";
			$Query="Update Question Set Version_Date='$F_Date' Where Question_Number>0";

			if(mysql_db_query($session_DBName,$Query,$Link))
			{
				$String=sprintf("<b>%s</b>",mysql_error());
				print("$String");
			}

			$Query="SELECT * FROM Question Where Question_Number";
			$Result = mysql_db_query($session_DBName,$Query,$Link);
			if($Result)
			{
				$File="questions_$F_Date.txt";
				if(CheckFile($Directory,$File))
				{
					$strLogFile = "../organizations/$Directory/$File";
					if($fp = fopen($strLogFile, "w"))
					{
						while($Row = mysql_fetch_array($Result))
						{

							$String = "'";
							$String = $String.$Row['Question_Number'];
							$String = $String."','";
							$String = $String.$Row['Language_ID'];
							$String = $String."','";
							$String = $String.$Row['Domain_ID'];
							$String = $String."','";
							$String = $String.$Row['Version_Date'];
							$String = $String."','";
							$String = $String.$Row['Category_ID'];
							$String = $String."','";
							$String = $String.$Row['Frametype_ID'];
							$String = $String."','";
							$String = $String.$Row['Is_Management'];
							$String = $String."','";
							$String = $String.EscapeCharacters($Row['Author']);
							$String = $String."','";
							$String = $String.EscapeCharacters($Row['Notes']);
							$String = $String."','";
							$String = $String.EscapeCharacters($Row['Title']);
							$String = $String."','";
							$String = $String.EscapeCharacters($Row['Support_Info']);
							$String = $String."','";
							$String = $String.EscapeCharacters($Row['Content']);
							$String = $String."'";
							$String = $String."\r\n";
							fwrite($fp, $String);
						}
						fclose($fp);

						$Query="Replace INTO Question_Archive SELECT * FROM Question";
						if(mysql_db_query($session_DBName,$Query,$Link))
						{
								$ErrMsg="Success!!!";
						}

						if(UpdateOrganizations()) $Msg="File 'question_import.txt' was successfully created and copied to each organization directory in the system.";
						else $Msg="File question_import.txt was successfully created but could not be written to the organization directories.";
					}
					else $ErrMsg = "NOTICE: File could not be opened.";

				}
				else $Msg="The Directory does not have write privledges.";
			}
			else $Msg="Database Error.";
		}
		return FALSE;
	}


	//////////////////////////////////////////////
	//Start/Restart PHP Page
	//////////////////////////////////////////////
	if($Insert==TRUE)
	{
		if(Validate_Form())
		{
			 $ErrMsg = "Form Action";
		}
		CloseDatabase();
	}

	//Pass Parameters
	if ($get_return_name && $get_return_URL) {
			$decode_return_url = DecodeReturnURL($get_return_URL);
			$options=$options."&get_return_URL=".$get_return_URL."&get_return_name=".$get_return_name;
	}
	$return_url = EncodeReturnURL("$PHP_SELF?$options");

?>

<style type="text/css">
body{ scrollbar-base-color: #EAD8B2 }
.FormText {  font-family:tahoma,sans-serif; font-size:13px; font-weight: bold; color: #376FA5; text-decoration: none}
.buttontext {  font-family:tahoma,sans-serif; font-size:13px; font-weight: bold; color: #0800FC; text-decoration: none}
.MessageText {  font-family:tahoma,sans-serif; font-size:13px; font-weight: bold; color: #097B27; text-decoration: none}
.FormValue { font-size: 13px; color: #000000; background-color: #F2E8D2}
.FormDisabled { font-size: 13px; font-weight: baold; color: #000000; text-decoration: none}
.PageText {  font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #000000; font-weight: bold; text-decoration: none}
</style>

<SCRIPT language="JavaScript" SRC="calendar.js"></SCRIPT>
<script language="JavaScript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


function DoSearch()
{

	DateObj=document.getElementById("F_Date");
	if(DateObj.value.length<10)
	{
		alert("Invalid Date. The Date must be in the following YYYY-MM-DD format \"2003-01-24\".  You can use the calendar icon to select a date in the correct format.")
		DateObj.select();
		DateObj.focus();
	}
	else
	{
		  FormObj = MM_findObj("AdminForm");
		  FormObj.action="question_release.php?FormAction=Release";
		  FormObj.submit();
	}

}


var bSave=false;
function SetFlag()
{
	bSave=true;
}

function CheckSave(newLocation)
{

	if(!bSave)
	{
		window.location.href = newLocation;
	}
	else
	{
		if(confirm("Would you like to save your changes to this form?"))
		{
			Update(newLocation);
		}
		else window.location.href = newLocation;
	}
}

function Branch(strNew)
{
	CheckSave(strNew);
}

//alert("Error:<?php echo $ErrMsg ?>");


</script>

<body bgcolor="#FFFCED" onLoad="MM_preloadImages('graphics/btn_update_up.gif','graphics/btn_update_down.gif','graphics/btn_back_down.gif')">
<div id="Back" style="position:absolute; left:12px; top:4px; width:250px; height:27px; z-index:3; visibility:<?php echo $bVisible ?>" class="FormText">
  <table border="0" width="100%" align="center" cellpadding="2" cellspacing="3">
    <tr>
      <td><a href="javascript:CheckSave('<?php echo $decode_return_url ?>');" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Back','','graphics/btn_back_down.gif',1)"><img name="Back" border="0" src="graphics/btn_back_up.gif" width="23" height="21"></a></td>
      <td><a class="buttontext" href="javascript:CheckSave('<?php echo $decode_return_url ?>');">Back
        to Questions List</a></td>
    </tr>
  </table>
</div>
<div id="Break" style="position:absolute; left:11px; top:29px; width:99%; height:18px; z-index:4"><hr></div>
<form name="AdminForm" method="post" action="">
  <div id="Item2" class="FormText" style="position:absolute; left:20px; top:61px; width:678px; height:153px; z-index:6; visibility: visible">
    <table cellpadding="5">
   <tr>
        <td class="FormText">Version Release:</td>
   </tr>
   <tr>
        <td class="FormText">Releasing a version of the questions will do the following:<ol><li>Date stamp all of the questions in the Current Questions tab.
            <li>Place a copy of the the version question set into Archive.
            <li>Create a &quot;questions_{date}.txt&quot; file on the server in
              the organizations/yourorganization/scripts folder.
          </ol>
          Please provide a date for the date stamp and filename by selecting the
          calendar icon below and choosing a date.</td>
   </tr>
   </table>
   <table cellpadding="5">
   <tr>
        <td class="PageText">Version Date:</td>
        <td valign="middle">
            <input class="FormValue" type="text" name="F_Date" id="F_Date" value ="<?php echo $F_Date; ?>">
            <a href="javascript:doNothing()" onClick="setDateField(document.getElementById('F_Date'));top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')">
            <img id="cal" src="graphics/calendar.gif" border=0 width="16" height="16"></a>
        </td>
        <td><a href="javascript:DoSearch();" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('GoSearch','','graphics/btn_go_down.gif',1)"><img name="GoSearch" border="0" align="absbottom" width="25" height="22" src="graphics/btn_go_up.gif"></a></td>
  	</tr>
  	</table>
  	<table cellpadding="5">
  	<tr><td class="MessageText"><blockquote><?php echo $Msg ?></td></tr>
  	<tr><td><input type="hidden" name="Insert" value="TRUE"></td></tr>
	<tr><td><input type="hidden" name="get_return_name" value="<?php echo $get_return_name ?>"></td></tr>
    <tr><td><input type="hidden" name="get_return_URL" value="<?php echo $get_return_URL ?>"></td></tr>
   </table>
  </div>
</form>
</body>
</html>
