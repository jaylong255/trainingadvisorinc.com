<?php

require_once ('../includes/common.php');
require_once ('../includes/Organization.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is at least an admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] == ROLE_ORG_ADMIN)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}

// Make sure we have a current org id and if not, send to security
if (!isset($_SESSION['orgId'])) {
    header("Location: /desktop/login/security.php");
    exit(0);
}

// Validate our new object
$org = new Organization($_SESSION['orgId']);
if (!$org) {
    header("Location: /desktop/login/error.php");
    exit(0);
}

if (!isset($_REQUEST['elementValue'])) {
  $_REQUEST['elementValue'] = '';
}



// If form previosly submitted with previously selected value
// then display that value
if (isset($_REQUEST['dataElementValues'])) {
  $smarty->assign('dataElementSelected', $_REQUEST['dataElementValues']);
} else {
  $smarty->assign('dataElementSelected', '');
}

// Form navigation: this value is passed as a GET param
// from the page containing the IFRAME these forms are
// loaded into.  We need to load the appropriate form
// to initialize it and then we can safely exit.
if (isset($_REQUEST['form'])) {

  $_SESSION['currentDataEditFrame'] = $_REQUEST['form'];

} else { // Form Processing

  // Form is being submitted so check for operation
  switch($_REQUEST['operation']) {
  case 'add': // SetData on insert will return the last_insert_id or new index
    $res = $org->SetData($_SESSION['currentDataEditFrame'],
		  '',
		  $_REQUEST['elementValue']);
    if ($res != 0)
      $smarty->assign('dataElementSelected', $res);
    break;
  case 'update':
    $res = $org->SetData($_SESSION['currentDataEditFrame'],
			 $_REQUEST['dataElementValues'],
			 $_REQUEST['elementValue']);
    $smarty->assign('hideAddDiv', TRUE);
    break;
  case 'delete':
    $res = $org->SetData($_SESSION['currentDataEditFrame'],
		  $_REQUEST['dataElementValues'],
		  '');
    break;
  default:
    error_log("Unknown operation '$_REQUEST[operation]' passed to org_data_edit.php");
    break;
  }
}

$editSelectLabels = unserialize(ORG_DATA_EDIT_SELECT_LABELS);
//$dataElementVals = $org->GetData($_SESSION['currentDataEditFrame']);
$smarty->assign('dataElement', $editSelectLabels[$_SESSION['currentDataEditFrame']]);
$smarty->assign('currentDataEditFrame', $_SESSION['currentDataEditFrame']);

// Gets the data element lists for the current frame
$smarty->assign('dataElementValues', $org->GetData($_SESSION['currentDataEditFrame']));
$smarty->assign('elementValue', $_REQUEST['elementValue']);

//if ($_SESSION['currentDataEditFrame'] == ORG_DATA_EDIT_CUSTOM_LABELS) {
//  $smarty->display('admin/org_data_edit_custom_labels.tpl');
//} else {
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/org_data_edit.tpl');
//}

exit(0);

?>
