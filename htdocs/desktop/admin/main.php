<?php

require_once ('../includes/common.php');
require_once ('../includes/Config.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}

// If we are accessing main.php for the first time, set us up
if (isset($_REQUEST['init']) && $_REQUEST['init']) {
  // If this is just a track supervisor, then drop then to the track edit page
  if (isset($_SESSION['role']) &&
      ($_SESSION['role'] == ROLE_CLASS_CONTACT_SUPERVISOR || $_SESSION['role'] == ROLE_CLASS_CONTACT_SUPERVISOR)) {
    $_SESSION['currentTab'] = TAB_TRACK;
    $_SESSION['currentSubTab'] = SUBTAB_NONE;
  } elseif (isset($_SESSION['role']) &&
	    ($_SESSION['superUser'] || $_SESSION['role'] != ROLE_END_USER)) {
    // If not a super user but an admin then send them to their organization page
    $_SESSION['currentTab'] = TAB_ORG;
    if ($_SESSION['role'] != ROLE_ORG_ADMIN)
      $_SESSION['currentSubTab'] = SUBTAB_ORG_REPORTS;
    else
      $_SESSION['currentSubTab'] = SUBTAB_ORG_INFO;
  }

  header("Location: /desktop/admin/main.php");
  exit(0);
}

if (isset($_REQUEST['changeOrgId']) && is_numeric($_REQUEST['changeOrgId']) &&
    $_REQUEST['changeOrgId']) {
  ChangeOrg($_REQUEST['changeOrgId']);
  if (isset($_REQUEST['tab']) && is_numeric($_REQUEST['tab']))
    $_SESSION['currentTab'] = $_REQUEST['tab'];
  if (isset($_REQUEST['subtab']) && is_numeric($_REQUEST['subtab']))
    $_SESSION['currentSubTab'] = $_REQUEST['subtab'];
  header("Location: /desktop/admin/main.php");
  exit(0);
}

$org = new Organization($_SESSION['orgId']);
$config = new Config($_SESSION['orgId']);

//$org->GetMotif($topLeftBg, $topMiddleBg, $topRightBg, $topLeftImg);

if (isset($_REQUEST['changeUiTheme']) && !empty($_REQUEST['changeUiTheme'])) {
  $config->SetValue(CONFIG_UI_THEME, $_REQUEST['changeUiTheme']);
  $_SESSION['uiTheme'] = $_REQUEST['changeUiTheme'];
}

// This is used to set the size of the browser window
// once upon load.  After that, this variable ensures
// the template does not call the SetSize() javascript
// function again.
$smarty->assign('initSession', $_SESSION['initSession']);

$_SESSION['initSession'] = FALSE;


// Setup the proper content window.  This make navigation much
// easier to swallow when the refresh button is pressed
switch($_SESSION['currentTab']) {
 case TAB_ORG:
   switch($_SESSION['currentSubTab']) {
   case TAB_NONE: // organization list
     $smarty->assign('frameTarget', 'org_list.php');
     break;
   case SUBTAB_ORG_INFO:
     $smarty->assign('frameTarget', 'org_info.php');
     break;
   case SUBTAB_ORG_CONTACT:
     $smarty->assign('frameTarget', 'org_contact.php');
     break;
   case SUBTAB_ORG_DATA:
     $smarty->assign('frameTarget', 'org_data.php');
     break;
   case SUBTAB_ORG_UTILITIES:
     $smarty->assign('frameTarget', 'org_utilities.php');
     break;
   case SUBTAB_ORG_REPORTS:
     $smarty->assign('frameTarget', 'org_reports.php');
     break;
   default:
     //TODO: put a nice error page here to indicate that the navigation went bad and no valid frameTarget could be found
     //$smarty->assign('frameTarget', '');
     break;
   }
   break;
 case TAB_USER:
   switch($_SESSION['currentSubTab']) {
   case SUBTAB_USER_LIST:
     $smarty->assign('frameTarget', 'user_list.php');
     break;
   case SUBTAB_USER_EDIT:
     $smarty->assign('frameTarget', 'user_edit.php?userId='.$_SESSION['editUserId']);
   default:
     break;
   }
   break;
 case TAB_TRACK:
   switch($_SESSION['currentSubTab']) {
   case TAB_NONE:
     $smarty->assign('frameTarget', 'track_list.php');
     break;
   case SUBTAB_TRACK_INFO:
     $smarty->assign('frameTarget', 'track_edit.php');
     break;
   case SUBTAB_TRACK_PARTICIPANTS:
     if ($_SESSION['participantShowAll'])
       $smarty->assign('frameTarget', 'participant_list.php?showAll=1');
     else
       $smarty->assign('frameTarget', 'participant_list.php');
     break;
   case SUBTAB_TRACK_ASSIGNMENTS:
     $smarty->assign('frameTarget', 'assignment_list.php');
     break;
   case SUBTAB_TRACK_FAQS:
     $smarty->assign('frameTarget', 'track_faqs.php');
     break;
   case SUBTAB_TRACK_PROGRESS:
     $smarty->assign('frameTarget', 'progress_list.php');
     break;
   case SUBTAB_TRACK_EMAIL:
     $smarty->assign('frameTarget', 'class_email.php');
     break;
   default:
     break;
   }
   break;
 case TAB_QUESTION:
   switch($_SESSION['currentSubTab']) {
   case SUBTAB_QUESTION_CURRENT:
     $smarty->assign('frameTarget', 'question_list.php');
     break;
   case SUBTAB_QUESTION_EDIT:
     $smarty->assign('frameTarget', 'question_edit.php?questionId='.$_SESSION['editQuestionId']);
     break;
   case SUBTAB_QUESTION_ARCHIVE:
     $smarty->assign('frameTarget', 'archive_list.php');
     break;
   case SUBTAB_QUESTION_SUMMARY:
     $smarty->assign('frameTarget', 'question_summary.php');
     break;
   case SUBTAB_QUESTION_ANALYSIS:
     $smarty->assign('frameTarget', 'question_analysis.php');
     break;
   case SUBTAB_QUESTION_VIEW_CURRENT:
     $smarty->assign('frameTarget', 'question_view.php?questionId='.$_SESSION['editQuestionId']);
     break;
   case SUBTAB_QUESTION_VIEW_ARCHIVED:
     $smarty->assign('frameTarget', 'question_view.php?questionId='.$_SESSION['editQuestionId']
		     .'&domainId='.$_SESSION['editQuestionDomainId']
		     .'&versionDate='.$_SESSION['versionDate']);
     break;
   case SUBTAB_QUESTION_CATEGORIES:
     $smarty->assign('frameTarget', 'org_data_edit.php?form='.ORG_DATA_EDIT_QUESTION_CATEGORIES);
     break;
   default:
     break;
   }
   break;
 case TAB_CUSTOMIZE:
   switch($_SESSION['currentSubTab']) {
   case TAB_NONE:
     $smarty->assign('frameTarget', 'file_man.php');
     break;
   case SUBTAB_CUSTOMIZE_FILEMAN:
     $smarty->assign('frameTarget', 'file_man.php');
     break;
   case SUBTAB_CUSTOMIZE_MISC:
     $smarty->assign('frameTarget', 'cust_misc.php');
     break;
   default:
     break;
   }
   break;
 case TAB_NONE:
   break;
 default:
   break;
}


SetupMotifDisplay($smarty, $config);

$smarty->assign('superUser', $_SESSION['superUser']);
$smarty->assign('role', $_SESSION['role']);
//$smarty->assign('trackAdmin', $_SESSION['trackAdmin']);

$smarty->assign('userId', $_SESSION['userId']);
//$smarty->assign('orgLogo', $_SESSION['orgLogo']);
$smarty->assign('orgName', $config->GetValue(CONFIG_ORG_NAME));
$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);

if (DEBUG) {
  $smarty->assign('orgId', $_SESSION['orgId']);
  $smarty->assign('dbName', $_SESSION['dbName']);
}

$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/main.tpl');


?>



