<?php

require_once ('../includes/common.php');
//require_once ('../includes/Organization.php');
require_once ('../includes/ReportAccuracyByTrack.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}

// Make sure we have a current org id and if not, send to security
if (!isset($_SESSION['orgId'])) {
    header("Location: /desktop/login/security.php");
    exit(0);
}

// TODO: make sure if contact, this user only gets the tracks for which they
// are responsible


// Create session and local/(script global) variables
$rep = NULL;
$trackId = '';
$questionId = 0;
$dateFrom = '';
$dateTo = '';
$resultFormat = 0;
$trackIdsSelected = array();

// Specify this as the current tab
$_SESSION['currentTab'] = TAB_ORG;
$_SESSION['currentSubTab'] = SUBTAB_ORG_REPORTS;


if (isset($_REQUEST['trackIdsSelected']))
  $trackIdsSelected = $_REQUEST['trackIdsSelected'];
if (isset($_REQUEST['questionId']))
  $questionId = $_REQUEST['questionId'];
if (isset($_REQUEST['dateFrom']))
  $dateFrom = $_REQUEST['dateFrom'];
if (isset($_REQUEST['dateTo']))
  $dateTo = $_REQUEST['dateTo'];

if (!isset($_REQUEST['resultFormatNumbers']) && !isset($_REQUEST['resultFormatPercentages'])) {
  $resultFormat = RESULT_FORMAT_NUMBERS;
} else {
  if (isset($_REQUEST['resultFormatNumbers']) && $_REQUEST['resultFormatNumbers'] == 1)
    $resultFormat |= RESULT_FORMAT_NUMBERS;
  if (isset($_REQUEST['resultFormatPercentages']) && $_REQUEST['resultFormatPercentages'] == 1)
    $resultFormat |= RESULT_FORMAT_PERCENTAGES;
}

if (!isset($_REQUEST['outputFormat'])) {
  $outputFormat = OUTPUT_FORMAT_HTML;
} else {
  switch($_REQUEST['outputFormat']) {
  case 'html':
    $outputFormat = OUTPUT_FORMAT_HTML;
    break;
  case 'csv':
    $outputFormat = OUTPUT_FORMAT_CSV;
    break;
  case 'pdf':
    $outputFormat = OUTPUT_FORMAT_PDF;
    break;
  default:
    error_log("org_report_accuracy_by_track.php: invalid output format passed to form: ".$_REQUEST['outputFormat']);
    $outputFormat = OUTPUT_FORMAT_HTML;
    break;
  }
}


// Specify this as the current tab
$_SESSION['currentTab'] = TAB_ORG;
$_SESSION['currentSubTab'] = SUBTAB_ORG_REPORTS;


$smarty->assign('superUser', $_SESSION['superUser']);
$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
//$smarty->display('admin/org_reports.tpl');


if (!is_numeric($questionId) ||
    ($dateTo && !preg_match("/\d{4}(-\d{2}){2}/", $dateTo)) ||
    ($dateFrom && !preg_match("/\d{4}(-\d{2}){2}/", $dateFrom))) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
} 

// Validate tracks, questions, and dates
if (isset($trackIdsSelected) && $trackIdsSelected) {
  foreach ($trackIdsSelected as $localTrackId) {
    if (!is_numeric($localTrackId)) {
      error_log("ort_report_accuracy_by_track.php: ERROR(Form Validation): non numeric value submitted for trackId($localTrackId)");
      header("Location: /desktop/login/perm_denied.php");
      exit(0);
    }
  }
}


// Check to see if form is being populated or submitted.  This first block
// of the if statement is the form being submitted.
if ((isset($_REQUEST['execute']) && $_REQUEST['execute'] == '1') ||
    (isset($_REQUEST['execute_x']) && isset($_REQUEST['execute_y']))) {

  // Create the report that has functions to 
  if ($_SESSION['role'] == ROLE_DIVISION_ADMIN ||
      $_SESSION['role'] == ROLE_DEPARTMENT_ADMIN ||
      $_SESSION['role'] == ROLE_CLASS_CONTACT_SUPERVISOR)
    $rep = new ReportAccuracyByTrack($_SESSION['orgId'], $trackIdsSelected, (int)$questionId, $dateFrom, $dateTo,
				     $_SESSION['divisionId'], $_SESSION['departmentId']);
  else
    $rep = new ReportAccuracyByTrack($_SESSION['orgId'], $trackIdsSelected, (int)$questionId, $dateFrom, $dateTo, 0, 0);
    
  $results = $rep->GetReportData();
  switch($outputFormat) {
  case OUTPUT_FORMAT_HTML:
    $smarty->assign('outputFormat', $_REQUEST['outputFormat']);
    $smarty->assign('dateFrom', $_REQUEST['dateFrom']);
    $smarty->assign('dateTo', $_REQUEST['dateTo']);
    $smarty->assign('questionId', $_REQUEST['questionId']);
    if ($resultFormat & RESULT_FORMAT_NUMBERS)
      $smarty->assign('resultFormatNumbers', 1);
    else
      $smarty->assign('resultFormatNumbers', 0);
    if ($resultFormat & RESULT_FORMAT_PERCENTAGES)
      $smarty->assign('resultFormatPercentages', 1);
    else
      $smarty->assign('resultFormatPercentages', 0);
    $smarty->assign('trackIdsSelected', implode('&trackIdsSelected[]=', $trackIdsSelected));
    $smarty->assign('trackList', $results);
    $smarty->assign('uiTheme', $_SESSION['uiTheme']);
    $smarty->display('admin/org_report_accuracy_by_track_results.tpl');
    break;
  case OUTPUT_FORMAT_PDF:
    $output = $rep->CreatePdf($results, $resultFormat & RESULT_FORMAT_NUMBERS, $resultFormat & RESULT_FORMAT_PERCENTAGES);
    //$smarty->assign('reportOutput', $output);
    //$smarty->display('admin/org_report_accuracy_by_track_results_test_pdf.tpl');
    break;
  case OUTPUT_FORMAT_CSV:
    $rep->CreateCsv($results, $resultFormat & RESULT_FORMAT_NUMBERS, $resultFormat & RESULT_FORMAT_PERCENTAGES);
    break;
  }

} else {

  // Only need access to the functions that get distinct elements so just a Report object works
  $rep = new Report($_SESSION['orgId']);
  $tracks = $rep->GetDistinctTracks();
  if (empty($tracks))
    $smarty->assign('reportsErrMsg', 'There are no tracks defined.  This report can be run after some tracks have been setup.');
  else {
    $trackIds = array_keys($tracks);
    $trackNames = array_values($tracks);

    $smarty->assign_by_ref('trackIds', $trackIds);
    $smarty->assign_by_ref('trackNames', $trackNames);
    $smarty->assign('trackIdsSelected', $trackIdsSelected);
    $smarty->assign_by_ref('questions', $rep->GetDistinctQuestionIds($trackIds));
    $smarty->assign('questionId', $questionId);
    $smarty->assign('deliveryDates', $rep->GetDistinctDeliveryDates($trackId, (int)$questionId));
    $smarty->assign('dateFrom', $dateFrom);
    $smarty->assign('dateTo', $dateTo);
    if (isset($_REQUEST['outputFormat']))
      $smarty->assign('outputFormat', $_REQUEST['outputFormat']);
    if (isset($_REQUEST['resultFormatNumbers']))
      $smarty->assign('resultFormatNumbers', $_REQUEST['resultFormatNumbers']);
    if (isset($_REQUEST['resultFormatPercentages']))
      $smarty->assign('resultFormatPercentages', $_REQUEST['resultFormatPercentages']);
  }

  $smarty->assign('superUser', $_SESSION['superUser']);
  $smarty->assign('currentTab', $_SESSION['currentTab']);
  $smarty->assign('currentSubTab', $_SESSION['currentSubTab']);

  $smarty->assign('uiTheme', $_SESSION['uiTheme']);
  $smarty->display('admin/org_report_accuracy_by_track.tpl');
}

exit(0);
?>
