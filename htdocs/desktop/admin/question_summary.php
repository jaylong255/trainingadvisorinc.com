<?php
/////////////////////////////////////////////////////////////////////////////
//	qanalysis_list.php
//		Page used for viewing question result data.
//

// Include libs which starts session and creates smarty objects
require_once ('../includes/common.php');
require_once ('../includes/Config.php');
require_once ('../includes/Question.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER &&
      $_SESSION['role'] != ROLE_CLASS_CONTACT_SUPERVISOR)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}


// Specify this as the current tab
$_SESSION['currentTab'] = TAB_QUESTION;
$_SESSION['currentSubTab'] = SUBTAB_QUESTION_SUMMARY;


// create new employee list
$errMsg = '';
$rc = 0;
$firstRow = 0;
$lastRow = 0;
$numRows = 0;
$numPages = 0;
$questionIds = array();
$questions = array();
$summaryTracks = array();
$config = new Config($_SESSION['orgId']);
$questionList = new Question($_SESSION['orgId']);
$user = new User($_SESSION['orgId'], $_SESSION['userId']);
$searchMask = 0;
$showLink = NULL;

$configIncludeUserColumn = $config->GetValue(CONFIG_SHOW_USER_THAT_ANSWERED);


if (isset($_SESSION['trackAdminSummary']) && $_SESSION['trackAdminSummary']) {
  $showLink = new TplCaption('Show summary from all tracks');
  $showLink->href = '/desktop/login/change_prefs.php';
  $showLink->params = 'returnUrl=/desktop/admin/question_summary.php&trackAdminSummary=0';
} else {
  $showLink = new TplCaption('Show summary from my tracks only');
  $showLink->href = '/desktop/login/change_prefs.php';
  $showLink->params = 'returnUrl=/desktop/admin/question_summary.php&trackAdminSummary=1';
}


if (isset($_REQUEST['searchBy'])) {
  $searchBy = $_REQUEST['searchBy'];
} else {
  $searchBy = SUMMARY_LIST_COLUMN_TRACK_ID;
}

if (isset($_REQUEST['searchFor'])) {
  $searchFor = $_REQUEST['searchFor'];
} else {
  $searchFor = '';
}

// If we should limit summary viewing to only those tracks managed by the current user
// then fetch their ids, otherwise the empty array initialized above will be passed
if ($_SESSION['trackAdminSummary'])
  $summaryTracks = $user->GetSupervisedTrackIds();
else
  $summaryTracks = FALSE;

$rc = $questionList->GetSummaryList($_SESSION['summaryShowColumns'], $searchBy, $searchFor,
				    $_SESSION['summarySortColumn'], $_SESSION['summarySortDescending'],
				    $_SESSION['summaryPageNumber'], $_SESSION['rowsPerPage'], $summaryTracks);

if ($rc === FALSE) {
  echo "Failed to get employee list\n";
  exit(1);
}

if (DEBUG & DEBUG_FORM) {
  $smarty->assign('GetArchiveList', $rc);
}


if (!empty($rc)) {
  list($questionIds, $questions) = $rc;
  $questionList->GetRowCounts($firstRow, $lastRow, $numRows);
  $numPages = $questionList->GetPageCount($_SESSION['rowsPerPage']);
}


// setup links across the top of the page
$smarty->assign('topLinkList', $showLink);

$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('columnLabels', $questionList->GetSummaryColumnLabels($configIncludeUserColumn));
$smarty->assign('showColumns', $_SESSION['summaryShowColumns']);

// Set Search params
$smarty->assign('searchBy', $searchBy);
$smarty->assign('searchFor', $searchFor);

// Set the one to be selected
$smarty->assign('sortColumn', $_SESSION['summarySortColumn']);
$smarty->assign('sortDescending', $_SESSION['summarySortDescending']);

$smarty->assign('firstRow', $firstRow);
$smarty->assign('lastRow', $lastRow);
$smarty->assign('numRows', $numRows);
$smarty->assign('pageNumber', $_SESSION['summaryPageNumber']);
$smarty->assign('numPages', $numPages);
$smarty->assign_by_ref('recordList', $questions);
$smarty->assign_by_ref('recordIds', $questionIds);

$smarty->assign('errMsg', $errMsg);
$questionList->Close();

$smarty->assign('toggleParamName', 'toggleSummaryColumn');
$smarty->assign('sortParamName', 'summarySortColumn');
$smarty->assign('idParamName', 'questionId');
$smarty->assign('pageNumberParamName', 'summaryPageNumber');
$smarty->assign('orderParamName', 'summarySortDescending');
$smarty->assign('showSelectedColumn', FALSE);
$smarty->assign('rowsPerPage', $_SESSION['rowsPerPage']);
$smarty->assign('localHeader', 'question_alt_header');
$smarty->assign('hideActionHeader', TRUE);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/generic_list.tpl');


exit(0);

?>
