<?php

require_once ('../includes/common.php');
require_once ('../includes/Organization.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  //    echo "Required session login information not present.";
  header("Location: /desktop/login/expired.php");
  exit;
}

// Ensure user is superuser
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] == ROLE_ORG_ADMIN)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}

// Make sure we have a current org id and if not, send to security
if (!isset($_SESSION['orgId'])) {
    header("Location: /desktop/login/security.php");
    exit(0);
}


$_SESSION['currentTab'] = TAB_ORG;
$_SESSION['currentSubTab'] = SUBTAB_ORG_DATA;


// common smarty assigns for all templates in org edit
$smarty->assign('create', FALSE);
$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('superUser', $_SESSION['superUser']);


$smarty->assign('editLabels', unserialize(ORG_DATA_EDIT_LABELS));
$smarty->assign('currentDataEditFrame', $_SESSION['currentDataEditFrame']);
$smarty->assign('frameBoarderSize', DEBUG_BORDER);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/org_data.tpl');


?>
