<?php

// Include libs which starts session and creates smarty objects
include_once ('../includes/common.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  //    echo "Required session login information not present.";
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or an org admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER &&
      $_SESSION['role'] != ROLE_CLASS_CONTACT_SUPERVISOR)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}

$_SESSION['currentTab'] = TAB_CUSTOMIZE;
$_SESSION['currentSubTab'] = SUBTAB_CUSTOMIZE_FILEMAN;

$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/file_man.tpl');

?>
