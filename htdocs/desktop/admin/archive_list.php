<?php
/////////////////////////////////////////////////////////////////////////////
//  archive_list.php
//	Page used for administration of questions for the current company.
//
//  Get parameters:



/////////////////////////////////////////////////////////////////////////////
//	archive_list.php
//		Page used for administration of questions stored on the current server
//


// Include libs which starts session and creates smarty objects
require_once ('../includes/common.php');
require_once ('../includes/Question.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_CLASS_CONTACT_SUPERVISOR &&
      $_SESSION['role'] != ROLE_END_USER)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}


// Specify this as the current tab
$_SESSION['currentTab'] = TAB_QUESTION;
$_SESSION['currentSubTab'] = SUBTAB_QUESTION_ARCHIVE;


if (isset($_REQUEST['searchBy'])) {
  $searchBy = $_REQUEST['searchBy'];
} else {
  $searchBy = QUESTION_LIST_COLUMN_TITLE;
}

if (isset($_REQUEST['searchFor'])) {
  $searchFor = $_REQUEST['searchFor'];
  $_SESSION['questionPageNumber'] = 1;
} else {
  $searchFor = '';
}

// If the questionId is set, it means we are deleting that question
if (isset($_REQUEST['questionId']) && $_REQUEST['questionId'] &&
    is_numeric($_REQUEST['questionId']) && isset($_REQUEST['domainId']) &&
    $_REQUEST['domainId'] && is_numeric($_REQUEST['domainId']) &&
    isset($_REQUEST['versionDate']) && $_REQUEST['versionDate']) {

  $questionList = new Question($_SESSION['orgId'], $_REQUEST['questionId'],
			       $_REQUEST['domainId'], $_REQUEST['versionDate']);

  if (isset($_REQUEST['restore']) && $_REQUEST['restore'])
    $rc = $questionList->UnArchiveQuestion($_REQUEST['domainId'], $_REQUEST['versionDate']);
  else {
    $rc = $questionList->DeleteArchivedQuestion();
  }

  if ($rc === RC_INVALID_ARG)
    $errMsg = RcToText($rc);
}


// create new employee list
$errMsg = '';
$rc = 0;
$firstRow = 0;
$lastRow = 0;
$numRows = 0;
$numPages = 0;
$createLink = '';
$questionIds = array();
$questions = array();
$questionList = new Question($_SESSION['orgId']);

$rc = $questionList->GetQuestionList($_SESSION['archiveShowColumns'], $searchBy, $searchFor,
				     $_SESSION['archiveSortColumn'], $_SESSION['archiveSortDescending'],
				     $_SESSION['archivePageNumber'], $_SESSION['rowsPerPage'],
				     $_SESSION['userId'], TRUE);

if ($rc === FALSE) {
  echo "Failed to get employee list\n";
  exit(1);
}

if (DEBUG & DEBUG_FORM) {
  $smarty->assign('GetArchiveList', $rc);
}


if (!empty($rc)) {
  list($questionIds, $questions) = $rc;
  $questionList->GetRowCounts($firstRow, $lastRow, $numRows);
  $numPages = $questionList->GetPageCount($_SESSION['rowsPerPage']);
}


// setup links across the top of the page
$smarty->assign('topLinkList', $createLink);

$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('columnLabels', $questionList->GetColumnLabels());
$smarty->assign('showColumns', $_SESSION['archiveShowColumns']);

// Set Search params
$smarty->assign('searchBy', $searchBy);
$smarty->assign('searchFor', $searchFor);

// Set the one to be selected
$smarty->assign('sortColumn', $_SESSION['archiveSortColumn']);
$smarty->assign('sortDescending', $_SESSION['archiveSortDescending']);

$smarty->assign('firstRow', $firstRow);
$smarty->assign('lastRow', $lastRow);
$smarty->assign('numRows', $numRows);
$smarty->assign('pageNumber', $_SESSION['archivePageNumber']);
$smarty->assign('numPages', $numPages);
$smarty->assign('recordList', $questions);
$smarty->assign('recordIds', $questionIds);

$smarty->assign('errMsg', $errMsg);
$questionList->Close();

$smarty->assign('toggleParamName', 'toggleArchiveColumn');
$smarty->assign('sortParamName', 'archiveSortColumn');
$smarty->assign('idParamName', 'questionId');
$smarty->assign('pageNumberParamName', 'archivePageNumber');
$smarty->assign('orderParamName', 'archiveSortDescending');
$smarty->assign('showSelectedColumn', FALSE);
$smarty->assign('rowsPerPage', $_SESSION['rowsPerPage']);
$smarty->assign('localHeader', 'question_header');
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/generic_list.tpl');


exit(0);

?>
