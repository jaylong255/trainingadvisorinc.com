<?php

// Include libs which starts session and creates smarty objects
require_once ('../includes/common.php');
require_once ('../includes/Config.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  //    echo "Required session login information not present.";
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or an org admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] == ROLE_ORG_ADMIN)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}

$_SESSION['currentTab'] = TAB_CUSTOMIZE;
$_SESSION['currentSubTab'] = SUBTAB_CUSTOMIZE_MISC;

$config = new Config($_SESSION['orgId']);

if (isset($_REQUEST['displayChoices']) &&
    ($_REQUEST['displayChoices'] == 'right' ||
     $_REQUEST['displayChoices'] == 'bottom')) {
  $config->SetValue(CONFIG_DISPLAY_CHOICES, $_REQUEST['displayChoices']);
}

// Get the list of available themes
chdir(THEME_ROOT);
$tmpArr = glob("*", GLOB_ONLYDIR);
$uiThemes = array();
foreach ($tmpArr as $val) {
  $uiThemes[$val] = $val;
}

if (isset($_REQUEST['submit'])) {
  if (isset($_REQUEST['useQuestionSetting']) && $_REQUEST['useQuestionSetting'])
    $config->SetValue(CONFIG_DISPLAY_CHOICES_USE_QUESTION_SETTING, 1);
  elseif (!isset($_REQUEST['useQuestionSetting']) || !$_REQUEST['useQuestionSetting'])
    $config->SetValue(CONFIG_DISPLAY_CHOICES_USE_QUESTION_SETTING, 0);

  if (isset($_REQUEST['trackAdminSummaryDefault']) && $_REQUEST['trackAdminSummaryDefault']) {
    $config->SetValue(CONFIG_TRACK_ADMIN_SUMMARY_DEFAULT, 1);
    $_SESSION['trackAdminSummary'] = 1;
  } elseif (!isset($_REQUEST['trackAdminSummaryDefault']) || !$_REQUEST['trackAdminSummaryDefault']) {
    $config->SetValue(CONFIG_TRACK_ADMIN_SUMMARY_DEFAULT, 0);
    $_SESSION['trackAdminSummary'] = 0;
  }

  if (isset($_REQUEST['emailFormat']) && $_REQUEST['emailFormat']) {
  	$config->SetValue(CONFIG_EMAIL_FORMAT, $_REQUEST['emailFormat']);
  }

  // If these are processed when the user is not a super user (ie these options not visible to regular users)
  // then they will be set in the database as not configured thus allowing a regular org admin to clear flags
  // that had been previously set by the SA account.
  if ($_SESSION['superUser']) {
    if (isset($_REQUEST['enableQuestionPrinting']) && $_REQUEST['enableQuestionPrinting'])
      $config->SetValue(CONFIG_LICENSED_PRINT_QUESTIONS, 1);
    elseif (!isset($_REQUEST['enableQuestionPrinting']) || !$_REQUEST['enableQuestionPrinting'])
      $config->SetValue(CONFIG_LICENSED_PRINT_QUESTIONS, 0);
    
    if (isset($_REQUEST['showUserThatAnswered']) && $_REQUEST['showUserThatAnswered'])
      $config->SetValue(CONFIG_SHOW_USER_THAT_ANSWERED, 1);
    elseif (!isset($_REQUEST['showUserThatAnswered']) || !$_REQUEST['showUserThatAnswered'])
      $config->SetValue(CONFIG_SHOW_USER_THAT_ANSWERED, 0);
    
    if (isset($_REQUEST['licenseHr']) && $_REQUEST['licenseHr'])
      $config->SetValue(CONFIG_LICENSED_HR_CONTENT, 1);
    else
      $config->SetValue(CONFIG_LICENSED_HR_CONTENT, 0);
    
    if (isset($_REQUEST['licenseAb1825']) && $_REQUEST['licenseAb1825'])
      $config->SetValue(CONFIG_LICENSED_AB1825_CONTENT, 1);
    else
      $config->SetValue(CONFIG_LICENSED_AB1825_CONTENT, 0);
    
    if (isset($_REQUEST['licenseEdu']) && $_REQUEST['licenseEdu'])
      $config->SetValue(CONFIG_LICENSED_EDU_CONTENT, 1);
    else
      $config->SetValue(CONFIG_LICENSED_EDU_CONTENT, 0);
  }

  // Check for theme changes
  if (isset($_REQUEST['uiTheme']) && !empty($_REQUEST['uiTheme']) &&
      $_REQUEST['uiTheme'] != $_SESSION['uiTheme']) {
    $config->SetValue(CONFIG_UI_THEME, $_REQUEST['uiTheme']);
    $_SESSION['uiTheme'] = $_REQUEST['uiTheme'];
  }
}

// Pull the list of available ui themes


$smarty->assign('displayChoices', $config->GetValue(CONFIG_DISPLAY_CHOICES));
$smarty->assign('useQuestionSetting', $config->GetValue(CONFIG_DISPLAY_CHOICES_USE_QUESTION_SETTING));
$smarty->assign('enableQuestionPrinting', $config->GetValue(CONFIG_LICENSED_PRINT_QUESTIONS));
$smarty->assign('showUserThatAnswered', $config->GetValue(CONFIG_SHOW_USER_THAT_ANSWERED));
$smarty->assign('trackAdminSummaryDefault', $config->GetValue(CONFIG_TRACK_ADMIN_SUMMARY_DEFAULT));
$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('superUser', $_SESSION['superUser']);
$smarty->assign('licenseHr', $config->GetValue(CONFIG_LICENSED_HR_CONTENT));
$smarty->assign('licenseAb1825', $config->GetValue(CONFIG_LICENSED_AB1825_CONTENT));
$smarty->assign('licenseEdu', $config->GetValue(CONFIG_LICENSED_EDU_CONTENT));
$smarty->assign('uiThemes', $uiThemes);
$smarty->assign('selectedTheme', $config->GetValue(CONFIG_UI_THEME));
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->assign('emailFormats', array('html', 'text'));
$smarty->assign('selectedEmailFormat', $config->GetValue(CONFIG_EMAIL_FORMAT));
$smarty->display('admin/cust_misc.tpl');

?>
