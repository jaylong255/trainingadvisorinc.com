<?php
/////////////////////////////////////////////////////////////////////////////
//	organization_list.php
//		Page used for administration of organizations stored on the current server
//


// Include libs which starts session and creates smarty objects
require_once ('../includes/common.php');
require_once ('../includes/User.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or an org admin
if (!((isset($_SESSION['superUser']) && $_SESSION['superUser']) ||
      (isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER))) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}


// Make sure if this is only a track admin this is the track they are super for
if ($_SESSION['role'] == ROLE_CLASS_CONTACT_SUPERVISOR) {
  $track = new Track($_SESSION['orgId'], $_SESSION['trackId']);
  if ($_SESSION['userId'] != $track->supervisorId) {
    error_log("Security error, contact attempted to access participant_list for track they "
	      ."do not own, redirecting to track denied for trackId(".$track->trackId);
    header("Location: /desktop/login/perm_denied.php?trackId=".$track->trackId);
    exit(0);
  }
}


// Specify this as the current tab
$_SESSION['currentTab'] = TAB_TRACK;
$_SESSION['currentSubTab'] = SUBTAB_TRACK_PARTICIPANTS;


if (isset($_REQUEST['searchBy'])) {
  $searchBy = $_REQUEST['searchBy'];
} else {
  $searchBy = USER_LIST_COLUMN_LAST_NAME;
}

if (isset($_REQUEST['searchFor'])) {
  $searchFor = $_REQUEST['searchFor'];
  $_SESSION['participantPageNumber'] = 1;
} else {
  $searchFor = '';
}

// create new employee list
$errMsg = '';
$rc = 0;
$firstRow = 0;
$lastRow = 0;
$numRows = 0;
$numPages = 0;
$showLink = 0;
$userIds = array();
$users = array();
$userList = new User($_SESSION['orgId']);
$linkArr = array();



if (isset($_REQUEST['participantId']) && $_REQUEST['participantId'] &&
    is_numeric($_REQUEST['participantId'])) {
  if (!isset($_REQUEST['action']) || ($_REQUEST['action'] != 'assign' && $_REQUEST['action'] != 'remove')) {
    error_log("participant_list.php: particpantId request param specified without coresponding action!");
  } else {
    //$userList->SetUserId($_REQUEST['participantId']);
    if ($_REQUEST['action'] == 'assign')
      $rc = $userList->AssignToTrack($_SESSION['trackId'], $_REQUEST['participantId']);
    if ($_REQUEST['action'] == 'remove')
      $rc = $userList->RemoveFromTrack($_SESSION['trackId'], $_REQUEST['participantId']);
    if ($rc != RC_OK)
      $errMsg = "Unable to Assign or Remove Participant: ".RcToText($rc);
  }
}


// Check for remove all and add all actions
if (isset($_REQUEST['removeAll']) && $_REQUEST['removeAll'] == 1) {

  error_log("Removing all users from track: ".$_SESSION['trackId']);
  $userList->AssignAllToTrack(0, $_SESSION['trackId']);

} elseif (isset($_REQUEST['addAll']) && $_REQUEST['addAll'] == 1) {

  error_log("Adding all users to track: ".$_SESSION['trackId']);
  $userList->AssignAllToTrack($_SESSION['trackId']);

}


// Setup links for the list header options
if ($_SESSION['participantShowAll']) {
  $showLink = new TplCaption('Show only users assigned to this class');
  $showLink->href = '/desktop/login/change_prefs.php';
  $showLink->params = 'returnUrl=/desktop/admin/participant_list.php&participantShowAll=0';
  //  $searchBy = array(USER_LIST_COLUMN_TRACK_ID, $searchBy);
  //  $searchFor = array('NULL', $searchFor);
  array_push($linkArr, $showLink);
  $showLink = new TplCaption('Add all to class');
  $showLink->href = '/desktop/admin/participant_list.php';
  $showLink->params = 'addAll=1';
  array_push($linkArr, $showLink);
} else {
  $showLink = new TplCaption('Show all users');
  $showLink->href = '/desktop/login/change_prefs.php';
  $showLink->params = 'returnUrl=/desktop/admin/participant_list.php&participantShowAll=1';
  //$searchBy = array(PARTICIPANT_COLUMN_TRACK_ID, $searchBy);
  //$searchFor = array($_SESSION['trackId'], $searchFor);
  array_push($linkArr, $showLink);
  $showLink = new TplCaption('Remove all from class');
  $showLink->href = '/desktop/admin/participant_list.php';
  $showLink->params = 'removeAll=1';
  $showLink->onClick = 'return WarnRemoveAllFromTrack()';
  array_push($linkArr, $showLink);
}


$userList->SetupTrackParticipants($_SESSION['trackId'], $_SESSION['participantShowAll']);

$rc = $userList->GetUserList($_SESSION['participantShowColumns'], $searchBy, $searchFor,
			     $_SESSION['participantSortColumn'], $_SESSION['participantSortDescending'],
			     $_SESSION['participantPageNumber'], $_SESSION['rowsPerPage'], $_SESSION['userId']);

if ($rc === FALSE) {
  echo "Failed to get employee list\n";
  exit(1);
}

if (DEBUG & DEBUG_FORM) {
  $smarty->assign('GetUserList', $rc);
}


if (!empty($rc)) {
  list($userIds, $users) = $rc;
  $userList->GetRowCounts($firstRow, $lastRow, $numRows);
  $numPages = $userList->GetPageCount($_SESSION['rowsPerPage']);
}

// This alone is enough information to identify a search column
// Strip Track column since we are only looking at participants of a given track
// The argument to GetColumnLabels here is an exclude list of columns we do not want
//$smarty->assign('columnLabels', $userList->GetColumnLabels(array(USER_LIST_COLUMN_TRACK_ID => '')));
$smarty->assign('columnLabels', $userList->GetColumnLabels());
$smarty->assign('showColumns', $_SESSION['participantShowColumns']);

// Keep the selected search by field
$smarty->assign('searchBy', $searchBy);

// Set the one to be selected
$smarty->assign('sortColumn', $_SESSION['participantSortColumn']);
$smarty->assign('sortDescending', $_SESSION['participantSortDescending']);


$smarty->assign('firstRow', $firstRow);
$smarty->assign('lastRow', $lastRow);
$smarty->assign('numRows', $numRows);
$smarty->assign('pageNumber', $_SESSION['participantPageNumber']);
$smarty->assign('numPages', $numPages);
$smarty->assign('recordList', $users);
$smarty->assign('recordIds', $userIds);

$smarty->assign('errMsg', $errMsg);

$userList->Close();



$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('toggleParamName', 'toggleParticipantColumn');
$smarty->assign('sortParamName', 'participantSortColumn');
$smarty->assign('deleteLink', 'participant_delete.php');
$smarty->assign('deleteLinkLabel', 'Remove');
$smarty->assign('idParamName', 'participantId');
$smarty->assign('pageNumberParamName', 'participantPageNumber');
$smarty->assign('orderParamName', 'participantSortDescending');
$smarty->assign('showSelectedColumn', FALSE);
$smarty->assign('rowsPerPage', $_SESSION['rowsPerPage']);
$smarty->assign('localHeader', 'track_header');
$smarty->assign('topLinkList', $linkArr);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/generic_list.tpl');


?>
