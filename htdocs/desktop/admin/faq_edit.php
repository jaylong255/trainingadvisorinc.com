<?php

// Libs
require_once("../includes/common.php");
require_once("../includes/Faq.php");


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or an org admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}


// Make sure if this is only a track admin this is the track they are super for
if ($_SESSION['role'] == ROLE_CLASS_CONTACT_SUPERVISOR) {
  $track = new Track($_SESSION['orgId'], $_SESSION['trackId']);
  if ($_SESSION['userId'] != $track->supervisorId) {
    error_log("Redirecting to track denied for trackId(".$track->trackId);
    header("Location: /desktop/login/perm_denied.php?trackId=".$track->trackId);
    exit;
  }
}


$errMsg = '';


$faq = new Faq($_SESSION['orgId'], $_SESSION['trackId']);


if (isset($_REQUEST['faqId'])) {

  if ($_REQUEST['faqId'] != 'To Be Assigned') {

    // Validate existing user id to make sure form content was not altered
    if (!is_numeric($_REQUEST['faqId'])) {
      header("Location: /desktop/login/perm_denied.php");
      exit;
    }

    $faq->SetFaqId($_REQUEST['faqId']);
  }

  // Form was submitted so we attempt to update user
  if (isset($_REQUEST['question'])) {
    
    $faq->question = $_REQUEST['question'];
    $faq->answer = $_REQUEST['answer'];
    
    if ($faq->UpdateInfo() != RC_OK) {
      error_log("Failed to update FAQ with faqId=".$faq->faqId);
      $errMsg = $faq->GetError();
    }
  } else {

    // Load user existing information for first time form edit
    $faq->SetFaqId($_REQUEST['faqId']);

  }    
    
}


$smarty->assign('errMsg', $errMsg);
$smarty->assign('faq', $faq);
$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/faq_edit.tpl');


?>
