__FIRST_NAME__ __LAST_NAME__,

Please note ALL questions are written around the NEW code requirements (NFPA 101, 2012: NFPA 110, 2010, etc.), as well as the 2017 TJC Standards. 

Please answer the latest Regulatory Questions waiting for you at http://www.trainingadvisorinc.com/desktop .  We appreciate your responses within one week.

Unanswered questions:  
In your assignments list there are __NUMBER_UNANSWERED__ questions awaiting your response.  

Take action now:
Answering these weekly questions is mandatory and vital to the success of our organization. You are at least 11 questions behind.  We are very concerned that you are this far behind.  If you are unable to catch up in the next few days, please contact me and your manager and explain the situation. 

If you have specific questions or concerns, email TrainingAdvisor@medxcelfacilities.com or call Sunny Khammoung at 317-275-9534.

