Hi __FIRST_NAME__ __LAST_NAME__,

This is John Smith with Training Advisor. We met briefly at the TASPA show - I was at the table with the magic trick.

As a reminder, Training Advisor is an online HR tool with ready-to-use courses on Sexual Harassment, FMLA, ADA, Discrimination, Diversity, Workforce Management, Employee Performance, Unemployment laws and related topics. 

You also have the capability to quickly add your own content including text, video, flash or graphics and use Training Advisor to distribute all your training material.

As discussed, you will receive five sample HR scenario questions. 

You now have your first sample question waiting for you.  Click on the provided link and enter your email address as the user id and the word sample for your password.   

Example: My email is john@trainingadvisorinc.com - To login I would enter: 

Username: john@trainingadvisorinc.com (all lowercase)

Password: sample (all lowercase)

In order to access the training click on this link http://www.trainingadvisorinc.com/desktop

After you have completed the questions I will contact you.

Feel free to contact me with any questions that you may have.  

Good luck with your answers and I look forward to speaking with you.


John H. Smith
Training Advisor 
john@trainingadvisorinc.com
801-277-1177







 


