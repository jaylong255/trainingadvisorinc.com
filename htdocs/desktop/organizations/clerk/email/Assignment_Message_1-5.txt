__FIRST_NAME__ __LAST_NAME__,

Please review the latest Clerk or Court training question waiting for you at __BASE_URL____LOGIN_PATH____QUERY_PARAMS__ .  We appreciate you completing your questions in a timely manner.

Remember, your USER ID is your email address.  Your temporary password is "clerk".  For example, for John Smith his email address is jsmith@clerk.org which is also his login information.  Example:

USER ID = jsmith@clerk.org
PASSWORD = clerk

Please change your password when you login to the system the first time.

As you proceed through the program, here's a tip:  Be sure to click on the "Next" buttons all the way through the program, then log out by clicking on either the "Understood" or "Contact" button (do not use the X button in the top right-hand corner of the screen).

Participation in the "Training Advisor" program will help you, as a supervisor, successfully respond to a variety of Human Resources, EEO and legal challenges.  Please don't hesitate to call HR directly if you have specific concerns or questions.  

Sincerely,

Kate Shockey, HR Manager






 


