#!/usr/bin/php
<?php

echo "PATH: ".getcwd();
require_once '../includes/Classes/PHPExcel/IOFactory.php';

if ($argc != 2) {
  echo "\tUsage: $argv[0] <filename>\n";
  exit(1);
}

echo "PATH: ".getcwd();

//$eStatusVals = ("", "Active", "Active", "Active", "Active", "On Leave",
//		"Active", "Terminated", "Terminated", "Active");

#$inputFileType = 'Excel5';
#$inputFileName = 'HR May 14 2015.xls';
$inputFileName = $argv[1];

#$objReader = PHPExcel_IOFactory::createReader($inputFileType);
#$objPHPExcelReader = $objReader->load($inputFileName);
$objPHPExcelReader = PHPExcel_IOFactory::load($inputFileName);

$loadedSheetNames = $objPHPExcelReader->getSheetNames();

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcelReader, 'CSV');

foreach($loadedSheetNames as $sheetIndex => $loadedSheetName) {
    $objWriter->setSheetIndex($sheetIndex);
    $path_parts = pathinfo($inputFileName);
    $objWriter->save($path_parts['dirname'].'/'.$path_parts['filename'].'.csv');
}




?>
