#!/bin/bash

mysql -u vector -p -NBe "select Database_Name from Organization" HR_Tools_Authentication|xargs mysqldump -u vector -p --add-drop-database --add-drop-table --databases HR_Tools_Authentication > dbs.sql

echo "Databases dumped to dbs.sql"
