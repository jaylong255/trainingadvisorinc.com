#!/usr/bin/php -q
<?php

chdir('../organizations');
$dirs = explode("\n", trim(`ls -1`));
//print_r($dirs, FALSE);

foreach ($dirs as $dir) {
  $dir = str_replace(' ', '\ ', $dir);
  echo "DIR::::::::::::::$dir\n";
  if (!file_exists("$dir/email"))
    echo "*** FAIL: directory $dir/email does not exist\n";
  if (!chdir("$dir/email")) {
    echo "*** FAIL: chdir to $dir/email\n";
    continue;
  }
  $custEmailFileNames = explode("\n", trim(`ls -1`));
  //print_r($custEmailFileNames, FALSE);
  //echo "\tEMAILFL_COUNT=".count($custEmailFileNames)."\n";
  if (count($custEmailFileNames)) {
    foreach ($custEmailFileNames as $custEmailFileName) {
      // Do our thing to the files
      if (empty($custEmailFileName) || strpos($custEmailFileName, "Subject") !== FALSE)
	continue;
      $custEmailFileBody = file_get_contents($custEmailFileName);
      //$custEmailFileBody = str_replace("http://www.trainingadvisorinc.com/desktop", "__URL__", $custEmailFileBody);
      $custEmailFileBody = str_replace('hrtoolsonline.com', 'trainingadvisorinc.com', $custEmailFileBody);
      $custEmailFileBody = str_replace('sabient', 'trainingadvisorinc', $custEmailFileBody);
      $custEmailFileBody = str_replace('http://www.trainingadvisorinc.com/desktop', '__URL__', $custEmailFileBody);
      $custEmailFileBody = str_replace('"__URL__ ."', '"__URL__"', $custEmailFileBody);
      $custEmailFileBody = str_replace('"__URL__ . "', '"__URL__"', $custEmailFileBody);
      $custEmailFileBody = str_replace('"__URL__ .   "', '"__URL__"', $custEmailFileBody);
      $custEmailFileBody = str_replace('"__URL__. "', '"__URL__"', $custEmailFileBody);
      $custEmailFileBody = str_replace('"__URL__ "', '"__URL__"', $custEmailFileBody);
      $custEmailFileBody = str_replace('"__URL__   "', '"__URL__"', $custEmailFileBody);
      $custEmailFileBody = str_replace('"__URL__  "', '"__URL__"', $custEmailFileBody);
      $custEmailFileBody = str_replace('>__URL__ .<', '>__URL__<', $custEmailFileBody);
      $custEmailFileBody = str_replace('>__URL__ . <', '>__URL__<', $custEmailFileBody);
      $custEmailFileBody = str_replace('>__URL__ .   <', '>__URL__<', $custEmailFileBody);
      $custEmailFileBody = str_replace('> __URL__<', '>__URL__<', $custEmailFileBody);
      $custEmailFileBody = str_replace('>__URL__ <', '>__URL__<', $custEmailFileBody);
      $custEmailFileBody = str_replace('>__URL__. <', '>__URL__<', $custEmailFileBody);
      $custEmailFileBody = str_replace('>__URL__   <', '>__URL__<', $custEmailFileBody);
      $custEmailFileBody = str_replace('>__URL__  <', '>__URL__<', $custEmailFileBody);
      $custEmailFileBody = str_replace('</a>.', '</a> .', $custEmailFileBody);
      $custEmailFileBody = str_replace('__URL__ to log in. ', '__URL__', $custEmailFileBody);
      $custEmailFileBody = str_replace('__URL__', '__BASE_URL____LOGIN_PATH____QUERY_PARAMS__', $custEmailFileBody);
      $custEmailFileBody = str_replace('/login/phi_login.php', '', $custEmailFileBody);
      $custEmailFileBody = str_replace('__/', '__', $custEmailFileBody);
      $custEmailFileBody = str_replace('__ "', '__"', $custEmailFileBody);
      $custEmailFileBody = str_replace('__ <', '__<', $custEmailFileBody);

      $fh = fopen($custEmailFileName, 'w');
      fwrite($fh, $custEmailFileBody);
      fclose($fh);

      //$custEmailFileBody = str_replace('"__URL__ .  We appreciate you completing your questions in a timely manner."', '"__URL__"', $custEmailFileBody);
      //$custEmailFileBody = str_replace('>__URL__ .  We appreciate you completing your questions in a timely manner.</a>', '>__URL__</a>. We appreciate you completing your questions in a timely manner.', $custEmailFileBody);
      //$custEmailFileBody = str_replace('__URL__ .   ', '__URL__', $custEmailFileBody);
      //$custEmailFileBody = str_replace('__URL__ . ', '__URL__', $custEmailFileBody);
      //$custEmailFileBody = str_replace(' __URL__', '__URL__', $custEmailFileBody);

      // Advisor website <a href="__URL__ to log in. ">__URL__ to log in. </a><BR>

      //$custEmailFileBody = preg_replace('/\"__URL__\s+\.\s*\"/', '"__URL__"', $custEmailFileBody);
      //$custEmailFileBody = str_replace('>__URL__ <', '>__URL__<', $custEmailFileBody);
      //echo "\n\n=========================================================\n";
      //echo "TEXT FILENAME: $custEmailFileName\n\n";
      //echo "$custEmailFileBody\n";
      //echo "\n\n\n";
    }
  }
  chdir("../..");
}

?>