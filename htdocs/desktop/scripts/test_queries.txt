-- Tests query used to pull participants on a track list
SELECT p.User_ID, p.Last_Question_Number, u.Full_Name, e.Name FROM Participants AS p LEFT JOIN User AS u USING (User_ID) LEFT JOIN Employment_Status AS e ON u.Employment_Status_ID=e.Employment_Status_ID WHERE p.Track_ID=35

-- Tests joint use of User_Assignment and Assignments Tables
SELECT (COUNT(ua.User_ID)+COUNT(a.User_ID)) as count FROM User_Assignment ua, Assignments a WHERE (ua.User_ID=9828 OR a.User_ID=9828) AND (ua.Completion_Date IS NULL OR DATE(a.Completion_Date)='0000-00-00') AND (ua.Track_ID IS NOT NULL OR a.Track_ID=0)
-- OR The following variant which should be identical but perhaps a little easier to follow
SELECT (COUNT(ua.User_ID)+COUNT(a.User_ID)) as count FROM User_Assignment ua, Assignments a WHERE (ua.User_ID=9828 AND ua.Completion_Date IS NULL AND ua.Track_ID IS NOT NULL) OR (a.User_ID=9828 AND DATE(a.Completion_Date)='0000-00-00' AND a.Track_ID=0)

-- Query count of unanswered questions
SELECT COUNT(User_ID) as count FROM Assignments WHERE User_ID='$User_ID' AND DATE(Completion_Date)='0000-00-00' AND Track_ID>0


