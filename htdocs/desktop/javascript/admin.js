var imgDesk = new Image();
var imgDeskb = new Image();
var bSave   = false;
var strSequenceURL = ''; // used in question_list.tpl



function GetSize() {

    cWidth=document.body.clientWidth;
    cHeight=document.body.clientHeight;
    
    if(window.innerWidth) {
	cWidth=window.innerWidth;
	cHeight=window.innerHeight;
    }
    
    var hFormDiv = document.getElementById("FormDiv");
    var hManillaFolder = document.getElementById("ManillaFolder");
    //var oToDeskBtn = document.getElementById("ToDesktop");
    
    hFormDiv.style.height = cHeight-187;
    hFormDiv.style.width = cWidth-47;
    
    hManillaFolder.style.height = cHeight-180;
    hManillaFolder.style.width = cWidth-40;
    
    //if (cWidth >= 800)	{
    //	oToDeskBtn.style.left = cWidth-470;
    //} else {
    //	oToDeskBtn.style.left = 330;
    //}
    
} // end function GetSize()



// remove an assignment from the class
function svcToggleClassAssignment(trackId, questionId, rowNum, actionNum, deleteRow) {

    if (!svc) {
	alert("Fatal Error: Could not remove assignment, creation of service instance failed.\nPlease contact support.");
	return;
    }

    if (deleteRow) {
	toggleTableRow(rowNum);
    } else {
	updateActionLabelStatus(rowNum, actionNum);
    }
    svc.toggleClassAssignment({params:[trackId, questionId],
			onSuccess: function(rc) { },
			onException: function(errObj) {
			if (deleteRow)
			  toggleTableRow(rowNum);
			else
			  updateActionLabelStatus(rowNum, actionNum);
			alert("Unable to add or remove user from class["+trackId+"]: " + errObj);
			}
		});
    return false;
}



// add and remove participants from the specified class
function svcToggleClassParticipant(userId, trackId, rowNum, deleteRow) {

    if (!svc) {
	alert("Fatal Error: Could not delete user, svc is not defined.\nPlease contact support.");
	return;
    }

    if (deleteRow) {
	toggleTableRow(rowNum);
    } else {
	updateActionLabelStatus(rowNum, 0);
    }
    svc.toggleClassParticipant({params:[userId, trackId],
			onSuccess: function(rc) { },
			onException: function(errObj) {
			if (deleteRow)
			  toggleTableRow(rowNum);
			else
			  updateActionLabelStatus(rowNum, 0);
			alert("Unable to add or remove user from class["+trackId+"]: " + errObj);
			}
		});
    return false;
}



// remove an assignment from the class
// TODO: Implement this function since code was copied
function svcUpdateClassSequence(trackId, questionId, srcRowNum, destRowNum, actionNum) {

    if (!svc) {
	alert("Fatal Error: Could not remove assignment, creation of service instance failed.\nPlease contact support.");
	return;
    }

//    toggleTableRow(rowNum);
//    updateActionLabelStatus(rowNum, actionNum);

    svc.updateClassSequence({params:[trackId, questionId, destRowNum],
			onSuccess: function(rc) { },
			onException: function(errObj) {
			reorderTableRows(destRowNum, srcRowNum);
			//updateActionLabelStatus(rowNum, actionNum);
			alert("Unable to change sequence number of question ["+questionId+"] in class["+trackId+"]: " + errObj);
			}
		});
    return false;
}



// TODO: Implement this function since code was copied
function svcToggleClassGroup(trackId, questionId, rowNum, actionNum, deleteRow) {

    if (!svc) {
	alert("Fatal Error: Could not remove assignment, creation of service instance failed.\nPlease contact support.");
	return;
    }

    if (deleteRow) {
	toggleTableRow(rowNum);
    } else {
	updateActionLabelStatus(rowNum, actionNum);
    }
    svc.toggleClassAssignment({params:[trackId, questionId],
			onSuccess: function(rc) { },
			onException: function(errObj) {
			reorderTableRows(destRowNum, srcRowNum);
			//updateActionLabelStatus(rowNum, actionNum);
			alert("Unable to change sequence number of question ["+questionId+"] in class["+trackId+"]: " + errObj);
			}
		});
    return false;
}



function svcDeleteUser(userId, name, rowNum) {
    if (!svc) {
	alert("Fatal Error: Could not delete user, svc is not defined.\nPlease contact support.");
	return;
    }

    var res = confirm("Are you sure you would like to permanently delete all data\n" +
	"associated with " + name + " [" + userId + "]?");
    if (!res)
	return;

    // Go ahead and hide the row assuming the operation will be successful
    toggleTableRow(rowNum);

    // Now actually make the service call to remove this user.
    // If it fails then we will warn the user and put the row back.
    svc.deleteUser({params:[userId],
			onSuccess: function(rc) { },
			onException:function(errObj) {
			toggleTableRow(rowNum, 0);
			alert("Unable to delete user["+userId+"]: " + errObj);
			}
		});
    return false;
}



function toggleTableRow(rowNum) {

    // There is a header row that must be accounted for
    rowNum++;

    var i = 0;
    var std = 'trDefault', alt = 'trDefaultAlt', tmp = '';
    var tbl = document.getElementById('dataTable');
    var tbl_row_cnt = tbl.getElementsByTagName('tr').length;
    //var row_html = tbl.rows[rowNum].innerHTML;
    //tbl.deleteRow(rowNum);
    var first_row_elem = document.getElementById('first_row');
    var last_row_elem = document.getElementById('last_row');
    var num_rows_elem = document.getElementById('num_rows');
    var first_row = first_row_elem.innerHTML;
    var last_row = last_row_elem.innerHTML;
    var num_rows = num_rows_elem.innerHTML;

    // Make the row invisible while we try to delete the record.
    // This is how I have decided to keep row numbers consistent
    // and this way row numbers can be specified from the server
    if (tbl.rows[rowNum].style.display == 'none') // show the row
	tbl.rows[rowNum].style.display = '';
    else
	tbl.rows[rowNum].style.display = 'none';


    for (i = 1; i < tbl_row_cnt; i++) {
	if (tbl.rows[i].style.display == 'none') {
	    tmp = std;
	    std = alt;
	    alt = tmp;
	}
	if (i % 2 == 0)
	    tbl.rows[i].className = alt;
	else
	    tbl.rows[i].className = std;
    }

    // First row was deleted so we have to increment the first row number
    if (rowNum == 1 && first_row > 1) {
	first_row++;
	first_row_elem.innerHTML = first_row;
    } else {
	last_row--;
	last_row_elem.innerHTML = last_row;
    }
    num_rows--;
    num_rows_elem.innerHTML = num_rows;
}



function reorderTableRows(srcRow, destRow) {
    // There is a header row that must be accounted for
    rowNum++;

    var i = 0;
    var std = 'trDefault', alt = 'trDefaultAlt', tmp = '';
    var tbl = document.getElementById('dataTable');
    var tbl_row_cnt = tbl.getElementsByTagName('tr').length;
    //var row_html = tbl.rows[rowNum].innerHTML;
    //tbl.deleteRow(rowNum);
    var first_row_elem = document.getElementById('first_row');
    var last_row_elem = document.getElementById('last_row');
    var num_rows_elem = document.getElementById('num_rows');
    var first_row = first_row_elem.innerHTML;
    var last_row = last_row_elem.innerHTML;
    var num_rows = num_rows_elem.innerHTML;

    // Make the row invisible while we try to delete the record.
    // This is how I have decided to keep row numbers consistent
    // and this way row numbers can be specified from the server
    if (tbl.rows[rowNum].style.display == 'none') // show the row
	tbl.rows[rowNum].style.display = '';
    else
	tbl.rows[rowNum].style.display = 'none';


    for (i = 1; i < tbl_row_cnt; i++) {
	if (tbl.rows[i].style.display == 'none') {
	    tmp = std;
	    std = alt;
	    alt = tmp;
	}
	if (i % 2 == 0)
	    tbl.rows[i].className = alt;
	else
	    tbl.rows[i].className = std;
    }

    // First row was deleted so we have to increment the first row number
    if (rowNum == 1 && first_row > 1) {
	first_row++;
	first_row_elem.innerHTML = first_row;
    } else {
	last_row--;
	last_row_elem.innerHTML = last_row;
    }
    num_rows--;
    num_rows_elem.innerHTML = num_rows;
}



function updateActionLabelStatus(rowNum, actionNum) {
    // Update the participant status with the appropriate links
    // There is a naming convention to use ids and names using
    // the action number appended to the word 'action'
    var tbl = document.getElementById('dataTable');
    var action_elem = document.getElementById('action'+actionNum+'-'+rowNum);
    
    if (action_elem.innerHTML.indexOf('emove') > 0) {
	action_elem.innerHTML = 'Add to Class';
    } else {
	action_elem.innerHTML = 'Remove';
    }
}



function SetupImgDesk(theme) {
  imgDesk.src = '../themes/'+theme+'/admin/to_desktop_up.png';
  imgDeskb.src = '../themes/'+theme+'/admin/to_desktop_down.png';
}



// The following are used question_list.tpl
function SelectCategory(newPage) {

    list = document.getElementById("category_list");
    i = list.selectedIndex;
    if (i > 0) {
//	key = document.getElementById("category_key");
	newPage += "&get_category=" + list[i].value;
    }
    window.location.href = newPage;
}


function SequencePopup(strURL, nTrack_ID, nQuestionNumber, nCurrentSequence, strLink, bSequence) {
  strSequenceURL = strURL+'?questionId='+nQuestionNumber;
//  alert(strSequenceURL);
  if (bSequence)
    objInput = document.getElementById('sequence_form');
  else
    objInput = document.getElementById('group_form');

  if (!objInput)
    return;

  if (nCurrentSequence == 0)
    objInput.number.value="New";
  else
    objInput.number.value = nCurrentSequence;

  if (bSequence)
    ShowPopupMenu('SequenceMenu', strLink, true);
  else {
    ShowPopupMenu('GroupMenu', strLink, true);
  }

  objInput.number.focus();
  objInput.number.select();
}


function DoChangeSequence(bReturn) {
    objInput = document.getElementById('sequence_form');
    if (!objInput) {
	if (bReturn) return false;
	return;
    }
    newvalue = objInput.number.value;
    strSequenceURL += '&sequenceId='+newvalue;
    // TODO: Do a web service call here to change the sequence number then update table.

    window.location.href = strSequenceURL;
    if (bReturn)
	return false;
}


function DoChangeGroup(bReturn) {
    objInput = document.getElementById('group_form');
    if (!objInput) {
	if (bReturn) return false;
	return;
    }
    newvalue = objInput.number.value;
    strSequenceURL += '&groupId='+newvalue;
    window.location.href = strSequenceURL;
    if (bReturn)
	return false;
}
// end functions for question_list.tpl


// added default second parameter limiting the maximum value
function ScrollSequence(bScrollUp, bSequence, minVal, maxVal) {

  if (bSequence) {
    objInput = document.getElementById('sequence_form');
  } else {
    objInput = document.getElementById('group_form');
  }
  
  if (!objInput)
    return false;

  value = objInput.number.value;
    // added by vector (vector@itpsg.com) should check for NaN on return value of parseInt
  n = parseInt(value, 10);
  if (bScrollUp)
    if (isNaN(n)) {
      if (value == "New") {
        n = 1;
      } else {
        objInput.number.value = "New";
        return;
	    }
    } else
      n = n + 1;
    else
      if (!isNaN(n))
        if (isNaN(minVal)) {
          n = (n > 1) ? n - 1:NaN;
        } else {
          n = (n > 1) ? n - 1:1;
        }
    //value = n.toString()

  if (!isNaN(n)) {
    if (n <= maxVal) {
	    objInput.number.value = n.toString();
    }
  } else {
	//alert("VALUE: " + n);
    if (value == "New" && !bScrollUp) {
      objInput.number.value = "Unassign";
    } else
      if ((value == "Unassign" && bScrollUp) ||
          (value != "Unassign" && !bScrollUp))
        objInput.number.value = "New";
  }
  objInput.number.focus();
  objInput.number.select();
}


// This function used only in user_edit.tpl
function ValidateNumber(obj) {

  i = obj.value.search(/[^0-9^.]/);
  if (i > -1)
    return false;
  else
    return true;
    
}  // end function ValidateNumber(obj)


// This function used only in user_edit.tpl
function ValidateHR() {

    FormObj = MM_findObj("role");
    // roles > 0 man admin or contact and require phone number
    if(FormObj.value > 0) {
	FormObj = MM_findObj("phone");
	if(FormObj.value.length > 4) {
	    return true;
	}
	return false;
    } else
	return true;
    
} // end function ValidateHR()


// This function used only in user_edit.tpl
function CheckName(strName) {

    if(strName.length > 0) {
	if(strName.indexOf('"')<0) return true;
    }

    return false;

} // end function CheckName()


// This function used only in user_edit.tpl
function NewUser(getReturnURL, returnURL, bAdd) {

    CheckSaveUserEdit("user_edit.php?get_user=000000&get_return_name=User List&get_return_URL=" + getReturnURL, returnURL, bAdd);

} // end function NewUser()


function HideQuestionTabs() {

	document.getElementById("CurrentTab1").style.visibility="hidden";
	document.getElementById("CurrentTab2").style.visibility="hidden";
	document.getElementById("ArchiveTab1").style.visibility="hidden";
	document.getElementById("ArchiveTab2").style.visibility="hidden";
	document.getElementById("SummaryTab1").style.visibility="hidden";
	document.getElementById("SummaryTab2").style.visibility="hidden";
	document.getElementById("AnalysisTab1").style.visibility="hidden";
	document.getElementById("AnalysisTab2").style.visibility="hidden";
	document.getElementById("CategoriesSelected").style.visibility="hidden";
	document.getElementById("CategoriesUnselected").style.visibility="hidden";

} // end function HideQuestionTabs()


function HideCustomizeTabs() {

	document.getElementById("FileMan1").style.visibility="hidden";
	document.getElementById("FileMan2").style.visibility="hidden";
	if (document.getElementById('CustMsg1')) {
		document.getElementById("CustMsg1").style.visibility="hidden";
		document.getElementById("CustMsg2").style.visibility="hidden";
	}

} // end function HideQuestionTabs()


function HideTrackTabs() {

    for(i = 1; i <= 6; i++) {
	Tab = document.getElementById("Track"+i);
	Tabg = document.getElementById("Track"+i+"g");
	Tab.style.visibility="hidden";
	Tabg.style.visibility="hidden";
    }

    document.getElementById("FormTab").style.visibility="visible";

} // end function HideTrackTabs


function AddTrack() {

    document.getElementById("FormTab").style.visibility="hidden";

    for(i = 1; i <= 6; i++) {
	Tab = document.getElementById("Track"+i);
	Tabg = document.getElementById("Track"+i+"g");
	if(i == 1)
	    Tab.style.visibility="visible";
	else {
	    Tab.style.visibility="hidden";
	    Tabg.style.visibility="hidden";
	}
    }

} // end function AddTrack()


// This function is used in track editing
function AddRecordTrackEdit(newPage) {

    if(FormValidateTrackEdit()) {
	FormObj = MM_findObj("AdminForm");
	//FormObj.action="track_edit.php?FormAction=Add&navigate="+newPage;
	FormObj.submit();
    }

} // end function AddRecord


// This function is used in track editing
function UpdateRecordTrackEdit() {

    if(FormValidateTrackEdit()) {
	FormObj = MM_findObj("AdminForm");
	//FormObj.action="track_edit.php?FormAction=Update&navigate="+newPage;
	FormObj.submit();
    }

} // end function UpdateRecord()


function AskAssignTrack(strTrackName) {
  if (confirm("This user is currently assigned to class \"" + strTrackName +
	      ".\" Are you sure you want to Reassign this user to this class?"))
     return true;

  return false;
}


function AddRecordQuestionEdit (newPage) {

    if(FormValidateQuestionEdit()) {
	FormObj = document.getElementById("AdminForm");
	FormObj.action='question_edit.php?FormAction=Add&navigate='+newPage;
	FormObj.submit();
    }

} // end function AddRecordQuestionEdit()


function FormValidateQuestionEdit() {

    FormObj = MM_findObj("languageId");
    if(FormObj.selectedIndex >= 0) {

	FormObj = MM_findObj("domainId");
	if(FormObj.selectedIndex >= 0) {

	    FormObj = MM_findObj("frameTypeId");
	    if(FormObj.selectedIndex >= 0) {
		return true;
	    } else {
		alert("You must provide a FrameType for this question.");
		FormObj.select();
	    }
	} else {
	    alert("You must provide a Domain for this question.");
	    FormObj.select();
	}
    } else {
	alert("You must provide a Language for this question.");
	FormObj.select();
    } // end if formObj selected

    return false;

} // end function FormValidateQuestionEdit()


function UpdateRecordQuestionEdit (newPage) {

    if(FormValidateQuestionEdit()) {
	FormObj = document.getElementById("AdminForm");
	//alert("bSupport: " + bSupport + ", bSave: " + bSave + " newPage: " + newPage);
	if (bSupport && !bSave)
	    FormObj.action="question_edit.php?FormAction=Update&bSupport=true&navigate="+newPage;
	else
	    FormObj.action="question_edit.php?FormAction=Update&navigate="+newPage;
	FormObj.submit();
    }

} // end function UpdateRecordQuestionEdit()


function SetTrackSubTab(nTab) {

    // This would be an list list so don't even bother showing anything
    if (nTab == 0) {
	HideTrackTabs();
	return true;
    }

    HideQuestionTabs();
    HideOrgTabs();
    document.getElementById("FormTab").style.visibility="hidden";
    for(i = 1; i <= 6; i++) {
	Tab = document.getElementById("Track"+i);
	Tabg = document.getElementById("Track"+i+"g");
	if(i==nTab) {
	    Tab.style.visibility="visible";
	    Tabg.style.visibility="hidden";
	} else {
	    Tab.style.visibility="hidden";
	    Tabg.style.visibility="visible";
	}
    }
    
} // end function SetTrackSubTab()


// used in user_edit.tpl
function FormValidateUserEdit() {

  var err = '';

  FormObj = document.getElementById("firstName");
  if(!CheckName(FormObj.value)) {
    err = err + "You must provide the First Name of the employee. Quotes are not allowed in the First Name.\r\n";
    FormObj.select();
    FormObj.focus();
  }

  FormObj = document.getElementById("lastName");
  if(!CheckName(FormObj.value)) {
    err = err + "You must provide the Last Name of the employee. Quotes are not allowed in the Last Name.\r\n";
    FormObj.select();
    FormObj.focus();
  }

  //FormObj = MM_findObj("ssn");
  //if(FormObj.value.length > 5) {
  //if(ValidateNumber(FormObj)) {
    //err = err + "Numeric Field: You must provide the last 6 numbers of the employee's Social Security Number.\r\n";
    //FormObj.select();
    //FormObj.focus();
  //}
  //}

  FormObj = MM_findObj("language");
  if(FormObj.selectedIndex <= 0) {
    err = err + "You must provide a Language identifier for the employee.\r\n";
  }

  if(!ValidateHR()) {
    err = err + "You must provide a phone number for all administrators and contacts.\r\n";
    FormObj.select();
    FormObj.focus();
  }

  if (err.length > 0) {
    alert(err);
    return false;
  }

  return true;

} // end function FormValidateUserEdit()


// This function used only for user_edit.tpl
function CheckSaveUserEdit(destination) {

  if(!bSave)
    window.location.href = destination;
  else {
    if(confirm("Would you like to save your changes to this form?")) {
	// Submit the form is all that is necessary since distinguishing between
	// add and update is now the responsibility of the php code
	FormObj = MM_findObj("AdminForm");
	FormObj.submit();
//       if(bAdd)
//         AddRecordUserEdit(encodeLocation);
//       else
//         UpdateRecordUserEdit(encodeLocation);
    } else {
	window.location.href = destination;
    }
  }
    
} // end function CheckSaveUserEdit


function AddRecordUserEdit(newPage) {

  if(FormValidateUserEdit()) {
	//alert("formValidate passed");
    FormObj = MM_findObj("AdminForm");
    //FormObj.action="user_edit.php?FormAction=Add&navigate="+newPage;
    FormObj.submit();
  }

} // end function AddRecordUserEdit()


function UpdateRecordUserEdit(newPage) {

  FormObj = MM_findObj("password");
    
  if(FormObj.value.length > 0)
    if(!confirm("Are you sure you want to set this user's password?"))
      FormObj.value='';
    
  if(FormValidateUserEdit()) {
    FormObj = MM_findObj("AdminForm");
    FormObj.submit();
  }

} // end function UpdateRecordUserEdit()



function FormValidateTrackEdit() {

    strName = document.getElementById("trackName").value;
    if(strName.length>0) {

	if(strName.indexOf('"')>0) {
	    alert("Quotes are not allowed in the Class Name.");
	    return false;
	}
	return true;
    } else {
	alert("You must provide a Name for the class.");
	FormObj.focus();
    }
    
    return false;

} // end function FormValidateTrackEdit()


function ValidateOrgForm(currentTab) {

    var errMsg = '';
    
    switch (currentTab) {
    case 1: // organization info from org_info
      if (document.getElementById("orgName").value.length <= 0)
	errMsg += "You must specify an Organizational Name.\r\n";
      if (document.getElementById("orgDirectory").value.length <= 0)
	errMsg += "You must specify an Organizational Directory.\r\n";
      break;
    case 2: // organization contact from org_contact
//	alert("Tab 2 handling has not yet been implemented in ValidateOrgForm of admin.js\n");
	// Validation for this case is handled by the php forms processing
      break;
    case 3: //
	alert("Tab 3 handling has not yet been implemented in ValidateOrgForm of admin.js\n");
      break;
    case 4: //
	alert("Tab 4 handling has not yet been implemented in ValidateOrgForm of admin.js\n");
      break;
    case 5: //
	alert("Tab 5 handling has not yet been implemented in ValidateOrgForm of admin.js\n");
      break;
    default:
      alert("An unknown tab (" + currentTab + " was provided to ValidateOrgForm\n");
      break;
    }

//    if(document.getElementById("F_Use_News").checked) {
//		
//	if(document.getElementById("F_News").value.length<=0) {
//	    alert("You must specify an html file in the News URL location, or uncheck the Organization News checkbox.");
//	    return false;
//	}
//    }

    if (errMsg != '') {
	alert(errMsg);
	return false;
    }

    document.OrgForm.submit();

    return true;

} // end function ValidateOrgForm()


function AddOrg() {

    document.getElementById("FormTab").style.visibility="hidden";
    for(i=1;i<=5;i++) {

	Tab = document.getElementById("Org"+i);
	Tabg = document.getElementById("Org"+i+"g");
	if(i==1) Tab.style.visibility="visible";
	else {
	    Tab.style.visibility="hidden";
	    Tabg.style.visibility="hidden";
	}
    }
}


function HideOrgTabs() {

    for(i = 1; i <= 5; i++) {
	Tab = document.getElementById("Org"+i);
	Tabg = document.getElementById("Org"+i+"g");
	if (Tab) {
	    Tab.style.visibility="hidden";
	    Tabg.style.visibility="hidden";
	}
    }
    document.getElementById("FormTab").style.visibility="visible";
}


function SetOrgSubTab(nTab) {

//    HideQuestionTabs();
//    HideTrackTabs();
    // This would be an organization list so don't even bother showing anything
    if (nTab == 0) {
	HideOrgTabs();
	return true;
    }

    document.getElementById("FormTab").style.visibility="hidden";
    for(i = 1; i <= 5; i++) {
	Tab = document.getElementById("Org"+i);
	Tabg = document.getElementById("Org"+i+"g");
	if(i==nTab) {
	    Tab.style.visibility="visible";
	    Tabg.style.visibility="hidden";
	} else if (Tab) {
	    Tab.style.visibility="hidden";
	    Tabg.style.visibility="visible";
	}
    }
    
}


function SetTab(tab, subTab) {
    
//    if (typeof(window[ 'subTab' ]) == "undefined") {
//       subTab = 0;
//    }

    for (i = 1; i <= 5; i++) {

	Tab = document.getElementById("Tab"+i);
	if (i == tab) {
	    Tab.style.visibility="visible";
	} else {
	    Tab.style.visibility="hidden";
	}
    }

    switch(tab) {
    case 0: // This always means hide all subtabs
	document.getElementById("FormTab").style.visibility="visible";
	HideQuestionTabs();
	HideTrackTabs();
	HideOrgTabs();
	HideCustomizeTabs();
	break;
    case 1: // Organization tab so load subtabs
	document.getElementById("FormTab").style.visibility="visible";
	HideTrackTabs();
	HideQuestionTabs();
	HideCustomizeTabs();
	SetOrgSubTab(subTab);
	break;
    case 2: // Employees tab so no subtabs
	document.getElementById("FormTab").style.visibility="visible";
	HideQuestionTabs();
	HideTrackTabs();
	HideOrgTabs();
	HideCustomizeTabs();
	break;
    case 3: // Tracks tab so load subtabs if any
	document.getElementById("FormTab").style.visibility="visible";
	HideQuestionTabs();
	HideOrgTabs();
	HideCustomizeTabs();
	SetTrackSubTab(subTab);
	break;
    case 4: // Questions Tab
	HideTrackTabs();
	HideOrgTabs();
	HideCustomizeTabs();
	document.getElementById("CurrentTab1").style.visibility="visible";
	document.getElementById("CurrentTab2").style.visibility="hidden";
	document.getElementById("ArchiveTab1").style.visibility="hidden";
	document.getElementById("ArchiveTab2").style.visibility="visible";
	document.getElementById("SummaryTab1").style.visibility="hidden";
	document.getElementById("SummaryTab2").style.visibility="visible";
	document.getElementById("AnalysisTab1").style.visibility="hidden";
	document.getElementById("AnalysisTab2").style.visibility="visible";
	document.getElementById("CategoriesSelected").style.visibility="hidden";
	document.getElementById("CategoriesUnselected").style.visibility="visible";
	SetQuestionSubTab(subTab);
	break;
    case 5: // Customize Tab
	document.getElementById("FormTab").style.visibility="visible";
	HideQuestionTabs();
	HideTrackTabs();
	HideOrgTabs();
	if (subTab == 0)
	  SetCustomizeSubTab(1);
	else
	  SetCustomizeSubTab(subTab);
	break;
    default:
	alert("SetTab(" + tab + ") was asked to show an undefined tab in admin.js");	
	break;
    }

} // end function SetTab()


function SetQuestionSubTab(subTab) {

    switch(subTab) {
    case 0: // invalid, show none at all
	break;
    case 1: // current questions
    case 2: // question edit
    case 6: // question view
	document.getElementById("CurrentTab1").style.visibility="visible";
	document.getElementById("CurrentTab2").style.visibility="hidden";
	document.getElementById("ArchiveTab1").style.visibility="hidden";
	document.getElementById("ArchiveTab2").style.visibility="visible";
	document.getElementById("SummaryTab1").style.visibility="hidden";
	document.getElementById("SummaryTab2").style.visibility="visible";
	document.getElementById("AnalysisTab1").style.visibility="hidden";
	document.getElementById("AnalysisTab2").style.visibility="visible";
	document.getElementById("CategoriesSelected").style.visibility="hidden";
	document.getElementById("CategoriesUnselected").style.visibility="visible";
	break;
    case 3: // archive questions
    case 7: // archived question view
	document.getElementById("CurrentTab1").style.visibility="hidden";
	document.getElementById("CurrentTab2").style.visibility="visible";
	document.getElementById("ArchiveTab1").style.visibility="visible";
	document.getElementById("ArchiveTab2").style.visibility="hidden";
	document.getElementById("SummaryTab1").style.visibility="hidden";
	document.getElementById("SummaryTab2").style.visibility="visible";
	document.getElementById("AnalysisTab1").style.visibility="hidden";
	document.getElementById("AnalysisTab2").style.visibility="visible";
	document.getElementById("CategoriesSelected").style.visibility="hidden";
	document.getElementById("CategoriesUnselected").style.visibility="visible";
	break;
    case 4: // summary
	document.getElementById("CurrentTab1").style.visibility="hidden";
	document.getElementById("CurrentTab2").style.visibility="visible";
	document.getElementById("ArchiveTab1").style.visibility="hidden";
	document.getElementById("ArchiveTab2").style.visibility="visible";
	document.getElementById("SummaryTab1").style.visibility="visible";
	document.getElementById("SummaryTab2").style.visibility="hidden";
	document.getElementById("AnalysisTab1").style.visibility="hidden";
	document.getElementById("AnalysisTab2").style.visibility="visible";
	document.getElementById("CategoriesSelected").style.visibility="hidden";
	document.getElementById("CategoriesUnselected").style.visibility="visible";
	break;
    case 5: // analysis
	document.getElementById("CurrentTab1").style.visibility="hidden";
	document.getElementById("CurrentTab2").style.visibility="visible";
	document.getElementById("ArchiveTab1").style.visibility="hidden";
	document.getElementById("ArchiveTab2").style.visibility="visible";
	document.getElementById("SummaryTab1").style.visibility="hidden";
	document.getElementById("SummaryTab2").style.visibility="visible";
	document.getElementById("AnalysisTab1").style.visibility="visible";
	document.getElementById("AnalysisTab2").style.visibility="hidden";
	document.getElementById("CategoriesSelected").style.visibility="hidden";
	document.getElementById("CategoriesUnselected").style.visibility="visible";
	break;
    case 8: // Categories
	document.getElementById("CurrentTab1").style.visibility="hidden";
	document.getElementById("CurrentTab2").style.visibility="visible";
	document.getElementById("ArchiveTab1").style.visibility="hidden";
	document.getElementById("ArchiveTab2").style.visibility="visible";
	document.getElementById("SummaryTab1").style.visibility="hidden";
	document.getElementById("SummaryTab2").style.visibility="visible";
	document.getElementById("AnalysisTab1").style.visibility="hidden";
	document.getElementById("AnalysisTab2").style.visibility="visible";
	document.getElementById("CategoriesSelected").style.visibility="visible";
	document.getElementById("CategoriesUnselected").style.visibility="hidden";
	break;
    default:
	alert("SetQuestionSubTab(" + subTab + ") was asked to show an undefined tab in admin.js");	
	break;
    }

    return true;

} // end function SetQuestionSubTab()



function SetCustomizeSubTab(subTab) {

    switch(subTab) {
    case 0: // invalid, show none at all
	break;
    case 1: // current questions
	document.getElementById("FileMan1").style.visibility="visible";
	document.getElementById("FileMan2").style.visibility="hidden";
	if (document.getElementById('CustMsg1')) {
		document.getElementById("CustMsg1").style.visibility="hidden";
		document.getElementById("CustMsg2").style.visibility="visible";
	}
	break;
    case 2: // archive questions
	document.getElementById("FileMan1").style.visibility="hidden";
	document.getElementById("FileMan2").style.visibility="visible";
	if (document.getElementById('CustMsg1')) {
		document.getElementById("CustMsg1").style.visibility="visible";
		document.getElementById("CustMsg2").style.visibility="hidden";
	}
	break;
    default:
	alert("SetCustomizeSubTab(" + subTab + ") was asked to show an undefined tab in admin.js");	
	break;
    }

    return true;

} // end function SetCustomizeSubTab()



function ChangeTrackRefs(str,str1) {
    document.getElementById("Track1Ref").href="track_edit.php?"+str;
    document.getElementById("Track2Ref").href="user_list.php?"+str+str1;
    document.getElementById("Track3Ref").href="question_list.php?"+str+str1;
    document.getElementById("Track4Ref").href="faq_list.php?"+str+str1;
    document.getElementById("Track5Ref").href="progress_list.php?"+str+str1;
    document.getElementById("Track6Ref").href="class_email.php?"+str+str1;
}



function SetSize() {

	window.moveTo(0,25);
	window.resizeTo(screen.width, screen.height-100);
//    window.location="../admin/main.php";
} // end function SetSize();


function SetLevel(value, userID, theme) {
    if (value == "1") {
	document.getElementById('FormWindow').src="track_list.php?get_assigned="+userID+"&get_show_assigned=1";
	document.getElementById("Tab1Grey").innerHTML =
	    '<img src="../themes/'+theme+'/admin/builder_org_behind.gif" width="112" height="24" border="0" id="OrgTab">';
	document.getElementById("Tab2Grey").innerHTML =
	    '<img src="../themes/'+theme+'/admin/builder_employee_behind.gif" width="112" height="24" border="0" id="EmployeeTab">';
	document.getElementById("Tab3Grey").innerHTML =
	    '<img src="../themes/'+theme+'/admin/builder_tracks_behind.gif" width="112" height="24" border="0" id="TracksTab">';
	document.getElementById("Tab4Grey").innerHTML =
	    '<img src="../themes/'+theme+'/admin/builder_questions_behind.gif" width="112" height="24" border="0" id="QuestionsTab">';
	document.getElementById("Tab5Grey").innerHTML =
	    '<img src="../themes/'+theme+'/admin/builder_customize_back.gif" width="112" height="24" border="0" id="CustomizeTab">';
	SetTab(3);
    }
} // end function SetLevel()


function Help() {
	alert("Help is not active.");
} // end function Help()



function BranchTab(newLocation) {
    FrameObj=document.getElementById("FormWindow");
    parent.FormWindow.Branch(newLocation);
    //document.getElementById('FormWindow').src=newLocation;
} // end function BranchTab()


function SetFlag() {
        bSave = true;
} // end function SetFlag()


function Find(newPage) {
    //if(FormValidate())
    //{
    FormObj = MM_findObj("AdminForm");
    FormObj.action="question_analysis2.php?FormAction=Find&navigate="+newPage;
    FormObj.submit();
    //}
} // end function Find()


function Update(newPage) {

    FormObj = MM_findObj("AdminForm");
    FormObj.action=newPage;
    FormObj.submit();
    
} // end function Update()


function UpdateValidate(newPage) {
    if(FormValidate()) {
	FormObj = MM_findObj("AdminForm");
	FormObj.action=newPage;
	FormObj.submit();
    }
} // end function UpdateValidate()


function ChangeOrg(orgName, topLeftBack, topLeftFront, topLeftBackRepeat, topMiddleBack, topMiddleFront, topMiddleBackRepeat, topRightBack, topRightFront, topRightBackRepeat, refObject) {

    //parent.document.getElementById("Title").innerHTML = orgName;
    refObject.document.getElementById("Title").innerHTML = orgName;

//    if (showLogo) {
//	if (topRightFront == '') {
//	    topRightFront="/desktop/organizations/hr_tools/motif/hrtools_logo.gif";
//        }

    //elm = parent.document.getElementById("top_right_img");
    elm = refObject.document.getElementById("top_right_img");
    if (topRightFront != '') {
	elm.src = topRightFront;
	elm.style.visibility="visible";
    } else {
	elm.style.visibility="hidden";
    }
//    } else {
//	parent.document.getElementById("Logo").style.visibility="hidden";
//    }

    elm = refObject.document.getElementById("top_left_bg");
    if (topLeftBack != '') {
	//elm = parent.document.getElementById("top_left_bg");
	elm.style.backgroundImage = 'url(' + topLeftBack + ')';
	if (topLeftBackRepeat == 1)
	    elm.style.backgroundRepeat='repeat-x';
	else
	    elm.style.backgroundRepeat='no-repeat';
    } else {
	elm.style.backgroundImage = 'url()';
        elm.style.backgroundRepeat='no-repeat';
    }
	

    //elm = parent.document.getElementById("top_middle_img");
    elm = refObject.document.getElementById("top_middle_img");
    if (topMiddleFront != '') {
	elm.src = topMiddleFront;
	elm.style.visibility="visible";
    } else {
	elm.style.visibility="hidden";
    }

    elm = refObject.document.getElementById("top_middle_bg");
    if (topMiddleBack != '') {
	//elm = parent.document.getElementById("top_middle_bg");
	elm.style.backgroundImage = 'url(' + topMiddleBack + ')';
	if (topMiddleBackRepeat == 1)
	    elm.style.backgroundRepeat='repeat-x';
	else
	    elm.style.backgroundRepeat='no-repeat';
    } else {
	elm.style.backgroundImage = 'url()';
        elm.style.backgroundRepeat='no-repeat';
    }

    elm = refObject.document.getElementById("top_right_bg");
    if (topRightBack != '') {
	//elm = parent.document.getElementById("top_right_bg");
	elm.style.backgroundImage = 'url(' + topRightBack + ')';
	if (topRightBackRepeat == 1)
	    elm.style.backgroundRepeat='repeat-x';
	else
	    elm.style.backgroundRepeat='no-repeat';
    } else {
	elm.style.backgroundImage = 'url()';
        elm.style.backgroundRepeat='no-repeat';
    }

    //elm = parent.document.getElementById("top_left_img");
    elm = refObject.document.getElementById("top_left_img");
    if (topLeftFront != '') {
	elm.src = topLeftFront;
	elm.style.visibility="visible";
    } else {
	elm.style.visibility="hidden";
    }

} // end function ChangeOrg()



// Function: InitTrackEdit()
// Purpose:  initialize the track editing page
function InitTrackEdit(fIsManagement, fIsBound, fIsLoop, bAddTrackEdit) {

    FormObj = MM_findObj("F_Is_Management");
    if(fIsManagement == '1')
	FormObj.checked="true";
    
    FormObj = MM_findObj("F_Is_Bound");
    if(fIsBound == '1')
	FormObj.checked="true";
    
    FormObj = MM_findObj("F_Is_Loop");
    if(fIsLoop == '1')
	FormObj.checked="true";
        
    if(bAddTrackEdit == '') {

	FormObj = document.getElementById("AddDiv");
	FormObj.style.visibility="visible";
	document.getElementById("F_Is_Management").checked=true;
	document.getElementById("F_Is_Bound").checked=true;
	parent.AddTrack();

    } else {

	FormObj = document.getElementById("AddDiv");
	FormObj.style.visibility="hidden";
	parent.SetTrackSubTab(1);
    }

} // end function InitTrackEdit()



function CheckSaveTrackEdit(decodeLocation, encodeLocation, bSave, bAddTrackEdit) {

    if(!bSave)
	window.location.href = decodeLocation;
    else {
	if(confirm("Would you like to save your changes to this form?")) {
	    if(bAddTrackEdit == '')
		AddRecord(encodeLocation);
	    else
		UpdateRecordTrackEdit(encodeLocation);
	}
	else window.location.href = decodeLocation;
    }

} // end function CheckSaveTrackEdit()


function WarnRemoveAllFromTrack()
{
  return confirm('Are you sure you want to remove all users from this track?');
}


function WarnAddAllToTrack()
{
  return confirm("If you have participants in other tracks this will remove them from those\n"+
	"tracks and add them to this track.  Are you sure you want to proceed?");
}


function WarnRemoveAllQuestionsFromTrack()
{
  return confirm('Are you sure you want to remove all questions from this track?');
}


function WarnAddAllQuestionsToTrack()
{
  return confirm('Are you sure you want to add all questions to this track?');
}


function ToggleAb1825Track()
{
    abCheck = document.getElementById("isAb1825");
    loopCheck = document.getElementById("isLooping");
    bindCheck = document.getElementById("isBinding");
    groupCheck = document.getElementById("groupAssignments");
    
    if (abCheck.checked) {
	loopCheck.checked = false;
	loopCheck.disabled = true;
	bindCheck.checked = true;
	bindCheck.disabled = true;
	groupCheck.checked = true;
	groupCheck.disabled = true;
    } else {
	loopCheck.disabled = false;
	bindCheck.disabled = false;
	groupCheck.disabled = false;
    }
}


/*
function BranchTrackEdit(strNew)
{
	CheckSaveTrackEdit(strNew,strNew);
}


*/


function InitQuestionEdit(bAddQuestionEdit) {

    //FormObj = document.getElementById("F_Is_Management");
    //if (fIsManagement == '1')
	//FormObj.checked=false;
    //else
    //	FormObj.checked=true;
    
    if (bAddQuestionEdit == '') {
	FormObj = document.getElementById("AddDiv");
	FormObj.style.visibility="visible";
	//	document.getElementById("PreviewDiv").style.visibility="hidden";
    } else {
	FormObj = document.getElementById("AddDiv");
	FormObj.style.visibility="hidden";
    }

} // end function InitQuestionEdit()


// This function only used in user_edit.tpl
function InitUserEdit(supervisor, contact, admin, bAdd) {

    FormObj = MM_findObj("F_Use_Supervisor");
    if(supervisor == 1)
	FormObj.checked="true";

    FormObj = MM_findObj("F_Is_HRContact");
    if(contact == 1)
	FormObj.checked="true";

    FormObj = MM_findObj("F_Is_Administrator");
    if(admin == 1)
	FormObj.checked="true";

    if(bAdd == '') {
	FormObj = MM_findObj("AddDiv");
	FormObj.style.visibility="visible";
	document.getElementById("F_Use_Supervisor").checked=true;
    } else {
	FormObj = MM_findObj("AddDiv");
	FormObj.style.visibility="hidden";
    }

} // end function InitUserEdit



var nPage = 1; //used for page numbering during question edit
function NextSection(theme) {

    nPage = nPage + 1;
    if (nPage == 10)
	document.getElementById("SectionNext").style.visibility = "hidden";
    else {
	document.getElementById("SectionBack").style.visibility = "visible";
	document.getElementById("SectionNext").style.visibility = "visible";
    }
    ChangeArea(nPage, theme);

} // end function NextSection()


function BackSection(theme) {

    nPage = nPage - 1;

    if (nPage == 1)
	document.getElementById("SectionBack").style.visibility = "hidden";
    else {
	document.getElementById("SectionBack").style.visibility = "visible";
	document.getElementById("SectionNext").style.visibility = "visible";
    }
    ChangeArea(nPage, theme);

} // end function BackSection()


function launchPopup(strWindowName, strPath, yesnoResizable, height, width) {

    var ScreenWidth=screen.width;
    var zeroX=((ScreenWidth/2)-(width/2));
    var zeroY=((ScreenHeight/2)-(height/2));
    if(width==0) {
	width=(ScreenWidth-10);
	zeroX=((ScreenWidth/2)-(ScreenWidth/2));
    }
    var ScreenHeight=screen.height;
    if(height==0) {
	height=(ScreenHeight-70);
	zeroY=((ScreenHeight/2)-(ScreenHeight/2));
    }
    
    var params = "toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=" + yesnoResizable + 
	",width=" + width+ ",height=" + height + ",left="+zeroX+",top="+zeroY;
    popup_window = window.open( strPath, strWindowName, params );
    popup_window.focus();

} // end function launchPopup()


function ChangeArea(nSection, theme) {

    nPage = nSection;
    for (i = 1; i <= 10; i++) {

	ArrowTD = document.getElementById("Arrow"+i);
	Section = document.getElementById("Section"+i);

	arrowHtml = '<img src="../themes/'+theme+'/admin/pointer.gif" width="12" height="11" border="0">';
	if (i == nSection) {
	    ArrowTD.innerHTML=arrowHtml;
	    Section.style.visibility="visible";
	} else {
	    ArrowTD.innerHTML=" ";
	    Section.style.visibility="hidden";
	}

	if (nSection == 2) {
	    
	    nType = document.getElementById("frameTypeId").value;
	    if (nType == "000001") {
		document.getElementById("mediaText").innerHTML="The media element for a \"Multiple Choice Text\" Frametype does not use this field.";
		document.getElementById("media").disabled=true;
		document.getElementById("media").className='FormDisabled';
	    } else {
		if(nType == "000002")
		    document.getElementById("mediaText").innerHTML =
			"The media element for a \"Multiple Choice Graphic\" Frametype should be a web friendly format such as \".gif\" or \".jpg\"";
		else
		    if (nType == "000003")
			document.getElementById("mediaText").innerHTML =
			    "The media element for a \"Multiple Choice Flash\" Frametype should be an \".swf\" file";
		    else
			if (nType == "000004")
			    document.getElementById("mediaText").innerHTML =
				"The media element for a \"Multiple Choice Video\" Frametype should be able to " +
				"play within the default installation of Windows Media Player 6.4";
		
		document.getElementById("media").disabled=false;
		document.getElementById("media").className='FormValue';
	    }
	}
	
    } // end for i = 1 to 10
    
    switch(nSection) {
    case 1:
	document.getElementById("AName").innerHTML = "Question Information";
	document.getElementById("SectionBack").style.visibility = "hidden";
	document.getElementById("SectionNext").style.visibility = "visible";
	break;
    case 2:
	document.getElementById("AName").innerHTML = "The Question";
	document.getElementById("SectionNext").style.visibility = "visible";
	document.getElementById("SectionBack").style.visibility = "visible";
	break;
    case 3:
	document.getElementById("AName").innerHTML = "Potential Answers";
	document.getElementById("SectionNext").style.visibility = "visible";
	document.getElementById("SectionBack").style.visibility = "visible";
	break;
    case 4:
	document.getElementById("AName").innerHTML = "The Purpose of the Question";
	document.getElementById("SectionNext").style.visibility = "visible";
	document.getElementById("SectionBack").style.visibility = "visible";
	break;
    case 5:
	document.getElementById("AName").innerHTML = "Possible Responses";
	document.getElementById("SectionNext").style.visibility = "visible";
	document.getElementById("SectionBack").style.visibility = "visible";
	break;
    case 6:
	document.getElementById("AName").innerHTML = "The Correct Response";
	document.getElementById("SectionNext").style.visibility = "visible";
	document.getElementById("SectionBack").style.visibility = "visible";
	break;
    case 7:
	document.getElementById("AName").innerHTML = "Supplemental Information";
	document.getElementById("SectionNext").style.visibility = "visible";
	document.getElementById("SectionBack").style.visibility = "visible";
	break;
    case 8:
	document.getElementById("AName").innerHTML = "Key Learning Points";
	document.getElementById("SectionNext").style.visibility = "visible";
	document.getElementById("SectionBack").style.visibility = "visible";
	break;
    case 9:
	document.getElementById("AName").innerHTML = "Additional Information";
	document.getElementById("SectionNext").style.visibility = "visible";
	document.getElementById("SectionBack").style.visibility = "visible";
	break;
    case 10:
	document.getElementById("AName").innerHTML = "Customize 'Thank You' Page";
	document.getElementById("SectionNext").style.visibility = "hidden";
	document.getElementById("SectionBack").style.visibility = "visible";
	break;
    default:
	document.getElementById("AName").innerHTML = "Question Area";
	break;
    }
    
    document.getElementById("AreaMenu").style.visibility="hidden";

} // end function ChangeArea()

/*
function AddQuestionData() {

    var re = new RegExp ('<br>', 'gi') ;
    
    document.getElementById("question").value = strStem.replace(re,"\n");
    document.getElementById("answerA").value = strFoil1.replace(re,"\n");
    document.getElementById("answerB").value = strFoil2.replace(re,"\n");
    if (typeof(strFoil3)!='undefined')
	document.getElementById("answerC").value = strFoil3.replace(re,"\n");
    if (typeof(strFoil4)!='undefined')
	document.getElementById("answerD").value = strFoil4.replace(re,"\n");
    if (typeof(strFoil5)!='undefined')
	document.getElementById("answerE").value = strFoil5.replace(re,"\n");
    document.getElementById("correctAnswer").selectedIndex = strCorrect-1;
    document.getElementById("purpose").value = strPurpose.replace(re,"\n");
    document.getElementById("responseA").value = strFoil1_Fdbk.replace(re,"\n");
    document.getElementById("responseB").value = strFoil2_Fdbk.replace(re,"\n");
    if (typeof(strFoil3_Fdbk)!='undefined')
	document.getElementById("responseC").value = strFoil3_Fdbk.replace(re,"\n");
    if (typeof(strFoil4_Fdbk)!='undefined')
	document.getElementById("responseD").value = strFoil4_Fdbk.replace(re,"\n");
    if (typeof(strFoil5_Fdbk)!='undefined')
	document.getElementById("responseE").value = strFoil5_Fdbk.replace(re,"\n");
    document.getElementById("correctResponse").value = strCorrect_Fdbk.replace(re,"\n");
    document.getElementById("keyLearningPoints").value = strLearnPoints.replace(re,"\n");
    //document.getElementById("supportInfo").value = strSupportInfo.replace(re,"\n");
    //document.getElementById("notes").value = strNotes.replace(re,"\n");
    document.getElementById("media").value = strMedia;
    
} // end function AddQuestionData()



function AddViewQuestionData() {

	var re = new RegExp ('<br>', 'gi') ;
	var answers = new Array('Not Specified', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H');

	document.getElementById("question").innerHTML = strStem.replace(re,"\n");
	document.getElementById("answerA").innerHTML = strFoil1.replace(re,"\n");
	document.getElementById("answerB").innerHTML = strFoil2.replace(re,"\n");
	if (typeof(strFoil3)!='undefined')
	  document.getElementById("answerC").innerHTML = strFoil3.replace(re,"\n");
	if (typeof(strFoil4)!='undefined')
	  document.getElementById("answerD").innerHTML = strFoil4.replace(re,"\n");
	if (typeof(strFoil5)!='undefined')
	  document.getElementById("answerE").innerHTML = strFoil5.replace(re,"\n");
	document.getElementById("correctAnswer").innerHTML = answers[strCorrect];
	document.getElementById("purpose").innerHTML = strPurpose.replace(re,"\n");
	document.getElementById("responseA").innerHTML = strFoil1_Fdbk.replace(re,"\n");
	document.getElementById("responseB").innerHTML = strFoil2_Fdbk.replace(re,"\n");
	if (typeof(strFoil3_Fdbk)!='undefined')
	  document.getElementById("responseC").innerHTML = strFoil3_Fdbk.replace(re,"\n");
	if (typeof(strFoil4_Fdbk)!='undefined')
	  document.getElementById("responseD").innerHTML = strFoil4_Fdbk.replace(re,"\n");
	if (typeof(strFoil5_Fdbk)!='undefined')
	  document.getElementById("responseE").innerHTML = strFoil5_Fdbk.replace(re,"\n");
	document.getElementById("correctResponse").innerHTML = strCorrect_Fdbk.replace(re,"\n");
	document.getElementById("keyLearningPoints").innerHTML = strLearnPoints.replace(re,"\n");
	document.getElementById("supportInfo").innerHTML = strSupportInfo.replace(re,"\n");
	// Notes come out of the database in a separate field
	//document.getElementById("notes").innerHTML = strNotes.replace(re,"\n");
	document.getElementById("media").innerHTML = strMedia;

} // end function AddQuestionData()
*/


function CheckSaveQuestionEdit(decodeLocation, encodeLocation, bAddQuestionEdit) {

    //alert("BSAVE: " + decodeLocation + "\r\n" + encodeLocation + "\r\n" + bAddQuestionEdit);
    if (!bSave && !bSupport)
	window.location.href = decodeLocation;
    else {
	if(confirm("Would you like to save your changes to this Question?")) {
	    if(bAddQuestionEdit == '')
		AddRecordQuestionEdit(encodeLocation);
	    else
		UpdateRecordQuestionEdit(encodeLocation);
	} else window.location.href = decodeLocation;
    }
    
} // end function CheckSaveQuestionEdit()


function CheckSave() {
    //alert("LOC: " + newLocation);
    if(!bSave) {
	return true;
    } else {
	return confirm("Are you sure you want to continue without saving your changes to this form?");
    }

} // end function CheckSave()


function BranchCheckSave(strNew) {
    CheckSave(strNew);
} // end function BranchCheckSave()


function SetNews() {

    if(document.getElementById("F_Use_News").checked) document.getElementById("F_News").disabled=false;
    else document.getElementById("F_News").disabled=true;
    
    document.getElementById("F_News").select();

} // end function SetNews()


var bSupport=false;
function SetSupport() {

    bSupport=true;

} // end function SetSupport()


function CheckBoxes(fUseNews, fUseMotif) {

    FormObj = MM_findObj("F_Use_Motif");
    if(fUseMotif == '1')
	FormObj.checked="true";
    
    FormObj = MM_findObj("F_Use_News");
    if(fUseNews == '1')
	FormObj.checked="true";
    else
	document.getElementById("F_News").disabled=true;
    
} // end function CheckBoxes


function ChangePointer(nPointer) {

    for(i = 0; i < 3; i++) {
	Pointer = document.getElementById("pointer"+i);
	if(i == nPointer)	{
	    Pointer.style.visibility="visible";
	    document.getElementById("IFrameDiv").style.visibility="visible";
	} else {
	    Pointer.style.visibility="hidden";
	}
    }
} // end function ChangePointer()


function DoSearch(elementString, actionString) {

    DateObj=document.getElementById(elementString);
    if(DateObj.value.length<10) {
	alert("Invalid Date. The Date must be in the following YYYY-MM-DD format \"2003-01-24\".  " +
	      "You can use the calendar icon to select a date in the correct format.");
	DateObj.select();
	DateObj.focus();
    } else {
	FormObj = MM_findObj("AdminForm");
	FormObj.action=actionString;
	FormObj.submit();
    }

} // end function DoSearch()


function DoResetSearch(sElement, sAction, bReset) {

  form = document.getElementById(sElement);

  if (bReset)
    form.search_input.value = '';

  if (form.search_input.value) {
    sAction += "&get_search=" + form.search_input.value;
    i = form.search_by.selectedIndex;
    sAction += "&get_search_by=" + form.search_key[i].text;
  }
  window.location.href=sAction;

  return false;

} // end function DoTrackSearch()



// This function used only in track_list.php
function ChangeMaxRows(sElement, sAction, bReturn) {

    form = document.getElementById(sElement);

    if (form.row_input.value > 0 && form.row_input.value < 1000) {
	sAction += "&get_max_rows=" + form.row_input.value;
	window.location.href = sAction;
    }

    if (bReturn)
	return false;

} // end function ChangeMaxTrackRows()


function ImportQuestions() {

	var ScreenWidth=screen.width;
	var ScreenHeight=screen.height;
	var zeroX=(ScreenWidth/2-400);
	var zeroY=(ScreenHeight/2-300);
	var options = "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=792,height=545,left="+zeroX+",top="+zeroY;
	hwndQuestions = window.open('question_import.php','importquestions',options);

} // end function Import Questions()


function ImportUsers() {

    var ScreenWidth=screen.width;
    var ScreenHeight=screen.height;
    var zeroX=(ScreenWidth/2-400);
    var zeroY=(ScreenHeight/2-300);
    var options = "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=792,height=545,left="+zeroX+",top="+zeroY;
    hwndUser = window.open('importusers.php','importusers',options);

} // end function ImportUsers


function UpdateSystem() {

    var ScreenWidth=screen.width;
    var ScreenHeight=screen.height;
    var zeroX=(ScreenWidth/2-400);
    var zeroY=(ScreenHeight/2-300);
    var options = "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=792,height=545,left="+zeroX+",top="+zeroY;
    hwndUser = window.open('../agent/update.php','updatesystem',options);

}


function UpdateDemoQuestions() {

    FormObj = MM_findObj("AdminForm");
    FormObj.action="organization_edit4.php?FormAction=Demo";
    FormObj.submit();

} // end function UpdateDemoQuestions()


function FormValidateFaq() {

	FormObj = MM_findObj("question");
	if(FormObj.value.length>0)
	{
		FormObj = MM_findObj("answer");
		if(FormObj.value.length>0)
		{
			return true;
		}
	}
	else
	{
		alert("You must provide a Name for the track.");
		FormObj.focus();
	}

	return false;
}


function FaqSubmit() {
	if(FormValidateFaq()) {
	  FormObj = MM_findObj("AdminForm");
	  FormObj.submit();
	}
}


function AskDeleteFaq(strFaqName, strURL) {

    if (confirm("Are you sure you want to permanently remove the FAQ \""+strFaqName+"\"?"))
	window.location.href = strURL;

} // end function AskDeleteFaq()


function OrgDataEditSubmit(strOperation) {

    // valid strOperation values are:
    // 'add', 'update', and 'delete'
    objNewName = MM_findObj("elementValue");
    objSelectedName = MM_findObj("dataElementValues");
    var objSelectedIndex = 0;
    
    // Perform data validation
    if (!objNewName.value && (strOperation == 'add' || strOperation == 'update')) {
	alert("The operation requested requires that a valid name be supplied.");
	return;
    }

    if (objSelectedName && !objSelectedName.value && strOperation == 'update') {
	alert("You must first select an existing value before you can update it.");
	return;
    }

    if (strOperation == 'delete' && objSelectedName && !objSelectedName.value) {
	alert("You must first select an existing value before you can delete it.");
	return;
    }

    if (strOperation == 'delete') {
	objSelectedIndex = objSelectedName.selectedIndex;
        if (!confirm("Confirm deletion of " + objSelectedName.options[objSelectedIndex].text)) {
	   return;
	}
	objNewName.value = "";
    }

    // Make sure we are not creating a duplicate
    if (strOperation == 'add' && objSelectedName) {
	for(i = 0; i < objSelectedName.options.length; ++i) {
	    if (objNewName.value == objSelectedName.options[i].text) {
		alert("The name you are attempting to add already exists.");
		return;
	    }
	}
    }

    // Data validation passed so process form submission
    objOperation = MM_findObj("operation");
    objOperation.value = strOperation;
    objForm = MM_findObj("AdminForm");
    objForm.submit();
    
} // end function OrgDataEditSubmit();


function OrgDataEditKeyPress(e, addDivName) {

    // valid strOperation values are:
    // 'add', 'update', and 'delete'
    objNewName = MM_findObj("elementValue");
    objSelectedName = MM_findObj("dataElementValues");
    objAddDiv = document.getElementById(addDivName);
    
    var keynum;
    var keychar;
    var nameExists = false;
    var i = 0;

    if (!objSelectedName)
	return false;

    // See if the typed in value is in the list
    for(i = 0; i < objSelectedName.options.length; ++i) {
	if (objNewName.value == objSelectedName.options[i].text) {
	    objAddDiv.style.visibility = 'hidden';
	    nameExists = true;
	} 
    }
    if (!nameExists)
	objAddDiv.style.visibility = 'visible';

    if(window.event) // IE
    {
	keynum = e.keyCode
    }
    else if(e.which) // Netscape/Firefox/Opera
    {
	keynum = e.which
    }
    keychar = String.fromCharCode(keynum);
    if (keychar == '\n' || keychar == '\r')
    {
	// Submit form
	if (!objSelectedName || !objSelectedName.value)
	    OrgDataEditSubmit('add');
	else
	    OrgDataEditSubmit('update');
    }

    return false;
}


function OrgDataChangeItem(addDivName) {

    IDobj = MM_findObj("dataElementValues");
    
    FormObj = MM_findObj("AdminForm");
    FormObj = MM_findObj("elementValue");
    objAddDiv = document.getElementById(addDivName);

    FormObj.value=IDobj.options[IDobj.selectedIndex].text;
    if(IDobj.value)
	objAddDiv.style.visibility='hidden';
    else
	objAddDiv.style.visibility='visible';

} // end function OrgDataChangeItem()


function OrgDataLabelUpdate() {

  FormObj = MM_findObj("AdminForm");
  FormObj.action="label_edit.php?FormAction=Update";
  FormObj.submit();

} // end function OrgDataLabelUpdate()



function AskDeleteRecord(strName) {
    return confirm("Are you sure you want to permanently remove \"" + strName +
	"\"?\nThis operation will permanently destroy all related information for this record.");
}



function AskEditQuestion(strName) {
    return confirm("Are you sure you want to edit question \"" + strName + "\"?\n" +
		   "Since you did not create this question and since this question\n" +
		   "may be used in other tracks, any changes to the content of\n" +
		   "the question will result in an entirely new question with a\n" +
		   "new question number being created just for you.");
}


function AskDeleteQuestion(strName) {
    return confirm("Are you sure you want to delete question \"" + strName + "\"?\n" +
		   "This operation will archive the question and also remove it\n" +
		   "from any tracks that might be using it to create assignments.\n" +
		   "All unanswered assignments having this question number will also\n" +
		   "be removed from the system.  If unsure, check class progress and\n" +
		   "ensure all participants have completed all assignments before\n" +
		   "archiving this question.\n");
}



function ToggleContactId(switchId, setId) {
	
	objSwitch = document.getElementById(switchId);
	objSet = document.getElementById(setId);

	if (objSwitch.checked) {
		objSet.selectedIndex = 0;
		objSet.disabled = true;
		objSet.className = 'FormDisabled';
	} else {
		objSet.disabled = false;
		objSet.className = 'FormValue';
	}
}


// =================================================================================
// Functions used for recurrence.php
// =================================================================================
function Range(nButton)
{
//	ClearT1();
//	ClearT2();
//	ClearT3();
	for(i=1;i<=3;i++)
	{
		if(i==nButton) document.getElementById("Table" + i).style.visibility="visible";
		else document.getElementById("Table" + i).style.visibility="hidden";
	}
}

function ClearT1()
{
	document.getElementById("Day1").checked=false;
	document.getElementById("Day2").checked=false;
	document.getElementById("DApart").disabled=true;
}

function ClearT2()
{
	document.getElementById("WApart").value=1;
	document.getElementById("Monday").checked=false;
	document.getElementById("Tuesday").checked=false;
	document.getElementById("Wednesday").checked=false;
	document.getElementById("Thursday").checked=false;
	document.getElementById("Friday").checked=false;
	document.getElementById("Saturday").checked=false;
	document.getElementById("Sunday").checked=false;
}

function ClearT3()
{
	document.getElementById("Month").checked=false;
	document.getElementById("Month1").checked=false;
	document.getElementById("MDays").disabled=true;
	document.getElementById("MApart1").disabled=true;
	document.getElementById("MApart2").disabled=true;
	document.getElementById("MNumber").disabled=true;
	document.getElementById("MDay").disabled=true;
}

function SetT3(choice)
{
	if(choice==1)value1=true;
	else value1=false;

	 document.getElementById("MNumber").disabled=value1;
	 document.getElementById("MDay").disabled=value1;
	 document.getElementById("MApart2").disabled=value1;
	 document.getElementById("MDays").disabled=!value1;
	 document.getElementById("MApart1").disabled=!value1;
}

function UpdateRecurrence()
{
	if(CheckInput())
	{
	  FormObj = document.getElementById("AdminForm");
	  FormObj.action="recurrence.php";
	  FormObj.submit();
	}
}

function Test()
{

	Recurrence = "M 5 W";
	window.opener.document.getElementById("F_Recurrence").value=Recurrence;
	window.close();
}

function validateNumber(obj)
{

     i= obj.value.search(/[^0-9^.]/);
	 if (i>-1) return false;
	 else return true;
}

function CheckInput()
{

   if(document.getElementById("Interval1").checked)
   {
		if(document.getElementById("Day1").checked)
		{
			FormObj=document.getElementById("DApart");
			if(validateNumber(FormObj))
			{
				if(FormObj.value>=1) return true;
				else {alert("Error: This is a numeric Field that must be 1 or greater.");FormObj.select();FormObj.focus();}
			}
			else {alert("Error: This is a numeric Field with a range from 1 to 31");FormObj.select();FormObj.focus();}
		}
		else return true;
   }
   if(document.getElementById("Interval2").checked)
   {

   			FormObj=document.getElementById("WApart");
   			if(validateNumber(FormObj))
   			{
   				if(FormObj.value>=1) return true;
   				else {alert("Error: This is a numeric Field that must be 1 or greater.");FormObj.select();FormObj.focus();}
   			}
   			else {alert("Error: This is a numeric Field that must be 1 or greater.");FormObj.select();FormObj.focus();}

   }
   if(document.getElementById("Interval3").checked)
    {
		var bOk=false;
		if(document.getElementById("Month").checked)
		{
			FormObj=document.getElementById("MDays");
			if(validateNumber(FormObj))
			{
				if((FormObj.value>=1)&&(FormObj.value<=31)) bOk=true;
				else {alert("Error: This is a numeric Field with a range from 1 to 31");FormObj.select();FormObj.focus();}
			}
			else alert("Error: This is a numeric Field with a range from 1 to 31");

			if(bOk)
			{
				FormObj=document.getElementById("MApart1");
				if(validateNumber(FormObj))
				{
					if(FormObj.value>=1) return true;
					else {alert("Error: This is a numeric Field that must be 1 or greater.");FormObj.select();FormObj.focus();}
				}
			}

		}
		else
		{
			FormObj=document.getElementById("MApart2");
			if(validateNumber(FormObj))
			{
				if(FormObj.value>=1) return true;
			   	else {alert("Error: This is a numeric Field that must be 1 or greater.");FormObj.select();FormObj.focus();}
			}
   			else {alert("Error: This is a numeric Field that must be 1 or greater.");FormObj.select();FormObj.focus();}
		}
   }

   return false;
}



function SetRecurrence(Recurrence, strRecurrence, create, periodicity, countDays,
		       countWeeks, weekDays, flagWeek, countMonths) {

	if (create) {
		// Add code here to submit form and then close the window
		window.opener.document.getElementById("recurrence").value=Recurrence;
		window.opener.document.getElementById("strRecurrence").value=strRecurrence;
		window.opener.SetFlag();
		window.close();
		return;
	}

	if(periodicity == 'Daily')
	{
		Range(1);
		document.getElementById("Interval1").checked=true;

		if(countDays > 0)
		{
			document.getElementById("DApart").disabled=false;
			if (countDays > 0) {
				document.getElementById("DApart").value=countDays;
			} else {
				document.getElementById("DApart").empty;
			}
			document.getElementById("Day1").disabled=false;
			document.getElementById("Day1").checked=true;

		}
		else
		{
			document.getElementById("Day2").disabled=false;
			document.getElementById("Day2").checked=true;
		}
	}
	if (periodicity == 'Weekly')
	{
		Range(2);
		document.getElementById("Interval2").checked=true;

		document.getElementById("WApart").disabled=false;
		document.getElementById("WApart").value=countWeeks;
		document.getElementById("Monday").checked=weekDays & 2;
		document.getElementById("Tuesday").checked=weekDays & 4;
		document.getElementById("Wednesday").checked=weekDays & 8;
		document.getElementById("Thursday").checked=weekDays & 16;
		document.getElementById("Friday").checked=weekDays & 32;
		document.getElementById("Saturday").checked=weekDays & 64;
		document.getElementById("Sunday").checked=weekDays & 1;
	}
	if (periodicity == 'Monthly')
	{
		Range(3);
		document.getElementById("Interval3").checked=true;


		if(countDays > 0)
		{
			document.getElementById("Month").checked=true;
			document.getElementById("MDays").disabled=false;
			document.getElementById("MDays").value=countDays;
			document.getElementById("MApart1").disabled=false;
			document.getElementById("MApart1").value=countMonths;
		}
		else
		{

			document.getElementById("Month1").checked=true;
			document.getElementById("MNumber").disabled=false;
			document.getElementById("MNumber").selectedIndex=flagWeek+1;
			document.getElementById("MDay").disabled=false;
			for(i = 0; i < 7; i++) {
				if (weekDays & (1 << i)) {
					document.getElementById("MDay").selectedIndex=i+1;
				}
			}
			document.getElementById("MApart2").disabled=false;
			document.getElementById("MApart2").value=countMonths;
		}
	}

}



// =================================================================================
// Functions used for file manager on customize tab
// =================================================================================
function loadIFrame(iFrameName, url) {
  alert("howdy");
/*
  if ( window.frames[iFrameName] ) {
    window.frames[iFrameName].location.href=url;
    return false;
  } else {
    return true;
  }
*/
}



// =================================================================================
// Functions used for organizational reports
// =================================================================================
function loadDistinctValues(trackId, questionId) {

/*  frames['FormWindow'].location.href="/desktop/admin/org_report_accuracy_by_track.php?trackId=" + trackId; */
  window.location="/desktop/admin/org_report_accuracy_by_track.php?trackId=" + trackId + "&questionId=" + questionId;
}


function reportAccuracyToggleTracks(baseId, checkCount) {

  var i = 0;
  var elm = null;
  var id = null;

  for(i=0; i < checkCount; i++) {
    id = baseId + i;
    elm = document.getElementById(id);
    if (elm.checked)
      elm.checked=false;
    else
      elm.checked=true;
  }

}
