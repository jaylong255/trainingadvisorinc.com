
// Global variables for entire portal when end users are logged into system
var imgTab1off = new Image();
var imgTab1on = new Image();
var imgTab2off = new Image();
var imgTab2on = new Image();
var imgTab3off = new Image();
var imgTab3on = new Image();
var imgTab4off = new Image();
var imgTab4on = new Image();


function ChangeUserTabs(nTab, theme) {

    var onImg;
    var offImg;

    imgTab1off.src = '../themes/'+theme+'/presentation/assigned_off.gif';
    imgTab1on.src = '../themes/'+theme+'/presentation/assigned_on.gif';
    imgTab2off.src = '../themes/'+theme+'/presentation/completed_off.gif';
    imgTab2on.src = '../themes/'+theme+'/presentation/completed_on.gif';
    imgTab3off.src = '../themes/'+theme+'/presentation/faq_off.gif';
    imgTab3on.src = '../themes/'+theme+'/presentation/faq_on.gif';
    imgTab4off.src = '../themes/'+theme+'/presentation/scorecard_off.jpg';
    imgTab4on.src = '../themes/'+theme+'/presentation/scorecard_on.jpg';
 
    for(i=1;i<=4;i++) {
	if(i==nTab) {
	    onImg = eval("imgTab"+i+"on.src");
	    document.images["imgTab"+i].src = onImg;
	} else {
	    offImg = eval("imgTab"+i+"off.src");
	    document.images["imgTab"+i].src = offImg;
	}
    }
    
    obj = document.getElementById("Instructions");
    switch(nTab) {
    case 1:
	obj.innerHTML='Below is a list of all questions that have been assigned to you that are '
	    + 'currently unanswered. Select any question listed to proceed with that question.';
	//document.getElementById('assignmentMenu').innerHTML=document.getElementById('divAssignmentList').innerHTML;
	break;
    case 2:
	obj.innerHTML='Ongoing access to your previous training topics is available to you '
	    + 'for future reference.  To view previous training click on the desired topic below.';
	//document.getElementById('assignmentMenu').innerHTML=document.getElementById('divCompletedAssignmentList').innerHTML;
	break;
    case 3:
	obj.innerHTML='Click on any frequently asked question below to view the answer';
	break;
	
    case 4:
	obj.innerHTML='This is your scorecard.  It reflects your success rate for the periods selected.';
	break;
    default:
	obj.innerHTML='Invalid tab selected, this is likely a bug so please report it to your contact. Thank you.';
	break;
    }
}
