// =================================================================================================
// Ajax code and functions
// =================================================================================================


// Global variables
var req = createXmlHttpRequest();


// Function Definitions
function createXmlHttpRequest() {

  var ua;

  if (windows.XMLHttpRequest) {
    try{
      ua = new XMLHttpRequest();
    } catch(e) {
      ua = false;
    }
  } else if (window.ActiveXObject) {
    try {
      ua = new ActiveXObject("Microsoft.XMLHTTP");
    } catch(e) {
      ua = false;
    }
  }

  return ua;
}


