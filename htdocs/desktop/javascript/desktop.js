//	<script language="JavaScript">

function getStyle(id, strCssRule){
    var strValue = "";
    var oElm = document.getElementById(id);
    if(document.defaultView && document.defaultView.getComputedStyle){
	strValue = document.defaultView.getComputedStyle(oElm, "").getPropertyValue(strCssRule);
    } else if(oElm.currentStyle){
	strCssRule = strCssRule.replace(/\-(\w)/g, function (strMatch, p1){
		return p1.toUpperCase();
	});
	strValue = oElm.currentStyle[strCssRule];
    }
    return strValue;
}


function SetFocus(obj) {
    obj.focus();
    return;
}


function SetObjectVisibility(obj, visible) {
    if (visible) {
	obj.style.visibility = 'visible';
	obj.style.display = 'inline';
    } else {
	obj.style.visibility = 'hidden';
	obj.style.display = 'none';
    }
}


function SetElementVisibility(id, visible) {
    var obj = document.getElementById(id);
    SetObjectVisibility(obj, visible);
}


function MoveTo(id, x, y, fit) {
    var elm = document.getElementById(id);
    var winWidth = document.body.clientWidth;
    var winHeight = document.body.clientHeight;

    if (x == -1) x = Math.round((winWidth - elm.offsetWidth) / 2);
    if (y == -1) y = Math.round((winHeight - elm.offsetHeight) / 2) + document.body.scrollTop;
    if (x < 0) x = 0;
    if (y < 0) y = 0;
    if (fit) {
	if (elm.offsetWidth > winWidth)
		elm.style.width = '97%'; //winWidth+'px';
	if (elm.offsetHeight > winHeight)
		elm.style.height = '97%'; //winHeight+'px';
    }
    elm.style.left = x + "px";
    elm.style.top = y + "px";

    return;
}


function MM_preloadImages() { //v3.0
    var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments;
    for (i=0; i < a.length; i++)
	if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}


function MM_swapImgRestore() { //v3.0
    var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0

    var p,i,x;
    
    if(!d)
	d=document;

    if((p = n.indexOf("?")) > 0 && parent.frames.length) {
	d=parent.frames[n.substring(p+1)].document;
	n=n.substring(0,p);
    }

    if(!(x=d[n])&&d.all)
	x=d.all[n];

    for (i = 0; !x && i < d.forms.length; i++)
	x=d.forms[i][n];

    for (i = 0; !x && d.layers && i < d.layers.length; i++)
	x = MM_findObj(n,d.layers[i].document);

    return x;

}

function MM_swapImage() { //v3.0
    var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array;
    for(i=0;i<(a.length-2);i+=3)
	if ((x=MM_findObj(a[i]))!=null) {
	    document.MM_sr[j++]=x;
	    if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];
	}
}


function SubmitForm() {

    obj = MM_findObj("User_ID");
    if (obj.value.length>0) {
	obj1 = MM_findObj("UPassword");
	if (obj1.value.length>0) {
	    FormObj = MM_findObj("AdminForm");
	    FormObj.action=location.href;
	    FormObj.submit();
	} else {
	    alert("Please type in your Password.  Passwords are case sensitive.");
	    obj.focus();
	}
    } else {
	alert("Please type in your User ID");
	obj.focus();
    }

} // end function SubmitForm()


function Branch(newLocation) {
    window.location.href = newLocation;
}

function AskDeleteTrack(strTrackName, strURL) {
    if (confirm("Are you sure you want to permanently remove the class \""+strTrackName+"\"?"))
	window.location.href = strURL;
}


function AskRestoreQuestion(strNumber, strVersion, strURL) {
    if (confirm("Are you sure you want to restore version \""+strVersion+"\" of question #"+strNumber+" ?"))
	window.location.href = strURL;
}


function AskCreateAssignment(strUserName, strTrackName, strURL) {

	if (confirm("Are you sure you want to assign \""+strUserName+"\" to the \""+strTrackName+"\" class?"))
		window.location.href = strURL;

} // end function AskCreateAssignment()


function AskRemoveAssignment(strUserName, strURL) {

	if (confirm("Are you sure you want remove the assignment for \""+strUserName+"\"?"))
		window.location.href = strURL;

} // end function AskRemoveAssignment()


function AskDeleteUser(strUserName, strURL) {

	if (confirm("Are you sure you want to permanently remove the employee \""+strUserName+"\"?\nThis operation will also delete all progress data and user assignments for this employee."))
		window.location.href = strURL;

} // end function AskDeleteUser


function ViewQuestion(bArchived, strNumber, strLanguage, strDomain, strVersion) {

    var ScreenWidth=screen.width;
    var ScreenHeight=screen.height;
    var zeroX=(ScreenWidth/2-400);
    var zeroY=(ScreenHeight/2-300);
    var options = "toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=792,height=545,left="+zeroX+",top="+zeroY;
    
    var strPageToOpen = "question_view.php?get_archived="+bArchived+"&get_question="+strNumber+"&get_language="+strLanguage+"&get_domain="+strDomain+"&get_version="+strVersion;
    window.open(strPageToOpen, "QuestionView", options);

} //end function ViewQuestion()


function BranchTo(strURL) {

    window.location.href = strURL;

} // end function BranchTo()


function swap(fm,to) {

    fm.src = to.src;
    return;

} // end function swap



// ALL THE FOLLOWING FUNCTIONS ARE ONLY USED IN q_type1.php
// THAT IS WHERE THEY WERE OBTAINED
// AS OF JAN 07 q_typeN.php were all consolidated into q_type.php
function ItemSelected (iSelected, theme) {

    var elm;
    var fitToWindow = true;
    var answers = new Array('0', 'A', 'B', 'C', 'D', 'E');

    // If first answer has not yet been chosen then this is it so set it here
    if (firstAnswer == 0) {
	firstAnswer = iSelected;
	if (firstAnswer == strCorrect)
	    correctAnswer = 1;
	else
	    correctAnswer = 2;
    }

    // Final answer is always set because it is well the latest in their final answer
    finalAnswer = iSelected;

    // Hide btnCover and show btnNext but only if they have the correct answer when requiring it
    if (review || !requireCorrectAnswer || finalAnswer == strCorrect)
	ShowNextButton(uiTheme);
    else
	HideNextButton(currentPage, requireCorrectAnswer, theme);

    UpdateFeedbackCloseButton(finalAnswer == strCorrect, theme);

    // popup feedback window.
    // LAME, NO MORE POPUPS FREAKIN ANNOYING LETS DO COOL LAYERED FADE IN AND FADE OUT INSTEAD
//    feedbackWin=window.open('feedback.php?nResult=' + nResult + '&iFirstChoice=' + iFirstChoice +
//			    '&iSelected=' + iSelected + '&currentPage=' + currentPage,'fb',
//			    'dependent=yes,toolbar=no,location=no,status=no,menubar=no,' +
//			    'scrollbars=no,resizable=no,width=508,height=462,screenX=200,screenY=300,');
//    feedbackWin.focus();



    elm = document.getElementById('headerFeedback');
    correctImgElm = document.getElementById('imageFeedbackCorrect');
    incorrectImgElm = document.getElementById('imageFeedbackIncorrect');

    if (finalAnswer == strCorrect) {
	elm.innerHTML = "You selected: <span class=\"Blue\">"+answers[finalAnswer]+"</span>&nbsp;&nbsp;This is the best answer.";
	SetObjectVisibility(incorrectImgElm, false);
	SetObjectVisibility(correctImgElm, true);
	//if (strCorrect_Fdbk.length) {
	//    elm = document.getElementById('correctFeedback');
	//    elm.innerHTML = strCorrect_Fdbk;
	//    SetObjectVisibility(elm, true);
	//}
    } else {
	elm.innerHTML = "You selected: <span class=\"Red\">"+answers[finalAnswer]+"</span>&nbsp;&nbsp;This answer is not correct.";
	SetObjectVisibility(incorrectImgElm, true);
	SetObjectVisibility(correctImgElm, false);
	//	SetObjectVisibility(elm, true);
	//	incorrectImgElm.style.visibility = 'visible';
	//	incorrectImgElm.style.display = 'block';
	//elm = document.getElementById('correctFeedback');
	//elm.innerHTML = strCorrect_Fdbk;
	//SetObjectVisibility(elm, false);
    }

    elm = document.getElementById('foilAnswer');
    elm.innerHTML = eval('strFoil'+finalAnswer);

    elm = document.getElementById('answerFeedback');
    elm.innerHTML = eval('strFoil'+finalAnswer+'_Fdbk');

    // See if this was a video or image question.
    // If so, make it disappear to prevent layering issues
    if (document.getElementById('playa')) {
	el1 = document.getElementById('playa');
	el1.style.visibility = 'hidden';
    }

    // Make presentation window
    el1 = document.getElementById('feedbackVeil');
    el1.style.height=document.body.clientHeight+'px';
    shiftOpacity('feedbackVeil', 65, 500);
    shiftOpacity('feedbackContent', 100, 500);
    MoveTo('feedbackContent', -1, -1, fitToWindow);

//    SabientAlertContentById('feedbackContent', 'feedbackTitle', 'FeedbackContent');
}


function CloseFeedbackWindow(theme) {
    shiftOpacity('feedbackVeil', 65, 500);
    shiftOpacity('feedbackContent', 100, 500);
    setTimeout("document.getElementById('imageFeedbackCorrect').style.visibility = 'hidden';", 500);
    setTimeout("document.getElementById('imageFeedbackCorrect').style.display = 'none';", 500);
    setTimeout("document.getElementById('imageFeedbackIncorrect').style.visibility = 'hidden';", 500);
    setTimeout("document.getElementById('imageFeedbackIncorrect').style.display = 'none';", 500);
    
    if (finalAnswer == strCorrect)
	GoMove(1, uiTheme);
    else // if not the correct answer then show the player again if video player
	if (document.getElementById('playa')) {
	    el1 = document.getElementById('playa');
	    el1.style.visibility = 'visible';
	}
}


function GoHome() {

    window.location.href="portal.php";

} // end function GoHome();


function GoQuestionList() {

    window.location.href="/desktop/admin/question_list.php";

} // end function GoHome();


function GoMove(nMoveDirection, theme){

    //alert("currentPage: " + currentPage + ", nMoveDirection: " + nMoveDirection);

    var strDivWrite = "";

    if(nMoveDirection == 1)
	currentPage++;
    else if(nMoveDirection == -1 && currentPage > 0)
	currentPage--;

    // Start the timer for the expiration of questions
    //if (preview)
    //    window.setTimeout('GoQuestionList()', (nextDelay+600)*1000);
    if (!preview && !review) {
	clearTimeout(pageTimer);
        pageTimer = window.setTimeout('GoHome()', (nextDelay+480)*1000);
    }

    switch (currentPage){
    case 0:
	// If it is a video clip display it so that it will start
	if (document.getElementById('playa')) {
	    SetElementVisibility('playa', true);
	    if (document.getElementById('mediaPlayer'))
		document.mediaPlayer.Play();
	}
	// If there is supposed to be a next delay then setup the timer
	if (nextDelay > 0 && !review) {
	    HideFoils();
	    setTimeout("ShowFoils()", nextDelay*1000);
	} else {
	    ShowFoils();
	}
	ReviseQuestionBar(currentPage, theme);
	document.getElementById("textHeader").innerHTML =
	  '<span class="HeaderText">The Question</span>';
	document.btnBackImage.src = '../themes/'+theme+'/presentation/btn_FAQ_back_dim.png';
	if (!review && ((requireCorrectAnswer && finalAnswer != strCorrect) || firstAnswer == 0))
	  HideNextButton(currentPage, requireCorrectAnswer, theme);
	else
	  ShowNextButton(uiTheme);
	SetElementVisibility('backButtonDiv', false);
	SetElementVisibility('nextButtonDiv', true);
	if (!preview && (progress || nextChild))
	  CreateProgressBar(progress);
	break;
    case 1:
	if (isAb1825) {
	  if (!preview && (progress || nextChild))
	    SetProgressBar('+'+progStep)
	  if (correctAnswer == 2)
	    incorrectInSet = incorrectInSet + 1.0;
	  if (assignmentSetSize > 0.0 && incorrectInSet / assignmentSetSize >= 0.25)
	    RestartAssignment();
	}
	// If it was a video clip, stop the player from playing
	if (document.getElementById('mediaPlayer')) {
	    document.mediaPlayer.Stop();
	}
	if(strPurpose.length == 0) {
	    GoMove(nMoveDirection, uiTheme);
	    return;
	}
	ReviseQuestionBar(currentPage, theme);
	document.getElementById("textHeader").innerHTML =
	  '<span class="HeaderText">Why It\'s Important</span>';
	document.btnBackImage.src = '../themes/'+theme+'/presentation/btn_FAQ_back_up.png';
	SetElementVisibility('backButtonDiv', true);
	if (!review && isAb1825) {
	    nextButtonActive=0;
	    HideNextButton(currentPage, requireCorrectAnswer, theme);
	    setTimeout("nextButtonActive=1;ShowNextButton(uiTheme)", 6000);
	}
	break;
    case 2:
	if (!preview && (progress || nextChild))
	  SetProgressBar('+'+progStep);
	if (strLearnPoints.length == 0) {
	    GoMove(nMoveDirection, uiTheme);
	    return;
	}
	document.getElementById("textHeader").innerHTML =
	  '<span class="HeaderText">Key Learning Points</span>';
	if (!review && isAb1825) {
	    nextButtonActive=0;
	    HideNextButton(currentPage, requireCorrectAnswer, theme);
	    setTimeout("nextButtonActive=1;ShowNextButton(uiTheme)", 7000);
	}
	break;
    case 3:
	if (!preview && (progress || nextChild))
	  SetProgressBar('+'+progStep);
	if(strSupport_Info.length == 0) {
	    GoMove(nMoveDirection, uiTheme);
	    return;
	}
	ReviseQuestionBar(currentPage, theme);
	document.getElementById("textHeader").innerHTML =
	  '<span class="HeaderText">Support Information</span>';
	if (!review && isAb1825) {
	    nextButtonActive=0;
	    HideNextButton(currentPage, requireCorrectAnswer, theme);
	    setTimeout("nextButtonActive=1;ShowNextButton(uiTheme)", 6000);
	}
	break;
    case 4:
	if (!preview && (progress || nextChild))
	  SetProgressBar('+'+progStep);
	SetElementVisibility('nextButtonDiv', true);
	ReviseQuestionBar(currentPage, theme);
	if(strNotes.length == 0) {
		GoMove(nMoveDirection, uiTheme);
		return;
	}
	document.getElementById("textHeader").innerHTML =
	  '<span class="HeaderText">Additional Comments</span>';
	if (!review && isAb1825) {
	    nextButtonActive=0;
	    HideNextButton(currentPage, requireCorrectAnswer, theme);
	    setTimeout("nextButtonActive=1;ShowNextButton(uiTheme)", 6000);
	}
	break;
    case 5:
	if (!preview && (progress || nextChild))
	  SetProgressBar('+'+progStep);
	// If this is a next child to go to then go there instead
	// of presenting the thank you page
	if (nextChild) {
	   exitInfo=1;
	   HideNextButton(currentPage, requireCorrectAnswer, theme);
	   SubmitAnswer();
	   return;
	}	
	SetElementVisibility('nextButtonDiv', false);
	ReviseQuestionBar(currentPage, theme);
	//if(strMode=='review') {
	//    GoHome();
	//    return;
	//}
	document.getElementById("textHeader").innerHTML =
	  '<span class="HeaderText">Thank You!</span>';
	break;
    }

    if (lastPage != currentPage)
      shiftOpacity('page_'+lastPage, 100, 500);
    shiftOpacity('page_'+currentPage, 100, 500);
    lastPage = currentPage;
}



// Force the user to answer the question before returning to the desktop
function AlertNoReturn() {

    alert("Please complete the question to return to the desktop!");

} // end function AlertNoReturn()



// Notify the user that they must select the correct anwer before continuing
function NotifyAnswerFirst() {
    
    alert("Please select the correct answer before proceeding!");

} // end function NotifyAnswerFirst


function CreateNewFormElement(formObj, elmName, elmValue) {
    var formElm = null;

    try {
	formElm = document.createElement('<input name="' + elmName + '" value="' + elmValue + '" />');
    } catch (e) {
	formElm = document.createElement('input');
	formElm.setAttribute('name', elmName);
	formElm.setAttribute('value', elmValue);
    }
    formObj.appendChild(formElm);
}

function SubmitAnswer() {
    var answerFormObj = null;
    var msgSubj = null;
    var msgBody = null;

    // Clear the page timer to prevent any funny business after completing a question
    // Of course, if submitted from portal pageTimer may not exist
    // so a try-catch block seems like a good idea
    try {
	if (pageTimer)
	  clearTimeout(pageTimer);
    } catch (err) { }

    answerFormObj = document.createElement("FORM");
    answerFormObj.style.visibility = 'hidden';
    answerFormObj.style.display = 'none';
    document.body.appendChild(answerFormObj);
    answerFormObj.method = 'POST';
    answerFormObj.action = 'thanks.php';
    if (!review && !preview) {
	CreateNewFormElement(answerFormObj, 'assignmentId', assignmentId);
	CreateNewFormElement(answerFormObj, 'firstAnswer', firstAnswer);
	CreateNewFormElement(answerFormObj, 'finalAnswer', finalAnswer);
	CreateNewFormElement(answerFormObj, 'correctAnswer', correctAnswer);
	CreateNewFormElement(answerFormObj, 'exitInfo', exitInfo);
    }
    // When using contact link on the portal or in other word desktop page there
    // is no nextChild variable defined so this must be done instead
    try {
	if (typeof(nextChild) == 'number')
	  CreateNewFormElement(answerFormObj, 'nextChild', nextChild);
    } catch (err) { }

    // Same goes for startTime
    try {
	if (typeof(startTime) == 'number')
	  CreateNewFormElement(answerFormObj, 'startTime', startTime);
    } catch (err) { }

    if (review) CreateNewFormElement(answerFormObj, 'review', review);
    if (preview) CreateNewFormElement(answerFormObj, 'preview', preview);
    msgToName = document.getElementById('msgTxtToName');
    msgToAddr = document.getElementById('msgTxtToAddr');
    msgSubj = document.getElementById('msgFormSubject');
    msgBody = document.getElementById('msgFormBody');

    if (msgSubj.value.length && msgBody.value.length) {
	CreateNewFormElement(answerFormObj, 'msgToName', msgToName.innerHTML);
	CreateNewFormElement(answerFormObj, 'msgToAddr', msgToAddr.innerHTML);
	CreateNewFormElement(answerFormObj, 'msgSubject', msgSubj.value);
	CreateNewFormElement(answerFormObj, 'msgBody', msgBody.value);
    }

    answerFormObj.submit();
}


function RestartAssignment() {

    alert("In order to proceed, you must get at least 75% of\r\nthese questions correct with your first response.\r\nYou now have an opportunity to restart this set of questions.");
    window.location.href='restart.php?assignmentId='+assignmentId;
}


function DisplayContacts() {
    var fitToWindow = true;

    // Make presentation window
    shiftOpacity('feedbackVeil', 65, 500);
    shiftOpacity('contactsContent', 100, 500);
    MoveTo('contactsContent', -1, -1, fitToWindow);
}


function HideContacts() {

    // Make presentation window
    shiftOpacity('contactsContent', 100, 500);
    shiftOpacity('feedbackVeil', 65, 500);
}


function DisplayFaq(answerHtml) {

    var fitToWindow = true;
    var elm = document.getElementById('answerContent');
    
    elm.innerHTML = answerHtml;
    
    // Make presentation window
    shiftOpacity('feedbackVeil', 65, 500);
    shiftOpacity('faqContent', 100, 500);
    MoveTo('faqContent', -1, -1, fitToWindow);
}


function HideFaq() {

    // Make presentation window
    shiftOpacity('faqContent', 100, 500);
    shiftOpacity('feedbackVeil', 65, 500);
}


function DisplayMailForm(toName, toAddress) {
    var fitToWindow = true;
    var elm = document.getElementById('msgTxtToName');
    elm.innerHTML = toName;
    elm = document.getElementById('msgTxtToAddr');
    elm.innerHTML = toAddress;

    // Make presentation window
    shiftOpacity('contactsContent', 100, 500);
    shiftOpacity('msgFormContent', 100, 500);
    MoveTo('msgFormContent', -1, -1, fitToWindow);
}


function HideMailForm() {
    var fitToWindow = true;

    // Make presentation window
    shiftOpacity('msgFormContent', 100, 500);
    shiftOpacity('feedbackVeil', 65, 500);
}


// THESE FUNCTIONS ARE USED THROUGHOUT THE PRESENTATION INTERFACE
// AND WERE ADDED BY JB
function ShowNextButton(theme) {

    elm = document.getElementById('btnNextLink');

    // Update the next button image and destination URL
    document.getElementById("btnNextImage").src='../themes/'+theme+'/presentation/btn_continue_up_greenbg.png';
    document.getElementById("btnNextLink").href="javascript:GoMove(1, uiTheme);";
    
} // end function ShowNextButton()


function HideNextButton(currentPage, requireCorrect, theme) {

    // Update the next button image and destination URL
    document.getElementById('btnNextImage').src='../themes/'+theme+'/presentation/btn_continue_dim_greenbg.png';
    if (currentPage == 0) {
      if (requireCorrect)
        document.getElementById("btnNextLink").href="javascript:alert('This question requires the correct answer before proceeding.');";
      else
        document.getElementById("btnNextLink").href="javascript:alert('Please select an answer to proceed.');";
    } else if (isAb1825) {
      document.getElementById("btnNextLink").href="javascript:alert('Please ensure you have read all content, then wait for the continue button to appear.');";
    } else {
      document.getElementById("btnNextLink").href="javascript:alert('Please follow the instructions below to proceed.');";
    }

} // end function HideNextButton()


function EnableFeedbackContinue() {

    elm = document.getElementById('divImgFeedbackClose');
    elm.innerHTML = "<img src=\"../themes/"+uiTheme+"/presentation/btn_continue_up_whitebg.png\" type=\"button\" name=\"closeFeedbackWindow\" id=\"closeFeedbackWindow\" \
         onmouseout=\"MM_swapImage('closeFeedbackWindow', '', '../themes/"+uiTheme+"/presentation/btn_continue_up_whitebg.png', 1);\" \
         onmouseover=\"MM_swapImage('closeFeedbackWindow', '', '../themes/"+uiTheme+"/presentation/btn_continue_down_whitebg.png', 1);\" \
         onclick=\"CloseFeedbackWindow(uiTheme);\">";

} // end function HideNextButton()


function ShowFoils() {

    for(i = 1; i <= 5; i++) {
	elm=document.getElementById('foil'+i);
	if (elm) {
	   elm.disabled=false;
	   elm.style.visibility='visible';
	}
	elm=document.getElementById('foilspan'+i);
	if (elm) {
	   elm.style.visibility='visible';
	}
	elm=document.getElementById('foillabel'+i);
	if (elm) {
	   elm.style.visibility='visible';
	}
    }
}


function HideFoils() {

    for(i = 1; i <= 5; i++) {
	elm=document.getElementById('foil'+i);
	if (elm) {
//	   elm.disabled=false;
	   elm.style.visibility='hidden';
	}
	elm=document.getElementById('foilspan'+i);
	if (elm) {
	   elm.style.visibility='hidden';
	}
	elm=document.getElementById('foillabel'+i);
	if (elm) {
	   elm.style.visibility='hidden';
	}
    }
}


function ReviseQuestionBar(currentPage, theme) {

    // Hide the question bar
    currentUrl = document.getElementById("qbarLeft").style.backgroundImage;

    if (currentPage == 0 && currentUrl != 'url(../themes/'+theme+'/presentation/qbar_left_long.png)')
	document.getElementById("qbarLeft").style.backgroundImage='url(../themes/'+theme+'/presentation/qbar_left_long.png)';
    else if (currentPage == 5 && currentUrl != 'url(../themes/'+theme+'/presentation/qbar_left_long_empty_with_seal.png)')
	document.getElementById("qbarLeft").style.backgroundImage='url(../themes/'+theme+'/presentation/qbar_left_long_empty_with_seal.png)';
    else if (currentPage > 0 && currentPage < 5 && currentUrl != 'url(../themes/'+theme+'/presentation/qbar_left_long_empty.png)')
	document.getElementById("qbarLeft").style.backgroundImage='url(../themes/'+theme+'/presentation/qbar_left_long_empty.png)';
    
} // end function HideQuestionBar


function SetImage(id, imagePath, finalAnswer, localCorrect, requireCorrectAnswer) {

    var elm = null;

    if (!nextButtonActive)
	return true;

    // If its the first page and it's the Back button then don't change the image
    if (currentPage == 0 && id.indexOf("Back") >= 0)
	return true;

    // If its the first page and a choice has not yet been selected then don't change the image
    if (currentPage == 0 && finalAnswer == 0)
	return true;

    // If its the last page and its the next button, then don't change the image
    if (id.indexOf("Next") >= 0) {
       if (currentPage >= 5 || (requireCorrectAnswer && finalAnswer != localCorrect && !review))
	  return true;
	//HideNextButton();
    }

    elm = document.getElementById(id);
    elm.src=imagePath;
    
    return true;
}


function ToggleCheckBoxes(baseId, checkCount, checked) {

  var i = 0;
  var elm = null;
  var id = null;

  for(i=1; i <= checkCount; i++) {
    id = baseId + i;
    elm = document.getElementById(id);
    //    if (elm.checked)
    if (checked)
      elm.checked=true;
    else
      elm.checked=false;
  }

}


function PrepareSelectedQuestions(url, prefix, start, end) {

    var strLoc = url;
    var selCnt = 0;
    var i = 0;

    for(i=start; i <= end; i++) {
	elm = document.getElementById(prefix+i);
	if (elm) {
	    if (elm.checked) {
		if (selCnt == 0)
		    strLoc = strLoc + '?aId[]=' + elm.value;
		else
		    strLoc = strLoc + '&aId[]=' + elm.value;
		selCnt = selCnt + 1;
	    }
	}
    }
    if (selCnt == 0)
	alert('You did not select any questions for printing\nPlease select some questions first');
    else
	parent.window.location = strLoc;
}


function ResizeFrameFull(frameName) {
    //    alert("frameName=" + frameName);
  elm = document.getElementById(frameName);
  elmDiv = frames.eval(frameName).document.getElementById("contentArea");
  if (document.all) // IE problem; wont show clientHeight if style height hasn't been set
    elmDiv.style.height = "1px";
  margin = 30;
  elm.style.height = parseInt(elmDiv.clientHeight + margin) + "px";
}


function UpdateFeedbackCloseButton(correct, theme) {

  elm = document.getElementById('divImgFeedbackClose');

  if (correct) {
    if (isAb1825 && progress > 0 && !review) {
      elm.innerHTML = "<img src=\"../themes/"+theme+"/presentation/btn_continue_dim.png\" type=\"button\" name=\"closeFeedbackWindow\" id=\"closeFeedbackWindow\" \
           onclick=\"alert('Please take a moment to review this very important information,\\r\\nthen wait until this button is enabled.');\">";
      setTimeout("EnableFeedbackContinue()", 13000);
    } else {
      elm.innerHTML = "<img src=\"../themes/"+theme+"/presentation/btn_continue_up_whitebg.png\" type=\"button\" name=\"closeFeedbackWindow\" id=\"closeFeedbackWindow\" \
           onmouseout=\"MM_swapImage('closeFeedbackWindow', '', '../themes/"+theme+"/presentation/btn_continue_up_whitebg.png', 1);\" \
           onmouseover=\"MM_swapImage('closeFeedbackWindow', '', '../themes/"+theme+"/presentation/btn_continue_down_whitebg.png', 1);\" \
           onclick=\"CloseFeedbackWindow(uiTheme);\">";
    }
  } else {
    elm.innerHTML = "<img src=\"../themes/"+theme+"/presentation/btn_try_again_up.png\" type=\"button\" name=\"closeFeedbackWindow\" id=\"closeFeedbackWindow\" \
         onmouseout=\"MM_swapImage('closeFeedbackWindow', '', '../themes/"+theme+"/presentation/btn_try_again_up.png', 1);\" \
         onmouseover=\"MM_swapImage('closeFeedbackWindow', '', '../themes/"+theme+"/presentation/btn_try_again_down.png', 1);\" \
         onclick=\"CloseFeedbackWindow(uiTheme);\">";
  }
}


function CreateProgressBar(prog) {
  manPB = new JS_BRAMUS.jsProgressBar(
		$('element6'),
		prog,
		{
			barImage	: Array(
						'bramus/percentImage_back4.png',
						'bramus/percentImage_back3.png',
						'bramus/percentImage_back2.png',
						'bramus/percentImage_back1.png'
					),
			animate: false
		}
	);
}


function SetProgressBar(amnt) {
  manPB.setPercentage(amnt);
}


function findPos(obj) {
 var obj2 = obj;
 var curtop = 0;
 var curleft = 0;
 if (document.getElementById || document.all) {
  do  {
   curleft += obj.offsetLeft-obj.scrollLeft;
   curtop += obj.offsetTop-obj.scrollTop;
   obj = obj.offsetParent;
   obj2 = obj2.parentNode;
   while (obj2!=obj) {
    curleft -= obj2.scrollLeft;
    curtop -= obj2.scrollTop;
    obj2 = obj2.parentNode;
   }
  } while (obj.offsetParent)
 } else if (document.layers) {
  curtop += obj.y;
  curleft += obj.x;
 }
 return [curtop, curleft];
}   // end of findPos()


function findPositionX(obj)
{
  var left = 0;
  if(obj.offsetParent)
  {
    while(1) 
    {
      left += obj.offsetLeft;

      if(!obj.offsetParent)
        break;

      obj = obj.offsetParent;

    }
  }
  else if(obj.x)
  {
    left += obj.x;
  }

  return left;
}
 

function findPositionY(obj) {
  var top = 0;

  if(obj.offsetParent)
  {
    while(1)
    {
      top += obj.offsetTop;
      if(!obj.offsetParent)
        break;
      obj = obj.offsetParent;
    }
  }
  else if(obj.y)
  {
    top += obj.y;
  }

  return top;
}


function CheckDisplayLoginBlock(login_block_id, display_target_id, showFlag) {
  var lbi = document.getElementById(login_block_id);
  var dti = document.getElementById(display_target_id);

  var coordLeft = findPositionX(dti);
  var coordTop = findPositionY(dti);

  if (!lbi && !dti)
    return;

  //list(coordLeft, coordTop) = findPos(dti);
  lbi.offsetLeft = coordLeft;
  lbi.offsetTop = coordTop;
  if (showFlag && lbi.style.visibility == 'hidden') {
    lbi.style.visibility = 'visible';
    lbi.style.display = 'inline';
  }
  
  if (!showFlag && lbi.style.visibility == 'visible') {
    lbi.style.visibility = 'hidden';
    lbi.style.display = 'none';
  }
}


// This function handles the special case where a username was changed
// then changed back to its original default value when a form was previously
// submitted with perhaps the incorrect organization but the correct username
// and password but the username still does change and must update the orgs.
function loginUserIdChange(elName) {

  elm = document.getElementById(elName);

  if (elm.value == elm.defaultValue && elm.value.length > 0)
    svcGetUserOrgs('User_ID', true);
}


function svcGetUserOrgs(elem) {
    // Get element based on its name
    elm = document.getElementById(elem);
    elmSel = document.getElementById('Org_ID');
    orgSel = document.getElementById('orgSelect');

    if (!svc) {
	alert("Fatal Error: Could not check login, svc is not defined.\nPlease contact support.");
	return;
    }

    // No action when no value
    if (elm.value.length == 0)
	return;

    elmSel.options.length = 0;
    orgSel.style.visibility="hidden";
    var lblOrg = document.getElementById('lbl_Org');
    lblOrg.style.visibility="hidden";
	
    // Now actually make the service call to remove this user.
    // If it fails then we will warn the user and put the row back.
    svc.getUserOrgs({params:[elm.value],
		    onSuccess: function(orgs) {
			var count = 0;
			var elm = document.getElementById('Org_ID');
			var lbl = document.getElementById('lbl_Org');
			var blk = document.getElementById('orgSelect');
			for (var prop in orgs) if (orgs.hasOwnProperty(prop)) count++;
			if (count == 1)
			  elm.options[elm.length] = new Option(orgs[prop], prop, true, true);
			else if (count > 1 || count == 0) {
			  elm.options[elm.length] = new Option("Please Select", -1, true, false);
			  for (var prop in orgs)
			  {
			    elm.options[elm.length] = new Option(orgs[prop], prop, false, false);
			  }
			  if (count > 1) {
			    //elm.disabled=false;
			    blk.style.visibility="visible";
			    blk.style.display="block";
			    lbl.style.visibility="visible";
			    lbl.style.display="block";
			  }
			}
		    },
		    onException:function(errObj) {
		      alert("Unable to lookup user orgs for username["+elm.value+"]: " + errObj);
		    }
		});
    return false;
}
