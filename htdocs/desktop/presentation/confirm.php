<?php

require_once('../includes/common.php');
require_once('../includes/db.php');

$Link = NULL;

OpenDatabase();

//$_SESSION['nResult'];
//$_SESSION['iStudentAnswered'];

// Get the confirmation caption from the db in the customizations section
//if ($DEBUG) {
//  echo "<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>\n";
//  print_r($_SESSION);
//}

$errMsg = '';
$sqlStr = "SELECT CapConfirmThanks FROM Question WHERE Question_Number = $_SESSION[qNumber]";

$sqlRslt = mysql_db_query($_SESSION['dbName'], $sqlStr, $Link);
if (!$sqlRslt) {
  $errMsg .= "Unable to retrieve question info from database, please contact your representative immediately<BR>\n";
  error_log("confirm.php: SQL(".mysql_errno($Link)."): ".mysql_error());
} else {

  $sqlRow = mysql_fetch_row($sqlRslt);
  
  if ($sqlRow[0] != '') {
    $smarty->assign('confirmCapThanks', $sqlRow[0]);
  } else {
    $smarty->assign('confirmCapThanks', "Thank you for completing today's training.");
  }

}

CloseDatabase($Link);

$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('presentation/confirm.tpl');
  

?>
