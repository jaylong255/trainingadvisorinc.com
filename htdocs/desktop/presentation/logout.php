<?php

require_once('../includes/common.php');
require_once('../includes/Config.php');
require_once('../includes/Assignment.php');

$a = new Assignment($_SESSION['orgId']);
$questions = $a->GetIncompleteAssignments($_SESSION['userId']);
if ($questions === RC_QUERY_FAILED) {

  $smarty->assign('statusMsg', 'Thank you for using Training Advisor!');

} else {

  if (count($questions))
    $smarty->assign('statusMsg', 'You are not current in your training.  '
		    .'The "Assigned" tab contains questions you need to answer.');
  else
    $smarty->assign('statusMsg', 'You are current in your training.  Thank you for using Training Advisor.');
  
}


if ($_SESSION['orgId'] == 405 || $_SESSION['orgId'] == 404) {
  $smarty->assign('loginUrl', 'phi_login.php');
} else {
  $smarty->assign('loginUrl', 'login.php');
}



//CloseDatabase();
$config = new Config($_SESSION['orgId']);
SetupMotifDisplay($smarty, $config);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('presentation/logout.tpl');

?>
