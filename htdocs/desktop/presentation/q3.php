<?php
	session_start();
?>
<html>
<head>
<style type="text/css">
	.QuestionText {  font-family: Arial, Helvetica, sans-serif; font-size:12pt; color:#000000; }
	.FoilText {  font-family: Arial, Helvetica, sans-serif; font-size:12pt; color:#000000; }
</style>
</head>
<body>
<?php
	if($session_ReviewMode!='admin')
	{
		$session_nResult=$nResult;
		$session_iFirstChoice=$iFirstChoice;
		//$session_iSelected=$iSelected;
		$session_iCurrentPage=$iCurrentPage;

		session_register("session_nResult");
		session_register("session_iFirstChoice");
		//session_register("session_iSelected");
		session_register("session_iCurrentPage");
	}
?>
<div id="theQuestion" style="position:absolute; left:24px; top:5px; width:325px; height:100px; z-index:2">
  <script language="JavaScript">
  		var strMediaPath='../organizations/<?php echo $session_Organization_Directory ?>/motif/'+parent.strMedia;
		if ( document.all )
		{
			strObjectToWrite = '<object id="MMF" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="320px" height="240px">\n';
			strObjectToWrite += '<param name="movie" value="'+strMediaPath+'">\n';
			strObjectToWrite += '<param name="quality" value="high">\n';
			strObjectToWrite += '<param name="play" value="true">\n';
			strObjectToWrite += '<param name="bgcolor" value="#FFFFFF">\n';
			strObjectToWrite += '</object>';
		}
		else
		{
			strObjectToWrite = '<embed name="MMF" src="'+strMediaPath+'" play="true" quality="high" swliveconnect="true" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="320px" height="240px" bgcolor="#FFFFFF"></embed>\n';
		}
		document.writeln(strObjectToWrite+"<BR>");
		document.writeln('<span class="QuestionText">' + parent.strStem + '</span>');
  </script>
</div>

<div id="theFoils" style="position:absolute; left:375px; top:5px; width:375px; height:100px; z-index:2">
  <span class="FoilText">
  <form id="foils">
  <table border="0" cellpadding="3" cellspacing="5">
  		<script language="JavaScript">
  			if (parent.strFoil1.length > 0){
  				document.writeln('<tr valign="top" align="left"><td width="12%"><input type="radio" id="foil1" name="foil" onclick="javascript:parent.itemSelected(1);"><label for="foil1"><b>A.</b></label></td>');
  				document.writeln('<td><label for="foil1">' + parent.strFoil1 + '</label></td></tr>');
  			}
  			if (parent.strFoil2.length > 0){
				document.writeln('<tr valign="top" align="left"><td><input type="radio" id="foil2" name="foil" onclick="javascript:parent.itemSelected(2);"><label for="foil2"><b>B.</b></label></td>');
  				document.writeln('<td><label for="foil2">' + parent.strFoil2 + '</label></td></tr>');
  			}
  			if (parent.strFoil3.length > 0){
				document.writeln('<tr valign="top" align="left"><td><input type="radio" id="foil3" name="foil" onclick="javascript:parent.itemSelected(3);"><label for="foil3"><b>C.</b></label></td>');
  				document.writeln('<td><label for="foil3">' + parent.strFoil3 + '</label></td></tr>');
  			}
  			if (parent.strFoil4.length > 0){
				document.writeln('<tr valign="top" align="left"><td><input type="radio" id="foil4" name="foil" onclick="javascript:parent.itemSelected(4);"><label for="foil4"><b>D.</b></label></td>');
  				document.writeln('<td><label for="foil4">' + parent.strFoil4 + '</label></td></tr>');
  			}
  			if (parent.strFoil5.length > 0){
				document.writeln('<tr valign="top" align="left"><td><input type="radio" id="foil5" name="foil" onclick="javascript:parent.itemSelected(5);"><label for="foil5"><b>E.</b></label></td>');
  				document.writeln('<td><label for="foil5">' + parent.strFoil5 + '</label></td></tr>');
  			}
  		</script>
  </table>
  </form>
  </span>
</div>

</body>
</html>