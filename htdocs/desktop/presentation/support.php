<?php

require_once('../includes/common.php');


if ($_SESSION['reviewMode'] != 'admin') {
  
  if (is_numeric($_REQUEST['nResult']))
    $_SESSION['nResult'] = $_REQUEST['nResult'];

  if (is_numeric($_REQUEST['iFirstChoice']))
    $_SESSION['iFirstChoice'] = $_REQUEST['iFirstChoice'];

  //$session_iSelected=$iSelected;
  $_SESSION['iCurrentPage'] = $_REQUEST['iCurrentPage'];
  
  //session_register("session_iSelected");
}

$smarty->assign('jsName', 'strSupport_Info');
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('presentation/generic_html.tpl');
?>