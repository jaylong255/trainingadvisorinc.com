<?php
	session_start();
?>

<html>
<head>
<title>Question</title>
<script language="JavaScript">

// Get all vars for entire question sequence.
<?php
	$Link;
	$session_Host;
	$session_User;
	$session_Password;
	$session_DBName;
	$session_User_ID;
	$session_Language_ID;
	$session_State_ID;
	$session_iSelected;
	$session_iFirstChoice;
	$session_iCurrentPage;
	$session_nResult;

	if($R!="review" && $R!="tracking" && $R!="admin")
	{
		$R=$session_ReviewMode;
		$Q=$session_Question_Number;
		$D=$session_Question_Domain_ID;
		$L=$session_Question_Language_ID;
		$T=$session_Question_Track_ID;
	}
	else
	{
		$session_ReviewMode=$R;
		session_register("session_ReviewMode");
	}

	function OpenDatabase()
	{
		global $session_Host;
		global $session_User;
		global $session_Password;
		global $Link;

		$Link = mysql_connect($session_Host,$session_User,$session_Password);
	}

	function CloseDatabase()
	{
		global $Link;
		mysql_close($Link);
	}

	OpenDatabase();

	$Query = "SELECT Notes, Version_Date, Content, Support_Info, Domain_ID, Language_ID FROM Question WHERE Question_Number=$Q AND Language_ID=$L AND Domain_ID=$D";

	$Result = mysql_db_query($session_DBName,$Query,$Link);
	if($Result)
	{
		if($Row = mysql_fetch_array($Result))
		{
			print($Row["Content"].'var strSupport_Info="'.$Row["Support_Info"].'";var strNotes="'.$Row["Notes"].'";');
			$session_Question_Number = $Q;
			$session_Version_Date = $Row['Version_Date'];
			$session_Question_Domain_ID = $Row['Domain_ID'];
			$session_Question_Language_ID = $Row['Language_ID'];
			session_register("session_Version_Date");
			session_register("session_Question_Number");
			session_register("session_Question_Domain_ID");
			session_register("session_Question_Language_ID");
			$session_Question_Track_ID = $T;
			session_register("session_Question_Track_ID");
		}
		else
		{
			print("// Error retrieving question. Error Processing Results.");
		}
	}
	else
	{
		print("// Error retrieving question. No Results in Query.");
	}
	CloseDatabase();
?>

var strMode = '<?php echo $R ?>';
var nResult=<?php echo $session_nResult; ?>;
var iFirstChoice = <?php echo $session_iFirstChoice ?>;
var iStudentAnswer = <?php echo $session_iSelected ?>;
var iCurrentPage = <?php echo $session_iCurrentPage ?>;	// 0=Question Frame 1=Why It's Important 2=Key Learning Points 3=Additional HR Comments 4=Thank You

function itemSelected(iSelected){
	iStudentAnswer = iSelected;

	// Score first time selection
	if (iFirstChoice == 0)
	{
		iFirstChoice = iSelected;
		if (iStudentAnswer == strCorrect)
		{
			nResult=1;
		}
	}

	// Hide btnCover and show btnNext
	if (iStudentAnswer == strCorrect){
		document.getElementById("btnNextCover").style.visibility = "hidden";
		document.getElementById("btnNext").style.visibility = "visible";
	}

	// popup feedback window.
	feedbackWin=window.open('feedback.php?nResult='+nResult+'&iFirstChoice='+iFirstChoice+'&iSelected='+iSelected+'&iCurrentPage='+iCurrentPage,'fb','dependent=yes,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=508,height=462,screenX=200,screenY=300,');
	feedbackWin.focus();
}

function goHome(){
	parent.window.location.href="portal.php";
}

function goMove(nMoveDirection){
	var strDivWrite = "";

	if(nMoveDirection == 1)
	{
		iCurrentPage++;
	}
	else if(nMoveDirection == -1 && iCurrentPage > 0)
	{
		iCurrentPage--;
	}

	switch (iCurrentPage){
		case 0:
			// Hide layers
			document.getElementById("textHeader").style.visibility = "hidden";
			document.getElementById("btnNext").style.visibility = "hidden";
			document.getElementById("btnNextCover").style.visibility = "hidden";
			document.getElementById("btnBack").style.visibility = "hidden";
			document.getElementById("textInstruction").style.visibility = "hidden";
			document.getElementById("navText").style.visibility = "hidden";
			document.getElementById("graphic").style.visibility = "visible";

			// Move textHeader down and Next button up
			document.getElementById("textHeader").style.top = "74px";

			// Write new layer for textHeader and btnNext
			strDivWrite = '<span class="HeaderText">The Question</span>';
			document.getElementById("textHeader").innerHTML = strDivWrite;
			strDivWrite = '<a href="javascript: goMove(1);" onmouseout="document.images.btnNextImage.src=\'graphics/btn_FAQ_next_up.gif\' " onmouseover="document.images.btnNextImage.src=\'graphics/btn_FAQ_next_down.gif\' "><img name="btnNextImage" id="btnNextImage" src="graphics/btn_FAQ_next_up.gif" alt="" border="0" /></a>';
			document.getElementById("btnNext").innerHTML = strDivWrite;
			if(strMode=='review')
			{
				strDivWrite = '<a href="javascript: goHome();" onmouseout="document.images.btnMenuImage.src=\'graphics/btn_FAQ_menu_up.gif\' " onmouseover="document.images.btnMenuImage.src=\'graphics/btn_FAQ_menu_down.gif\' "><img name="btnMenuImage" id="btnMenuImage" src="graphics/btn_FAQ_menu_up.gif" alt="" border="0" /></a>';
				document.getElementById("btnMenu").innerHTML = strDivWrite;
			}

			// Show layers.
			document.getElementById("textHeader").style.visibility = "visible";
			document.getElementById("btnBackCover").style.visibility = "visible";
			document.getElementById("btnNext").style.visibility = "visible";

			parent.window.BodyInfo.location.href='q4.php?nResult=' + nResult + '&iFirstChoice=' + iFirstChoice + '&iCurrentPage=' + iCurrentPage;
		break;
		case 1:
			// Hide layers
			document.getElementById("textHeader").style.visibility = "hidden";
			document.getElementById("btnNext").style.visibility = "hidden";
			document.getElementById("btnNextCover").style.visibility = "hidden";
			document.getElementById("btnBackCover").style.visibility = "hidden";
			document.getElementById("textInstruction").style.visibility = "hidden";
			document.getElementById("graphic").style.visibility = "hidden";

			// Move textHeader down and Next button up
			document.getElementById("textHeader").style.top = "100px";

			// Write new layer for textHeader and btnNext
			strDivWrite = '<span class="HeaderText">Why It\'s Important</span>';
			document.getElementById("textHeader").innerHTML = strDivWrite;
			strDivWrite = '<a href="javascript: goMove(1);" onmouseout="document.images.btnNextImage.src=\'graphics/btn_FAQ_wht_next_up.gif\' " onmouseover="document.images.btnNextImage.src=\'graphics/btn_FAQ_wht_next_down.gif\' "><img name="btnNextImage" id="btnNextImage" src="graphics/btn_FAQ_wht_next_up.gif" alt="" border="0" /></a>';
			document.getElementById("btnNext").innerHTML = strDivWrite;
			strDivWrite = '<a href="javascript: goMove(-1);" onmouseout="document.images.btnBackImage.src=\'graphics/btn_FAQ_wht_back_up.gif\' " onmouseover="document.images.btnBackImage.src=\'graphics/btn_FAQ_wht_back_down.gif\' "><img name="btnBackImage" id="btnBackImage" src="graphics/btn_FAQ_wht_back_up.gif" alt="" border="0" /></a>';
			document.getElementById("btnBack").innerHTML = strDivWrite;
			strDivWrite = '<img name="Cover" id="Cover" src="graphics/btn_FAQ_wht_next_dim.gif" alt="" border="0" />';
			document.getElementById("btnNextCover").innerHTML = strDivWrite;
			if(strMode=='review')
			{
				strDivWrite = '<a href="javascript: goHome();" onmouseout="document.images.btnMenuImage.src=\'graphics/btn_FAQ_wht_menu_up.gif\' " onmouseover="document.images.btnMenuImage.src=\'graphics/btn_FAQ_wht_menu_down.gif\' "><img name="btnMenuImage" id="btnMenuImage" src="graphics/btn_FAQ_wht_menu_up.gif" alt="" border="0" /></a>';
				document.getElementById("btnMenu").innerHTML = strDivWrite;
			}

			// Show layers.
			document.getElementById("textHeader").style.visibility = "visible";
			document.getElementById("btnNext").style.visibility = "visible";
			document.getElementById("btnBack").style.visibility = "visible";
			document.getElementById("navText").style.visibility = "visible";

			// Change iframe layer
			parent.window.BodyInfo.location.href='why.php?nResult=' + nResult + '&iFirstChoice=' + iFirstChoice + '&iCurrentPage=' + iCurrentPage;

		break;
		case 2:
			document.getElementById("textHeader").style.visibility = "hidden";
			document.getElementById("btnNext").style.visibility = "hidden";
			document.getElementById("btnNextCover").style.visibility = "hidden";
			document.getElementById("btnBackCover").style.visibility = "hidden";
			document.getElementById("textInstruction").style.visibility = "hidden";
			document.getElementById("graphic").style.visibility = "hidden";

			// Move textHeader down and Next button up
			document.getElementById("textHeader").style.top = "100px";

			strDivWrite = '<a href="javascript: goMove(1);" onmouseout="document.images.btnNextImage.src=\'graphics/btn_FAQ_wht_next_up.gif\' " onmouseover="document.images.btnNextImage.src=\'graphics/btn_FAQ_wht_next_down.gif\' "><img name="btnNextImage" id="btnNextImage" src="graphics/btn_FAQ_wht_next_up.gif" alt="" border="0" /></a>';
			document.getElementById("btnNext").innerHTML = strDivWrite;
			strDivWrite = '<a href="javascript: goMove(-1);" onmouseout="document.images.btnBackImage.src=\'graphics/btn_FAQ_wht_back_up.gif\' " onmouseover="document.images.btnBackImage.src=\'graphics/btn_FAQ_wht_back_down.gif\' "><img name="btnBackImage" id="btnBackImage" src="graphics/btn_FAQ_wht_back_up.gif" alt="" border="0" /></a>';
			document.getElementById("btnBack").innerHTML = strDivWrite;
			if(strMode=='review')
			{
				strDivWrite = '<a href="javascript: goHome();" onmouseout="document.images.btnMenuImage.src=\'graphics/btn_FAQ_wht_menu_up.gif\' " onmouseover="document.images.btnMenuImage.src=\'graphics/btn_FAQ_wht_menu_down.gif\' "><img name="btnMenuImage" id="btnMenuImage" src="graphics/btn_FAQ_wht_menu_up.gif" alt="" border="0" /></a>';
				document.getElementById("btnMenu").innerHTML = strDivWrite;
			}

			// Show layers.
			document.getElementById("textHeader").style.visibility = "visible";
			document.getElementById("btnNext").style.visibility = "visible";
			document.getElementById("btnBack").style.visibility = "visible";
			document.getElementById("navText").style.visibility = "visible";

			// Write new layer for textHeader
			strDivWrite = '<span class="HeaderText">Key Learning Points</span>';
			document.getElementById("textHeader").innerHTML = strDivWrite;
			// Change iframe layer
			parent.window.BodyInfo.location.href='key.php?nResult=' + nResult + '&iFirstChoice=' + iFirstChoice + '&iCurrentPage=' + iCurrentPage;
		break;
		case 3:
			if(strMode!='admin')
			{
				if (strSupport_Info.length > 0)
				{
					document.getElementById("textHeader").style.visibility = "hidden";
					document.getElementById("btnNext").style.visibility = "hidden";
					document.getElementById("btnBackCover").style.visibility = "hidden";
					document.getElementById("textInstruction").style.visibility = "hidden";
					document.getElementById("graphic").style.visibility = "hidden";

					// Move textHeader down and Next button up
					document.getElementById("textHeader").style.top = "100px";

					strDivWrite = '<a href="javascript: goMove(1);" onmouseout="document.images.btnNextImage.src=\'graphics/btn_FAQ_wht_next_up.gif\' " onmouseover="document.images.btnNextImage.src=\'graphics/btn_FAQ_wht_next_down.gif\' "><img name="btnNextImage" id="btnNextImage" src="graphics/btn_FAQ_wht_next_up.gif" alt="" border="0" /></a>';
					document.getElementById("btnNext").innerHTML = strDivWrite;
					strDivWrite = '<a href="javascript: goMove(-1);" onmouseout="document.images.btnBackImage.src=\'graphics/btn_FAQ_wht_back_up.gif\' " onmouseover="document.images.btnBackImage.src=\'graphics/btn_FAQ_wht_back_down.gif\' "><img name="btnBackImage" id="btnBackImage" src="graphics/btn_FAQ_wht_back_up.gif" alt="" border="0" /></a>';
					document.getElementById("btnBack").innerHTML = strDivWrite;
					if(strMode=='review')
					{
						strDivWrite = '<a href="javascript: goHome();" onmouseout="document.images.btnMenuImage.src=\'graphics/btn_FAQ_wht_menu_up.gif\' " onmouseover="document.images.btnMenuImage.src=\'graphics/btn_FAQ_wht_menu_down.gif\' "><img name="btnMenuImage" id="btnMenuImage" src="graphics/btn_FAQ_wht_menu_up.gif" alt="" border="0" /></a>';
						document.getElementById("btnMenu").innerHTML = strDivWrite;
					}

					// Show layers.
					document.getElementById("textHeader").style.visibility = "visible";
					document.getElementById("btnNext").style.visibility = "visible";
					document.getElementById("btnBack").style.visibility = "visible";
					document.getElementById("navText").style.visibility = "visible";
					// Write new layer for textHeader
					strDivWrite = '<span class="HeaderText">Supplemental Information</span>';
					document.getElementById("textHeader").innerHTML = strDivWrite;
					// Change iframe layer
					parent.window.BodyInfo.location.href='support.php?nResult=' + nResult + '&iFirstChoice=' + iFirstChoice + '&iCurrentPage=' + iCurrentPage;
				}
				else
				{
					if(nMoveDirection == 1)
					{
						goMove(1);
					}
					else if(nMoveDirection == -1)
					{
						goMove(-1);
					}
				}
			}
			else
			{
				document.getElementById("textHeader").style.visibility = "hidden";
				document.getElementById("btnNext").style.visibility = "hidden";
				document.getElementById("btnNextCover").style.visibility = "hidden";
				document.getElementById("btnBackCover").style.visibility = "hidden";
				document.getElementById("textInstruction").style.visibility = "hidden";
				document.getElementById("graphic").style.visibility = "hidden";

				// Move textHeader down and Next button up
				document.getElementById("textHeader").style.top = "100px";

				strDivWrite = '<a href="javascript: goMove(1);" onmouseout="document.images.btnNextImage.src=\'graphics/btn_FAQ_wht_next_up.gif\' " onmouseover="document.images.btnNextImage.src=\'graphics/btn_FAQ_wht_next_down.gif\' "><img name="btnNextImage" id="btnNextImage" src="graphics/btn_FAQ_wht_next_up.gif" alt="" border="0" /></a>';
				document.getElementById("btnNext").innerHTML = strDivWrite;
				strDivWrite = '<a href="javascript: goMove(-1);" onmouseout="document.images.btnBackImage.src=\'graphics/btn_FAQ_wht_back_up.gif\' " onmouseover="document.images.btnBackImage.src=\'graphics/btn_FAQ_wht_back_down.gif\' "><img name="btnBackImage" id="btnBackImage" src="graphics/btn_FAQ_wht_back_up.gif" alt="" border="0" /></a>';
				document.getElementById("btnBack").innerHTML = strDivWrite;

				// Show layers.
				document.getElementById("textHeader").style.visibility = "visible";
				document.getElementById("btnBack").style.visibility = "visible";
				document.getElementById("btnNext").style.visibility = "visible";
				document.getElementById("navText").style.visibility = "visible";
				// Write new layer for textHeader
				strDivWrite = '<span class="HeaderText">Supplemental Information</span>';
				document.getElementById("textHeader").innerHTML = strDivWrite;
				// Change iframe layer
				parent.window.BodyInfo.location.href='support.php?nResult=' + nResult + '&iFirstChoice=' + iFirstChoice + '&iCurrentPage=' + iCurrentPage;
			}
		break;
		case 4:
			if(strMode!='admin')
			{
				if (strNotes.length > 0)
				{
					document.getElementById("textHeader").style.visibility = "hidden";
					document.getElementById("btnNext").style.visibility = "hidden";
					document.getElementById("btnBackCover").style.visibility = "hidden";
					document.getElementById("textInstruction").style.visibility = "hidden";
					document.getElementById("graphic").style.visibility = "hidden";

					// Move textHeader down and Next button up
					document.getElementById("textHeader").style.top = "100px";

					strDivWrite = '<a href="javascript: goMove(1);" onmouseout="document.images.btnNextImage.src=\'graphics/btn_FAQ_wht_next_up.gif\' " onmouseover="document.images.btnNextImage.src=\'graphics/btn_FAQ_wht_next_down.gif\' "><img name="btnNextImage" id="btnNextImage" src="graphics/btn_FAQ_wht_next_up.gif" alt="" border="0" /></a>';
					document.getElementById("btnNext").innerHTML = strDivWrite;
					strDivWrite = '<a href="javascript: goMove(-1);" onmouseout="document.images.btnBackImage.src=\'graphics/btn_FAQ_wht_back_up.gif\' " onmouseover="document.images.btnBackImage.src=\'graphics/btn_FAQ_wht_back_down.gif\' "><img name="btnBackImage" id="btnBackImage" src="graphics/btn_FAQ_wht_back_up.gif" alt="" border="0" /></a>';
					document.getElementById("btnBack").innerHTML = strDivWrite;
					if(strMode=='review')
					{
						strDivWrite = '<a href="javascript: goHome();" onmouseout="document.images.btnMenuImage.src=\'graphics/btn_FAQ_wht_menu_up.gif\' " onmouseover="document.images.btnMenuImage.src=\'graphics/btn_FAQ_wht_menu_down.gif\' "><img name="btnMenuImage" id="btnMenuImage" src="graphics/btn_FAQ_wht_menu_up.gif" alt="" border="0" /></a>';
						document.getElementById("btnMenu").innerHTML = strDivWrite;
					}

					// Show layers.
					document.getElementById("textHeader").style.visibility = "visible";
					document.getElementById("btnNext").style.visibility = "visible";
					document.getElementById("btnBack").style.visibility = "visible";
					document.getElementById("navText").style.visibility = "visible";
					// Write new layer for textHeader
					strDivWrite = '<span class="HeaderText">Additional HR Comments</span>';
					document.getElementById("textHeader").innerHTML = strDivWrite;
					// Change iframe layer
					parent.window.BodyInfo.location.href='additional.php?nResult=' + nResult + '&iFirstChoice=' + iFirstChoice + '&iCurrentPage=' + iCurrentPage;
				}
				else
				{
					if(nMoveDirection == 1)
					{
						goMove(1);
					}
					else if(nMoveDirection == -1)
					{
						goMove(-1);
					}
				}
			}
			else
			{
				document.getElementById("textHeader").style.visibility = "hidden";
				document.getElementById("btnNext").style.visibility = "hidden";
				document.getElementById("btnBackCover").style.visibility = "hidden";
				document.getElementById("textInstruction").style.visibility = "hidden";
				document.getElementById("graphic").style.visibility = "hidden";

				// Move textHeader down and Next button up
				document.getElementById("textHeader").style.top = "100px";

				strDivWrite = '<a href="javascript: goMove(1);" onmouseout="document.images.btnNextImage.src=\'graphics/btn_FAQ_wht_next_up.gif\' " onmouseover="document.images.btnNextImage.src=\'graphics/btn_FAQ_wht_next_down.gif\' "><img name="btnNextImage" id="btnNextImage" src="graphics/btn_FAQ_wht_next_up.gif" alt="" border="0" /></a>';
				document.getElementById("btnNext").innerHTML = strDivWrite;
				strDivWrite = '<a href="javascript: goMove(-1);" onmouseout="document.images.btnBackImage.src=\'graphics/btn_FAQ_wht_back_up.gif\' " onmouseover="document.images.btnBackImage.src=\'graphics/btn_FAQ_wht_back_down.gif\' "><img name="btnBackImage" id="btnBackImage" src="graphics/btn_FAQ_wht_back_up.gif" alt="" border="0" /></a>';
				document.getElementById("btnBack").innerHTML = strDivWrite;

				// Show layers.
				document.getElementById("textHeader").style.visibility = "visible";
				document.getElementById("btnBack").style.visibility = "visible";
				document.getElementById("btnNextCover").style.visibility = "visible";
				document.getElementById("navText").style.visibility = "visible";
				// Write new layer for textHeader
				strDivWrite = '<span class="HeaderText">Additional HR Comments</span>';
				document.getElementById("textHeader").innerHTML = strDivWrite;
				// Change iframe layer
				document.getElementById("navText").style.visibility = "hidden";
				parent.window.BodyInfo.location.href='additional.php?nResult=' + nResult + '&iFirstChoice=' + iFirstChoice + '&iCurrentPage=' + iCurrentPage;
			}
		break;
		case 5:
			if(strMode=='review')
			{
				goHome();
			}
			else
			{
				strDivWrite = '<span class="HeaderText">Thank You!</span>';
				document.getElementById("textHeader").innerHTML = strDivWrite;
				document.getElementById("btnNextCover").style.visibility = "hidden";
				document.getElementById("btnBackCover").style.visibility = "hidden";
				document.getElementById("btnNext").style.visibility = "hidden";
				document.getElementById("btnBack").style.visibility = "hidden";
				document.getElementById("navText").style.visibility = "hidden";
				strDivWrite = '<iframe name="BodyInfo" id="BodyInfo" src="confirm.php?session_Result='+ nResult +'&session_Foil_Selected='+ iFirstChoice +'" marginheight="0" marginwidth="0" width="792" height="380" scrolling="auto" frameborder="0"></iframe>';
				document.getElementById("bodyArea").innerHTML = strDivWrite;
			}
		break;
	}

}

</script>
<style type="text/css">
	.HeaderText {  font-family: Arial, Helvetica, sans-serif; font-size:16pt; color:#000000; font-weight:bold; }
	.Instructions {  font-family: Arial, Helvetica, sans-serif; font-size:12pt; color:#000000; font-weight:bold }
	.navigation {  font-family: Arial, Helvetica, sans-serif; font-size:12pt; color:#000000; }
</style>
</head>
<body bgcolor="#FFFFFF" <?php if($session_bInQuestion==TRUE){print('onload="javascript:
goMove(0);"');}else{if($R!='admin'){$session_bInQuestion=TRUE;session_register('session_bInQuestion');}} ?>
BACKGROUND="graphics/present_header.gif" BGPROPERTIES=FIXED style="background-repeat:no-repeat">
<div style="position:absolute; left:629px; top:0px; z-index:1">
	<img id="Logo" src="<?php echo $session_Organization_Logo ?>"></img>
</div>
<div id="textHeader" style="position:absolute; left:0px; top:74px; width:792px; height:29px; z-index:1; text-align:center;">
  <span class="HeaderText">The Question</span> </div>
<div id="graphic" style="position:absolute; left:0px; top:103px; width:792px; height:59px; z-index:2">
  <img src="graphics/qbar.gif">
</div>
<div id="textInstruction" style="position:absolute; left:100px; top:110px; width:325px; height:37px; z-index:3;">
  <span class="Instructions">Click on the best answer.</span><br><span class="Instructions"style="color:#366EA5">Your first choice will be recorded.</span>
</div>

<?php
	if($R=='review' || $R=='admin')
	{
		print('<div id="btnNext" style="position:absolute; left:697px; top:106px; width:8px; height:19px; z-index:4; visibility:visible;"><a href="javascript: goMove(1);" onmouseout="document.images.btnNextImage.src=\'graphics/btn_FAQ_next_up.gif\' " onmouseover="document.images.btnNextImage.src=\'graphics/btn_FAQ_next_down.gif\' "><img name="btnNextImage" id="btnNextImage" src="graphics/btn_FAQ_next_up.gif" alt="" border="0" /></a></div>');
		print('<div id="btnNextCover" style="position:absolute; left:697px; top:106px; width:8px; height:19px; z-index:5; visibility:hidden;"><img name="Cover" id="Cover" src="graphics/btn_FAQ_next_dim.gif" alt="" border="0" /></div>');
		print('<div id="btnBack" style="position:absolute; left:622px; top:106px; width:8px; height:19px; z-index:6; visibility:visible;"><a href="javascript: goMove(-1);" onmouseout="document.images.btnBackImage.src=\'graphics/btn_FAQ_back_up.gif\' " onmouseover="document.images.btnBackImage.src=\'graphics/btn_FAQ_back_down.gif\' "><img name="btnBackImage" id="btnBackImage" src="graphics/btn_FAQ_back_up.gif" alt="" border="0" /></a></div>');
		print('<div id="btnBackCover" style="position:absolute; left:622px; top:106px; width:8px; height:19px; z-index:7; visibility:visible;"><img name="Cover" id="Cover" src="graphics/btn_FAQ_back_dim.gif" alt="" border="0" /></div>');
		// Show Back to Menu Button
		if($R!='admin')
		{
			print('<div id="btnMenu" style="position:absolute; left:543px; top:106px; width:8px; height:19px; z-index:4; visibility:visible;"><a href="javascript: goHome();" onmouseout="document.images.btnMenuImage.src=\'graphics/btn_FAQ_menu_up.gif\' " onmouseover="document.images.btnMenuImage.src=\'graphics/btn_FAQ_menu_down.gif\' "><img name="btnMenuImage" id="btnMenuImage" src="graphics/btn_FAQ_menu_up.gif" alt="" border="0" /></a></div>');
		}
	}
	else if($R=='tracking')
	{
		print('<div id="btnNext" style="position:absolute; left:697px; top:106px; width:8px; height:19px; z-index:4; visibility:hidden;"><a href="javascript: goMove(1);" onmouseout="document.images.btnNextImage.src=\'graphics/btn_FAQ_next_up.gif\' " onmouseover="document.images.btnNextImage.src=\'graphics/btn_FAQ_next_down.gif\' "><img name="btnNextImage" id="btnNextImage" src="graphics/btn_FAQ_next_up.gif" alt="" border="0" /></a></div>');
		print('<div id="btnNextCover" style="position:absolute; left:697px; top:106px; width:8px; height:19px; z-index:5; visibility:visible;"><img name="Cover" id="Cover" src="graphics/btn_FAQ_next_dim.gif" alt="" border="0" /></div>');
		print('<div id="btnBack" style="position:absolute; left:622px; top:106px; width:8px; height:19px; z-index:6; visibility:hidden;"><a href="javascript: goMove(-1);" onmouseout="document.images.btnBackImage.src=\'graphics/btn_FAQ_back_up.gif\' " onmouseover="document.images.btnBackImage.src=\'graphics/btn_FAQ_back_down.gif\' "><img name="btnBackImage" id="btnBackImage" src="graphics/btn_FAQ_back_up.gif" alt="" border="0" /></a></div>');
		print('<div id="btnBackCover" style="position:absolute; left:622px; top:106px; width:8px; height:19px; z-index:7; visibility:visible;"><img name="Cover" id="Cover" src="graphics/btn_FAQ_back_dim.gif" alt="" border="0" /></div>');
	}
?>

<div id="navText" style="position:absolute; left:585px; top:134px; width:188px; height:22px; z-index:6; visibility:hidden; text-align:right;">
  <span class="navigation"><em>Click Next to continue.</em></span>
</div>

<div id="bodyArea" style="position:absolute; left:0px; top:165px; width:792px; height:390px; z-index:6">
		<iframe name="BodyInfo" id="BodyInfo" src="q4.php" marginheight="0" marginwidth="0" width="792" height="380" scrolling="auto" frameborder="0">
		</iframe>
</div>

</body>
</html>
