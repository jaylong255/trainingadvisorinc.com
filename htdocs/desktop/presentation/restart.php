<?php

require_once('../includes/common.php');
require_once('../includes/Assignment.php');

//error_log("Entering thanks.php with request vars: ".print_r($_REQUEST, TRUE));

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit;
}


// If this is the demo organization then do not update record status
if ($_SESSION['orgId'] == 2) {
  error_log('thanks.php: returning to the portal because this is the demo organization');
  header('Location: portal.php');
  exit(0);
}


$a = NULL;

if (!isset($_REQUEST['assignmentId']) || $_REQUEST['assignmentId'] == 0) {
  error_log('restart.php: called without passing in assignmentId, sending user back to portal to attempt graceful recovery');
  header('Location: portal.php');
} else {
  $a = new Assignment($_SESSION['orgId'], $_REQUEST['assignmentId']);
  $a->ResetAssignmentSet();
  header("Location: q_type.php?assignmentId=$a->parentAssignmentId");
}

exit(0);

?>