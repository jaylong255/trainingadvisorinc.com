<?php

include('../includes/common.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit;
}

$_SESSION['currentTab'] = TAB_ASSIGNED;

$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('presentation/menu1.tpl');
exit(0);

?>