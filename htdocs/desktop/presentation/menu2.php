<?php
require_once('../includes/common.php');
require_once('../includes/Config.php');
require_once('../includes/Assignment.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit;
}

$_SESSION['currentTab'] = TAB_COMPLETED;

//$maxHistory = 50;
$config = new Config($_SESSION['orgId']);
$assignment = new Assignment($_SESSION['orgId']);
// See ../includes/Assignment.php for other code related to completed assignment list limit
//$assignments = $assignment->GetCompletedAssignments($_SESSION['userId'], $maxHistory);
$assignments = $assignment->GetCompletedAssignments($_SESSION['userId']);

if ($config->GetValue(CONFIG_LICENSED_PRINT_QUESTIONS))
  $smarty->assign('printFeatureEnabled', TRUE);

$smarty->assign('assignments', $assignments);
$smarty->assign('divAssignments', array());
$smarty->assign('deptAssignments', array());
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->assign('review', 1);
$smarty->display('presentation/menu2.tpl');

exit(0);

?>