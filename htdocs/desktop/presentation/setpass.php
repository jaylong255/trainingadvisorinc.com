<?php

require_once('../includes/common.php');
require_once('../includes/User.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit;
}


$msg = '';
$errMsg = '';


if(isset($_REQUEST['New']) && $_REQUEST['New'])	{

  if(strlen($_REQUEST['New']) == 0)
    $errMsg .= "* Please enter a new password.<br>";

  if(strlen($_REQUEST["Confirm"]) == 0)
    $errMsg .= "* Please confirm your new password.<br>";

  if(strcmp($_REQUEST["New"],$_REQUEST["Confirm"]) != 0)
    $errMsg .= "* New password incorrectly confirmed, please try again.<br>";
  
  if (!$errMsg) {

    $currentUser = new User($_SESSION['orgId'], $_SESSION['userId']);
    $currentUser->password=$_REQUEST['New'];
    $rc = $currentUser->UpdateUser();
    if ($rc != RC_OK) {
      $errMsg .= 'Unable to change password: '.RcToText($rc);
    } else {
      header('Location: portal.php');
      exit(0);
    }
  }

}

$smarty->assign('msg', $msg);
$smarty->assign('errMsg', $errMsg);
$smarty->assign('orgLogo', $_SESSION['orgLogo']);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('presentation/setpass.tpl');

?>

