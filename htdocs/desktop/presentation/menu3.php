<?php
require_once('../includes/common.php');
require_once('../includes/Faq.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit;
}


// Set the current tab so that tabs will return to their rightful place on a page reload
$_SESSION['currentTab'] = TAB_FAQ;


$faq = new Faq($_SESSION['orgId']);
$newFaq = $faq->GetFaqForUser($_SESSION['userId']);
$smarty->assign('faqs', $newFaq);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('presentation/menu3.tpl');

?>