<?php
require_once('../includes/common.php');
require_once('../includes/Assignment.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit;
}

$_SESSION['currentTab'] = TAB_PREFERENCES;


// Global vars
$days = 7; // Fetch the past week by default
$stats = array();
$dateOptions = array('Past Week', 'Past Month', 'Past Quarter', 'Past 6 Months', 'Past Year');
$dateOptionSelected = 0;
$assignmentReport = new Assignment($_SESSION['orgId']);


// Form requesting scoring over the past period was submitted
if (isset($_REQUEST['dateOption'])) {

  $dateOptionSelected = $_REQUEST['dateOption'];

  switch ($_REQUEST['dateOption']) {
  case 0: // 'Past Week'
    $days = 7;
    break;
  case 1: // 'Past Month'
    $days = 30;
    break;
  case 2: // 'Past Quarter'
    $days = 90;
    break;
  case 3: // 'Past 6 Months'
    $days = 180;
    break;
  case 4: // 'Past Year'
    $days = 365;
    break;
  default:
    error_log('menu4.php: Unknown date option submitted: '.$_REQUEST['dateOption']);
    break;
  }

}

$stats = $assignmentReport->GetScorecard($_SESSION['userId'], $days);

$smarty->assign_by_ref('stats', $stats);
$smarty->assign_by_ref('dateOptions', $dateOptions);
$smarty->assign('dateOptionSelected', $dateOptionSelected);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('presentation/menu4.tpl');


?>
