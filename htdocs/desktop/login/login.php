<?php

require_once("../includes/common.php");

/*
// Uncomment below section for maintenance page
$smarty->assign('uiTheme', 'Default');
$smarty->assign('showTaLogo', TRUE);
$smarty->assign('showPoweredBy', TRUE);
//$smarty->assign('ErrMsg', $errMsg);
$smarty->assign('copyrightYear', 2003);
$smarty->display('login/maintenance.tpl');
exit;
*/

$isValid = FALSE;
$errMsg  = '';
if (isset($_REQUEST['User_ID']))
  $userId = $_REQUEST['User_ID'];
else
  $userId = '';
if (isset($_REQUEST['Org_ID']))
  $orgId = $_REQUEST['Org_ID'];
else
  $orgId = '';

if (isset($_REQUEST['User_ID']) && isset($_REQUEST['UPassword']))  {

  if ($orgId == -1)
    $rc = RC_INVALID_LOGIN_ORG;
  else {
    if (isset($_REQUEST['saveOrg']) && $_REQUEST['saveOrg'] &&
	!isset($_COOKIE['taiorg'])) {
      if (!setcookie('taiorg', $_REQUEST['Org_ID'], 0, '/desktop/'))
	error_log('*** ERROR: unable to set cookie containing org '.$_REQUEST['Org_ID'].' for user '.$_REQUEST['User_ID']);
    }
    $rc = UserLogin($_REQUEST['User_ID'], $_REQUEST['UPassword'], $_REQUEST['Org_ID']);
    if($rc == RC_OK) {
      if($_SESSION['superUser']) {
	header('Location: ../admin/main.php');
      } else {
	print('<script type="text/JavaScript">
		var ScreenWidth=screen.width;
		var ScreenHeight=screen.height;
	        var zeroX=(ScreenWidth/2-400);
		var zeroY=(ScreenHeight/2-300);
		var options = "toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=792,height=545,left="+zeroX+",top="+zeroY;
		var strPageToOpen = (window.location.search != null) ? "../presentation/portal.php"+window.location.search : "../presentation/portal.php";
		window.location=strPageToOpen;
	</script> ');
      }
      exit;
    }
  }

  $errMsg = RcToText($rc);
  $smarty->assign('User_ID', $_REQUEST['User_ID']);
  $smarty->assign('showOrgIdSelect', TRUE);

} else {

  $smarty->assign('User_ID', $userId);
  $smarty->assign('showOrgIdSelect', FALSE);
  session_destroy();
}

if (isset($_REQUEST['fromEmail']) || isset($_COOKIE['taiorg'])) {
  if (isset($_COOKIE['taiorg']))
    $smarty->assign('Org_ID', $_COOKIE['taiorg']);
  else if (isset($_REQUEST['Org_ID']))
    $smarty->assign('Org_ID', $_REQUEST['Org_ID']);
  $smarty->assign('fromEmail', 1);
  $smarty->assign('showOrgIdSelect', FALSE);
}

$smarty->assign('uiTheme', 'Default');
$smarty->assign('showTaLogo', TRUE);
$smarty->assign('showPoweredBy', TRUE);
$smarty->assign('ErrMsg', $errMsg);
$smarty->assign('copyrightYear', 2003);

$smarty->display('login/login.tpl');

?>
