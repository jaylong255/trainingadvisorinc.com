<?php



class XmlUtility {


  var $xmlArray = array();


  function XmlUtility($fileOrUrl = '') {
    if ($fileOrUrl)
      $this->XmlFileToArray($fileOrUrl);
  }


  function XmlFileToArray($__url) {
    $contents = file_get_contents($__url);
    return $this->XmlToArray($contents);
  }


  function XmlToArray(&$contents) {

    $xmlValues = array();

    $parser = xml_parser_create('');
    if(!$parser)
        return false;

    xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, 'UTF-8');
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, trim($contents), $xmlValues);
    xml_parser_free($parser);
    if (!$xmlValues) {
      error_log("XmlUtility::XmlToArray(url=$__url): error, failed to parse xml into array");
      return array();
    }
    //error_log("_)_)_)__)_)_)_)_)_)_)_ XML VALUES: ".print_r($xmlValues, TRUE));
    $this->xmlArray = array();
    $last_tag_ar =& $this->xmlArray;
    $parents = array();
    $last_counter_in_tag = array(1=>0);
    foreach ($xmlValues as $data) {
      switch($data['type']) {
      case 'open':
	$last_counter_in_tag[$data['level']+1] = 0;
	$new_tag = array('name' => $data['tag']);
	if(isset($data['attributes']))
	  $new_tag['attributes'] = $data['attributes'];
	if(isset($data['value']) && trim($data['value']))
	  $new_tag['value'] = trim($data['value']);
	$last_tag_ar[$last_counter_in_tag[$data['level']]] = $new_tag;
	$parents[$data['level']] =& $last_tag_ar;
	$last_tag_ar =& $last_tag_ar[$last_counter_in_tag[$data['level']]++];
	break;
      case 'complete':
	$new_tag = array('name' => $data['tag']);
	if(isset($data['attributes']))
	  $new_tag['attributes'] = $data['attributes'];
	if(isset($data['value']) && trim($data['value']))
	  $new_tag['value'] = trim($data['value']);
	
	$last_count = count($last_tag_ar)-1;
	$last_tag_ar[$last_counter_in_tag[$data['level']]++] = $new_tag;
	break;
      case 'close':
	$last_tag_ar =& $parents[$data['level']];
	break;
      default:
	break;
      };
    }
    //    return $this->xmlArray;
  }


  //
  // use this to get node of tree by path with '/' terminator
  //
  function GetValueByPath($__tag_path) {
    $tmp_arr =& $this->xmlArray;
    $tag_path = explode('/', $__tag_path);
    foreach($tag_path as $tag_name) {
      $res = false;
      foreach($tmp_arr as $key => $node) {
	if(is_int($key) && $node['name'] == $tag_name) {
	  $tmp_arr = $node;
	  $res = true;
	  break;
	}
      }
      if(!$res)
	return false;
    }
    return $tmp_arr;
  }


  // 
  // Getter for internal data elements
  //
  function GetXmlData() {
    return $this->xmlArray;
  }


  //$arr = my_xml2array('test.xml');
  //print_r(get_value_by_path($arr, 'tag/sub_tag/sub_sub_tag')); 
}