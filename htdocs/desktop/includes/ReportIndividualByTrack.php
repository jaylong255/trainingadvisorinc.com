<?php

require_once('Report.php');


class ReportIndividualByTrack extends Report {

  var $allTracks = array();
  var $reportData = array();
  var $columnDefs = array();

  // Returns an array structured like the following:
  // (<track_id> => ( (<track_id>, <track_name>, <num_participants>, <num_assignments>, <num_answered>, <num_unanswered>, <num_correct>),
  //                       (<login_name>, <num_assigned>, <num_answered>, <num_unanswered>, <num_correct>)
  //                   (...
  //                 (<track_id>, <track_name>, <num_participants>, <num_assignments>, <num_answered>, <num_unanswered>, <num_correct>),
  //                   (<login_name>, <num_assigned>, <num_answered>, <num_unanswered>, <num_correct>)
  //                   (...
  //                 (...
  //
  function ReportIndividualByTrack($orgId, $trackIds, $dateFrom, $dateTo, $divId, $deptId) {

    $this->Report($orgId);
    $allTracks = $this->GetDistinctTracks();
    $selectColumns = array();
    $idx = 0;
    $subIdx = 0;

    // Create the report that has functions to
    $this->columnDefs[] = array('label' => 'Login Name',
				'type' => 'text',
				'hdrIdx' => 2,
				'datIdx' => 0,
				'pdfWidth' => '1.5em');
    $this->columnDefs[] = array('label' => 'Assigned Questions',
				'type' => 'int',
				'hdrIdx' => 3,
				'datIdx' => 1,
				'pdfWidth' => '0.9em');
    $this->columnDefs[] = array('label' => 'Answered',
				'type' => 'int',
				'hdrIdx' => 4,
				'datIdx' => 2,
				'pdfWidth' => '0.9em');
    $this->columnDefs[] = array('label' => 'Unanswered',
				'type' => 'int',
				'hdrIdx' => 5,
				'datIdx' => 3,
				'pdfWidth' => '0.9em');
    $this->columnDefs[] = array('label' => 'Correct',
				'type' => 'int',
				'hdrIdx' => 6,
				'datIdx' => 4,
				'pdfWidth' => '0.9em');
    $this->columnDefs[] = array('label' => 'Division',
				'type' => 'text',
				'hdrIdx' => 2,
				'datIdx' => 0,
				'pdfWidth' => '1.5em');

    foreach($trackIds as $trackId) {
      $this->Select(array(USER_TABLE.'.'.$this->userSqlColumns[USER_COLUMN_ID],
			  USER_TABLE.'.'.$this->userSqlColumns[USER_COLUMN_FULL_NAME]),
		    USER_TABLE);
      $this->LeftJoinUsing(ASSIGNMENT_TABLE, $this->userSqlColumns[USER_COLUMN_ID]);
      $this->ReportConstraints($trackId, $dateFrom, $dateTo, $userId, $divId, $deptId);
      $this->SortBy(array(USER_TABLE.'.'.$this->userSqlColumns[USER_COLUMN_LAST_NAME], USER_TABLE.'.'.$this->userSqlColumns[USER_COLUMN_FIRST_NAME]));
      $this->GroupBy(USER_TABLE.'.'.$this->userSqlColumns[USER_COLUMN_ID]);
      $results = $this->Query(MANY_ROWS, VALUES_FORMAT);

      if (count($results) == 0)
	continue;

      $idx = array_push($this->reportData, array(0 => array($trackId, $allTracks[$trackId], count($results), 0, 0, 0, 0)));
      foreach ($results as $user) {

        /*

          I don't have time to train myself on all this legacy custom code
          so i'm throwing in this hack move because a customer wants to
          display the division name on this report beside the user name

          11-01-2016 ~Jay
          files:
          templates/admin/org_report_individual_by_track.tpl
          templates/admin/org_report_individual_by_track_results.tpl
          includes/ReportIndividualByTrack.php
          admin/org_report_individual_by_track.php

          files tracked by git commits in the repository

          ideally you want to join this in with the initial sql statement
          instead of doing select in a php loop, but what are you gonna do?

        */

        $sql = "
          SELECT Division.Name AS division
          FROM Division JOIN User ON Division.Division_ID = User.Division_ID
          WHERE User.User_ID = '". $user[0] ."'
        ";

        $brt_division = "Not Set";
        $brt_result = mysql_query($sql,$this->dbLink);
        while($brt_row = mysql_fetch_array($brt_result)){
          $brt_division = $brt_row['division'];
        }

	$subIdx = array_push($this->reportData[$idx-1], array($user[1],
							      $this->GetAssigned($trackId, $dateFrom, $dateTo, $user[0]),
							      $this->GetAnswered($trackId, $dateFrom, $dateTo, $user[0]),
							      $this->GetUnanswered($trackId, $dateFrom, $dateTo, $user[0]),
							      $this->GetCorrect($trackId, $dateFrom, $dateTo, $user[0]),
                    $brt_division
                  ));

	// Keep track of totals and update column header fields with a running total
	for($i = 3; $i <= 6; $i++)
	  $this->reportData[$idx-1][0][$i] += $this->reportData[$idx-1][$subIdx-1][$i-2];
      }
    }

    return $this->reportData;
  }


  function GetAssigned($trackId, $dateFrom, $dateTo, $userId, $divId, $deptId) {

    $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->ReportConstraints($trackId, $dateFrom, $dateTo, $userId, $divId, $deptId);
    return $this->Query(SINGLE_VALUE, VALUES_FORMAT);

  }


  function GetAnswered($trackId, $dateFrom, $dateTo, $userId, $divId, $deptId) {

    $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->ReportConstraints($trackId, $dateFrom, $dateTo, $userId, $divId, $deptId);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].'!=0');
    return $this->Query(SINGLE_VALUE, VALUES_FORMAT);

  }


  function GetUnanswered($trackId, $dateFrom, $dateTo, $userId, $divId, $deptId) {

    $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->ReportConstraints($trackId, $dateFrom, $dateTo, $userId, $divId, $deptId);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].'=0');
    return $this->Query(SINGLE_VALUE, VALUES_FORMAT);

  }


  function GetCorrect($trackId, $dateFrom, $dateTo, $userId, $divId, $deptId) {

    $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->ReportConstraints($trackId, $dateFrom, $dateTo, $userId, $divId, $deptId);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY].'='.RESULT_CORRECT);
    return $this->Query(SINGLE_VALUE, VALUES_FORMAT);

  }


  function ReportConstraints($trackId, $dateFrom, $dateTo, $userId = 0, $divId, $deptId) {

    if (!$trackId) {
      error_log("ReportAccuracyByTrack::GetAssigned(trackId=$trackId,dateFrom=$dateFrom,dateTo=$dateTo,userId=$userId): ".
		"Invalid track ID passed to function, track ID must be specified and non-zero");
      return FALSE;
    }

    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID].'='.$trackId);
    if ($dateFrom)
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DELIVERY_DATE].">='".$dateFrom."'");
    if ($dateTo)
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DELIVERY_DATE]."<='".$dateTo."'");
    if ($userId > 0)
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID]."=$userId");
    if (is_numeric($divId) && (int)$divId > 0)
      $this->Where(USER_TABLE.'.'.$this->userSqlColumns[USER_COLUMN_DIVISION_ID]."=$divId");
    if (is_numeric($deptId) && (int)$deptId > 0)
      $this->Where(USER_TABLE.'.'.$this->userSqlColumns[USER_COLUMN_DEPARTMENT_ID]."=$deptId");
  }


  function GetReportData() {
    return $this->reportData;
  }


  function CreatePdf($title) {
    $this->CreateBasePdf($title, $this->columnDefs,
			 $this->reportData, 'Class_Individual_Assignemnts.pdf');
  }


  function CreateCsv() {
    $this->CreateBaseCsv($this->columnDefs, $this->reportData,
			 'Class_Individual_Assignments.csv');
  }
}
