<?php

require_once('Smarty2/Smarty.class.php');

class SmartyUpgradeManager extends Smarty {

  function SmartyUpgradeManager () {

#    $tplRoot = "/data/templates/internal";

    $this->template_dir = SMARTY_TEMPLATE_DIR;
    $this->compile_dir  = SMARTY_COMPILE_DIR;
    $this->config_dir   = SMARTY_CONFIG_DIR;
    $this->cache_dir    = SMARTY_CACHE_DIR;
#    $this->caching      = FALSE;
    $this->use_sub_dirs = SMARTY_USE_SUBDIRS;
    if (DEBUG & DEBUG_SMARTY)
      $this->debugging    = SMARTY_DEBUGGING;

    if (DEBUG & DEBUG_SESSION)
      $this->assign_by_ref('SESSION', $_SESSION);
  }

}


$smarty  = new SmartyUpgradeManager;

?>