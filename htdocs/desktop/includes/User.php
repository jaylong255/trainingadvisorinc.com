<?php

require_once('common.php');
require_once('DatabaseObject.php');

class User extends DatabaseObject {

  // Sql Columns
  var $userListSqlColumns = array();
  var $userListLabelColumns = array();
  var $userSqlColumns = array();
  var $participantSqlColumns = array();
  var $trackListSqlColumns = array();
  var $divisionSqlColumns = array();
  var $departmentSqlColumns = array();
  var $languageSqlColumns = array();
  var $employmentStatusSqlColumns = array();
  var $domainSqlColumns = array();
  var $contactInfoSqlColumns = array();

  var $orgId = 0; // The organization ID to which the user belongs
  var $userId = 0;
  var $login = '';
  var $firstName = '';
  var $lastName = '';
  var $password = '';
  var $email = '';
  var $phone = '';
  var $useSupervisor = FALSE;
  var $languageId = 1;
  var $contactId = 0;
  var $departmentId = 0;
  var $divisionId = 0;
  var $employmentStatusId = 0;
  var $domainId = 0;
  var $role = 0;
  var $custom1Id = 0;
  var $custom2Id = 0;
  var $custom3Id = 0;
  var $employmentStatusDate = '';

  // Special variable for determining if we are using consultants or not
  var $participantTrackId = 0;
  var $participantShowAll = FALSE;

  // These were carried over from the Organization
  // Class and could very well apply here as well
  var $cacheError = '';
  var $rc = 0;
  var $myqslOutput = '';


  function User($orgId, $newUserId = 0) {

    $this->orgId = (int)$orgId;
    if (!$orgId) {
      error_log("User.User(orgId=".$this->orgId.", newUserId=$newUserId): Invalid orgId");
      return;
    }

    $newUserId = (int)$newUserId;

    $this->userListSqlColumns = unserialize(USER_LIST_SQL_COLUMNS);
    $this->userListLabelColumns = unserialize(USER_LIST_LABELS);
    $this->userSqlColumns = unserialize(USER_SQL_COLUMNS);
    $this->participantSqlColumns = unserialize(PARTICIPANT_SQL_COLUMNS);
    $this->trackListSqlColumns = unserialize(TRACK_LIST_SQL_COLUMNS);
    $this->divisionSqlColumns = unserialize(DIVISION_SQL_COLUMNS);
    $this->departmentSqlColumns = unserialize(DEPARTMENT_SQL_COLUMNS);
    $this->languageSqlColumns = unserialize(LANGUAGE_SQL_COLUMNS);
    $this->employmentStatusSqlColumns = unserialize(EMPLOYMENT_STATUS_SQL_COLUMNS);
    $this->domainSqlColumns = unserialize(DOMAIN_SQL_COLUMNS);

    // Initialize employment status id to active
    $this->employmentStatusId = 1;
    $this->DatabaseObject(ORG_DB_NAME_PREFIX.$this->orgId);

    if (!$this->initialized) {
      return FALSE;
    }

    if (is_numeric($newUserId) && $newUserId) {
      $this->SetUserId($newUserId);
    }
  }


  function SetUserId($newUserId) {

    $newUserId = (int)$newUserId;
    if (!$newUserId) {
      error_log("User.SetUserId(newUserId=$newUserId): Invalid userId");
      return RC_INVALID_ARG;
    }

    $values = array();
    $roles = unserialize(ROLES);
    
    if ($this->userId != $newUserId) {
      $this->userId = $newUserId;

      // Super Admin is a special situation with user data not stored in individual organization user tables
      if ($this->userId == 1) {
	$this->login = 'sa123456';
	$this->firstName = 'Super';
	$this->lastName = 'Admin';
	$this->email = 'jsmith@sabient.com';
	$this->phone = '801-277-1177';
	$this->useSupervisor = 0; // FALSE
	$this->languageId = 1; // English
	$this->contactId = 0; // No contact
	$this->departmentId = 0; // No department
	$this->divisionId = 0; // No division
	$this->employmentStatusId = 1; // Active
	$this->domainId = 0; // No domain
	$this->custom1Id = 0;
	$this->custom2Id = 0;
	$this->custom3Id = 0;
	$this->employmentStatusDate = '2000-01-01';
	$this->role = ROLE_ORG_ADMIN;
      } else {
	$this->Select('*', USER_TABLE);
	$this->Where(USER_TABLE.'.'.$this->userSqlColumns[USER_COLUMN_ID]." = ".$this->userId);
	$values = $this->Query(SINGLE_ROW);
	if ($values === FALSE)
	  return RC_QUERY_FAILED;
	$this->login = $values[$this->userSqlColumns[USER_COLUMN_LOGIN]];
	$this->firstName = $values[$this->userSqlColumns[USER_COLUMN_FIRST_NAME]];
	$this->lastName = $values[$this->userSqlColumns[USER_COLUMN_LAST_NAME]];
	$this->email = $values[$this->userSqlColumns[USER_COLUMN_EMAIL]];
	$this->phone = $values[$this->userSqlColumns[USER_COLUMN_PHONE]];
	$this->useSupervisor = $values[$this->userSqlColumns[USER_COLUMN_USE_SUPERVISOR]];
	$this->languageId = $values[$this->userSqlColumns[USER_COLUMN_LANGUAGE_ID]];
	$this->contactId = $values[$this->userSqlColumns[USER_COLUMN_CONTACT_ID]];
	$this->departmentId = $values[$this->userSqlColumns[USER_COLUMN_DEPARTMENT_ID]];
	$this->divisionId = $values[$this->userSqlColumns[USER_COLUMN_DIVISION_ID]];
	$this->employmentStatusId = $values[$this->userSqlColumns[USER_COLUMN_EMPLOYMENT_STATUS_ID]];
	// for domainId '' is not a valid value
	if (!is_numeric($values[$this->userSqlColumns[USER_COLUMN_DOMAIN_ID]]))
	  $this->domainId = 0;
	else
	  $this->domainId = $values[$this->userSqlColumns[USER_COLUMN_DOMAIN_ID]];
	$this->custom1Id = $values[$this->userSqlColumns[USER_COLUMN_CUSTOM1_ID]];
	$this->custom2Id = $values[$this->userSqlColumns[USER_COLUMN_CUSTOM2_ID]];
	$this->custom3Id = $values[$this->userSqlColumns[USER_COLUMN_CUSTOM3_ID]];
	$this->employmentStatusDate = $values[$this->userSqlColumns[USER_COLUMN_EMPLOYMENT_STATUS_DATE]];
	$this->role = (int)($values[$this->userSqlColumns[USER_COLUMN_ROLE]]);
      }
      
      /*
      if ($this->useSupervisor && $this->trackId != 0) {
	$tempTrack = new Track($this->orgId, $this->trackId);
	$this->contactId = $tempTrack->supervisorId;
      }
      */
    }
    return RC_OK;
  }


  public static function GetUserOrgs($username) {

    if(!$username)
      return RC_PASSWORD_REQUIRED;

    $db = new DatabaseObject(AUTH_DB_NAME, AUTH_DB_USER, AUTH_DB_PASS, DB_HOST);
    if (!$db) {
      error_log("User.GetUserOrgs($username): Failed to connect to ".AUTH_DB_NAME.' database');
      return RC_AUTH_DB_CONNECT_FAILED;
    }
    
    $authUserSqlColumns = unserialize(AUTH_USER_SQL_COLUMNS);
    $authOrgSqlColumns = unserialize(AUTH_ORG_SQL_COLUMNS);

    $db->Select(array(AUTH_ORG_TABLE.'.'.$authOrgSqlColumns[AUTH_ORG_COLUMN_ID],
		      AUTH_ORG_TABLE.'.'.$authOrgSqlColumns[AUTH_ORG_COLUMN_NAME]),
		AUTH_ORG_TABLE);
    $db->LeftJoinUsing(AUTH_USER_TABLE, $authUserSqlColumns[AUTH_USER_COLUMN_ORGANIZATION_ID]);
    //    $db->LeftJoinUsing(array(AUTH_USER_TABLE.'.'.$authUserSqlColumns[AUTH_USER_COLUMN_ORGANIZATION_ID], '=',
    //			     AUTH_ORG_TABLE.'.'.$authOrgSqlColumns[AUTH_ORG_COLUMN_ID]),
    //		       AUTH_ORG_TABLE);
    $db->Where(AUTH_USER_TABLE.'.'.$authUserSqlColumns[AUTH_USER_COLUMN_LOGIN_ID]."='".esql($username)."'");

    /*
    $cnt = $db->QueryCount();
    if ($cnt === FALSE) {
      error_log("User.GetUserOrgs($username): ".RcToText(RC_QUERY_FAILED));
      return RC_QUERY_FAILED;
    }
    if ($cnt == 0) {
      error_log("User.ValidateLogin($username, $password): User Unknown: ".RcToText(RC_USER_UNKNOWN));
      return RC_USER_UNKNOWN;
    }    
    */

    $rows = $db->Query(NAME_VALUES);

    // Test for database or query execution failure
    if ($rows === FALSE) {
      error_log("User.GetUserOrgs($username): ".RcToText(RC_QUERY_FAILED));
      return RC_QUERY_FAILED;
    }

    // Set user and org ID if query successful even if password incorrect so we can identify
    // users and user ids for those users that fail to authenticate successfully.  User import
    // sync feature takes advantage of this.
    //$userId = $rows[0][$authUserSqlColumns[AUTH_USER_COLUMN_ID]];
    //$orgId = $rows[0][$authOrgSqlColumns[AUTH_ORG_COLUMN_ID]];

    // Validate password
    //if (crypt($password, $rows[0][$authUserSqlColumns[AUTH_USER_COLUMN_PASSWORD]]) !=
    //$rows[0][$authUserSqlColumns[AUTH_USER_COLUMN_PASSWORD]]) {
    //error_log("User.ValidateLogin($username, $password): Password incorrect");
    //return RC_INCORRECT_PASSWORD;
    //}

		 /*
      $this->Select(array('u.User_ID', 'u.Password', 'u.Login_ID', 'o.Database_Name',
		      'o.Organization_ID', 'o.Organization_Name'),
		array('User as u', 'Organization as o'));
      $link->Where("u.Organization_ID = o.Organization_ID AND u.Login_ID = '".esql($username)."'");
		 */

    return $rows;
  }


  function ValidateLogin($username, $password, &$userId, &$orgId) {

    if(!$username || !$password)
      return RC_PASSWORD_REQUIRED;

    $db = new DatabaseObject(AUTH_DB_NAME, AUTH_DB_USER, AUTH_DB_PASS, DB_HOST);
    if (!$db) {
      error_log("User.ValidateLogin($username,$password): Failed to connect to ".AUTH_DB_NAME.' database');
      return RC_AUTH_DB_CONNECT_FAILED;
    }
    
    $authUserSqlColumns = unserialize(AUTH_USER_SQL_COLUMNS);
    $authOrgSqlColumns = unserialize(AUTH_ORG_SQL_COLUMNS);
    $db->Select(array(AUTH_USER_TABLE.'.'.$authUserSqlColumns[AUTH_USER_COLUMN_ID],
		      AUTH_USER_TABLE.'.'.$authUserSqlColumns[AUTH_USER_COLUMN_PASSWORD],
		      AUTH_USER_TABLE.'.'.$authUserSqlColumns[AUTH_USER_COLUMN_LOGIN_ID],
		      AUTH_ORG_TABLE.'.'.$authOrgSqlColumns[AUTH_ORG_COLUMN_DATABASE_NAME],
		      AUTH_ORG_TABLE.'.'.$authOrgSqlColumns[AUTH_ORG_COLUMN_ID],
		      AUTH_ORG_TABLE.'.'.$authOrgSqlColumns[AUTH_ORG_COLUMN_NAME]),
		AUTH_USER_TABLE);
    $db->LeftJoinUsing(AUTH_ORG_TABLE, $authOrgSqlColumns[AUTH_ORG_COLUMN_ID]);
    //    $db->LeftJoinUsing(array(AUTH_USER_TABLE.'.'.$authUserSqlColumns[AUTH_USER_COLUMN_ORGANIZATION_ID], '=',
    //			     AUTH_ORG_TABLE.'.'.$authOrgSqlColumns[AUTH_ORG_COLUMN_ID]),
    //		       AUTH_ORG_TABLE);
    $db->Where(AUTH_USER_TABLE.'.'.$authUserSqlColumns[AUTH_USER_COLUMN_LOGIN_ID]." = '".esql(trim($username))."'");
    if ($orgId)
      $db->Where(AUTH_USER_TABLE.'.'.$authUserSqlColumns[AUTH_USER_COLUMN_ORGANIZATION_ID].'='.esql($orgId));
    
    $cnt = $db->QueryCount();

    if ($cnt === FALSE) {
      error_log("User.ValidateLogin($username,$password): ".RcToText(RC_QUERY_FAILED));
      return RC_QUERY_FAILED;
    }

    if ($cnt == 0) {
      error_log("User.ValidateLogin($username, $password): User Unknown: ".RcToText(RC_USER_UNKNOWN));
      return RC_USER_UNKNOWN;
    }    

    // If this is an initial login and the orgId was not specified 
    // sort of skip the dup login check because
    // we need to determine in what organizations this userId exists.
    if ($cnt > 1 && $orgId) {
      error_log("User.ValidateLogin($username, $password): ".RcToText(RC_DUPLICATE_LOGIN));
      return RC_DUPLICATE_LOGIN;
    }

    $rows = $db->Query(MANY_ROWS);

    // Test for database or query execution failure
    if ($rows === FALSE) {
      error_log("User.ValidateLogin($username, $password): ".RcToText(RC_QUERY_FAILED));
      return RC_QUERY_FAILED;
    }

    // Set user and org ID if query successful even if password incorrect so we can identify
    // users and user ids for those users that fail to authenticate successfully.  User import
    // sync feature takes advantage of this.
    $userId = $rows[0][$authUserSqlColumns[AUTH_USER_COLUMN_ID]];
    $orgId = $rows[0][$authOrgSqlColumns[AUTH_ORG_COLUMN_ID]];

    // Validate password
    if (crypt($password, $rows[0][$authUserSqlColumns[AUTH_USER_COLUMN_PASSWORD]]) !=
	$rows[0][$authUserSqlColumns[AUTH_USER_COLUMN_PASSWORD]]) {
      error_log("User.ValidateLogin($username, $password): Password incorrect");
      return RC_INCORRECT_PASSWORD;
    }

		 /*
      $this->Select(array('u.User_ID', 'u.Password', 'u.Login_ID', 'o.Database_Name',
		      'o.Organization_ID', 'o.Organization_Name'),
		array('User as u', 'Organization as o'));
      $link->Where("u.Organization_ID = o.Organization_ID AND u.Login_ID = '".esql($username)."'");
		 */

    return RC_OK;
  }


  function GetFullName($lastFirst = TRUE) {
    if ($lastFirst)
      return $this->lastName.", ".$this->firstName;
    else
      return $this->firstName." ".$this->lastName;
  }


  //  function GetColumnLabels($excludeArray = NULL)
  function GetColumnLabels() {
    return $this->userListLabelColumns;
    /*
      foreach ($excludeArray as $key => $val) {
	$excludeArray[$key] = $this->userListLabelColumns[$key];
      }
      return array_diff_assoc($this->userListLabelColumns, $excludeArray);
    */
  }


  // This function returns an array of track Ids for which this user
  // is supervisor.  This is useful for login time when a users role
  // is classified for access levels
  function GetSupervisedTrackIds() {

    $trackSqlColumns = unserialize(TRACK_SQL_COLUMNS);

    $this->Select($trackSqlColumns[TRACK_COLUMN_ID], TRACK_TABLE);
    $this->Where($trackSqlColumns[TRACK_COLUMN_SUPERVISOR_ID].'='.$this->userId);
    $rows = $this->Query(MANY_VALUES, VALUES_FORMAT);
    if ($rows === FALSE) {
      error_log("User.GetSupervisedTrackIds(): Unable to get supervised trackIds for user ".$this->userId);
      return array();
    }

    return $rows;
  }



  // This function is used during import and login to detect duplicates
  function GetUserIdByLogin($newLogin, $logDuplicates = 1) {

    //    if (!is_numeric($userId))
    //      return RC_INVALID_ARG;

    if (!$this->orgId) {
      // Using new db handle since static function
      error_log("*** ERROR: No Organization ID specified for duplicate login check");
      return RC_ORG_INIT_DATA_FAILED;
    }
    
    $db = new DatabaseObject(AUTH_DB_NAME, AUTH_DB_USER, AUTH_DB_PASS, DB_HOST);
    if (!$db)
      return RC_AUTH_DB_CONNECT_FAILED;
        
    //$authUserSqlColumns = unserialize(AUTH_USER_SQL_COLUMNS);
    //$db->Select(array($authUserSqlColumns[AUTH_USER_COLUMN_LOGIN_ID]),
    //	AUTH_USER_TABLE);
    //$db->Where($authUserSqlColumns[AUTH_USER_COLUMN_LOGIN_ID]."='".esql($newLogin)."'");
    //$db->Where($authUserSqlColumns[AUTH_ORG_COLUMN_ID]."=".$this->orgId);
    $authUserSqlColumns = unserialize(AUTH_USER_SQL_COLUMNS);
    $db->Select(array($authUserSqlColumns[AUTH_USER_COLUMN_ID]),
		AUTH_USER_TABLE);
    $db->Where($authUserSqlColumns[AUTH_USER_COLUMN_LOGIN_ID]."='".esql($newLogin)."'");
    $db->Where($authUserSqlColumns[AUTH_USER_COLUMN_ORGANIZATION_ID]."=".$this->orgId);

    $rows = $db->Query(MANY_VALUES, VALUES_FORMAT);

    // Test for multiple rows meaning duplicate usernames found
    if (count($rows) > 1) {
      if ($logDuplicates) {
	error_log("User.CheckUniqueOrgLogin($newLogin): Found logins to be > 1, this should never happen");
	error_log("\tROWS: ".print_r($rows, TRUE)."\n");
      }
      $db->Close();
      $db->Open($this->dbName, DB_USER, DB_PASS, DB_HOST);
      return RC_DUPLICATE_LOGIN;
    }

    // Return condition for no such user
    if (count($rows) == 0) {
      $db->Close();
      $db->Open($this->dbName, DB_USER, DB_PASS, DB_HOST);
      return RC_USER_UNKNOWN;
    }

    // Restore connection back to original database
    $db->Open($this->dbName, DB_USER, DB_PASS, DB_HOST);

    return $rows[0];
  }


  function DeleteUser($userId) {

    $rc = TRUE;

    $oldDbName = $this->dbName;
    if (!$this->Open($this->dbName, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST))
      return FALSE;

    // Delete from User assignment table
    $assignmentSqlColumns = unserialize(ASSIGNMENT_SQL_COLUMNS);
    $this->Where($assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID].'='.$userId);
    if (!$this->Delete(ASSIGNMENT_TABLE)) {
      error_log("User.DeleteUser($userId): Failed to delete user from ".ASSIGNMENT_TABLE);
      $rc = FALSE;
    }
    
    // Remove them as the contact for tracks or others if any
    $trackSqlColumns = unserialize(TRACK_SQL_COLUMNS);
    $this->Where($trackSqlColumns[TRACK_COLUMN_SUPERVISOR_ID].'='.$userId);
    if (!$this->Update(array($trackSqlColumns[TRACK_COLUMN_SUPERVISOR_ID] => 'NULL'),
		       TRACK_TABLE)) {
      error_log("User.DeleteUser($userId): Failed to update ".TRACK_COLUMN_SUPERVISOR_ID." with value NULL");
      $rc = FALSE;
    }

    // Remove as contact for any other users
    $this->Where($this->userSqlColumns[USER_COLUMN_CONTACT_ID].'='.$userId);
    if (!$this->Update(array($this->userSqlColumns[USER_COLUMN_CONTACT_ID] => 'NULL'),
		       USER_TABLE)) {
      error_log("User.DeleteUser($userId): Failed to update ".USER_TABLE." with value NULL");
      $rc = FALSE;
    }
    
    // Finally remove the user entry from User table entirely
    $this->Where($this->userSqlColumns[USER_COLUMN_ID].'='.$userId);
    if (!$this->Delete(USER_TABLE)) {
      error_log("User.DeleteUser($userId): Failed to delete user from ".USER_TABLE);
      $rc = FALSE;
    }
    
    // Now we have to remove this user from the global database
    // delete from the auth db
    $oldDbName = $this->dbName;
    if (!$this->Open(AUTH_DB_NAME, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST))
      return FALSE;
    
    $authUserSqlColumns = unserialize(AUTH_USER_SQL_COLUMNS);
    $this->Where($authUserSqlColumns[AUTH_USER_COLUMN_ID].'='.$userId);
    if (!$this->Delete(AUTH_USER_TABLE))
      return FALSE;
    
    if (!$this->Open($oldDbName))
      return FALSE;

    return $rc;
  }


  // Setup params for user list (optional for getting track participants)
  function SetupTrackParticipants($trackId, $showAll) {

    if (!is_numeric($trackId)) {
      error_log("User::SetupTrackParticipants(trackId=$trackId, showAll=$showAll): invalid argument for trackId");
      return RC_INVALID_ARG;
    }

    $this->participantTrackId = $trackId;
    if ($showAll)
      $this->participantShowAll = TRUE;
    else
      $this->participantShowAll = FALSE;

    return RC_OK;
  }


  // Purely an administrative function so hard setting the db is fine for now
  function GetUserList($columnIds, $searchColumnId, $searchFor, $sortColumnId,
		       $sortOrder, &$pageNumber, $rowsPerPage, $userId) {

    if (empty($columnIds)) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("User.GetUserList(): No columns selected for user list so immediately returning with empty result sets");
      return array();
    }

    if ($pageNumber <= 0) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("User.GetUserList(): Invalid Page Number($pageNumber) passed, adjusting to 1");
      $pageNumber = 1;
    }

    if ($rowsPerPage <= 0) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("User.GetUserList(): Invalid Rows Per Page($rowsPerPage) passed, adjusting to 1");
      $rowsPerPage = 1;
    }

    $userListColumns = array();
    $searchExact = FALSE;
    $orderedJoinArray = array();
    $leftJoinArray = array();
    $userIds = array();
    $userNames = array();
    $usr = new User($this->orgId, $userId);

    // Setup the sort by columns.  Since multiple users can have the same last name or email address, etc..
    // Then in order to get consistent results, we need to add the id column as a secondary sort field if
    // it isn't already there.
    if ($sortColumnId != USER_LIST_COLUMN_ID) {
      $sortColumns = array(USER_LIST_TABLE.'.'.$this->userListSqlColumns[$sortColumnId],
			   USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_ID]);
    } else {
      $sortColumns = USER_LIST_TABLE.'.'.$this->userListSqlColumns[$sortColumnId];
    }

    // Now only select the columns currently selected in the interface for display
    for ($i = 0; $i < count($this->userListSqlColumns); $i++) {
      if (1 << $i & $columnIds) {
	switch (1 << $i) {
	case USER_LIST_COLUMN_DIVISION_ID:
	  array_push($userListColumns, DIVISION_TABLE.'.'.$this->divisionSqlColumns[DIVISION_COLUMN_NAME]);
	  $leftJoinArray[DIVISION_TABLE] = array(USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_DIVISION_ID], '=',
					     DIVISION_TABLE.'.'.$this->divisionSqlColumns[DIVISION_COLUMN_ID]);

	  break;
	case USER_LIST_COLUMN_DEPARTMENT_ID:
	  array_push($userListColumns, DEPARTMENT_TABLE.'.'.$this->departmentSqlColumns[DEPARTMENT_COLUMN_NAME]);
	  $leftJoinArray[DEPARTMENT_TABLE] = array(USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_DEPARTMENT_ID], '=',
					       DEPARTMENT_TABLE.'.'.$this->departmentSqlColumns[DEPARTMENT_COLUMN_ID]);
	  break;
	case USER_LIST_COLUMN_LANGUAGE_ID:
	  array_push($userListColumns, LANGUAGE_TABLE.'.'.$this->languageSqlColumns[LANGUAGE_COLUMN_NAME]);
	  $leftJoinArray[LANGUAGE_TABLE] = array(USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_LANGUAGE_ID], '=',
					     LANGUAGE_TABLE.'.'.$this->languageSqlColumns[LANGUAGE_COLUMN_ID]);
	  break;
	case USER_LIST_COLUMN_EMPLOYMENT_STATUS_ID:
	  array_push($userListColumns, EMPLOYMENT_STATUS_TABLE.'.'.
		     $this->employmentStatusSqlColumns[EMPLOYMENT_STATUS_COLUMN_NAME]);
	  $leftJoinArray[EMPLOYMENT_STATUS_TABLE] = array(USER_LIST_TABLE.'.'.
						      $this->userListSqlColumns[USER_LIST_COLUMN_EMPLOYMENT_STATUS_ID], '=',
						      EMPLOYMENT_STATUS_TABLE.'.'.
						      $this->employmentStatusSqlColumns[EMPLOYMENT_STATUS_COLUMN_ID]);
	  break;
	case USER_LIST_COLUMN_DOMAIN_ID:
	  array_push($userListColumns, DOMAIN_TABLE.'.'.$this->domainSqlColumns[DOMAIN_COLUMN_ABBREVIATION]);
	  $leftJoinArray[DOMAIN_TABLE] = array(USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_DOMAIN_ID], '=',
					   DOMAIN_TABLE.'.'.$this->domainSqlColumns[DOMAIN_COLUMN_ID]);
	  break;
	default:
	  array_push($userListColumns, USER_LIST_TABLE.'.'.$this->userListSqlColumns[1 << $i]);
	  break;
	}
      }
    }

    // If this is a participant list we need the track id and name even if not explicity queried
    if ($this->participantTrackId) {
      array_push($userListColumns, TRACK_LIST_TABLE.'.'.$this->trackListSqlColumns[TRACK_LIST_COLUMN_NAME],
		 PARTICIPANT_TABLE.'.'.$this->participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID]);
      if ($this->participantShowAll) {
	if (!isset($orderedJoinArray[PARTICIPANT_TABLE]))
	  array_push($orderedJoinArray, array(LEFT_JOIN, PARTICIPANT_TABLE,
					      array(USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_ID], '=',
						    PARTICIPANT_TABLE.'.'.$this->participantSqlColumns[PARTICIPANT_COLUMN_USER_ID],
						    ' AND ', PARTICIPANT_TABLE.'.'.$this->participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID],
						    '=', $this->participantTrackId)));
      } else {
	if (!isset($orderedJoinArray[PARTICIPANT_TABLE]))
	  array_push($orderedJoinArray, array(RIGHT_JOIN, PARTICIPANT_TABLE,
					      array(USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_ID], '=',
						    PARTICIPANT_TABLE.'.'.$this->participantSqlColumns[PARTICIPANT_COLUMN_USER_ID],
						    ' AND ', PARTICIPANT_TABLE.'.'.$this->participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID],
						    '=', $this->participantTrackId)));
      }

      if (!isset($leftJoinArray[TRACK_LIST_TABLE]))
      	$leftJoinArray[TRACK_LIST_TABLE] = array(PARTICIPANT_TABLE.'.'.$this->participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID], '=',
      						 TRACK_LIST_TABLE.'.'.$this->trackListSqlColumns[TRACK_LIST_COLUMN_ID]);
    }
    
    // If getting participant list we also always query the employmentstatus
    if ($this->participantTrackId)
      array_push($userListColumns, USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_EMPLOYMENT_STATUS_ID]);

    // Always query the user id and name
    array_push($userListColumns, USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_ID]);
    array_push($userListColumns, USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_LAST_NAME]);
    array_push($userListColumns, USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_FIRST_NAME]);

    // This allows for easy schema modification
    $this->Select($userListColumns, USER_LIST_TABLE);
    if (!empty($orderedJoinArray))
      $this->OrderedJoinUsing($orderedJoinArray);
    if (!empty($leftJoinArray))
      $this->LeftJoinUsing($leftJoinArray);
    if (!empty($rightJoinArray))
      $this->RightJoinUsing($rightJoinArray);
    $this->SetupUserSearch($searchColumnId, $searchFor, $searchExact);
    if ($this->participantTrackId && !$this->participantShowAll)
      $this->Where(PARTICIPANT_TABLE.'.'.$this->participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID].'='.$this->participantTrackId);
    $this->SortBy($sortColumns, $sortOrder);
    $this->SetPage($pageNumber, $rowsPerPage);
    // Make sure super user is always excluded
    if ($userId != 1 || $this->orgId != 8)
      $this->Where(USER_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_ID]."!=1");
    switch($usr->role) {
    case ROLE_DIVISION_ADMIN:
      $this->Where(USER_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_DIVISION_ID].'='.$usr->divisionId);
      break;
    case ROLE_DEPARTMENT_ADMIN:
      $this->Where(USER_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_DEPARTMENT_ID].'='.$usr->departmentId);
      break;
    default:
      break;
    }
    $pageCount = $this->GetPageCount($rowsPerPage);
    if ($pageNumber > $pageCount && $pageCount > 0) {
      error_log("User.GetUserList(): Page Number ($pageNumber) is greater than actual pages ($pageCount)");
      $pageNumber = $pageCount;
      error_log("Calling SetPage with pageNumber=$pageNumber, rowsPerPage=$rowsPerPage");
      $this->SetPage($pageNumber, $rowsPerPage);
    }
    $results = $this->Query(MANY_ROWS, VALUES_FORMAT);

    // Add actions for this listing
    for($i = 0; $i < count($results); $i++) {
      array_push($userNames, array_pop($results[$i]).' '.array_pop($results[$i]));
      array_push($userIds, array_pop($results[$i]));
      if ($this->participantTrackId) {
	$tmpEmploymentStatusId = array_pop($results[$i]);
	$trackId = array_pop($results[$i]);
	$trackName = array_pop($results[$i]);
	$ref1->href=$_SERVER['PHP_SELF'];
	//error_log($trackId." == ".$this->participantTrackId);
	if ($trackId == $this->participantTrackId) {
	  $ref1 = new TplCaption('Remove');
	  $ref1->href=$_SERVER['PHP_SELF'];
	  $ref1->isService = TRUE;
	  if ($this->participantShowAll)
	    $ref1->onClick="svcToggleClassParticipant('$userIds[$i]', '$this->participantTrackId', $i, 0);";
	  else
	    $ref1->onClick="svcToggleClassParticipant('$userIds[$i]', '$this->participantTrackId', $i, 1);";
	  array_push($results[$i], $ref1);
	} elseif ($tmpEmploymentStatusId == 3) {
	  array_push($results[$i], 'Terminated');
	} else {
	  $ref1 = new TplCaption('Add to Class');
	  $ref1->href=$_SERVER['PHP_SELF'];
	  $ref1->isService = TRUE;
	  $ref1->onClick="svcToggleClassParticipant('$userIds[$i]', '$this->participantTrackId', $i, 0);";
	  array_push($results[$i], $ref1);
	}
      } else {
	// Add actions for this listing
	$ref1 = new TplCaption('Edit');
	$ref1->href='user_edit.php';
	$ref2 = new TplCaption('Delete');
	$ref2->href=$_SERVER['PHP_SELF'];
	$ref2->isService = TRUE;
	$ref2->onClick="svcDeleteUser('$userIds[$i]', '$userNames[$i]', $i);";
	array_push($results[$i], array($ref1, $ref2));
      }
    }

    return array($userIds, $results);
  }


  function GetSearchFieldNamebyId($searchColumnId) {
    
    $searchField = '';

    switch($searchColumnId) {
    case USER_LIST_COLUMN_ID:
      $searchField = USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_ID];
      break;
    case USER_LIST_COLUMN_LOGIN:
      $searchField = USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_LOGIN];
      break;
    case USER_LIST_COLUMN_LAST_NAME:
      $searchField = USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_LAST_NAME];
      break;
    case USER_LIST_COLUMN_FIRST_NAME:
      $searchField = USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_FIRST_NAME];
      break;
    case USER_LIST_COLUMN_EMAIL:
      $searchField = USER_LIST_TABLE.'.'.$this->userListSqlColumns[USER_LIST_COLUMN_EMAIL];
      break;
    case USER_LIST_COLUMN_DIVISION_ID:
      $searchField = DIVISION_TABLE.'.'.$this->divisionSqlColumns[DIVISION_COLUMN_NAME];
      break;
    case USER_LIST_COLUMN_DEPARTMENT_ID:
      $searchField = DEPARTMENT_TABLE.'.'.$this->departmentSqlColumns[DEPARTMENT_COLUMN_NAME];
      break;
    case USER_LIST_COLUMN_LANGUAGE_ID:
      $searchField = LANGUAGE_TABLE.'.'.$this->languageSqlColumns[LANGUAGE_COLUMN_NAME];
      break;
    case USER_LIST_COLUMN_EMPLOYMENT_STATUS_ID:
      $searchField = EMPLOYMENT_STATUS_TABLE.'.'.$this->employmentStatusSqlColumns[EMPLOYMENT_STATUS_COLUMN_NAME];
      break;
    case USER_LIST_COLUMN_DOMAIN_ID:
      $searchField = DOMAIN_TABLE.'.'.$this->domainSqlColumns[DOMAIN_COLUMN_NAME];
      break;
    default:
      $searchField = '';
      error_log("User.GetSearchFieldNameById($searchColumnId): Unknown search column specified in first arg");
      break;
    }
    
    return $searchField;
  }

  function SetupUserSearch($searchColumnId, $searchFor, $searchExact = FALSE) {

    if (gettype($searchColumnId) == 'array') {
      for($i = 0; $i < count($searchColumnId); $i++) {
	if ($searchFor[$i]) {
	  // Override if necessary searchExact param
	  if ($searchColumnId[$i] == USER_LIST_COLUMN_ID && is_numeric($searchFor[$i]))
	    $searchExactParam = TRUE;
	  else
	    $searchExactParam = $searchExact;
	  $searchField = $this->GetSearchFieldNameById($searchColumnId[$i]);
	  $this->SearchBy($searchField, $searchFor[$i], $searchExactParam);
	}
      }
    } else {
      if ($searchFor) {
	// Override if necessary searchExact param
	if ($searchColumnId == USER_LIST_COLUMN_ID && is_numeric($searchFor))
	  $searchExact = TRUE;
	$searchField = $this->GetSearchFieldNameById($searchColumnId);
	$this->SearchBy($searchField, $searchFor, $searchExact);
      }
    }
  }


  function UpdateUser() {

    // This is also used to create new users.  Any time the userId is 0
    // we know it is a new user and must be inserted instead of updated
    if ($this->userId == 0) {

      // add to the auth db
      $oldDbName = $this->dbName;
      if (!$this->Open(AUTH_DB_NAME, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST))
	return RC_AUTH_DB_CONNECT_FAILED;
      
      $authSqlColumns = unserialize(AUTH_USER_SQL_COLUMNS);
      $this->userId = $this->Insert(array($authSqlColumns[AUTH_USER_COLUMN_ID] => $this->userId,
					  $authSqlColumns[AUTH_USER_COLUMN_LOGIN_ID] => $this->login,
					  $authSqlColumns[AUTH_USER_COLUMN_PASSWORD] => crypt($this->password, makesalt()),
					  $authSqlColumns[AUTH_USER_COLUMN_ORGANIZATION_ID] => $this->orgId),
				    AUTH_USER_TABLE);
      if (!$this->userId) {
	$this->userId = 0;
	return RC_INSERT_FAILED;
      }
      
      if (!$this->Open($oldDbName))
	return RC_ORG_DB_CONNECT_FAILED;
      
      $rc = $this->Insert(array($this->userSqlColumns[USER_COLUMN_ID] => $this->userId,
				$this->userSqlColumns[USER_COLUMN_FIRST_NAME] => $this->firstName,
				$this->userSqlColumns[USER_COLUMN_LAST_NAME] => $this->lastName,
				$this->userSqlColumns[USER_COLUMN_FULL_NAME] => $this->lastName.', '.$this->firstName,
				$this->userSqlColumns[USER_COLUMN_PHONE] => $this->phone,
				$this->userSqlColumns[USER_COLUMN_LOGIN] => $this->login,
				$this->userSqlColumns[USER_COLUMN_EMAIL] => $this->email,
				$this->userSqlColumns[USER_COLUMN_CONTACT_ID] => $this->contactId,
				$this->userSqlColumns[USER_COLUMN_USE_SUPERVISOR] => $this->useSupervisor,
				$this->userSqlColumns[USER_COLUMN_LANGUAGE_ID] => $this->languageId,
				$this->userSqlColumns[USER_COLUMN_DEPARTMENT_ID] => $this->departmentId,
				$this->userSqlColumns[USER_COLUMN_DIVISION_ID] => $this->divisionId,
				$this->userSqlColumns[USER_COLUMN_EMPLOYMENT_STATUS_ID] => $this->employmentStatusId,
				$this->userSqlColumns[USER_COLUMN_DOMAIN_ID] => $this->domainId,
				$this->userSqlColumns[USER_COLUMN_CUSTOM1_ID] => $this->custom1Id,
				$this->userSqlColumns[USER_COLUMN_CUSTOM2_ID] => $this->custom2Id,
				$this->userSqlColumns[USER_COLUMN_CUSTOM3_ID] => $this->custom3Id,
				$this->userSqlColumns[USER_COLUMN_ROLE] => $this->role),
			  USER_TABLE);

      if ($rc === FALSE) {
	$this->cacheError = $this->GetErrorMessage();
	return RC_UPDATE_INFO_FAILED;
      }

    } else {

      // update the auth db
      $oldDbName = $this->dbName;
      if (!$this->Open(AUTH_DB_NAME, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST))
	return RC_AUTH_DB_CONNECT_FAILED;
      
      $authSqlColumns = unserialize(AUTH_USER_SQL_COLUMNS);
      $this->Where($authSqlColumns[AUTH_USER_COLUMN_ID].'='.$this->userId);
      if ($this->password)
	$rc = $this->Update(array($authSqlColumns[AUTH_USER_COLUMN_LOGIN_ID] => "$this->login",
				  $authSqlColumns[AUTH_USER_COLUMN_PASSWORD] => crypt($this->password, makesalt())),
			    AUTH_USER_TABLE);
      else
	$rc = $this->Update(array($authSqlColumns[AUTH_USER_COLUMN_LOGIN_ID] => "$this->login"),
			    AUTH_USER_TABLE);

      if (!$rc) {
	error_log("User.UpdateUser(): FAILED UPDATE USER id=".$this->userId.": ".$this->GetErrorMessage());
	return RC_UPDATE_FAILED;
      }

      
      if (!$this->Open($oldDbName))
	return RC_ORG_DB_CONNECT_FAILED;

      $this->Where($this->userSqlColumns[USER_COLUMN_ID].'='.$this->userId);
      $rc = $this->Update(array($this->userSqlColumns[USER_COLUMN_FIRST_NAME] => $this->firstName,
				$this->userSqlColumns[USER_COLUMN_LAST_NAME] => $this->lastName,
				$this->userSqlColumns[USER_COLUMN_FULL_NAME] => $this->lastName.', '.$this->firstName,
				$this->userSqlColumns[USER_COLUMN_PHONE] => $this->phone,
				$this->userSqlColumns[USER_COLUMN_LOGIN] => "$this->login",
				$this->userSqlColumns[USER_COLUMN_EMAIL] => $this->email,
				$this->userSqlColumns[USER_COLUMN_CONTACT_ID] => $this->contactId,
				$this->userSqlColumns[USER_COLUMN_USE_SUPERVISOR] => $this->useSupervisor,
				$this->userSqlColumns[USER_COLUMN_LANGUAGE_ID] => $this->languageId,
				$this->userSqlColumns[USER_COLUMN_DEPARTMENT_ID] => $this->departmentId,
				$this->userSqlColumns[USER_COLUMN_DIVISION_ID] => $this->divisionId,
				$this->userSqlColumns[USER_COLUMN_EMPLOYMENT_STATUS_ID] => $this->employmentStatusId,
				$this->userSqlColumns[USER_COLUMN_DOMAIN_ID] => $this->domainId,
				$this->userSqlColumns[USER_COLUMN_CUSTOM1_ID] => $this->custom1Id,
				$this->userSqlColumns[USER_COLUMN_CUSTOM2_ID] => $this->custom2Id,
				$this->userSqlColumns[USER_COLUMN_CUSTOM3_ID] => $this->custom3Id,
				$this->userSqlColumns[USER_COLUMN_ROLE] => $this->role),
			  USER_TABLE);
      if (!$rc) {
	$this->cacheError = $this->GetErrorMessage();
	return RC_UPDATE_INFO_FAILED;
      }

    }

    // If the password was to be reset it has been taken care of by now
    // So now we need to blank it again to tell the app not to change it anymore
    $this->password = '';

    //$this->Close();
    return RC_OK;
  }


  // Add a user to a track
  function AssignToTrack($trackId, $userId = 0) {
    
    $useUserId = $userId;

    // If the userId is 0 the current user is assumed but if not this can be used statically
    if ($useUserId == 0)
      $useUserId = $this->userId;
    
    // If it is still 0 we are hosed
    if ($useUserId == 0) {
      error_log("User::AssignToTrack(trackId=$trackId, userId=$userId): Invalid user and the user objects id has not been set");
      return FALSE;
    }

    if (!$this->Replace(array($this->participantSqlColumns[PARTICIPANT_COLUMN_USER_ID] => $useUserId,
			      $this->participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID] => $trackId,
			      $this->participantSqlColumns[PARTICIPANT_COLUMN_PLACED_DATE] => 'NOW()'),
			PARTICIPANT_TABLE)) {
      error_log("User::AssignToTrack(trackId=$trackId, userId=$userId): Failed to add user ".$useUserId.' to track');
      return FALSE;
    }
    
    return RC_OK;
  }
  
  
  // Remove a user from a track
  function RemoveFromTrack($trackId, $userId = 0) {
    
    $useUserId = $userId;

    // If the userId is 0 the current user is assumed but if not this can be used statically
    if ($useUserId == 0)
      $useUserId = $this->userId;
    
    // If it is still 0 we are hosed
    if ($useUserId == 0) {
      error_log("User::RemoveFromTrack(trackId=$trackId, userId=$userId): Invalid user and the user objects id has not been set");
      return FALSE;
    }

    $this->Where($this->participantSqlColumns[PARTICIPANT_COLUMN_USER_ID].'='.$useUserId.' AND '.
		 $this->participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID].'='.$trackId);
    if (!$this->Delete(PARTICIPANT_TABLE)) {
      error_log("User::RemoveFromTrack(trackId=$trackId, userId=$userId): Failed to remove user ".$useUserId.' from track');
      return FALSE;
    }

    return RC_OK;
  }


  // Check to see if member is in track
  function MemberOfTrack($trackId, $userId = 0) {
    $useUserId = $userId;

    // If the userId is 0 the current user is assumed but if not this can be used statically
    if ($useUserId == 0)
      $useUserId = $this->userId;
    
    // If it is still 0 we are hosed
    if ($useUserId == 0) {
      error_log("User::RemoveFromTrack(trackId=$trackId, userId=$userId): Invalid user and the user objects id has not been set");
      return RC_USER_UNKNOWN;
    }

    $this->Select($this->participantSqlColumns[PARTICIPANT_COLUMN_ID], PARTICIPANT_TABLE);
    $this->Where($this->participantSqlColumns[PARTICIPANT_COLUMN_USER_ID].'='.$useUserId.' AND '.
		 $this->participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID].'='.$trackId);
    $res = $this->QueryCount();
    $this->ReInit();
    if ($res === FALSE) {
      error_log("CRIT(service): Failed to query user to see who was in track");
      return RC_QUERY_FAILED;
    }

    if ($res > 0)
      return TRUE;
    
    return FALSE;
  }


  // Remove all users from track
  function AssignAllToTrack($trackId, $removeFromTrackId = 0) {
    if (!is_numeric($trackId) || $trackId < 0 || !is_numeric($removeFromTrackId) || $removeFromTrackId < 0) {
      error_log("User::AssignAllToTrack(trackId=$trackId, removeFromTrackId=$removeFromTrackId): "
		."trackId must be nonnegative numeric value");
      return FALSE;
    }

    // If trackId is zero we are removing all users from a given track
    if ($trackId == 0) {
      $this->Where($this->participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID].'='.$removeFromTrackId);
      $this->Delete(PARTICIPANT_TABLE);
    } else {
      // Update all users with the new trackId
      $sqlStr = 'REPLACE INTO '.PARTICIPANT_TABLE.' ('.$this->participantSqlColumns[PARTICIPANT_COLUMN_USER_ID].', '
	.$this->participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID].', '
	.$this->participantSqlColumns[PARTICIPANT_COLUMN_PLACED_DATE].') '
	.'(SELECT '.$this->userSqlColumns[USER_COLUMN_ID].", $trackId, NOW() FROM ".USER_TABLE
	.' WHERE '.$this->userSqlColumns[USER_COLUMN_ID].' != 1 AND '
	.$this->userSqlColumns[USER_COLUMN_EMPLOYMENT_STATUS_ID].' != 3)';
      if (!$this->Execute($sqlStr)) {
	error_log("User::AssignAllToTrack(trackId=$trackId, removeFromTrackId=$removeFromTrackId): "
		  ."Failed to replace into participant table");
	return FALSE;
      }
    }

    return TRUE;
  }


  // Remove all users from this system.  This is incredibly dangerous and should
  // be limited to super user access only
  function DeleteAll($thisOrgId) {

    $orgId = (int)$thisOrgId;

    $db = new DatabaseObject(AUTH_DB_NAME, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST);
    if (!$db) {
      error_log("User::Delete(orgId=$orgId): Failed to connect to ".AUTH_DB_NAME.' database');
      return RC_AUTH_DB_CONNECT_FAILED;
    }
    
    $authUserSqlColumns = unserialize(AUTH_USER_SQL_COLUMNS);
    $db->Where($authUserSqlColumns[AUTH_USER_COLUMN_ORGANIZATION_ID]."='$orgId'");
    $db->Delete(AUTH_USER_TABLE);

    if (!$db->Open(ORG_DB_NAME_PREFIX.$orgId)) {
      error_log("User::Delete(orgId=$orgId): Failed to reopen organization database");
      return RC_ORG_DB_CONNECT_FAILED;
    }

    // To maintain referential integrity we must delete
    // everything associated with all users
    
    // Remove everyone from all tables that store the user id
    $db->Delete(PARTICIPANT_TABLE, TRUE);
    $db->Delete(ASSIGNMENT_TABLE, TRUE);
    $db->Delete(USER_ASSIGNMENT_TABLE, TRUE);
    $db->Delete(USER_TABLE, TRUE);
    $db->Close();
    
    return RC_OK;
  }

  
  // Import users from csv file uploaded by an admin user
  function ImportFile($fileName, $sync, &$msg, $userId) {

    // Local vars
    $fh = NULL;
    $userHeader = array();
    $userData = array();
    $tempUserList = array();
    $tempUser = array();
    $existingUserList = array();
    $emptyFieldsCheck = array();
    $roleDescriptors = array();
    $updateUserList = array();
    $divisions = array();
    $divisionsToCreate = array();
    $departments = array();
    $departmentsToCreate = array();
    $domains = array();
    $roles = array();
    $lineNumber = 1;
    $pageNumber = 1;
    $usersImported = 0;
    $usersUpdated = 0;
    $tmpmsg = '';
    $msg = '';
    $i = 0;

    // Verify file exists and is readable
    if (!is_readable($fileName)) {
      error_log("User.ImportFile(Unable to read uploaded file '$fileName' in tmp dir '".
		sys_get_temp_dir()."', please have sysadmin admin check this");
      $msg .= "Failed to read from CSV file.<BR>\n";
      return RC_OPEN_FILE_FAILED;
    }

    // Open the CSV file
    $fh = fopen($fileName, 'r');
    if ($fh === FALSE) {
      error_log("User.ImportFile($fileName,$msg): Failed to open file");
      $msg .= "Failed to open CSV file.<BR>\n";
      return RC_OPEN_FILE_FAILED;
    }
    
    $roleDescriptors = array_flip(unserialize(ROLE_SELECT_LABELS));
    $languages = Organization::GetLanguages($this->orgId);
    $statuses = array_change_key_case(array_flip(Organization::GetEmploymentStatusValues($this->orgId)), CASE_LOWER);
    $domains = Organization::GetDomainAbbreviations($this->orgId);
    $divisions = array_change_key_case(array_flip(Organization::GetDivisions($this->orgId)), CASE_LOWER);
    $departments = array_change_key_case(array_flip(Organization::GetDepartments($this->orgId)), CASE_LOWER);
    $domains = array_change_key_case(array_flip(Organization::GetDomainAbbreviations($this->orgId)), CASE_LOWER);
    $tracks = array_change_key_case(array_flip(Track::GetTracks($this->orgId)), CASE_LOWER);
				 
    //error_log("DIVISIONS: ".print_r($divisions, TRUE));
    //error_log("DEPARTMENTS: ".print_r($departments, TRUE));
    //error_log("DOMAINS: ".print_r($domains, TRUE));
				 //    error_log("STATUSES: ".print_r($statuses, TRUE));
				 //    return RC_OK;
    if ($divisions === FALSE) {
      error_log("User.Importfile($fileName, $msg): failed to read divisions from database");
      $msg .= "User.Importfile($fileName, $msg): failed to read divisions from database";
    }

    if ($departments === FALSE) {
      error_log("User.Importfile($fileName, $msg): failed to read departments from database");
      $msg .= "User.Importfile($fileName, $msg): failed to read departments from database";
    }

    if ($tracks === FALSE) {
      error_log("User.Importfile($fileName, $msg): failed to read classes from database");
      $msg .= "User.Importfile($fileName, $msg): failed to read classes from database";
    }

    // Read column header from CSV
    $userHeader = fgetcsv($fh, 16384);
    if ($userHeader === FALSE) {
      error_log("User.ImportFile($fileName, $msg): failed to read csv header info");
      $msg .= "Failed to read header from CSV file.<BR>\n";
      return RC_CSV_READ_FAILED;
    }

    if (count($userHeader) < 15) {
      $msg .= "Invalid number of columns in first line of import file, at least 15 columns must exist.<BR>\n";
      return RC_CSV_INVALID_DATA_FORMAT;
    }

    // First we need to validate all the data and usernames
    while ($userData = fgetcsv($fh, 1024)) {
      $lineNumber++;

      // If all fields empty then skip line and move on
      $emptyFieldsCheck = array_filter($userData);
      if (empty($emptyFieldsCheck))
      	continue;

      if (count($userData) < 15) {
	$msg .= "Invalid number of columns, at least 15 columns must exist on import at line $lineNumber, skipping...<BR>\n";
	continue;
      }

      // Cleanup spurious whitespace
      for($i = 0; $i < 15; $i++)
	$userData[$i] = trim($userData[$i]);

      // Perform data validation
      if (!isset($userData[0]) || !strlen($userData[0]))
	$msg .= "Mandatory field username missing at line $lineNumber in import file.<BR>\n";
      else {
	$rc = $this->GetUserIdByLogin($userData[0], 1);
	if ($sync && $rc > 0)
	  $updateUserList[$rc] = $userData[0];
	else if (!$sync && $rc > 0)
	  $msg .= "User $userData[0] at line $lineNumber already exists, if syncing please select synchronize, otherwise change the login name.<BR>\n";
	else if ($rc < 0 && $rc != RC_USER_UNKNOWN)
	  $msg .= "Fatal error while checking for duplicate logins: ".RcToText($rc)."<BR>\n";
	// else we create the user as new which requires no special action here.
      }
      if (strlen($userData[0]) > MAX_USERNAME_LENGTH)
	$msg .= "Username $userData[0] exceeds max of ".MAX_USERNAME_LENGTH." characters.<BR>\n";
      if (!isset($userData[1]) || !strlen($userData[1]))
	$msg .= "Mandatory field password missing for user $userData[0] at line $lineNumber in import file.<BR>\n";
      if (strlen($userData[1]) > MAX_PASSWORD_LENGTH)
	$msg .= "Password for user $userData[0] at line $lineNumber exceeds max of ".MAX_USERNAME_LENGTH." characters.<BR>\n";
      if (!strlen($userData[2]))
	$msg .= "Mandatory field first name missing for user $userData[0] at line $lineNumber in import file.<BR>\n";
      if (strlen($userData[2]) > MAX_FIRST_LENGTH)
	$msg .= "First Name for user $userData[0] longer than max of ".MAX_FIRST_LENGTH." characters.<BR>\n";
      if (!strlen($userData[3]))
	$msg .= "Mandatory field last name missing for user $userData[0] at line $lineNumber in import file.<BR>\n";
      if (strlen($userData[3]) > MAX_LAST_LENGTH)
	$msg .= "Last Name for user $userData[0] on line $lineNumber exceeds max of ".MAX_LAST_LENGTH." characters.<BR>\n";
      if (strlen($userData[4]) > MAX_EMAIL_LENGTH)
	$msg .= "Email Address for user $userData[0] on line $lineNumber exceeds max of ".MAX_EMAIL_LENGTH." characters.<BR>\n";
      if ($userData[5] && !isset($divisions[strtolower($userData[5])]))
	$divisionsToCreate[trim($userData[5])] = 1;
	//$msg .= "Division '$userData[5]' specified for user $userData[0] on line $lineNumber is invalid.<BR>\n";
      if ($userData[6] && !isset($departments[strtolower($userData[6])]))
	$departmentsToCreate[trim($userData[6])] = 1;
	//$msg .= "Department '$userData[6]' specified for user $userData[0] on line $lineNumber is invalid.<BR>\n";
      if ($userData[7] && strncasecmp($userData[7], 'yes', 3) && strncasecmp($userData[7], 'no', 2))
	$msg .= "Invalid value for user $userData[0] on line $lineNumber in column $userHeader[7], only Yes or No allowed.<BR>\n";
      if ($userData[8] && strncasecmp($userData[8], 'yes', 3) && strncasecmp($userData[8], 'no', 2))
	$msg .= "Invalid value for user $userData[0] on line $lineNumber in column $userHeader[8], only Yes or No allowed.<BR>\n";
      if ($userData[9] && strncasecmp($userData[9], 'english', 7))
 	$msg .= "Invalid language for user $userData[0] at line $lineNumber in column $userHeader[9], only english supported.<BR>\n";
      if ($userData[10] && !preg_match("/active/i", $userData[10]) && !preg_match("/terminat/i", $userData[10])
	  &&!preg_match("/leave/i", $userData[10]))
	$msg .= "Invalid status for user $userData[0] at line $lineNumber in column $userHeader[10], ".
	  "only active, inactive, and on leave are valid, '$userData[10]' was provided.<BR>\n";
      if ($userData[11] && !isset($domains[strtolower($userData[11])]))
	    $msg .= "Domain '$userData[11]' specified for user $userData[0] on line $lineNumber is invalid.<BR>\n";
      if ($userData[12] && !isset($tracks[strtolower($userData[12])]))
	$msg .= "User $userData[0] at line $lineNumber the class: $userData[12] must be created before users can be placed into it.<BR>\n";
      if ((int)(str_replace('-', '', trim($userData[13]))) == 0)
	$userData[13] = '';
      if (strlen($userData[13]) > 15)
	$msg .= "User $userDAta[0] at line $lineNumber has too long of a phone number, phone numbers may be up to 15 characters in length.<BR>\n";
      $userData[14] = trim($userData[14]);
      if (preg_match("/^div.*\s+admin.*/i", $userData[14]))
	$userData[14] = 'Division Administrator';
      elseif (preg_match("/^dep.*\s+admin.*/i", $userData[14]))
	$userData[14] = 'Department Administrator';
      if ($userData[14] && (strncasecmp($userData[14], 'Administrator', 13) && strncasecmp($userData[14], 'Department Administrator', 24) &&
			    strncasecmp($userData[14], 'Division Administrator', 22) && strncasecmp($userData[14], 'Regular Employee/User', 21) &&
			    strncasecmp($userData[14], 'Contact or Class Supervisor', 27)))
	$msg .= "Invalid value for user $userData[0] on line $lineNumber in column $userHeader[14], '$userData[14]' is not a valid role. '".trim($userData[14])."<BR>\n";
    }

    if ($msg) {
      $msg .= "0 Users imported.  Please fix any data errors reported and try again.<BR>\n";
      return FALSE;
    }

    // Create divisions and departments for the users in the import.
    if (count($divisionsToCreate) > 0 || count($departmentsToCreate) > 0) {
      $org = new Organization($this->orgId);
      foreach (array_keys($divisionsToCreate) as $div) {
	$rc = $org->SetData(ORG_DATA_EDIT_DIVISIONS, '', $div);
	if ($rc == RC_INVALID_ARG || $rc == FALSE)
	  $msg .= "Unable to automatically add new division: $div, ".RcToText($rc)."<BR>\n";
	else
	  $tmpmsg .= "<BR>&nbsp;&nbsp;&nbsp;Automatically added division: $div\n";
      }
      $tmpmsg .= "<BR>\n";
      foreach (array_keys($departmentsToCreate) as $dept) {
	$rc = $org->SetData(ORG_DATA_EDIT_DEPARTMENTS, '', $dept);
	if ($rc == RC_INVALID_ARG || $rc == FALSE)
	  $msg .= "Unable to automatically add new department: $dept, ".RcToText($rc)."<BR>\n";
	else
	  $tmpmsg .= "<BR>&nbsp;&nbsp;&nbsp;Automatically added department: $dept\n";
      }
      $tmpmsg .= "<BR>\n";
    }

    // Go back to beginning of file after validating data
    if (!rewind($fh)) {
      $msg .= "FATAL ERROR: Unable to reset file pointer after data validate, contact admin.<BR>\n";
      return FALSE;
    }

    // Eat the header
    fgetcsv($fh, 1024);

    // Get the list of existing users from the database so we know which ones to update and which ones to create
    $tempUserList = $this->GetUserList(USER_LIST_COLUMN_LOGIN, NULL, NULL, USER_LIST_COLUMN_LOGIN,
				       TRUE, $pageNumber, 50000, $userId);
    foreach ($tempUserList[1] as $tempUser)
      if ($tempUser[0] != 1)
	array_push($existingUserList, $tempUser[0]);

    // Second run through file actually performing import
    while ($userData = fgetcsv($fh, 1024)) {
      
      // Fix ups
      if (empty($userData[14]))
	$userData[14] = 'Regular Employee/User';

      // If all fields empty then skip line and move on
      $emptyFieldsCheck = array_filter($userData);
      if (empty($emptyFieldsCheck))
      	continue;

      if ($sync) {
	$updateUserId = array_search($userData[0], $updateUserList);
	if ($updateUserId === NULL || $updateUserId === FALSE)
	  $this->UserReInit($this->orgId);
	else {
	  $this->UserReInit($this->orgId);
	  $this->SetUserId($updateUserId);
	}
      } else {
	$this->UserReInit($this->orgId);
      }

      $this->login = $userData[0];
      $this->password = $userData[1];
      $this->firstName = $userData[2];
      $this->lastName = $userData[3];
      if ($userData[4])
	$this->email = $userData[4];
      else
	$this->email = '';
      if ($userData[5])
	$this->divisionId = (int)($divisions[strtolower($userData[5])]);
      if ($userData[6])
	$this->departmentId = (int)($departments[strtolower($userData[6])]);


      if (!strncasecmp($userData[7], 'yes', 3))
	$this->useSupervisor = TRUE;
      if (!strncasecmp($userData[8], 'yes', 3))
	$this->role = ROLE_CLASS_CONTACT_SUPERVISOR;
      if ($userData[9])
      	$this->languageId = (int)(array_search(strtolower($userData[9]), array_map('strtolower', $languages)));
      else
	$this->languageId = 1;
      $userData[10] = trim($userData[10]);
      if ($userData[10]) {
	if (preg_match("/active/i", $userData[10]))
	  $userData[10] = 'active';
	elseif (preg_match("/leave/i", $userData[10]))
	  $userData[10] = 'on leave';
	elseif (preg_match("/terminate/i", $userData[10]))
	  $userData[10] = 'terminated';
      	$this->employmentStatusId = (int)($statuses[$userData[10]]);
      } else
	$this->employmentStatusId = 1;

      if ($userData[11])
	$this->domainId = (int)($domains[strtolower($userData[11])]);
      else
	$this->domainId = 1;

      $this->phone = trim($userData[13]);

      $userData[14] = trim($userData[14]);
      if (preg_match("/^div.*\s+admin.*/i", $userData[14]))
	$userData[14] = 'Division Administrator';
      elseif (preg_match("/^dep.*\s+admin.*/i", $userData[14]))
	$userData[14] = 'Department Administrator';
      $this->role = $roleDescriptors[$userData[14]];

      //      $this->DumpAll();
      
      $rc = $this->UpdateUser();
      if ($rc != RC_OK) {
	$msg .= "Failed to create new user $userData[0]: ".RcToText($rc).".<BR>\n";
      } else {
	$usersImported++;
	if (!empty($userData[12]))
	  $this->AssignToTrack($tracks[strtolower($userData[12])]);
      }
    }

    // Terminate all users not in the import if sync is enabled
    if ($sync) {
      for ($i = 0; $i < count($existingUserList); $i++) {
	$this->UserReInit($this->orgId);
	$rc = $this->SetUserId($tempUserList[0][$i]);
	if ($this->employmentStatusId != 3 &&
	    (array_search($existingUserList[$i], $updateUserList) === NULL ||
	     array_search($existingUserList[$i], $updateUserList) === FALSE)) {
	  $this->employmentStatusId = 3;
	  //$this->DumpAll();

	  $rc = $this->UpdateUser();
	  if ($rc != RC_OK)
	    $msg .= "Failed to set status to Terminated for existing non-imported user ".$this->login.": ".RcToText($rc).".<BR>\n";
	  else
	    $usersUpdated++;
	}
      }
      if ($usersUpdated)
	$msg .= "Successfully terminated $usersUpdated accounts not included in the import.<BR>\n";
    }
    
    if ($usersImported)
      $msg .= "Successfully imported $usersImported user account(s).<BR>$tmpmsg<BR>\n";
    
    return TRUE;
  }


  function GetParticipantTracks() {
    
    $trackSqlColumns = unserialize(TRACK_SQL_COLUMNS);

    $this->Select(array(PARTICIPANT_TABLE.'.'.$this->participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID],
			TRACK_TABLE.'.'.$trackSqlColumns[TRACK_COLUMN_NAME]),
		  PARTICIPANT_TABLE);
    $this->LeftJoinUsing(TRACK_TABLE, $this->participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID]);
    $this->SearchBy($this->participantSqlColumns[PARTICIPANT_COLUMN_USER_ID], $this->userId, TRUE);
    $results = $this->Query(NAME_VALUES);
    return $results;
  }


  function UserReInit($orgId) {

    if (!$orgId) {
      error_log("User::ReInit(orgId=$orgId): Invalid orgId");
      return FALSE;
    }

    $this->orgId = (int)$orgId; // The organization ID to which the user belongs
    $this->userId = 0;
    $this->login = '';
    $this->firstName = '';
    $this->lastName = '';
    $this->password = '';
    $this->email = '';
    $this->phone = '';
    $this->useSupervisor = FALSE;
    $this->languageId = 1;
    $this->contactId = 0;
    $this->departmentId = 0;
    $this->divisionId = 0;
    $this->employmentStatusId = 0;
    $this->domainId = 0;
    $this->custom1Id = 0;
    $this->custom2Id = 0;
    $this->custom3Id = 0;
    $this->employmentStatusDate = '';
    $this->role = ROLE_END_USER;
    
    // Special variable for determining if we are using consultants or not
    $this->participantTrackId = 0;
    
    // These were carried over from the Organization
    // Class and could very well apply here as well
    $this->cacheError = '';
    $this->rc = 0;
    $this->myqslOutput = '';
  }


  function DumpAll() {

    $roles = array_flip(unserialize(ROLES));

    // Sql Columns
    /*
    error_log("USER_LIST_SQL_COLUMNS:");
    error_log(print_r($userListSqlColumns, TRUE));
    error_log("USER_LIST_LABEL_COLUMNS:");
    error_log(print_r($userListLabelColumns, TRUE));
    error_log("USER_SQL_COLUMNS:");
    error_log(print_r($this->userSqlColumns), TRUE);
    error_log("TRACK_LIST_SQL_COLUMNS:");
    error_log(print_r($trackListSqlColumns), TRUE);
    error_log("DIVISION_SQL_COLUMNS:");
    error_log(print_r($divisionSqlColumns), TRUE);
    error_log("DEPARTMENT_SQL_COLUMNS:");
    error_log(print_r($departmentSqlColumns), TRUE);
    error_log("LANGUAGE_SQL_COLUMNS:");
    error_log(print_r($languageSqlColumns), TRUE);
    error_log("EMPLOYMENT_SQL_COLUMNS:");
    error_log(print_r($employmentStatusSqlColumns), TRUE);
    error_log("DOMAIN_SQL_COLUMNS:");
    error_log(print_r($domainSqlColumns), TRUE);
    */

    error_log("-------------------------------------------------------------------------------------------------------------> "
	      ."USER DUMP:orgId=$this->orgId, userId=$this->userId, login=$this->login, password=$this->password, "
	      ."firstName=$this->firstName, lastName=$this->lastName, phone=$this->phone, email=$this->email, "
	      ."useSupervisor=$this->useSupervisor, languageId=$this->languageId, contactId=$this->contactId, "
	      ."departmentId=$this->departmentId, divisionId=$this->divisionId, employmentStatusId=$this->employmentStatusId, "
	      ."stateId=$this->domainId, custom1Id=$this->custom1Id, custom2Id=$this->custom2Id, custom3Id=$this->custom3Id, "
	      ."employmentStatusDate=$this->employmentStatusDate, role=".$roles[($this->role)]."($this->role)\n"
	      ."------------------------------------------------------------------------------------------------------------>\n");

  }

}
