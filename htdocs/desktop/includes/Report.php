<?php

// Global definitions for use with this class and report oriented html query forms
define('RESULT_FORMAT_NUMBERS', 1);
define('RESULT_FORMAT_PERCENTAGES', 2);

define('OUTPUT_FORMAT_HTML', 1);
define('OUTPUT_FORMAT_CSV', 2);
define('OUTPUT_FORMAT_PDF', 3);


require_once('DatabaseObject.php');
require_once('CustomCsv.php'); // Provides version agnostic CSV read/write functions
require_once('tcpdf/tcpdf.php');

//require_once('Organization.php');
//require_once('agent/agent.php');


class Report extends DatabaseObject {

  // Instance variables
  //var $userAssignmentSqlColumns = array();
  var $trackSqlColumns = array();
  var $domainSqlColumns = array();
  var $departmentSqlColumns = array();
  var $questionDataSqlColumns = array();
  var $assignmentSqlColumns = array();
  var $userSqlColumns = array();
  var $questionSqlColumns = array();
  var $dbName = '';
  var $orgId = 0;
  var $role = 0;
  var $divisionId = 0;
  var $departmentId = 0;


  // Class methods
  function Report($orgId, $role = 0, $divisionId = 0, $departmentId = 0) {

    $this->trackSqlColumns = unserialize(TRACK_SQL_COLUMNS);
    $this->domainSqlColumns = unserialize(DOMAIN_SQL_COLUMNS);
    $this->departmentSqlColumns = unserialize(DEPARTMENT_SQL_COLUMNS);
    $this->questionDataSqlColumns = unserialize(QUESTION_DATA_SQL_COLUMNS);
    $this->assignmentSqlColumns = unserialize(ASSIGNMENT_SQL_COLUMNS);
    $this->questionSqlColumns = unserialize(QUESTION_SQL_COLUMNS);
    $this->userSqlColumns = unserialize(USER_SQL_COLUMNS);
    $this->orgId = (int)$orgId;
    
    if ($role) {
      $this->role = $role;
      $this->divisionId = $divisionId;
      $this->departmentId = $departmentId;
    }

    if (!is_numeric($orgId)) {
      error_log("Report::Report(orgId=$orgId): orgId not numeric");
      return FALSE;
    }

    $this->DatabaseObject(ORG_DB_NAME_PREFIX.$this->orgId);
    
    if (!$this->initialized)
      return FALSE;
  }

  // Get question ids
  function GetDistinctQuestionIds($trackId = '') {

    $qDataQIds = array();
    $aQIds = array();

    if ($trackId) {
      if (gettype($trackId) == 'array') {
	foreach($trackId as $tId) {
	  $qDataQIds =
	    array_unique(array_merge($qDataQIds,
				     $this->GetDistinctValues($this->questionDataSqlColumns[QUESTION_DATA_COLUMN_QUESTION_ID],
							      QUESTION_DATA_TABLE, '',
							      $this->questionDataSqlColumns[QUESTION_DATA_COLUMN_TRACK_ID],
							      (int)$tId)));
	  $aQIds =
	    array_unique(array_merge($aQIds,
				     $this->GetDistinctValues($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER],
							      ASSIGNMENT_TABLE, '',
							      $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID],
							      (int)$tId)));
	}
      } else {
	$qDataQIds =
	  $this->GetDistinctValues($this->questionDataSqlColumns[QUESTION_DATA_COLUMN_QUESTION_ID],
				   QUESTION_DATA_TABLE, '', $this->questionDataSqlColumns[QUESTION_DATA_COLUMN_TRACK_ID],
				   (int)$trackId);
	$aQIds =
	  $this->GetDistinctValues($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER],
				   ASSIGNMENT_TABLE, '', $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID],
				   (int)$trackId);
      }
    } else {
      $qDataQIds = $this->GetDistinctValues($this->questionDataSqlColumns[QUESTION_DATA_COLUMN_QUESTION_ID],
					    QUESTION_DATA_TABLE);
      $aQIds = $this->GetDistinctValues($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER],
					ASSIGNMENT_TABLE);
    }
    
    if ($qDataQIds === FALSE || $aQIds === FALSE) {
      error_log("Report::GetDistinctQuestionIds(trackId=$trackId): failed to get question ids from tables");
      return FALSE;
    }
    
    $aggregate = array_unique(array_merge($qDataQIds, $aQIds));
    sort($aggregate);

    return $aggregate;
  }

  function GetDistinctVersionDates() {
    $a1 = $this->GetDistinctValues('DATE('.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_VERSION_DATE].')',
				   QUESTION_DATA_TABLE);
    $a2 = $this->GetDistinctValues('DATE('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_VERSION_DATE].')',
				   ASSIGNMENT_TABLE);
    $aggregate = array_unique(array_merge($a1, $a2));
    sort($aggregate);
    
    return $aggregate;
  }

  function GetDistinctCompletionDates() {
    $a1 = $this->GetDistinctValues('DATE('.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_COMPLETION_DATE].')',
				   QUESTION_DATA_TABLE);
    $a2 = $this->GetDistinctValues('DATE('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].')',
				   ASSIGNMENT_TABLE);
    
    $aggregate = array_unique(array_merge($a1, $a2));
    sort($aggregate);
    if (preg_match("/0+-0+-0+[ 0+:0+:0+]?/", $aggregate[0]))
      array_shift($aggregate);
    return $aggregate;
  }

  function GetDistinctDomains() {
    $a1 = $this->GetDistinctValues(array(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_DOMAIN_ID],
					 DOMAIN_TABLE.'.'.$this->domainSqlColumns[DOMAIN_COLUMN_NAME]),
				   QUESTION_DATA_TABLE,
				   array(DOMAIN_TABLE => $this->questionDataSqlColumns[QUESTION_DATA_COLUMN_DOMAIN_ID]),
				   '', '', NAME_VALUES, KEYS_FORMAT);
    $a2 = $this->GetDistinctValues(array(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DOMAIN_ID],
					 DOMAIN_TABLE.'.'.$this->domainSqlColumns[DOMAIN_COLUMN_NAME]),
				   ASSIGNMENT_TABLE,
				   array(DOMAIN_TABLE => $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DOMAIN_ID]),
				   '', '', NAME_VALUES, KEYS_FORMAT);
    $aggregate = $a1 + $a2;
    ksort($aggregate);
    
    return $aggregate;
  }
  
  function GetDistinctTracks() {
    $a1 = $this->GetDistinctValues(array(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_TRACK_ID],
					 TRACK_TABLE.'.'.$this->trackSqlColumns[TRACK_COLUMN_NAME]),
				   QUESTION_DATA_TABLE,
				   array(TRACK_TABLE => $this->questionDataSqlColumns[QUESTION_DATA_COLUMN_TRACK_ID]),
				   '', '', NAME_VALUES, KEYS_FORMAT);
    $a2 = $this->GetDistinctValues(array(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID],
					 TRACK_TABLE.'.'.$this->trackSqlColumns[TRACK_COLUMN_NAME]),
				   ASSIGNMENT_TABLE,
				   array(TRACK_TABLE => $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID]),
				   '', '', NAME_VALUES, KEYS_FORMAT);
    $aggregate = $a1 + $a2;
    ksort($aggregate);
    
    return $aggregate;
  }
  
  
  function GetDistinctDepartments() {
    $a1 = $this->GetDistinctValues(array(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_DEPARTMENT_ID],
					 DEPARTMENT_TABLE.'.'.$this->departmentSqlColumns[DEPARTMENT_COLUMN_NAME]),
				   QUESTION_DATA_TABLE,
				   array(DEPARTMENT_TABLE => $this->questionDataSqlColumns[QUESTION_DATA_COLUMN_DEPARTMENT_ID]),
				   '', '', NAME_VALUES, KEYS_FORMAT);
    $a2 = $this->GetDistinctValues(array(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DEPARTMENT_ID],
					 DEPARTMENT_TABLE.'.'.$this->departmentSqlColumns[DEPARTMENT_COLUMN_NAME]),
				   ASSIGNMENT_TABLE,
				   array(DEPARTMENT_TABLE => $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DEPARTMENT_ID]),
				   '', '', NAME_VALUES, KEYS_FORMAT);
    $aggregate = $a1 + $a2;
    ksort($aggregate);
    
    return $aggregate;
  }


  // This function is used in the Reports subtab of the Organization tab
  function GetDistinctDeliveryDates($trackId, $questionId) {
    $searchField = array();
    $searchValue = array();
    
    if (is_numeric($trackId) && $trackId > 0) {
      array_push($searchField, $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID]);
      array_push($searchFor, $trackId);
    }

    if (is_numeric($questionId) && $questionId > 0) {
      array_push($searchField, $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER]);
      array_push($searchFor, $questionId);
    }
    return $this->GetDistinctValues('DATE('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DELIVERY_DATE].')',
				    ASSIGNMENT_TABLE, '', $searchField, $searchValue);
  }


  // Used in question_analysis.php
  function GetDistinctValues($field, $table, $joinArray = '', $searchOn = '', $searchFor = '',
			     $formatType = MANY_VALUES, $format = VALUES_FORMAT) {
    
    if (!$this->orgId) {
      error_log("Report.GetDistinctValues(): Invalid organization ID");
      return FALSE;
    }

    //$db = new DatabaseObject(ORG_DB_NAME_PREFIX.$this->orgId);
    $this->Select($field, $table, TRUE);

    if (gettype($joinArray) == 'array') {
      $this->InnerJoinUsing($joinArray);
    }

    if ($table == ASSIGNMENT_TABLE) {
      if ($this->role == ROLE_DIVISION_ADMIN)
	$this->SearchBy($table.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DIVISION_ID], $this->divisionId);
      elseif ($this->role == ROLE_DEPARTMENT_ADMIN)
	$this->SearchBy($table.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DEPARTMENT_ID], $this->departmentId);
    }

    if (gettype($searchOn) == 'array' && count($searchOn) > 0) {
      for($i = 0; $i < count($searchOn); $i++) {
	$this->SearchBy($searchOn[$i], $searchFor[$i]);
      }
    } elseif (gettype($searchOn) != 'array' && $searchOn) {
      $this->SearchBy($searchOn, $searchFor);
    }

    $this->SortBy($field);
    if (($rows = $this->Query($formatType, $format)) === FALSE) {
      error_log("Report::GetDistinctValues(): Unable to select question ids from db: ".
		ORG_DB_NAME_PREFIX.$this->orgId);
      return FALSE;
    }

    return $rows;
  }


  function ExecuteSearch($searchParams) {
    
    // Local vars
    $results = array();
    $localSearchParams = array();

    //error_log('ExecuteSearch(): SEARCH PARAMS='.print_r($searchParams, TRUE));

    // Init
    $results['numResults'] = '';
    $results['numCorrect'] = '';
    $results['numIncorrect'] = '';
    $results['numIncomplete'] = '';
    $results['numUnderstood'] = '';
    $results['numContact'] = '';
    $results['numAnswerA'] = '';
    $results['numAnswerB'] = '';
    $results['numAnswerC'] = '';
    $results['numAnswerD'] = '';
    $results['numAnswerE'] = '';

    $results['numResults'] = $this->GetSearchCount($searchParams,
						   $this->questionDataSqlColumns[QUESTION_DATA_COLUMN_QUESTION_ID],
						   QUESTION_DATA_TABLE);
    $results['numResults'] += $this->GetSearchCount($searchParams,
						    $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER],
						    ASSIGNMENT_TABLE);
    
    // Process search based on results (correct/incorrect)
    if ($searchParams['resultId'] == RESULT_INCORRECT) { // incorrect questions
      $results['numIncorrect'] = 'X';
      $results['numCorrect'] = '--';
    } elseif ($searchParams['resultId'] == RESULT_CORRECT) { // correct questions
      $results['numIncorrect'] = '--';
      $results['numCorrect'] = 'X';
    } elseif ($searchParams['resultId'] === FALSE || $searchParams['resultId'] == '') {
      // criteria not chosen as search param so query numbers
      $localSearchParams = $searchParams;
      $localSearchParams['resultId'] = RESULT_INCORRECT;
      $results['numIncorrect'] = $this->GetSearchCount($localSearchParams,
						       $this->questionDataSqlColumns[QUESTION_DATA_COLUMN_RESULT],
						       QUESTION_DATA_TABLE);
      $results['numIncorrect'] += $this->GetSearchCount($localSearchParams,
							$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY],
							ASSIGNMENT_TABLE);
      $localSearchParams['resultId'] = RESULT_CORRECT;
      $results['numCorrect'] = $this->GetSearchCount($localSearchParams,
						     $this->questionDataSqlColumns[QUESTION_DATA_COLUMN_RESULT],
						     QUESTION_DATA_TABLE);
      $results['numCorrect'] += $this->GetSearchCount($localSearchParams,
						      $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY],
						      ASSIGNMENT_TABLE);
    } else { // Not yet finished
      $results['numIncorrect'] = '--';
      $results['numCorrect'] = '--';
    }

    // Process exit info (understood, contact, or not yet finished results
    if ($searchParams['exitInfo'] == EXIT_INFO_CONTACT) { // questions where user chose to contact someone at end of question
      $results['numContact'] = 'X';
      $results['numUnderstood'] = '--';
    } elseif ($searchParams['exitInfo'] == EXIT_INFO_UNDERSTOOD) { // understood questions
      $results['numContact'] = '--';
      $results['numUnderstood'] = 'X';
    } elseif ($searchParams['exitInfo'] === FALSE || $searchParams['exitInfo'] == '') {
      // search criteria not specifying exitInfo so get counts
      $localSearchParams = $searchParams;
      $localSearchParams['exitInfo'] = EXIT_INFO_CONTACT;
      $results['numContact'] = $this->GetSearchCount($localSearchParams,
						     $this->questionDataSqlColumns[QUESTION_DATA_COLUMN_EXIT_INFO],
						     QUESTION_DATA_TABLE);
      $results['numContact'] += $this->GetSearchCount($localSearchParams,
						      $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO],
						      ASSIGNMENT_TABLE);
      $localSearchParams['exitInfo'] = EXIT_INFO_UNDERSTOOD;
      $results['numUnderstood'] = $this->GetSearchCount($localSearchParams,
							$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_EXIT_INFO],
							QUESTION_DATA_TABLE);
      $results['numUnderstood'] += $this->GetSearchCount($localSearchParams,
							 $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO],
							 ASSIGNMENT_TABLE);
    } else {
      $results['numContact'] = '--';
      $results['numUnderstood'] = '--';
    }


    if ($searchParams['multipleChoiceAnswerSelected'] == '01') {
      $results['numAnswerA'] = 'X';
      $results['numAnswerB'] = '--';
      $results['numAnswerC'] = '--';
      $results['numAnswerD'] = '--';
      $results['numAnswerE'] = '--';
    } elseif ($searchParams['multipleChoiceAnswerSelected'] == '02') {
      $results['numAnswerA'] = '--';
      $results['numAnswerB'] = 'X';
      $results['numAnswerC'] = '--';
      $results['numAnswerD'] = '--';
      $results['numAnswerE'] = '--';
    } elseif ($searchParams['multipleChoiceAnswerSelected'] == '03') {
      $results['numAnswerA'] = '--';
      $results['numAnswerB'] = '--';
      $results['numAnswerC'] = 'X';
      $results['numAnswerD'] = '--';
      $results['numAnswerE'] = '--';
    } elseif ($searchParams['multipleChoiceAnswerSelected'] == '04') {
      $results['numAnswerA'] = '--';
      $results['numAnswerB'] = '--';
      $results['numAnswerC'] = '--';
      $results['numAnswerD'] = 'X';
      $results['numAnswerE'] = '--';
    } elseif ($searchParams['multipleChoiceAnswerSelected'] == '05') {
      $results['numAnswerA'] = '--';
      $results['numAnswerB'] = '--';
      $results['numAnswerC'] = '--';
      $results['numAnswerD'] = '--';
      $results['numAnswerE'] = 'X';
    } else {
      $localSearchParams = $searchParams;
      $localSearchParams['multipleChoiceAnswerSelected'] = '01';
      $results['numAnswerA'] = $this->GetSearchCount($localSearchParams,
						     $this->questionDataSqlColumns[QUESTION_DATA_COLUMN_FOIL_SELECTED],
						     QUESTION_DATA_TABLE);
      $results['numAnswerA'] += $this->GetSearchCount($localSearchParams,
						      $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_FIRST_ANSWER],
						      ASSIGNMENT_TABLE);
      $localSearchParams['multipleChoiceAnswerSelected'] = '02';
      $results['numAnswerB'] = $this->GetSearchCount($localSearchParams,
						     $this->questionDataSqlColumns[QUESTION_DATA_COLUMN_FOIL_SELECTED],
						     QUESTION_DATA_TABLE);
      $results['numAnswerB'] += $this->GetSearchCount($localSearchParams,
						      $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_FIRST_ANSWER],
						      ASSIGNMENT_TABLE);
      $localSearchParams['multipleChoiceAnswerSelected'] = '03';
      $results['numAnswerC'] = $this->GetSearchCount($localSearchParams,
						     $this->questionDataSqlColumns[QUESTION_DATA_COLUMN_FOIL_SELECTED],
						     QUESTION_DATA_TABLE);
      $results['numAnswerC'] += $this->GetSearchCount($localSearchParams,
						      $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_FIRST_ANSWER],
						      ASSIGNMENT_TABLE);
      $localSearchParams['multipleChoiceAnswerSelected'] = '04';
      $results['numAnswerD'] = $this->GetSearchCount($localSearchParams,
						     $this->questionDataSqlColumns[QUESTION_DATA_COLUMN_FOIL_SELECTED],
						     QUESTION_DATA_TABLE);
      $results['numAnswerD'] += $this->GetSearchCount($localSearchParams,
						      $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_FIRST_ANSWER],
						      ASSIGNMENT_TABLE);
      $localSearchParams['multipleChoiceAnswerSelected'] = '05';
      $results['numAnswerE'] = $this->GetSearchCount($localSearchParams,
						     $this->questionDataSqlColumns[QUESTION_DATA_COLUMN_FOIL_SELECTED],
						     QUESTION_DATA_TABLE);
      $results['numAnswerE'] += $this->GetSearchCount($localSearchParams,
						      $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_FIRST_ANSWER],
						      ASSIGNMENT_TABLE);
    }

    if ($searchParams['completionDateFrom'] || $searchParams['completionDateTo'] ||
	$searchParams['exitInfo'] === '0' || $searchParams['resultId'] === '0') {
      $results['numIncomplete'] = 'X';
    } else {
      $localSearchParams = $searchParams;
      $localSearchParams['resultId'] = RESULT_INCOMPLETE;
      $results['numIncomplete'] = $this->GetSearchCount($localSearchParams,
							$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID],
							ASSIGNMENT_TABLE);
    }

    return $results;
  }


  function GetSearchCount(&$searchParams, &$column, $table) {

    // Local vars
    $rows = 0;

    $this->Select('COUNT('.$column.') AS Number', $table);

    if ($table == QUESTION_DATA_TABLE) {

      if (is_numeric($searchParams['questionNumber']))
	$this->SearchBy(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_QUESTION_ID],
			$searchParams['questionNumber'], TRUE);
      if ($searchParams['versionDateFrom'])
	$this->SearchBy(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_VERSION_DATE],
			$searchParams['versionDateFrom'], TRUE, ' >= ');
      if ($searchParams['versionDateTo'])
	$this->SearchBy(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_VERSION_DATE],
			$searchParams['versionDateTo'], TRUE, ' <= ');
      if ($searchParams['completionDateFrom'])
	$this->SearchBy(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_COMPLETION_DATE],
			$searchParams['completionDateFrom'], TRUE, ' >= ');
      if ($searchParams['completionDateTo']) {
	$this->SearchBy(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_COMPLETION_DATE],
			$searchParams['completionDateTo'], TRUE, ' <= ');
	$this->SearchBy(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_COMPLETION_DATE],
			0, TRUE, ' != ');
      }
      if (is_numeric($searchParams['domainId']))
	$this->SearchBy(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_DOMAIN_ID],
			$searchParams['domainId'], TRUE);
      if (is_numeric($searchParams['trackId']))
	$this->SearchBy(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_TRACK_ID],
			$searchParams['trackId'], TRUE);
      if (is_numeric($searchParams['departmentId']))
	$this->SearchBy(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_DEPARTMENT_ID],
			$searchParams['departmentId'], TRUE);
      if (is_numeric($searchParams['resultId']))
	$this->SearchBy(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_RESULT],
			$searchParams['resultId'], TRUE);
      if (is_numeric($searchParams['multipleChoiceAnswerSelected']))
	$this->SearchBy(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_FOIL_SELECTED],
			$searchParams['multipleChoiceAnswerSelected'], TRUE);
      if (is_numeric($searchParams['exitInfo']))
	$this->SearchBy(QUESTION_DATA_TABLE.'.'.$this->questionDataSqlColumns[QUESTION_DATA_COLUMN_EXIT_INFO],
			$searchParams['exitInfo'], TRUE);
    } else {

      if (is_numeric($searchParams['questionNumber']))
	$this->SearchBy(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER],
			$searchParams['questionNumber'], TRUE);
      if ($searchParams['versionDateFrom'])
	$this->SearchBy(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_VERSION_DATE],
			$searchParams['versionDateFrom'], TRUE, ' >= ');
      if ($searchParams['versionDateTo'])
	$this->SearchBy(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_VERSION_DATE],
			$searchParams['versionDateTo'], TRUE, ' <= ');
      if ($searchParams['completionDateFrom'])
	$this->SearchBy(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE],
			$searchParams['completionDateFrom'], TRUE, ' >= ');
      if ($searchParams['completionDateTo']) {
	$this->SearchBy(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE],
			$searchParams['completionDateTo'], TRUE, ' <= ');
	$this->SearchBy(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE],
			0, TRUE, ' != ');
      }
      if (is_numeric($searchParams['domainId']))
	$this->SearchBy(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DOMAIN_ID],
			$searchParams['domainId'], TRUE);
      if (is_numeric($searchParams['trackId']))
	$this->SearchBy(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID],
			$searchParams['trackId'], TRUE);
      if (is_numeric($searchParams['departmentId']))
	$this->SearchBy(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DEPARTMENT_ID],
			$searchParams['departmentId'], TRUE);
      if (is_numeric($searchParams['resultId']))
	$this->SearchBy(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY],
			$searchParams['resultId'], TRUE);
      if (is_numeric($searchParams['multipleChoiceAnswerSelected']))
	$this->SearchBy(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_FIRST_ANSWER],
			$searchParams['multipleChoiceAnswerSelected'], TRUE);
      if (is_numeric($searchParams['exitInfo']))
	$this->SearchBy(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO],
			$searchParams['exitInfo'], TRUE);

      // Make sure we do not double count results migrated from user assignment previous to version upgrade date.
      $this->Where(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DELIVERY_DATE].
		   '>= \''.MIGRATION_DATE_V2_1."'");
    }

    if (($rows = $this->Query(SINGLE_VALUE, VALUES_FORMAT)) === FALSE) {
      error_log("Report::GetSearchCount(searchParams=".print_r($searchParams, TRUE).
		", column=$column): Unable to select question ids from db: ".ORG_DB_NAME_PREFIX.$this->orgId);
      return FALSE;
    }

    return $rows;
  }


  function CreateBasePdf($title, $colDefs, $results, $fname) {
    // new PDF document with landscape orientation, inche units
    // format is last argument and hopefully letter is the default
    $html = '';
    $pdf = new tcpdf('L');
    $pdf->SetCreator('Training Advisor');
    $pdf->SetAuthor('Training Advisor System');
    $pdf->SetSubject($title);
    $pdf->SetTitle('Training Advisor Report');
    $pdf->SetKeywords('report, individual, participant, date range, answered');
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetHeaderData(PDF_HEADER_LOGO, 50, 'Training Advisor Report:', $title);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetMargins(PDF_MARGIN_LEFT, 23, PDF_MARGIN_RIGHT);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    //    $pdf->UseTableHeader();
    //$pdf->UseCSS();
    //$pdf->SetFont('Arial','',12);

    foreach ($results as $trackData) {
      $html = '<span class="OrgInfoText">Report for Class: '.$trackData[0][1].'</span><BR>
  <table border="0" align="center" bgcolor="#FFFFFF">
    <tr bgcolor="#DDDDDD">'."\n";
      for($i = 0; $i < count($colDefs); $i++) {
	$html .= '<td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: '.$colDefs[$i]['pdfWidth'].'; font-weight: bold;"';
	if ($colDefs[$i]['type'] == 'text')
	  $html .= ' align="left"';
	$html .= '>'.$colDefs[$i]['label'].' ('.$trackData[0][$colDefs[$i]['hdrIdx']].")</td>\n";
      }
      $html .= "    </tr>\n";
      for($i = 1; $i < count($trackData); $i++) {
	$html .= "<tr bgcolor='#FFFFFF'>\n";
	for ($j = 0; $j < count($colDefs); $j++) {
	  $html .= '<td style="font-size: 10pt; width: '.$colDefs[$j]['pdfWidth'].';"';
	  if ($colDefs[$j]['type'] == 'text')
	    $html .= ' align="left">'.$trackData[$i][$colDefs[$j]['datIdx']]."</td>\n";
	  if ($colDefs[$j]['type'] == 'int')
	    $html .= '>'.$trackData[$i][$colDefs[$j]['datIdx']]."</td>\n";
	  elseif ($colDefs[$j]['type'] == 'float')
	    $html .= sprintf(">%.1f\%</td>\n", $trackData[$i][$colDefs[$j]['datIdx']]);
	}
	$html .= "    </tr>\n";
      }
      $html .= "  </table>\n";
      $pdf->AddPage('L');
      $pdf->WriteHTML($html);
    }

    $pdf->SetAuthor('Training Advisor Inc.');
    $pdf->SetTitle($title);
    $pdf->SetFont('Helvetica', 'B', 20);
    $pdf->SetTextColor(50, 60, 100);
    $pdf->Output($fname, 'D');
  }



  function CreateBaseCsv($colDefs, $results, $fname) {

    $outputArr = array();

    if (empty($results)) {
      error_log("Report::CreateCsv(): empty results data passed to CSV function");
      return;
    }

    $outputArr = array();
    //$fp = fopen('php://output', 'w');
    $csv = new CustomCsv('php://output', 'w');

    // Output headers
    $csv->WriteHeaders($fname);

    // Output CSV header information:
    $outputArr[] = 'Class';
    foreach($colDefs as $colDef)
      $outputArr[] = $colDef['label'];
    $csv->WriteCsv($outputArr);

    foreach ($results as $trackData) {
      for($i = 1; $i < count($trackData); $i++) {
	$outputArr = array($trackData[0][1]);
	for($j = 0; $j < count($colDefs); $j++) {
	  if ($colDefs[$j]['type'] == 'float')
	    $outputArr[] = sprintf('%.1f', $trackData[$i][$colDefs[$j]['datIdx']]);
	  else
	    $outputArr[] = $trackData[$i][$colDefs[$j]['datIdx']];
	}
	$csv->WriteCsv($outputArr);
      }
    }

    $csv->CloseCsv();

    return;
  }



  //Simple table
  function BasicTable(&$pdf, $header, $data) {

    //Header
    foreach($header as $col)
      $pdf->MultiCell(40,7,$col,1);
    //    $pdf->Ln();

    //Data
    foreach($data as $row) {
      foreach($row as $col)
	$pdf->MultiCell(40,6,$col,1);
      //      $pdf->Ln();
    }
  }
  

  //Better table
  function ImprovedTable(&$pdf, $header, $data) {

    //Column widths
    $w=array(40,35,40,45);
    
    //Header
    for($i=0; $i < count($header); $i++)
      $pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
    $pdf->Ln();

    //Data
    foreach($data as $row) {
      $pdf->Cell($w[0], 6, $row[0], 'LR');
      $pdf->Cell($w[1], 6, $row[1], 'LR');
      $pdf->Cell($w[2], 6, number_format($row[2]),'LR',0,'R');
      $pdf->Cell($w[3], 6, number_format($row[3]),'LR',0,'R');
      $pdf->Ln();
    }

    //Closure line
    $pdf->Cell(array_sum($w),0,'','T');
  }


  //Colored table
  function FancyTable(&$pdf, $header, $data) {

    //Colors, line width and bold font
    $pdf->SetFillColor(255,0,0);
    $pdf->SetTextColor(255);
    $pdf->SetDrawColor(128,0,0);
    $pdf->SetLineWidth(.3);
    $pdf->SetFont('','B');

    //Header
    $w=array(40,35,40,45);
    for($i=0; $i < count($header); $i++)
      $pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C', true);
    $pdf->Ln();

    //Color and font restoration
    $pdf->SetFillColor(224,235,255);
    $pdf->SetTextColor(0);
    $pdf->SetFont('');

    //Data
    $fill=false;
    foreach($data as $row) {
      $pdf->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
      $pdf->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
      $pdf->Cell($w[2],6,number_format($row[2]),'LR',0,'R',$fill);
      $pdf->Cell($w[3],6,number_format($row[3]),'LR',0,'R',$fill);
      $pdf->Ln();
      $fill=!$fill;
    }

    $pdf->Cell(array_sum($w),0,'','T');
  }




}
