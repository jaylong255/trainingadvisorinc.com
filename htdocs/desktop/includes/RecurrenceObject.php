<?php
/////////////////////////////////////////////////////////////////////////////
//	RecurrenceObject.php
//		Defines and class used to handle recurrence data types.

define('RECUR_NOT_SET', '');
define('RECUR_DAILY', 'Daily');
define('RECUR_WEEKLY', 'Weekly');
define('RECUR_MONTHLY', 'Monthly');

/////////////////////////////////////////////////////////////////////////////
//	RecurrenceObject
//		Class used to create and manipulate token strings containing information
//		on recurrence and scheduling for questions.
class RecurrenceObject {

  var		$m_flagFrequency = RECUR_NOT_SET;
  var		$m_countDays = 0;
  var		$m_countWeeks = 0;
  var		$m_countMonths = 0;
  var		$m_weekDays = 0;
  var		$m_flagWeek = 0;
  var           $m_recurrence = '';
  var           $m_strRecurrence = '';
  var           $weekDayNames = array("", "Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
  

  function RecurrenceObject($strToken = "") {
    if ($strToken) {
      $this->m_recurrence = $strToken;
      $this->ParseTokenString($strToken);
    }
  }
											// Data access methods
	function GetFrequency()	{ return $this->m_flagFrequency; }

	function IsDaily() { return ($this->m_flagFrequency == RECUR_DAILY); }
	function IsEveryWeekday() { return ($this->m_flagFrequency == RECUR_DAILY && $this->m_countDays == 0); }
	function GetDaysApart() { return $this->m_countDays; }

	function IsWeekly() { return ($this->m_flagFrequency == RECUR_WEEKLY); }
	function GetWeeksApart() { return $this->m_countWeeks; }
	function GetWeekdays()
	{
	  return $this->m_weekDays;
	  //		for ($i=0; $i < 7; $i++)
	  //			$array[$i] = $this->m_arrayWeekdays[$i];
	}

  function GetWeekDay() {
    return log($this->m_weekDays, 2)+1;
  }

  function GetWeekDaysList() {
    return $this->weekDayNames;
  }

	function IsMonthly() { return ($this->m_flagFrequency == RECUR_MONTHLY); }
	function IsMonthlyByDate() { return ($this->m_flagFrequency == RECUR_MONTHLY && $this->m_countDays > 0); }
	function GetMonthlyDate() { return $this->m_countDays; }
	function IsMonthlyByWeekday() { return ($this->m_flagFrequency == RECUR_MONTHLY && $this->m_countDays == 0); }
	function GetMonthsApart() { return $this->m_countMonths; }
	function GetWeekFlag() { return $this->m_flagWeek; }

											// Data assignment methods
	function SetEveryWeekday()
	{
		$this->m_flagFrequency = RECUR_DAILY;
		$this->m_countDays = 0;
	}

	function SetDaily($nDaysApart)
	{
		$this->m_flagFrequency = RECUR_DAILY;
		$this->m_countDays = $nDaysApart;
	}

	function SetWeekly($nWeeksApart, $weekDays)
	{
		$this->m_flagFrequency = RECUR_WEEKLY;
		$this->m_countWeeks = $nWeeksApart;
		// This is a 7 bit representation of days
		// each bit is a day beginning with bit position 0
		// as Sunday, bit position 1 as Monday, etc...
		$this->m_weekDays = $weekDays;
		/* // old representation
		for ($i=0; $i < 7; $i++) {
		  if ($arrayWeekdays[$i])
		    $this->m_weekDays |= 1 << $i;
		}
		*/
	}

	function SetMonthlyByDate($nDay, $nMonthsApart)
	{
		$this->m_flagFrequency = RECUR_MONTHLY;
		$this->m_countDays = $nDay;
		$this->m_countMonths = $nMonthsApart;
	}

	function SetMonthlyByWeekday($flagWeek, $weekDay, $nMonthsApart)
	{
		$this->m_flagFrequency = RECUR_MONTHLY;
		$this->m_countDays = 0;
		$this->m_flagWeek = $flagWeek;
		/*
		for ($i=0; $i < 7; $i++) {
		  if ($arrayWeekdays[$i])
		    $this->m_weekDays |= 1 << $i;
		} */
		$this->m_weekDays = $weekDay;

		$this->m_countMonths = $nMonthsApart;
	}
											// Token string methods
	function CreateVerboseTokenString()
	{
		$arrayWeekFlags = array ("first","second","third","fourth","last");
		$this->m_strRecurrence = "";
		switch($this->m_flagFrequency):
			case RECUR_DAILY:
				if ($this->m_countDays > 0)
					$this->m_strRecurrence .= "Every $this->m_countDays day(s)";
				else
					$this->m_strRecurrence .= "Every weekday";
				break;
			case RECUR_WEEKLY:
				$this->m_strRecurrence = "Every $this->m_countWeeks week(s) on";
				for ($i=0; $i < 7; $i++) {
					if ($this->m_weekDays & 1 << $i)
					  $this->m_strRecurrence .= " ".$this->weekDayNames[$i + 1];
				}
				break;
			case RECUR_MONTHLY:
				if ($this->m_countDays > 0)
					$this->m_strRecurrence = "Day $this->m_countDays of every $this->m_countMonths month(s)";
				else {
					$this->m_strRecurrence = "The ".$arrayWeekFlags[$this->m_flagWeek];
					for ($i=0; $i < 7; $i++) {
						if ($this->m_weekDays & 1 << $i)
						  $this->m_strRecurrence .= " ".$this->weekDayNames[$i + 1];
					}
					$this->m_strRecurrence .= " of every $this->m_countMonths month(s)";
				}
				break;
			default:
				return NULL;
		endswitch;

		if (DEBUG & DEBUG_RECURRENCE)
		  error_log("RecurrenceObject.CreateVerboseTokenString(): strRecurrence = ".$this->m_strRecurrence);

		return $this->m_strRecurrence;
	}

	function CreateTokenString()
	{
		$arrayWeekdayStrings = array("S","M","T","W","t","F","s");
		$arrayWeekFlags = array ("-F","-S","-T","-f","-L");
		$this->m_recurrence = "";
		switch($this->m_flagFrequency):
			case RECUR_DAILY:
				$this->m_recurrence = "D";
				if ($this->m_countDays > 0)
					$this->m_recurrence .= " $this->m_countDays";
				else
					$this->m_recurrence .= " 0";
				break;
			case RECUR_WEEKLY:
				$this->m_recurrence = "W $this->m_countWeeks";
				for ($i=0; $i < 7; $i++) {
				  if ($this->m_weekDays & 1 << $i)
				    $this->m_recurrence .= " ".$arrayWeekdayStrings[$i];
				}
				break;
			case RECUR_MONTHLY:
				$this->m_recurrence = "M";
				if ($this->m_countDays > 0)
					$this->m_recurrence .= " $this->m_countDays $this->m_countMonths";
				else {
					$this->m_recurrence .= " ".$arrayWeekFlags[$this->m_flagWeek];
					$this->m_recurrence .= " $this->m_countMonths";
					for ($i=0; $i < 7; $i++) {
						if ($this->m_weekDays & 1 << $i)
						  $this->m_recurrence .= " ".$arrayWeekdayStrings[$i];
					}
				}
				break;
			default:
				return NULL;
		endswitch;

		if (DEBUG & DEBUG_RECURRENCE)
		  error_log("RecurrenceObject.CreateTokenString(): recurrence = ".$this->m_recurrence);

		return $this->m_recurrence;
	}

	function ParseTokenString($strToken)
	{
		$arrayWeekdayStrings = array("S","M","T","W","t","F","s");
		$arrayWeekFlags = array ("-F","-S","-T","-f","-L");

		$this->m_flagFrequency = RECUR_NOT_SET;
		$this->m_countDays = 0;
		$this->m_countWeeks = 0;
		$this->m_countMonths = 0;
		$this->m_weekDays = 0;
		$this->m_flagWeek = 0;
		$this->m_recurrence = $strToken;

		$token = strtok($strToken, " ");

		if ($token == "D") {
			$this->m_flagFrequency = RECUR_DAILY;
			$token = strtok(" ");
			$this->m_countDays = (int)$token;

			// Every weekday if day count is 0
			//if($m_countDays == 0)
			//  $this->Day="Weekday";
			//else {
			//			  $this->Day="Number";
			//$this->Day = 
			//$DApart=$this->GetDaysApart();
			//}
		}
		else if ($token == "W") {
			$this->m_flagFrequency = RECUR_WEEKLY;
			$token = strtok(" ");
			$this->m_countWeeks = (int)$token;
			$token = strtok(" ");

			while ($token) {
				for ($i=0; $i < 7; $i++) {
				  if ($token == $arrayWeekdayStrings[$i])
				    $this->m_weekDays |= 1 << $i;
				}
				$token = strtok(" ");
			}
		}
		else if ($token == "M") {
			$this->m_flagFrequency = RECUR_MONTHLY;
			$token = strtok(" ");
			if ($token[0] == '-') {
				for ($i=0; $i < 5; $i++) {
					if ($token == $arrayWeekFlags[$i])
						$this->m_flagWeek = $i;
				}
				$token = strtok(" ");
				$this->m_countMonths = (int)$token;
				$token = strtok(" ");
				while ($token) {
					for ($i=0; $i < 7; $i++) {
						if ($token == $arrayWeekdayStrings[$i])
						  $this->m_weekDays |= 1 << $i;
					}
					$token = strtok(" ");
				}
			}
			else {
				$this->m_countDays = (int)$token;
				$token = strtok(" ");
				$this->m_countMonths = (int)$token;
			}
		}
	}


  


  function DumpRecurrence() {

    //$arrayWeekdays = array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
    //$arrayWeekFlags = array("First","Second","Third","Fourth","Last");
    
    /*
  $temp = $this->CreateTokenString();
  error_log("1st pass = $temp<br>\n");
  $this->ParseTokenString($temp);
  $temp = $this->CreateTokenString();
  error_log("2nd pass = $temp<br>\n");
    */
    
    $flag = $this->GetFrequency();
    error_log("obj->GetFrequency() = $flag<br>\n");
    
    if ($this->IsDaily())
      error_log("obj->IsDaily() = TRUE<br>\n");
    else
      error_log("obj->IsDaily() = FALSE<br>\n");
    
    if ($this->IsEveryWeekday())
      error_log("obj->IsEveryWeekday() = TRUE<br>\n");
    else
      error_log("obj->IsEveryWeekday() = FALSE<br>\n");
    
    $n = $this->GetDaysApart();
    error_log("obj->GetDaysApart() = $n<br>\n");
    
    $is = $this->IsWeekly();
    error_log("obj->IsWeekly() = $is<br>\n");
    
    $n = $this->GetWeeksApart();
    error_log("obj->GetWeeksApart() = $n<br>\n");
    
    //$array = array(false, false, false, false, false, false, false);
    error_log("obj->GetWeekdays() = ".$this->GetWeekdays());
    /*
  for ($i=0; $i < 7; $i++) {
    if ($array[$i])
      error_log(" $arrayWeekdays[$i]");
  }
    */
    error_log("<br>\n");
    
    $is = $this->GetWeekDay();
    error_log("obj->GetWeekDay() = $is<br>\n");
    
    $is = $this->IsMonthly();
    error_log("obj->IsMonthly() = $is<br>\n");
    
    $is = $this->IsMonthlyByDate();
    error_log("obj->IsMonthlyByDate() = $is<br>\n");
    
    $n = $this->GetMonthlyDate();
    error_log("obj->GetMonthlyDate() = $n<br>\n");
    
    $n = $this->GetMonthsApart();
    error_log("obj->GetMonthsApart() = $n<br>\n");
    
    $is = $this->IsMonthlyByWeekday();
    error_log("obj->IsMonthlyByWeekday() = $is<br>\n");
    
    $n = $this->GetWeekFlag();
    error_log("obj->GetWeekFlag() = $n<br>\n");
    
    error_log("<br>\n");
  }
  
}
?>