<?php
/////////////////////////////////////////////////////////////////////////////
//	DataFileObject
//		Object used for reading and parsing coma delimited data files.
class DataFileObject
{
	var		$arrayRecords;			// Lines read from the data file
	var		$index;					// Index of current record
	var		$strDelimiter;			// Character used as a column separator
	var		$arrayColumnNames;		// Array of column names in intended order
	var		$arrayColumns;			// Array of columns for current record indexed using column names

	function DataFileObject($strFile = NULL)
	{
		$this->arrayRecords = array();
		$this->index = 0;
		$this->strDelimiter = ',';
		$this->arrayColumnNames = array();
		$this->arrayColumns = array();

		if ($strFile)
			$this->Open($strFile);
	}
											// Data access methods
	function SetDelimiter($char)
	{
		$this->strDelimiter = $char;
	}

	function GetDelimiter()
	{
		return $this->strDelimiter;
	}

	function GetColumn($strColumnName)
	{
		return $this->arrayColumns[$strColumnName];
	}

											// Operation methods
	function Open($strFile)
	{
		$this->arrayRecords = array();
		$this->index = 0;
		$i = 0;
											// Attempt to open and read in the file contents
		$fp = fopen($strFile, "r");
		if ($fp != FALSE) {
			$size = filesize($strFile);
			while (!feof($fp)) {
				$line = fgets($fp, $size);
				$line = trim($line);
				if (strlen($line))
					$this->arrayRecords[$i++] = $line;
			}
			fclose($fp);
			return true;
		}
		return false;
	}

	function SpecifyColumns($arrayColumns)
	{
		$this->arrayColumnNames = $arrayColumns;
	}

	function GetNextColumn(&$text)
	{
		$len = strlen($text);
		$in_string = false;
		$bFound = false;

		for ($i=0; $i < $len && !$bFound; $i++) {
			$char = $text[$i];

			if ($in_string) {
				if (($char == '"') || ($char == '\'') || ($char == '`')) {
					if ($i > 0 && $text[$i-1] != '\\') {
						$in_string = false;
						$text[$i] = ' ';
					}
				}
			}
	        else if (($char == '"') || ($char == '\'') || ($char == '`')) {
		        $in_string = true;
				$text[$i] = ' ';
			}
			else if ($char == $this->strDelimiter) {
				$text[$i] = ' ';
				$bFound = true;
			}
		}
		$column = substr($text, 0, $i);
		$text = substr($text, $i);

		return $column;
	}

	function GetNextRecord()
	{
		$nlines = count($this->arrayRecords);
		$n = 0;
		if ($this->index < $nlines) {

			$line = $this->arrayRecords[$this->index++];

			$strColumn = $this->GetNextColumn($line);
			while ($strColumn) {

				$name = $this->arrayColumnNames[$n++];
				$this->arrayColumns[$name] = trim($strColumn);

				$strColumn = $this->GetNextColumn($line);
			}
			return true;
		}
		return false;
	}
}
?>
