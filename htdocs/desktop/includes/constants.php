<?php

// Size of borders for debugging layout
define('DEBUG_BORDER', 1);

// Debug information type flags
define('DEBUG_NONE', 0x00000000);
define('DEBUG_SQL', 0x00000001);
define('DEBUG_FORM', 0x00000002);
define('DEBUG_CLASS_FUNCTIONS', 0x00000004);
define('DEBUG_CALL_TRACE', 0x00000008); // Logs all function calls with their arguments and meanings
define('DEBUG_SESSION', 0x00000010); // Puts session objects into smarty debug console
define('DEBUG_SMARTY', 0x00000020); // Debug template and presentation layer
define('DEBUG_RECURRENCE', 0x00000040); // Debug recurrence interface and use in agent
define('DEBUG_LIB', 0x00000080);
define('DEBUG_AGENT', 0x00000100);
define('DEBUG_ALL', 0xFFFFFFFF);

// --------------------------------------------------------------------------------------
// Definitions for column lists.  This is how it should have been done in the first place
// --------------------------------------------------------------------------------------

// This was necessary because all user assignments were migrated to the new assignment
// table and without this check in the query, question analysis would be incorrect because
// assignments would be counted twice for assignments delivered prior to the migration.
define('MIGRATION_DATE_V2_1', '2008-03-16');


// This is the envelope sender address that will be used when the agent sends email
//define('AGENT_ENVELOPE_SENDER_ADDRESS', 'agent@trainingadvisorinc.com');
// Above constant defined in hrt_config_* series of files



// Suitable defaults
// Maximum number of pages allowed in 
define('MAX_PAGES', 5000);
// Maximum number of tabs allowed in the interface (only used for data validation)
define('MAX_TABS', 500);
// Rows per page default used at login
define('DEFAULT_ROWS_PER_PAGE', 100);

define('AB1825_POLICY_QUESTION', 50900);


// Tabs to control the currently selected tab
// This is a nicety to manage the reload button
// in the browser
define('TAB_NONE', 0);
// Top level tabs in administrative interface
define('TAB_ORG', 1);
define('TAB_USER', 2);
define('TAB_TRACK', 3);
define('TAB_QUESTION', 4);
define('TAB_CUSTOMIZE', 5);

// Dummy sub tab to indicate no tabs should be shown
define('SUBTAB_NONE', 0);

// Second level subtabs in administrative interface for organization
define('SUBTAB_ORG_LIST', 0); // Virtual subtab to handle org list
define('SUBTAB_ORG_INFO', 1);
define('SUBTAB_ORG_CONTACT', 2);
define('SUBTAB_ORG_DATA', 3);
define('SUBTAB_ORG_UTILITIES', 4);
define('SUBTAB_ORG_REPORTS', 5);
define('SUBTAB_ORG_DELETE', 6); // Virtual subtab to handle org deletion ops

// Second level subtabs in administrative interface.  Ok there really
// are not any subtabs but this is realy a meachanism for keeping track
// of which page we are on
define('SUBTAB_USER_LIST', 0);
define('SUBTAB_USER_EDIT', 1);

// Second level subtabs in administrative interface for questions
define('SUBTAB_QUESTION_CURRENT', 1);
define('SUBTAB_QUESTION_EDIT', 2);
define('SUBTAB_QUESTION_ARCHIVE', 3);
define('SUBTAB_QUESTION_SUMMARY', 4);
define('SUBTAB_QUESTION_ANALYSIS', 5);
define('SUBTAB_QUESTION_VIEW_CURRENT', 6);
define('SUBTAB_QUESTION_VIEW_ARCHIVED', 7);
define('SUBTAB_QUESTION_CATEGORIES', 8);

// Second level subtabs in administrative interface for tracks
define('SUBTAB_TRACK_INFO', 1);
define('SUBTAB_TRACK_PARTICIPANTS', 2);
define('SUBTAB_TRACK_ASSIGNMENTS', 3);
define('SUBTAB_TRACK_FAQS', 4);
define('SUBTAB_TRACK_PROGRESS', 5);
define('SUBTAB_TRACK_EMAIL', 6);


// Second level subtabs in administrative interface for customize
define('SUBTAB_CUSTOMIZE_FILEMAN', 1);
define('SUBTAB_CUSTOMIZE_MISC', 2);


// User tabs in the presentation area (no need to worry about name space conflicts)
define('TAB_ASSIGNED', 101);
define('TAB_COMPLETED', 102);
define('TAB_FAQ', 103);
define('TAB_PREFERENCES', 104);

define('PAGE_QUESTION', 0);
//define('PAGE_', );
//define('PAGE_', );
//define('PAGE_', );
//define('PAGE_', );


define('ROLE_END_USER', 0x00000000);
define('ROLE_ORG_ADMIN', 0x00000001);
define('ROLE_DIVISION_ADMIN', 0x00000002);
define('ROLE_DEPARTMENT_ADMIN', 0x00000004);
define('ROLE_CLASS_CONTACT_SUPERVISOR', 0x00000008);

define('ROLES', serialize(array(ROLE_END_USER => 'user', ROLE_ORG_ADMIN => 'org_admin',
				ROLE_DIVISION_ADMIN => 'div_admin', ROLE_DEPARTMENT_ADMIN => 'dept_admin',
				ROLE_CLASS_CONTACT_SUPERVISOR => 'track_admin')));

define('ROLE_SELECT_LABELS', serialize(array(ROLE_END_USER => 'Regular Employee/User',
					     ROLE_ORG_ADMIN => 'Administrator',
					     ROLE_DIVISION_ADMIN => 'Division Administrator',
					     ROLE_DEPARTMENT_ADMIN => 'Department Administrator',
					     ROLE_CLASS_CONTACT_SUPERVISOR => 'Contact or Class Supervisor')));


define('CSV_EXPECTED_COLUMN_COUNT', 38);

define('AB1825_CONTACT_FULL_NAME', 'John Smith');
define('AB1825_CONTACT_EMAIL', 'john@trainingadvisorinc.com');
define('AB1825_CONTACT_PHONE', '801-277-1177');


// Authorititave database from which questions are pulled
define('AUTHORITATIVE_ORG_ID', '8');
define('AUTHORITATIVE_QUESTION_DB', 'ORG8');


// Authentication database information
// Authentication database organization table
define('AUTH_ORG_TABLE', 'Organization');

// Authentication database org table columns
define('AUTH_ORG_COLUMN_ID', 0x00000001);
define('AUTH_ORG_COLUMN_DATABASE_NAME', 0x00000002);
define('AUTH_ORG_COLUMN_NAME', 0x00000004);

// SQL columns for above table
define('AUTH_ORG_SQL_COLUMNS', serialize(array(AUTH_ORG_COLUMN_ID => 'Organization_ID',
					       AUTH_ORG_COLUMN_DATABASE_NAME => 'Database_Name',
					       AUTH_ORG_COLUMN_NAME => 'Organization_Name')));

// Authentication database user table name
define('AUTH_USER_TABLE', 'User');

// Authentication database user talbe columns
define('AUTH_USER_COLUMN_ID', 0x00000001);
define('AUTH_USER_COLUMN_LOGIN_ID', 0x00000002);
define('AUTH_USER_COLUMN_PASSWORD', 0x00000004);
define('AUTH_USER_COLUMN_ORGANIZATION_ID', 0x00000008);

// SQL columns for above table
define('AUTH_USER_SQL_COLUMNS', serialize(array(AUTH_USER_COLUMN_ID => 'User_ID',
						AUTH_USER_COLUMN_LOGIN_ID => 'Login_ID',
						AUTH_USER_COLUMN_PASSWORD => 'Password',
						AUTH_USER_COLUMN_ORGANIZATION_ID => 'Organization_ID')));


// Organization List Table (schema from db)
define('ORG_LIST_TABLE', 'Organization');


// Organization List columns
// These bit positions *MUST* correspond to the indexes in the arrays below
define('ORG_LIST_COLUMN_ID', 0x00000001);
define('ORG_LIST_COLUMN_DB_NAME', 0x00000002);
define('ORG_LIST_COLUMN_NAME', 0x00000004);


define ('ORG_LIST_SQL_COLUMNS', serialize(array(ORG_LIST_COLUMN_ID => 'Organization_ID',
						ORG_LIST_COLUMN_DB_NAME => 'Database_Name',
						ORG_LIST_COLUMN_NAME => 'Organization_Name')));

// Labels displayed in the interface
define('ORG_LIST_LABELS', serialize(array(ORG_LIST_COLUMN_ID => 'Organization ID',
					  ORG_LIST_COLUMN_NAME => 'Organization Name')));


//define('ORG_TABLE', 'Organization');
//
//define('ORG_COLUMN_CONTACT_ID', 0x00000001);
//define('ORG_COLUMN_NAME', 0x00000002);
//define('ORG_COLUMN_DIRECTORY', 0x00000004);
//define('ORG_COLUMN_LOGO', 0x00000008);
//define('ORG_COLUMN_NEWS_URL', 0x00000010);
//define('ORG_COLUMN_DELINQUENCY_NOTIFICATION', 0x00000020);
//define('ORG_COLUMN_USE_NEWS', 0x00000040);
//define('ORG_COLUMN_USE_MOTIF', 0x00000080);
//define('ORG_SQL_COLUMNS', serialize(array(ORG_COLUMN_CONTACT_ID => 'HR_Contact_ID',
//					  ORG_COLUMN_NAME => 'Name',
//					  ORG_COLUMN_DIRECTORY => 'Directory',
//					  ORG_COLUMN_LOGO => 'Logo',
//					  ORG_COLUMN_NEWS_URL => 'HR_News_URL',
//					  ORG_COLUMN_DELINQUENCY_NOTIFICATION => 'Delinquency_Notification',
//					  ORG_COLUMN_USE_NEWS => 'Use_Company_News',
//					  ORG_COLUMN_USE_MOTIF => 'Use_Company_Motif')));

define('CONFIG_TABLE', 'Config');

define('CONFIG_COLUMN_ID', 0x00000001);
define('CONFIG_COLUMN_NAME', 0x00000002);
define('CONFIG_COLUMN_VALUE', 0x00000004);
define('CONFIG_COLUMN_TYPE', 0x00000008);
define('CONFIG_COLUMN_DEFAULT', 0x00000010);
define('CONFIG_COLUMN_DEFAULT_TYPE', 0x00000020);
define('CONFIG_COLUMN_DATE_CREATED', 0x00000040);
define('CONFIG_COLUMN_DATE_MODIFIED', 0x00000080);
define('CONFIG_COLUMN_OBSOLETE', 0x00000100);

define('CONFIG_SQL_COLUMNS', serialize(array(CONFIG_COLUMN_ID => 'Config_ID',
					     CONFIG_COLUMN_NAME => 'Name',
					     CONFIG_COLUMN_VALUE => 'Value',
					     CONFIG_COLUMN_TYPE => 'Type',
					     CONFIG_COLUMN_DEFAULT => 'Default_Value',
					     CONFIG_COLUMN_DEFAULT_TYPE => 'Default_Type',
					     CONFIG_COLUMN_DATE_CREATED => 'Date_Created',
					     CONFIG_COLUMN_DATE_MODIFIED => 'Date_Modified',
					     CONFIG_COLUMN_OBSOLETE => 'Obsolete')));


// Org related stuff
define('ORG_DATA_EDIT_DEPARTMENTS', 0);
define('ORG_DATA_EDIT_DIVISIONS', 1);
/*
define('ORG_DATA_EDIT_CUSTOM1', 2);
define('ORG_DATA_EDIT_CUSTOM2', 3);
define('ORG_DATA_EDIT_CUSTOM3', 4);
define('ORG_DATA_EDIT_CUSTOM_LABELS', 5);
*/
define('ORG_DATA_EDIT_EMPLOYMENT_STATUS', 2);
define('ORG_DATA_EDIT_QUESTION_CATEGORIES', 3);

define('ORG_DATA_EDIT_LABELS', serialize(array(ORG_DATA_EDIT_DEPARTMENTS => 'Edit Departments',
					       ORG_DATA_EDIT_DIVISIONS => 'Edit Divisions',
					       ORG_DATA_EDIT_EMPLOYMENT_STATUS => 'Edit Employment Status')));
					       
/*
					       ORG_DATA_EDIT_CUSTOM1 => 'Edit Custom1*',
					       ORG_DATA_EDIT_CUSTOM2 => 'Edit Custom2*',
					       ORG_DATA_EDIT_CUSTOM3 => 'Edit Custom3*',
					       ORG_DATA_EDIT_CUSTOM_LABELS => 'Edit Custom Labels*',
*/

define('ORG_DATA_EDIT_SELECT_LABELS', serialize(array(ORG_DATA_EDIT_DEPARTMENTS => 'Department',
						      ORG_DATA_EDIT_DIVISIONS => 'Division',
						      ORG_DATA_EDIT_EMPLOYMENT_STATUS => 'Employment Status',
						      ORG_DATA_EDIT_QUESTION_CATEGORIES => 'Question Category')));
					       
/*
						      ORG_DATA_EDIT_CUSTOM1 => 'Custom 1',
						      ORG_DATA_EDIT_CUSTOM2 => 'Custom 2',
						      ORG_DATA_EDIT_CUSTOM3 => 'Custom 3',
						      ORG_DATA_EDIT_CUSTOM_LABELS => 'Custom Labels',
*/


// Track List Table (schema from db)
define('TRACK_LIST_TABLE', 'Track');

// Track List columns
// These bit positions *MUST* correspond to the indexes in the arrays below
define('TRACK_LIST_COLUMN_ID', 0x00000001);
define('TRACK_LIST_COLUMN_NAME', 0x00000002);
define('TRACK_LIST_COLUMN_SUPERVISOR', 0x00000004);

// The first element in this array is the Track table name
define('TRACK_LIST_SQL_COLUMNS', serialize(array(TRACK_LIST_COLUMN_ID => 'Track_ID',
						 TRACK_LIST_COLUMN_NAME => 'Name',
						 TRACK_LIST_COLUMN_SUPERVISOR => 'Supervisor_ID')));

define('TRACK_LIST_LABELS', serialize(array(TRACK_LIST_COLUMN_ID => 'Track ID',
					    TRACK_LIST_COLUMN_NAME => 'Name',
					    TRACK_LIST_COLUMN_SUPERVISOR => 'Supervisor')));



define('FAQ_LIST_TABLE', 'Faq');

define('FAQ_LIST_COLUMN_ID', 0x00000001);
define('FAQ_LIST_COLUMN_QUESTION', 0x00000002);
define('FAQ_LIST_COLUMN_ANSWER', 0x00000004);

define('FAQ_LIST_SQL_COLUMNS', serialize(array(FAQ_LIST_COLUMN_ID => 'Faq_ID',
					       FAQ_LIST_COLUMN_QUESTION => 'Question',
					       FAQ_LIST_COLUMN_ANSWER => 'Answer')));


define('FAQ_TABLE', 'Faq');

define('FAQ_COLUMN_ID', 0x00000001);
define('FAQ_COLUMN_TRACK_ID', 0x00000002);
define('FAQ_COLUMN_QUESTION', 0x00000004);
define('FAQ_COLUMN_ANSWER', 0x00000008);

define('FAQ_SQL_COLUMNS', serialize(array(FAQ_COLUMN_ID => 'Faq_ID',
					  FAQ_COLUMN_TRACK_ID => 'Track_ID',
					  FAQ_COLUMN_QUESTION => 'Question',
					  FAQ_COLUMN_ANSWER => 'Answer')));

define('FAQ_LIST_LABELS', serialize(array(FAQ_LIST_COLUMN_ID => 'FAQ ID',
					  FAQ_LIST_COLUMN_QUESTION => 'Question',
					  FAQ_LIST_COLUMN_ANSWER => 'Answer')));



// Track schema
define('TRACK_TABLE', 'Track');

// Track SQL Columns
define('TRACK_COLUMN_ID', 0x00000001);
define('TRACK_COLUMN_NAME', 0x00000002);
define('TRACK_COLUMN_SUPERVISOR_ID', 0x00000004);
define('TRACK_COLUMN_IS_MANAGEMENT', 0x00000008);
define('TRACK_COLUMN_IS_BINDING', 0x00000010);
define('TRACK_COLUMN_IS_LOOPING', 0x00000020);
define('TRACK_COLUMN_IS_AB1825', 0x00000040);
define('TRACK_COLUMN_EMAIL_REPLY_TO', 0x00000080);
define('TRACK_COLUMN_RECURRENCE', 0x00000100);
define('TRACK_COLUMN_START_DATE', 0x00000200);
define('TRACK_COLUMN_DELINQUENCY_NOTIFICATION', 0x00000400);
define('TRACK_COLUMN_LAST_QUESTION_NUMBER', 0x00000800);
define('TRACK_COLUMN_LAST_QUESTION_DATE', 0x00001000);
define('TRACK_COLUMN_TRACK_NEWS_URL', 0x00002000);

// Track Column list
define('TRACK_SQL_COLUMNS', serialize(array(TRACK_COLUMN_ID => 'Track_ID',
					    TRACK_COLUMN_NAME => 'Name',
					    TRACK_COLUMN_SUPERVISOR_ID => 'Supervisor_ID',
					    TRACK_COLUMN_IS_MANAGEMENT => 'Is_Management',
					    TRACK_COLUMN_IS_BINDING => 'Is_Binding',
					    TRACK_COLUMN_IS_LOOPING => 'Is_Looping',
					    TRACK_COLUMN_IS_AB1825 => 'Is_Ab1825',
					    TRACK_COLUMN_EMAIL_REPLY_TO => 'Email_Reply_To',
					    TRACK_COLUMN_RECURRENCE => 'Recurrence',
					    TRACK_COLUMN_START_DATE => 'Start_Date',
					    TRACK_COLUMN_DELINQUENCY_NOTIFICATION => 'Delinquency_Notification',
					    TRACK_COLUMN_LAST_QUESTION_NUMBER => 'Last_Question_Number',
					    TRACK_COLUMN_LAST_QUESTION_DATE => 'Last_Question_Date',
					    TRACK_COLUMN_TRACK_NEWS_URL => 'Track_News_URL')));


// Queue table and field
define('QUEUE_TABLE', 'Queue');
define('QUEUE_COLUMN_TRACK_ID', 0x00000001);
define('QUEUE_COLUMN_QUESTION_ID', 0x00000002);
define('QUEUE_COLUMN_SEQUENCE', 0x00000004);
define('QUEUE_COLUMN_GROUP_ID', 0x00000008);
define('QUEUE_SQL_COLUMNS', serialize(array(QUEUE_COLUMN_TRACK_ID => 'Track_ID',
					    QUEUE_COLUMN_QUESTION_ID => 'Question_Number',
					    QUEUE_COLUMN_SEQUENCE => 'Sequence',
					    QUEUE_COLUMN_GROUP_ID => 'GroupID')));



// Department table
define('PARTICIPANT_TABLE', 'Participant');
define('PARTICIPANT_COLUMN_ID', 0x00000001);
define('PARTICIPANT_COLUMN_USER_ID', 0x00000002);
define('PARTICIPANT_COLUMN_TRACK_ID', 0x00000004);
define('PARTICIPANT_COLUMN_LAST_QUESTION_NUMBER', 0x00000008);
define('PARTICIPANT_COLUMN_PLACED_DATE', 0x00000010);

define('PARTICIPANT_SQL_COLUMNS', serialize(array(PARTICIPANT_COLUMN_ID => 'Participant_ID',
						  PARTICIPANT_COLUMN_USER_ID => 'User_ID',
						  PARTICIPANT_COLUMN_TRACK_ID => 'Track_ID',
						  PARTICIPANT_COLUMN_LAST_QUESTION_NUMBER => 'Last_Question_Number',
						  PARTICIPANT_COLUMN_PLACED_DATE => 'Placed_Date')));


// Organization List Table (schema from db)
define('USER_LIST_TABLE', 'User');


// Organization List columns
// These bit positions *MUST* correspond to the indexes in the arrays below
define('USER_LIST_COLUMN_ID', 0x00000001);
define('USER_LIST_COLUMN_LOGIN', 0x00000002);
define('USER_LIST_COLUMN_LAST_NAME', 0x00000004);
define('USER_LIST_COLUMN_FIRST_NAME', 0x00000008);
define('USER_LIST_COLUMN_EMAIL', 0x00000010);
define('USER_LIST_COLUMN_DIVISION_ID', 0x00000020);
define('USER_LIST_COLUMN_DEPARTMENT_ID', 0x00000040);
define('USER_LIST_COLUMN_LANGUAGE_ID', 0x00000080);
define('USER_LIST_COLUMN_EMPLOYMENT_STATUS_ID', 0x00000100);
define('USER_LIST_COLUMN_DOMAIN_ID', 0x00000200);


define('USER_LIST_SQL_COLUMNS', serialize(array(USER_LIST_COLUMN_ID => 'User_ID',
						USER_LIST_COLUMN_LOGIN => 'Login_ID',
						USER_LIST_COLUMN_LAST_NAME => 'Last_Name',
						USER_LIST_COLUMN_FIRST_NAME => 'First_Name',
						USER_LIST_COLUMN_EMAIL => 'Email',
						USER_LIST_COLUMN_DIVISION_ID => 'Division_ID',
						USER_LIST_COLUMN_DEPARTMENT_ID => 'Department_ID',
						USER_LIST_COLUMN_LANGUAGE_ID => 'Language_ID',
						USER_LIST_COLUMN_EMPLOYMENT_STATUS_ID => 'Employment_Status_ID',
						USER_LIST_COLUMN_DOMAIN_ID => 'State_ID'))); // State_ID is really a Domain_ID

// Labels displayed in the interface
define('USER_LIST_LABELS', serialize(array(USER_LIST_COLUMN_ID => 'Employee ID',
					   USER_LIST_COLUMN_LOGIN => 'Login',
					   USER_LIST_COLUMN_LAST_NAME => 'Last Name',
					   USER_LIST_COLUMN_FIRST_NAME => 'First Name',
					   USER_LIST_COLUMN_EMAIL => 'Email Address',
					   USER_LIST_COLUMN_DIVISION_ID => 'Division',
					   USER_LIST_COLUMN_DEPARTMENT_ID => 'Department',
					   USER_LIST_COLUMN_LANGUAGE_ID => 'Language',
					   USER_LIST_COLUMN_EMPLOYMENT_STATUS_ID => 'Status',
					   USER_LIST_COLUMN_DOMAIN_ID => 'State')));


// Users table (schema from db)
define('USER_TABLE', 'User');

// Employee List columns
define('USER_COLUMN_ID', 0x00000001);
define('USER_COLUMN_LOGIN', 0x00000002);
define('USER_COLUMN_FIRST_NAME', 0x00000004);
define('USER_COLUMN_LAST_NAME', 0x00000008);
define('USER_COLUMN_FULL_NAME', 0x00000010);
define('USER_COLUMN_PHONE', 0x00000020);
define('USER_COLUMN_EMAIL', 0x00000040);
define('USER_COLUMN_USE_SUPERVISOR', 0x00000080);
define('USER_COLUMN_LANGUAGE_ID', 0x00000100);
define('USER_COLUMN_CONTACT_ID', 0x00000200);
define('USER_COLUMN_DEPARTMENT_ID', 0x00000400);
define('USER_COLUMN_DIVISION_ID', 0x00000800);
define('USER_COLUMN_EMPLOYMENT_STATUS_ID', 0x00001000);
define('USER_COLUMN_DOMAIN_ID', 0x0002000);
define('USER_COLUMN_CUSTOM1_ID', 0x0004000);
define('USER_COLUMN_CUSTOM2_ID', 0x0008000);
define('USER_COLUMN_CUSTOM3_ID', 0x0010000);
define('USER_COLUMN_EMPLOYMENT_STATUS_DATE', 0x0020000);
define('USER_COLUMN_ROLE', 0x00400000);
//define('USER_COLUMN_', 0x000);

// User Schema
define('USER_SQL_COLUMNS', serialize(array(USER_COLUMN_ID => 'User_ID',
					   USER_COLUMN_LOGIN => 'Login_ID',
					   USER_COLUMN_FIRST_NAME => 'First_Name',
					   USER_COLUMN_LAST_NAME => 'Last_Name',
					   USER_COLUMN_FULL_NAME => 'Full_Name',
					   USER_COLUMN_PHONE => 'Phone',
					   USER_COLUMN_EMAIL => 'Email',
					   USER_COLUMN_USE_SUPERVISOR => 'Use_Supervisor',
					   USER_COLUMN_LANGUAGE_ID => 'Language_ID',
					   USER_COLUMN_CONTACT_ID => 'Contact_User_ID',
					   USER_COLUMN_DEPARTMENT_ID => 'Department_ID',
					   USER_COLUMN_DIVISION_ID => 'Division_ID',
					   USER_COLUMN_EMPLOYMENT_STATUS_ID => 'Employment_Status_ID',
					   USER_COLUMN_DOMAIN_ID => 'State_ID',
					   USER_COLUMN_CUSTOM1_ID => 'Custom1_ID',
					   USER_COLUMN_CUSTOM2_ID => 'Custom2_ID',
					   USER_COLUMN_CUSTOM3_ID => 'Custom3_ID',
					   USER_COLUMN_EMPLOYMENT_STATUS_DATE => 'Employment_Status_Date',
					   USER_COLUMN_ROLE => 'Role')));




// User Assignment table
define('USER_ASSIGNMENT_TABLE', 'User_Assignment');
define('USER_ASSIGNMENT_COLUMN_QUESTION_NUMBER', 0x00000001);
define('USER_ASSIGNMENT_COLUMN_USER_ID', 0x00000002);
define('USER_ASSIGNMENT_COLUMN_TRACK_ID', 0x00000004);
define('USER_ASSIGNMENT_COLUMN_DELIVERY_DATE', 0x00000008);
define('USER_ASSIGNMENT_COLUMN_COMPLETION_DATE', 0x00000010);
define('USER_ASSIGNMENT_COLUMN_EXIT_INFO', 0x00000020);
define('USER_ASSIGNMENT_COLUMN_VERSION_DATE', 0x00000040);
define('USER_ASSIGNMENT_COLUMN_DOMAIN_ID', 0x00000080);
define('USER_ASSIGNMENT_COLUMN_LANGUAGE_ID', 0x00000100);

define('USER_ASSIGNMENT_SQL_COLUMNS', serialize(array(USER_ASSIGNMENT_COLUMN_QUESTION_NUMBER =>
						      USER_ASSIGNMENT_TABLE.'.Question_Number',
						      USER_ASSIGNMENT_COLUMN_USER_ID =>
						      USER_ASSIGNMENT_TABLE.'.User_ID',
						      USER_ASSIGNMENT_COLUMN_TRACK_ID =>
						      USER_ASSIGNMENT_TABLE.'.Track_ID',
						      USER_ASSIGNMENT_COLUMN_DELIVERY_DATE =>
						      'DATE('.USER_ASSIGNMENT_TABLE.'.Delivery_Date)',
						      USER_ASSIGNMENT_COLUMN_COMPLETION_DATE =>
						      USER_ASSIGNMENT_TABLE.'.Completion_Date',
						      USER_ASSIGNMENT_COLUMN_EXIT_INFO =>
						      USER_ASSIGNMENT_TABLE.'.Exit_Info',
						      USER_ASSIGNMENT_COLUMN_VERSION_DATE =>
						      'DATE('.USER_ASSIGNMENT_TABLE.'.Version_Date)',
						      USER_ASSIGNMENT_COLUMN_DOMAIN_ID =>
						      USER_ASSIGNMENT_TABLE.'.Domain_ID',
						      USER_ASSIGNMENT_COLUMN_LANGUAGE_ID =>
						      USER_ASSIGNMENT_TABLE.'.Language_ID')));


// User Assignment table
define('ASSIGNMENT_TABLE', 'Assignment');
define('ASSIGNMENT_COLUMN_ID', 0x00000001);
define('ASSIGNMENT_COLUMN_PARENT_ID', 0x00000002);
define('ASSIGNMENT_COLUMN_USER_ID', 0x00000004);
define('ASSIGNMENT_COLUMN_TRACK_ID', 0x00000008);
define('ASSIGNMENT_COLUMN_QUESTION_NUMBER', 0x00000010);
define('ASSIGNMENT_COLUMN_VERSION_DATE', 0x00000020);
define('ASSIGNMENT_COLUMN_DOMAIN_ID', 0x00000040);
define('ASSIGNMENT_COLUMN_LANGUAGE_ID', 0x0000080);
define('ASSIGNMENT_COLUMN_DELIVERY_DATE', 0x00000100);
define('ASSIGNMENT_COLUMN_DUE_DATE', 0x00000200);
define('ASSIGNMENT_COLUMN_COMPLETION_DATE', 0x00000400);
define('ASSIGNMENT_COLUMN_FIRST_ANSWER', 0x00000800);
define('ASSIGNMENT_COLUMN_FINAL_ANSWER', 0x00001000);
define('ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY', 0x00002000);
define('ASSIGNMENT_COLUMN_ANSWERED_LATE', 0x00004000);
define('ASSIGNMENT_COLUMN_EXIT_INFO', 0x00008000);
define('ASSIGNMENT_COLUMN_DEPARTMENT_ID', 0x00010000);
define('ASSIGNMENT_COLUMN_DIVISION_ID', 0x00020000);
define('ASSIGNMENT_COLUMN_COMPLETED_SECONDS', 0x00040000);

define('DEFAULT_ASSIGNMENT_SORT_COLUMN', ASSIGNMENT_COLUMN_ID);
define('DEFAULT_ASSIGNMENT_SEARCH_COLUMN', ASSIGNMENT_COLUMN_ID);

define('ASSIGNMENT_SQL_COLUMNS', serialize(array(ASSIGNMENT_COLUMN_ID => 'Assignment_ID',
						 ASSIGNMENT_COLUMN_PARENT_ID => 'Assignment_Parent_ID',
						 ASSIGNMENT_COLUMN_USER_ID => 'User_ID',
						 ASSIGNMENT_COLUMN_TRACK_ID =>'Track_ID',
						 ASSIGNMENT_COLUMN_QUESTION_NUMBER => 'Question_Number',
						 ASSIGNMENT_COLUMN_VERSION_DATE => 'Version_Date',
						 ASSIGNMENT_COLUMN_DOMAIN_ID => 'Domain_ID',
						 ASSIGNMENT_COLUMN_LANGUAGE_ID => 'Language_ID',
						 ASSIGNMENT_COLUMN_DELIVERY_DATE => 'Delivery_Date',
						 ASSIGNMENT_COLUMN_DUE_DATE => 'Due_Date',
						 ASSIGNMENT_COLUMN_COMPLETION_DATE => 'Completion_Date',
						 ASSIGNMENT_COLUMN_FIRST_ANSWER => 'First_Answer',
						 ASSIGNMENT_COLUMN_FINAL_ANSWER => 'Final_Answer',
						 ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY => 'Answered_Correctly',
						 ASSIGNMENT_COLUMN_ANSWERED_LATE => 'Answered_Late',
						 ASSIGNMENT_COLUMN_EXIT_INFO => 'Exit_Info',
						 ASSIGNMENT_COLUMN_DEPARTMENT_ID => 'Department_ID',
						 ASSIGNMENT_COLUMN_DIVISION_ID => 'Division_ID',
						 ASSIGNMENT_COLUMN_COMPLETED_SECONDS => 'Completed_Seconds')));


// Labels displayed in the interface
define('ASSIGNMENT_LIST_LABELS', serialize(array(ASSIGNMENT_COLUMN_USER_ID => 'User Name',
						 ASSIGNMENT_COLUMN_QUESTION_NUMBER => 'Question #',
ASSIGNMENT_COLUMN_VERSION_DATE => 'Version Date',
						 ASSIGNMENT_COLUMN_DOMAIN_ID => 'Domain',
						 ASSIGNMENT_COLUMN_LANGUAGE_ID => 'Language',
						 ASSIGNMENT_COLUMN_DELIVERY_DATE => 'Delivery Date',
						 ASSIGNMENT_COLUMN_COMPLETION_DATE => 'Completion Date',
						 ASSIGNMENT_COLUMN_EXIT_INFO => 'Exit Info')));


// Department table
define('DEPARTMENT_TABLE', 'Department');
define('DEPARTMENT_COLUMN_ID', 0x00000001);
define('DEPARTMENT_COLUMN_NAME', 0x00000002);

define('DEPARTMENT_SQL_COLUMNS', serialize(array(DEPARTMENT_COLUMN_ID => 'Department_ID',
						 DEPARTMENT_COLUMN_NAME => 'Name')));


define('DIVISION_TABLE', 'Division');
define('DIVISION_COLUMN_ID', 0x00000001);
define('DIVISION_COLUMN_NAME', 0x00000002);

define('DIVISION_SQL_COLUMNS', serialize(array(DIVISION_COLUMN_ID => 'Division_ID',
					       DIVISION_COLUMN_NAME => 'Name')));


define('CUSTOM1_TABLE', 'Custom1');
define('CUSTOM1_COLUMN_ID', 0x00000001);
define('CUSTOM1_COLUMN_NAME', 0x00000002);
define('CUSTOM1_SQL_COLUMNS', serialize(array(CUSTOM1_COLUMN_ID => 'Custom1_ID',
					      CUSTOM1_COLUMN_NAME => 'Name')));

define('CUSTOM2_TABLE', 'Custom2');
define('CUSTOM2_COLUMN_ID', 0x00000001);
define('CUSTOM2_COLUMN_NAME', 0x00000002);
define('CUSTOM2_SQL_COLUMNS', serialize(array(CUSTOM2_COLUMN_ID => 'Custom2_ID',
					      CUSTOM2_COLUMN_NAME => 'Name')));

define('CUSTOM3_TABLE', 'Custom3');
define('CUSTOM3_COLUMN_ID', 0x00000001);
define('CUSTOM3_COLUMN_NAME', 0x00000002);
define('CUSTOM3_SQL_COLUMNS', serialize(array(CUSTOM3_COLUMN_ID => 'Custom3_ID',
					      CUSTOM3_COLUMN_NAME => 'Name')));

define('CUSTOM_LABEL_TABLE', 'Custom_Label_Lookup');
define('CUSTOM_LABEL_COLUMN_CUSTOM1', 0x00000001);
define('CUSTOM_LABEL_COLUMN_CUSTOM2', 0x00000002);
define('CUSTOM_LABEL_COLUMN_CUSTOM3', 0x00000004);
define('CUSTOM_LABEL_SQL_COLUMNS', serialize(array(CUSTOM_LABEL_COLUMN_CUSTOM1 => 'Custom1',
						   CUSTOM_LABEL_COLUMN_CUSTOM2 => 'Custom2',
						   CUSTOM_LABEL_COLUMN_CUSTOM3 => 'Custom3')));


define('EMPLOYMENT_STATUS_TABLE', 'Employment_Status');
define('EMPLOYMENT_STATUS_COLUMN_ID', 0x00000001);
define('EMPLOYMENT_STATUS_COLUMN_NAME', 0x00000002);
define('EMPLOYMENT_STATUS_SQL_COLUMNS', serialize(array(EMPLOYMENT_STATUS_COLUMN_ID => 'Employment_Status_ID',
							EMPLOYMENT_STATUS_COLUMN_NAME => 'Name')));



define('LANGUAGE_TABLE', 'Language');
define('LANGUAGE_COLUMN_ID', 0x00000001);
define('LANGUAGE_COLUMN_NAME', 0x00000002);
define('LANGUAGE_SQL_COLUMNS', serialize(array(LANGUAGE_COLUMN_ID => 'Language_ID',
					       LANGUAGE_COLUMN_NAME => 'Name')));



// Question List table
define('QUESTION_LIST_TABLE', 'Question');

// Question List columns
define('QUESTION_LIST_COLUMN_NUMBER', 0x00000001);
define('QUESTION_LIST_COLUMN_PARENT_NUMBER', 0x00000002);
define('QUESTION_LIST_COLUMN_TITLE', 0x00000004);
define('QUESTION_LIST_COLUMN_CATEGORY', 0x00000008);
define('QUESTION_LIST_COLUMN_LANGUAGE', 0x00000010);
define('QUESTION_LIST_COLUMN_DOMAIN', 0x00000020);
define('QUESTION_LIST_COLUMN_VERSION_DATE', 0x00000040);
// The first element in this array is the Question List table name
define('QUESTION_LIST_SQL_COLUMNS', serialize(array(QUESTION_LIST_COLUMN_NUMBER => 'Question_Number',
						    QUESTION_LIST_COLUMN_PARENT_NUMBER => 'Parent_Question_Number',
						    QUESTION_LIST_COLUMN_TITLE => 'Title',
						    QUESTION_LIST_COLUMN_CATEGORY => 'Category_ID',
						    QUESTION_LIST_COLUMN_LANGUAGE => 'Language_ID',
						    QUESTION_LIST_COLUMN_DOMAIN => 'Domain_ID',
						    QUESTION_LIST_COLUMN_VERSION_DATE => 'Version_Date')));

define('QUESTION_LIST_LABELS', serialize(array(QUESTION_LIST_COLUMN_NUMBER => 'Question #',
					       QUESTION_LIST_COLUMN_PARENT_NUMBER => 'Parent #',
					       QUESTION_LIST_COLUMN_TITLE => 'Title',
					       QUESTION_LIST_COLUMN_CATEGORY => 'Category',
					       QUESTION_LIST_COLUMN_LANGUAGE => 'Language',
					       QUESTION_LIST_COLUMN_DOMAIN => 'Domain',
					       QUESTION_LIST_COLUMN_VERSION_DATE => 'Version Date')));



// Question table
define('QUESTION_TABLE', 'Question');
define('QUESTION_ARCHIVE_TABLE', 'Question_Archive');

// Question columns
define('QUESTION_COLUMN_ID', 0x00000001);
define('QUESTION_COLUMN_PARENT_ID', 0x00000002);
define('QUESTION_COLUMN_LANGUAGE_ID', 0x00000004);
define('QUESTION_COLUMN_DOMAIN_ID', 0x00000008);
define('QUESTION_COLUMN_CATEGORY_ID', 0x00000010);
define('QUESTION_COLUMN_FRAMETYPE_ID', 0x00000020);
define('QUESTION_COLUMN_VERSION_DATE', 0x00000040);
define('QUESTION_COLUMN_IS_MANAGEMENT', 0x00000080);
define('QUESTION_COLUMN_AUTHOR', 0x00000100);
define('QUESTION_COLUMN_NOTES', 0x00000200);
define('QUESTION_COLUMN_TITLE', 0x00000400);

define('QUESTION_COLUMN_QUESTION', 0x00000800);
define('QUESTION_COLUMN_MULTIPLE_CHOICE1', 0x00001000);
define('QUESTION_COLUMN_MULTIPLE_CHOICE2', 0x00002000);
define('QUESTION_COLUMN_MULTIPLE_CHOICE3', 0x00004000);
define('QUESTION_COLUMN_MULTIPLE_CHOICE4', 0x00008000);
define('QUESTION_COLUMN_MULTIPLE_CHOICE5', 0x00010000);
define('QUESTION_COLUMN_FEEDBACK_CHOICE1', 0x00020000);
define('QUESTION_COLUMN_FEEDBACK_CHOICE2', 0x00040000);
define('QUESTION_COLUMN_FEEDBACK_CHOICE3', 0x00080000);
define('QUESTION_COLUMN_FEEDBACK_CHOICE4', 0x00100000);

define('QUESTION_COLUMN_FEEDBACK_CHOICE5', 0x00200000);
define('QUESTION_COLUMN_CORRECT_ANSWER', 0x00400000);
define('QUESTION_COLUMN_PURPOSE', 0x0800000);
define('QUESTION_COLUMN_LEARNING_POINTS', 0x01000000);
define('QUESTION_COLUMN_MEDIA_FILE_PATH', 0x02000000);
define('QUESTION_COLUMN_MEDIA_DURATION_SECONDS', 0x04000000);
define('QUESTION_COLUMN_SUPPORT_INFO', 0x08000000);
define('QUESTION_COLUMN_CAP_CONFIRM_THANKS', 0x10000000);
define('QUESTION_COLUMN_REQUIRE_CORRECT_ANSWER', 0x20000000);
define('QUESTION_COLUMN_DISPLAY_CHOICES', 0x40000000);
define('QUESTION_COLUMN_OWNER_USER_ID', 0x80000000);


// Org Columns list
define('QUESTION_SQL_COLUMNS', serialize(array(QUESTION_COLUMN_ID => 'Question_Number',
					       QUESTION_COLUMN_PARENT_ID => 'Parent_Question_Number',
					       QUESTION_COLUMN_LANGUAGE_ID => 'Language_ID',
					       QUESTION_COLUMN_DOMAIN_ID => 'Domain_ID',
					       QUESTION_COLUMN_CATEGORY_ID => 'Category_ID',
					       QUESTION_COLUMN_FRAMETYPE_ID => 'Frametype_ID',
					       QUESTION_COLUMN_VERSION_DATE => 'Version_Date',
					       QUESTION_COLUMN_IS_MANAGEMENT => 'Is_Management',
					       QUESTION_COLUMN_AUTHOR => 'Author',
					       QUESTION_COLUMN_NOTES => 'Notes',
					       QUESTION_COLUMN_TITLE => 'Title',
					       QUESTION_COLUMN_QUESTION => 'Question',
					       QUESTION_COLUMN_MULTIPLE_CHOICE1 => 'Multiple_Choice1',
					       QUESTION_COLUMN_MULTIPLE_CHOICE2 => 'Multiple_Choice2',
					       QUESTION_COLUMN_MULTIPLE_CHOICE3 => 'Multiple_Choice3',
					       QUESTION_COLUMN_MULTIPLE_CHOICE4 => 'Multiple_Choice4',
					       QUESTION_COLUMN_MULTIPLE_CHOICE5 => 'Multiple_Choice5',
					       QUESTION_COLUMN_FEEDBACK_CHOICE1 => 'Feedback_Choice1',
					       QUESTION_COLUMN_FEEDBACK_CHOICE2 => 'Feedback_Choice2',
					       QUESTION_COLUMN_FEEDBACK_CHOICE3 => 'Feedback_Choice3',
					       QUESTION_COLUMN_FEEDBACK_CHOICE4 => 'Feedback_Choice4',
					       QUESTION_COLUMN_FEEDBACK_CHOICE5 => 'Feedback_Choice5',
					       QUESTION_COLUMN_CORRECT_ANSWER => 'Correct_Answer',
					       QUESTION_COLUMN_PURPOSE => 'Purpose',
					       QUESTION_COLUMN_LEARNING_POINTS => 'Learning_Points',
					       QUESTION_COLUMN_MEDIA_FILE_PATH => 'Media_File_Path',
					       QUESTION_COLUMN_MEDIA_DURATION_SECONDS => 'Media_Duration_Seconds',
					       QUESTION_COLUMN_SUPPORT_INFO => 'Support_Info',
					       QUESTION_COLUMN_CAP_CONFIRM_THANKS => 'CapConfirmThanks',
					       QUESTION_COLUMN_REQUIRE_CORRECT_ANSWER => 'RequireCorrectAnswer',
					       QUESTION_COLUMN_DISPLAY_CHOICES => 'Display_Choices',
					       QUESTION_COLUMN_OWNER_USER_ID => 'Owner_User_ID')));


define('QUESTION_DATA_TABLE', 'Question_Data');
define('QUESTION_DATA_COLUMN_QUESTION_ID', 0x00000001);
define('QUESTION_DATA_COLUMN_VERSION_DATE', 0x00000002);
define('QUESTION_DATA_COLUMN_DOMAIN_ID', 0x00000004);
define('QUESTION_DATA_COLUMN_COMPLETION_DATE', 0x00000008);
define('QUESTION_DATA_COLUMN_RESULT', 0x00000010);
define('QUESTION_DATA_COLUMN_FOIL_SELECTED', 0x00000020);
define('QUESTION_DATA_COLUMN_EXIT_INFO', 0x00000040);
define('QUESTION_DATA_COLUMN_TRACK_ID', 0x00000080);
define('QUESTION_DATA_COLUMN_DEPARTMENT_ID', 0x00000100);
define('QUESTION_DATA_COLUMN_LANGUAGE_ID', 0x00000200);

define('QUESTION_DATA_SQL_COLUMNS', serialize(array(QUESTION_DATA_COLUMN_QUESTION_ID => 'Question_Number',
						    QUESTION_DATA_COLUMN_VERSION_DATE => 'Version_Date',
						    QUESTION_DATA_COLUMN_DOMAIN_ID => 'Domain_ID',
						    QUESTION_DATA_COLUMN_COMPLETION_DATE => 'Completion_Date',
						    QUESTION_DATA_COLUMN_RESULT => 'Result',
						    QUESTION_DATA_COLUMN_FOIL_SELECTED => 'Foil_Selected',
						    QUESTION_DATA_COLUMN_EXIT_INFO => 'Exit_Info',
						    QUESTION_DATA_COLUMN_TRACK_ID => 'Track_ID',
						    QUESTION_DATA_COLUMN_DEPARTMENT_ID => 'Department_ID',
						    QUESTION_DATA_COLUMN_LANGUAGE_ID => 'Language_ID')));



define('SUMMARY_LIST_COLUMN_QUESTION_ID', 0x00000001);
define('SUMMARY_LIST_COLUMN_VERSION_DATE', 0x00000002);
define('SUMMARY_LIST_COLUMN_COMPLETION_DATE', 0x00000004);
define('SUMMARY_LIST_COLUMN_USER_ID', 0x00000008);
define('SUMMARY_LIST_COLUMN_ANSWER_SELECTED', 0x00000010);
define('SUMMARY_LIST_COLUMN_RESULT', 0x00000020);
define('SUMMARY_LIST_COLUMN_EXIT_INFO', 0x00000040);
define('SUMMARY_LIST_COLUMN_TRACK_ID', 0x00000080);
define('SUMMARY_LIST_COLUMN_DEPARTMENT_ID', 0x00000100);
define('SUMMARY_LIST_COLUMN_DOMAIN_ID', 0x00000200);
define('SUMMARY_LIST_COLUMN_LANGUAGE_ID', 0x00000400);

define('SUMMARY_LIST_LABELS', serialize(array(SUMMARY_LIST_COLUMN_QUESTION_ID => 'Question #',
					      SUMMARY_LIST_COLUMN_VERSION_DATE => 'Version Date',
					      SUMMARY_LIST_COLUMN_COMPLETION_DATE => 'Completion Date',
					      SUMMARY_LIST_COLUMN_USER_ID => 'User Name',
					      SUMMARY_LIST_COLUMN_ANSWER_SELECTED => 'Answer Selected',
					      SUMMARY_LIST_COLUMN_RESULT => 'Result',
					      SUMMARY_LIST_COLUMN_EXIT_INFO => 'Exit Info',
					      SUMMARY_LIST_COLUMN_TRACK_ID => 'Track',
					      SUMMARY_LIST_COLUMN_DEPARTMENT_ID => 'Department',
					      SUMMARY_LIST_COLUMN_DOMAIN_ID => 'Domain')));
//SUMMARY_LIST_COLUMN_LANGUAGE_ID => 'Language';

define('SUMMARY_LIST_SQL_COLUMNS', serialize(array(SUMMARY_LIST_COLUMN_QUESTION_ID => 'Question_Number',
						   SUMMARY_LIST_COLUMN_VERSION_DATE => 'Version_Date',
						   SUMMARY_LIST_COLUMN_COMPLETION_DATE => 'Completion_Date',
						   SUMMARY_LIST_COLUMN_USER_ID => 'User_ID',
						   SUMMARY_LIST_COLUMN_ANSWER_SELECTED => 'Foil_Selected',
						   SUMMARY_LIST_COLUMN_RESULT => 'Result',
						   SUMMARY_LIST_COLUMN_EXIT_INFO => 'Exit_Info',
						   SUMMARY_LIST_COLUMN_TRACK_ID => 'Track_ID',
						   SUMMARY_LIST_COLUMN_DEPARTMENT_ID => 'Department_ID',
						   SUMMARY_LIST_COLUMN_DOMAIN_ID => 'Domain_ID')));

define('SUMMARY_LIST_AS_SQL_COLUMNS', serialize(array(SUMMARY_LIST_COLUMN_QUESTION_ID => 'Question_Number',
						      SUMMARY_LIST_COLUMN_VERSION_DATE => 'Version_Date',
						      SUMMARY_LIST_COLUMN_COMPLETION_DATE => 'Completion_Date',
						      SUMMARY_LIST_COLUMN_USER_ID => 'Full_Name',
						      SUMMARY_LIST_COLUMN_ANSWER_SELECTED => 'Foil_Selected',
						      SUMMARY_LIST_COLUMN_RESULT => 'Result',
						      SUMMARY_LIST_COLUMN_EXIT_INFO => 'Exit_Info',
						      SUMMARY_LIST_COLUMN_TRACK_ID => 'Track',
						      SUMMARY_LIST_COLUMN_DEPARTMENT_ID => 'Department',
						      SUMMARY_LIST_COLUMN_DOMAIN_ID => 'Domain')));
//SUMMARY_LIST_COLUMN_LANGUAGE_ID => 'Language_ID'
define('SUMMARY_LIST_COLUMNS_MAP', serialize(array(SUMMARY_LIST_COLUMN_QUESTION_ID => ASSIGNMENT_COLUMN_QUESTION_NUMBER,
						   SUMMARY_LIST_COLUMN_VERSION_DATE => ASSIGNMENT_COLUMN_VERSION_DATE,
						   SUMMARY_LIST_COLUMN_COMPLETION_DATE => ASSIGNMENT_COLUMN_COMPLETION_DATE,
						   SUMMARY_LIST_COLUMN_USER_ID => ASSIGNMENT_COLUMN_USER_ID,
						   SUMMARY_LIST_COLUMN_ANSWER_SELECTED => ASSIGNMENT_COLUMN_FIRST_ANSWER,
						   SUMMARY_LIST_COLUMN_RESULT => ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY,
						   SUMMARY_LIST_COLUMN_EXIT_INFO => ASSIGNMENT_COLUMN_EXIT_INFO,
						   SUMMARY_LIST_COLUMN_TRACK_ID => ASSIGNMENT_COLUMN_TRACK_ID,
						   SUMMARY_LIST_COLUMN_DEPARTMENT_ID => ASSIGNMENT_COLUMN_DEPARTMENT_ID,
						   SUMMARY_LIST_COLUMN_DOMAIN_ID => ASSIGNMENT_COLUMN_DOMAIN_ID,
						   SUMMARY_LIST_COLUMN_LANGUAGE_ID => ASSIGNMENT_COLUMN_LANGUAGE_ID)));



// No assignment table since it is a join from two other tables, queue and question
// Assignment List columns
define('QUEUE_LIST_COLUMN_QUESTION_ID', 0x00000001);
define('QUEUE_LIST_COLUMN_SEQUENCE', 0x00000002);
define('QUEUE_LIST_COLUMN_GROUP_ID', 0x00000004);
define('QUEUE_LIST_COLUMN_QUESTION_TITLE', 0x00000008);
define('QUEUE_LIST_COLUMN_QUESTION_CATEGORY_ID', 0X00000010);
define('QUEUE_LIST_COLUMN_QUESTION_LANGUAGE_ID', 0X00000020);
define('QUEUE_LIST_COLUMN_QUESTION_DOMAIN_ID', 0X00000040);
define('QUEUE_LIST_COLUMN_QUESTION_VERSION_DATE', 0x00000080);
define('QUEUE_LIST_COLUMN_TRACK_ID', 0x00000100);


// Assignment list sql columns not necessary since the assignment list is
// just a join between the queue table and the question table
// The code pulls column information from those schema definition constants
// to get the column and table names needed to complete the assignment list queries
define('QUEUE_LIST_LABELS', serialize(array(QUEUE_LIST_COLUMN_QUESTION_ID => 'Question #',
						 QUEUE_LIST_COLUMN_SEQUENCE => 'Sequence',
						 QUEUE_LIST_COLUMN_GROUP_ID => 'Group',
						 QUEUE_LIST_COLUMN_QUESTION_TITLE => 'Title',
						 QUEUE_LIST_COLUMN_QUESTION_CATEGORY_ID => 'Category',
						 QUEUE_LIST_COLUMN_QUESTION_LANGUAGE_ID => 'Language',
						 QUEUE_LIST_COLUMN_QUESTION_DOMAIN_ID => 'Domain',
						 QUEUE_LIST_COLUMN_QUESTION_VERSION_DATE => 'Version Date')));




define('QUEUE_LIST_SQL_COLUMNS', serialize(array(QUEUE_LIST_COLUMN_QUESTION_ID => QUESTION_TABLE.'.Question_Number',
						 QUEUE_LIST_COLUMN_SEQUENCE => QUEUE_TABLE.'.Sequence',
						 QUEUE_LIST_COLUMN_GROUP_ID => QUEUE_TABLE.'.GroupID',
						 QUEUE_LIST_COLUMN_QUESTION_TITLE => QUESTION_TABLE.'.Title',
						 QUEUE_LIST_COLUMN_QUESTION_CATEGORY_ID => QUESTION_TABLE.'.Category_ID',
						 QUEUE_LIST_COLUMN_QUESTION_LANGUAGE_ID => QUESTION_TABLE.'.Language_ID',
						 QUEUE_LIST_COLUMN_QUESTION_DOMAIN_ID => QUESTION_TABLE.'.Domain_ID',
						 QUEUE_LIST_COLUMN_QUESTION_VERSION_DATE => QUESTION_TABLE.'.Version_Date')));




define('DOMAIN_TABLE', 'Domain');
define('DOMAIN_COLUMN_ID', 0x00000001);
define('DOMAIN_COLUMN_NAME', 0x00000002);
define('DOMAIN_COLUMN_ABBREVIATION', 0x00000004);
define('DOMAIN_SQL_COLUMNS', serialize(array(DOMAIN_COLUMN_ID => 'Domain_ID',
					     DOMAIN_COLUMN_NAME => 'Name',
					     DOMAIN_COLUMN_ABBREVIATION => 'Abbreviation')));


define('CATEGORY_TABLE', 'Category'); 
define('CATEGORY_COLUMN_ID', 0x00000001);
define('CATEGORY_COLUMN_NAME', 0x00000002);
define('CATEGORY_SQL_COLUMNS', serialize(array(CATEGORY_COLUMN_ID => 'Category_ID',
					       CATEGORY_COLUMN_NAME => 'Name')));


define('FRAMETYPE_TABLE', 'Frametype');
define('FRAMETYPE_COLUMN_ID', 0x00000001);
define('FRAMETYPE_COLUMN_NAME', 0x00000002);
define('FRAMETYPE_COLUMN_ITEM_NAME', 0x00000004);
define('FRAMETYPE_SQL_COLUMNS', serialize(array(FRAMETYPE_COLUMN_ID => 'Frametype_ID',
						FRAMETYPE_COLUMN_NAME => 'Name',
						FRAMETYPE_COLUMN_ITEM_NAME => 'ItemName')));


define('EMAIL_QUEUE_TABLE', 'Email_Queue');
define('EMAIL_QUEUE_COLUMN_ID', 0x00000001);
define('EMAIL_QUEUE_COLUMN_ORGANIZATION_ID', 0x00000002);
define('EMAIL_QUEUE_COLUMN_TRACK_ID', 0x00000004);
define('EMAIL_QUEUE_COLUMN_USER_ID', 0x00000008);
define('EMAIL_QUEUE_COLUMN_FROM_ADDRESS', 0x00000010);
define('EMAIL_QUEUE_COLUMN_SUBJECT', 0x00000020);
define('EMAIL_QUEUE_COLUMN_BODY', 0x00000040);
define('EMAIL_QUEUE_COLUMN_SUBMITTED_DATE', 0x00000080);
define('EMAIL_QUEUE_COLUMN_SCHEDULED_DATE', 0x00000100);
define('EMAIL_QUEUE_COLUMN_PROGRESS_DATE', 0x00000200);
define('EMAIL_QUEUE_COLUMN_PROCESSED_DATE', 0x00000400);
define('EMAIL_QUEUE_COLUMN_STATUS', 0x00000800);
define('EMAIL_QUEUE_COLUMN_RECIPIENT_COUNT', 0x00001000);
define('EMAIL_QUEUE_COLUMN_DELIVERED_COUNT', 0x00002000);
define('EMAIL_QUEUE_COLUMN_LAST_RECIPIENT', 0x00004000);
define('EMAIL_QUEUE_SQL_COLUMNS', serialize(array(EMAIL_QUEUE_COLUMN_ID => 'Email_Queue_ID',
						  EMAIL_QUEUE_COLUMN_ORGANIZATION_ID => 'Organization_ID',
						  EMAIL_QUEUE_COLUMN_TRACK_ID => 'Track_ID',
						  EMAIL_QUEUE_COLUMN_USER_ID => 'User_ID',
						  EMAIL_QUEUE_COLUMN_FROM_ADDRESS => 'From_Address',
						  EMAIL_QUEUE_COLUMN_SUBJECT => 'Subject',
						  EMAIL_QUEUE_COLUMN_BODY => 'Body',
						  EMAIL_QUEUE_COLUMN_SUBMITTED_DATE => 'Submitted_Date',
						  EMAIL_QUEUE_COLUMN_SCHEDULED_DATE => 'Scheduled_Date',
						  EMAIL_QUEUE_COLUMN_PROGRESS_DATE => 'Progress_Date',
						  EMAIL_QUEUE_COLUMN_PROCESSED_DATE => 'Processed_Date',
						  EMAIL_QUEUE_COLUMN_STATUS => 'Status',
						  EMAIL_QUEUE_COLUMN_RECIPIENT_COUNT => 'Recipient_Count',
						  EMAIL_QUEUE_COLUMN_DELIVERED_COUNT => 'Delivered_Count',
						  EMAIL_QUEUE_COLUMN_LAST_RECIPIENT => 'Last_Recipient')));

define('EMAIL_QUEUE_LABELS', serialize(array(EMAIL_QUEUE_COLUMN_FROM_ADDRESS => 'From Address',
					     EMAIL_QUEUE_COLUMN_SUBJECT => 'Subject',
					     EMAIL_QUEUE_COLUMN_SUBMITTED_DATE => 'Submitted',
					     EMAIL_QUEUE_COLUMN_PROCESSED_DATE => 'Delivered')));


define('ORG_DB_NAME_PREFIX', 'ORG');
define('ORG_SQL_FILE', '../../../scripts/create_db.sql');

// Definitions for database abstraction
define('RIGHT_JOIN', 'RIGHT JOIN');
define('LEFT_JOIN', 'LEFT JOIN');

// Constant definitions for options in the UI
define('MULTIPLE_CHOICE_OPTIONS', serialize(array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E')));

define('EXIT_INFO_INCOMPLETE', 0);
define('EXIT_INFO_UNDERSTOOD', 1);
define('EXIT_INFO_CONTACT', 2);
define('EXIT_INFO_OPTIONS', serialize(array(EXIT_INFO_INCOMPLETE => 'Incomplete', EXIT_INFO_UNDERSTOOD => 'Understood',
					    EXIT_INFO_CONTACT => 'Contact')));
define('RESULT_INCOMPLETE', 0);
define('RESULT_CORRECT', 1);
define('RESULT_INCORRECT', 2);
define('RESULT_OPTIONS', serialize(array(RESULT_INCOMPLETE => 'Incomplete', RESULT_CORRECT => 'Correct',
					 RESULT_INCORRECT => 'Incorrect')));

// Message Queue status values
define('EMAIL_QUEUE_STATUS_PENDING', 'pending');
define('EMAIL_QUEUE_STATUS_PROCESSING', 'processing');
define('EMAIL_QUEUE_STATUS_SENT', 'sent');


// Max limits on field lenghts
define('MAX_USERNAME_LENGTH', 45);
define('MAX_PASSWORD_LENGTH', 30);
define('MAX_FIRST_LENGTH', 30);
define('MAX_LAST_LENGTH', 30);
define('MAX_EMAIL_LENGTH', 80);


// Return codes used by functions
define ('RC_OK', -1);
define ('RC_ORG_SQL_FILE_NOT_FOUND', -2);
define ('RC_CREATE_DB_FAILED', -3);
define ('RC_ORG_DIRECTORY_EXISTS', -4);
define ('RC_ORG_NAME_EXISTS', -5);
define ('RC_CREATE_ID_FAILED', -6);
define ('RC_CREATE_DIRECTORY_FAILED', -7);
define ('RC_UPDATE_DB_NAME_FAILED', -8);
define ('RC_MYSQL_CLI_NOT_FOUND', -9);
define ('RC_MYSQL_SCRIPT_FAILED', -10);
define ('RC_ORG_INIT_DATA_FAILED', -11);
define ('RC_DELETE_ORG_FAILED', -12);
define ('RC_SELECT_DB_FAILED', -13);
define ('RC_UPDATE_INFO_FAILED', -14);
define ('RC_INVALID_DATA_FORMAT', -15);
define ('RC_AUTH_DB_CONNECT_FAILED', -16);
define ('RC_ORG_DB_CONNECT_FAILED', -17);
define ('RC_DUPLICATE_LOGIN', -18);
define ('RC_INSERT_FAILED', -19);
define ('RC_DELETE_FAILED', -20);
define ('RC_PASSWORD_REQUIRED', -21);
define ('RC_QUERY_FAILED', -22);
define ('RC_USER_UNKNOWN', -23);
define ('RC_INCORRECT_PASSWORD', -24);
define ('RC_LOGIN_FAILED', -25);
define ('RC_INVALID_ARG', -26);
define ('RC_NO_MORE_NUMBERS', -27);
define ('RC_INVALID_SEQUENCE', -28);
define ('RC_UPDATE_FAILED', -29);
define ('RC_INVALID_GROUP_ID', -30);
define ('RC_OPEN_FILE_FAILED', -31);
define ('RC_CSV_READ_FAILED', -32);
define ('RC_CSV_INVALID_QUESTION_NUMBER', -33);
define ('RC_CSV_INVALID_DATA_FORMAT', -34);
define ('RC_ACCOUNT_INACTIVE', -35);
define ('RC_OBJECT_INSTANCE_REQUIRED', -36);
define ('RC_FILE_NOT_FOUND', -37);
// Following added for organization creation, covers failed porting of questions from ORG8 to org being created
define ('RC_ADD_QUESTION_FAILED', -38);
// Following added to account for the sanity check of an empty directory on org create and delete
define ('RC_ORG_DIRECTORY_EMPTY', -39);
// Following added to account for failure of internal functions that link to centrally managed media
define ('RC_MEDIA_UNSPECIFIED', -40);
define ('RC_PARENT_QUESTION_NOEXIST', -41);
define ('RC_CLASS_CREATE_NO_AB1825_CONTENT', -42);
define ('RC_COPY_EMAIL_TEMPLATES_FAILED', -43);
define ('RC_INVALID_LOGIN_ORG', -44);
define ('RC_ORG_DIR_WHITESPACE', -45);
define ('RC_UPLOAD_EXCEEDED_MAX_INI_SIZE', -46);
define ('RC_UPLOAD_EXCEEDED_MAX_FORM_SIZE', -47);
define ('RC_UPLOAD_INCOMPLETE', -48);
define ('RC_UPLOAD_NO_FILE', -49);
define ('RC_UPLOAD_NO_TMP_DIR', -50);
define ('RC_UPLOAD_WRITE_FAILED', -51);
define ('RC_UPLOAD_EXTENSION_STOP', -52);
define ('RC_UPLOAD_ERR_UNKNOWN', -53);
?>
