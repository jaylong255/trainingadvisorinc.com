<?php

require_once('constants.php');
require_once('Config.php');
require_once('Organization.php');
require_once('User.php');
require_once('Track.php');

# This fucntion validates logins and initializes sessions
function UserLogin($username, $password, $orgId) {

  $userId = 0;

  if (empty($orgId) || !is_numeric($orgId))
    $orgId = 0;

  $rc = User::ValidateLogin($username, $password, $userId, $orgId);

  if ($rc !== RC_OK)
    return $rc;

  $user = new User($orgId, $userId);
  
  if($user->employmentStatusId == 3) {	// Deny Access
    error_log("LIB:UserLogin($username,$password): Account has Employment_Status_ID == 3 (Inactive).");
    session_destroy();
    return RC_ACCOUNT_INACTIVE;
  }
  
  InitSession($user);

  return RC_OK;
  
} // end function UserLogin()



function ChangeOrg($newOrgId) {

  //  $org = new Organization($newOrgId);
  $config = new Config($newOrgId);
  
  // Setup the new organization ID in the session
  $_SESSION['orgId'] = $newOrgId;
  $_SESSION['dbName'] = sprintf("%s%d", ORG_DB_NAME_PREFIX, $newOrgId);
  $_SESSION['uiTheme'] = $config->GetValue(CONFIG_UI_THEME);

  /*
  // Set organization home directory
  $_SESSION['orgDirectory'] = $org->GetDirectory();
      
  // Set the organization logo path/url
  if($org->GetLogo() &&
     file_exists(APPLICATION_ROOT.'/organizations/'
		 .$_SESSION['orgDirectory'].'/motif/'.$org->GetLogo())) {
    $_SESSION['orgLogo'] = '/desktop/organizations/'.$_SESSION['orgDirectory'].'/motif/'.$org->GetLogo();
  } else {
    $_SESSION['orgLogo'] = '/desktop/organizations/hr_tools/motif/hrtools_logo.gif';
  }

  $_SESSION['orgName'] = $org->GetName();

  // Setup the news file path/url
  if($org->GetNewsUrl() &&
     file_exists($org->GetNewsUrl())) {
    if(stristr($org->GetNewsUrl(), 'http://')) {
      $_SESSION['newsUrl'] = $org->GetNewsUrl();
    } else {
      $_SESSION['newsUrl'] = '/desktop/organizations/'.$_SESSION['orgDirectory']
	.'/news/'.$org->GetNewsUrl();
    }
  }
  */
  
  //  return $org;

} // end function ChangeOrg();


// name: esql
//
// arguments: db field
// returns: escaped db field
//
// desc: simple wrapper (less typing) for escaping db fields
//   it also checks to see if magic_quotes_gpc is turned on
//   and if so, leaves the field alone.  This helps to
//   eliminate confusion over this configuration variable.
function esql($field) {
  if ((bool)ini_get('magic_quotes_gpc'))
    return $field;
  else
    return mysql_escape_string($field);
}


function makesalt() {
  srand((double)microtime()*1000000);
  $saltseeds =
    array('.','/','0','1','2','3','4','5','6','7','8','9',
	  'a','b','c','d','e','f','g','h','i','j','k','l','m',
	  'n','o','p','q','r','s','t','u','v','w','x','y','z',
	  'A','B','C','D','E','F','G','H','I','J','K','L','M',
	  'N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
  return $saltseeds[rand(0,63)] . $saltseeds[rand(0,63)];
}


// This function takes an array of fields and concatenates them to the first string argument
// using a CSV format
function sputcsv(&$target, $fields, $delimiter = ',', $enclosure = '"', $mysql_null = false) {
    $delimiter_esc = preg_quote($delimiter, '/');
    $enclosure_esc = preg_quote($enclosure, '/');

    $output = array();
    foreach ($fields as $field) {
        if ($field === null && $mysql_null) {
            $output[] = 'NULL';
            continue;
        }

        $output[] = preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field) ? (
            $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure
        ) : $field;
    }

    $target .= join($delimiter, $output) . "\n";
}



// This function checks to see if the plague of all PHP
// coding gpc_magic_quotes is enabled.  If so, it cleanses
// Request params
function CheckMagicQuotes() {
  
  if ((bool)ini_get('magic_quotes_gpc')) {
    foreach($_REQUEST as $key => $val)
      $_REQUEST[$key] = stripslahes($val);
    foreach($_COOKIE as $key => $val)
      $_COOKIE[$key] = stripslashes($val);
  }

}


//////////////////////////////////////////////
// Process submitted recurrence definition form
//////////////////////////////////////////////
function SetRecurrence()
{

  $weekDays = 0;

  if (DEBUG & DEBUG_LIB)
    error_log("SetRecurrence(): Creating new recurrence object and building token...");

  $obj = new RecurrenceObject();
  if($_REQUEST['periodicity'] == "Daily") {
    
    if($_REQUEST['Day'] == "Weekday") {
      $obj->SetEveryWeekday();
      //echo "SetEveryWeekday();<br>\n";
      //$obj->DumpRecurrence();
    } else if($_REQUEST['Day'] == "Number") {
      $obj->SetDaily($_REQUEST['DApart']);
      //echo "SetDaily($DApart);<br>\n";
      //$obj->DumpRecurrence();
    }
  } else if($_REQUEST['periodicity'] == "Weekly") {

    if(isset($_REQUEST['Sunday']) && $_REQUEST['Sunday'] == 1) $weekDays |= 1;
    if(isset($_REQUEST['Monday']) && $_REQUEST['Monday'] == 1) $weekDays |= 1 << 1;
    if(isset($_REQUEST['Tuesday']) && $_REQUEST['Tuesday'] == 1) $weekDays |= 1 << 2;
    if(isset($_REQUEST['Wednesday']) && $_REQUEST['Wednesday'] == 1) $weekDays |= 1 << 3;
    if(isset($_REQUEST['Thursday']) && $_REQUEST['Thursday'] == 1) $weekDays |= 1 << 4;
    if(isset($_REQUEST['Friday']) && $_REQUEST['Friday'] == 1) $weekDays |= 1 << 5;
    if(isset($_REQUEST['Saturday']) && $_REQUEST['Saturday'] == 1) $weekDays |= 1 << 6;
    
    //$weekdays = array($Sunday, $Monday, $Tuesday, $Wednesday, $Thursday, $Friday, $Saturday);
    $obj->SetWeekly($_REQUEST['WApart'], $weekDays);
    //echo "SetWeekly($WApart, array($Sunday, $Monday, $Tuesday, $Wednesday, $Thursday, $Friday, $Saturday));<br>\n";
    if (DEBUG & DEBUG_RECURRENCE)
      $obj->DumpRecurrence();
  } else if($_REQUEST['periodicity'] == "Monthly") {

    if($_REQUEST['Month'] == 'Number') {
      $obj->SetMonthlyByDate($_REQUEST['MDays'], $_REQUEST['MApart1']);
      //echo "SetMonthlyByDate($Mdays, $MApart1);<br>\n";
      //$obj->DumpRecurrence();
    } else if($_REQUEST['Month'] == "MString") {
      //$weekdays = array(false, false, false, false, false, false, false);
      //$weekdays[$MDay-1]=true;
      $weekDays = 1 << ((int)($_REQUEST['MDay']) - 1);
      
      $obj->SetMonthlyByWeekday((int)($_REQUEST['MNumber'])-1, $weekDays, $_REQUEST['MApart2']);
      //echo "SetMonthlyByWeekday($MNumber-1, $MDay-1, $MApart2);<br>\n";
      $obj->DumpRecurrence();
    }
  }

  $obj->CreateVerboseTokenString();
  $obj->CreateTokenString();
  return $obj;
}




// Function: InitSession()
// Purpose:  Initialize all session variables not handled in the UserLogin function
// Params:
function InitSession($user) {

  $_SESSION['userId'] = $user->userId;
  ChangeOrg($user->orgId);

  $config = new Config($user->orgId);

  $_SESSION['superUser'] = FALSE;
  $_SESSION['currentSubTab'] = TAB_NONE;

  // Check for super user status.  Currently the super user
  // account is simply hard coded into the system as the first
  // record of the User table.
  if ($user->userId == 1) {
    $_SESSION['superUser'] = TRUE;
    $_SESSION['currentTab'] = TAB_ORG;
  } else
    $_SESSION['currentTab'] = TAB_ASSIGNED;

  $_SESSION['userName']           = $user->login;
  $_SESSION['contactId']          = $user->contactId;
  $_SESSION['languageId']         = $user->languageId;
  $_SESSION['departmentId']       = $user->departmentId;
  $_SESSION['divisionId']         = $user->divisionId;
  $_SESSION['statusId']           = $user->employmentStatusId;
  $_SESSION['stateId']            = $user->domainId;
  $_SESSION['trackId']            = 0; //$user->trackId;
  $_SESSION['role']               = $user->role;
  $_SESSION['trackAdminSummary']  = $config->GetValue(CONFIG_TRACK_ADMIN_SUMMARY_DEFAULT);
  $_SESSION['uiTheme']            = $config->GetValue(CONFIG_UI_THEME);

  /*
   // Putting users in multiple tracks invalidates this we need a better way of doing it.
  // If this is not a super user then display the news url associated with the track in which the user lives
  // Note that this overrides the setting above which displays organization news URL
  // but only when the trackUrl has been specified
  if (!$_SESSION['superUser'] && $user->trackId) {
    $track = new Track($user->orgId, $user->trackId);

    if ($track->trackNewsUrl)
      $_SESSION['newsUrl'] = $track->trackNewsUrl;
    //else
    //$_SESSION['newsUrl'] = URL_ROOT.'/organizations/'.$_SESSION['orgDirectory'].'/news/'.$_SESSION['Track_News_URL'];
  }
  */

  // Everything from this point forward need only be done for admins and possibly contacts
  if ($_SESSION['role'] == ROLE_END_USER) {
    $_SESSION['initSession'] = TRUE;
    return;
  }


  // Rows per page applied to all lists
  $_SESSION['rowsPerPage'] = DEFAULT_ROWS_PER_PAGE;

  // organization display session vars.  We initially display all columns
  $_SESSION['orgShowColumns'] = 0;
  for ($i = 0; $i < count(array_keys(unserialize(ORG_LIST_SQL_COLUMNS))); $i++)
    if (1 << $i != ORG_LIST_COLUMN_DB_NAME)
      $_SESSION['orgShowColumns'] += 1 << $i;
  $_SESSION['orgSortColumn'] = ORG_LIST_COLUMN_NAME;
  $_SESSION['orgSortDescending'] = FALSE;
  $_SESSION['orgPageNumber'] = 1;
  $_SESSION['currentDataEditFrame'] = ORG_DATA_EDIT_DEPARTMENTS;

  // User display session vars
  $_SESSION['userShowColumns'] = 0;
  for ($i = 0; $i < count(array_keys(unserialize(USER_LIST_SQL_COLUMNS))); $i++)
    $_SESSION['userShowColumns'] += 1 << $i;
  $_SESSION['userSortColumn'] = USER_LIST_COLUMN_LAST_NAME;
  $_SESSION['userSortDescending'] = FALSE;
  $_SESSION['userPageNumber'] = 1;

  // Participants display session vars
  $_SESSION['participantShowColumns'] = 0;
  for ($i = 0; $i < count(array_keys(unserialize(USER_LIST_SQL_COLUMNS))); $i++)
    $_SESSION['participantShowColumns'] += 1 << $i;
  $_SESSION['participantSortColumn'] = USER_LIST_COLUMN_LAST_NAME;
  $_SESSION['participantSortDescending'] = FALSE;
  $_SESSION['participantPageNumber'] = 1;
  $_SESSION['participantShowAll'] = FALSE;

  // Track display session vars
  $_SESSION['trackShowColumns'] = 0;
  for ($i = 0; $i < count(array_keys(unserialize(TRACK_LIST_SQL_COLUMNS))); $i++)
    $_SESSION['trackShowColumns'] += 1 << $i;
  $_SESSION['trackSortColumn'] = TRACK_LIST_COLUMN_NAME;
  $_SESSION['trackSortDescending'] = FALSE;
  $_SESSION['trackPageNumber'] = 1;
  // Setup trackId so that page reloads can keep track of which tab we were on
  $_SESSION['trackId'] = 0;
  
  // Faq display session vars
  $_SESSION['faqShowColumns'] = 0;
  for ($i = 0; $i < count(array_keys(unserialize(FAQ_LIST_SQL_COLUMNS))); $i++)
    $_SESSION['faqShowColumns'] += 1 << $i;
  $_SESSION['faqSortColumn'] = FAQ_LIST_COLUMN_ID;
  $_SESSION['faqSortDescending'] = FALSE;
  $_SESSION['faqPageNumber'] = 1;
  
  // Question display session vars
  $_SESSION['questionShowColumns'] = 0;
  for ($i = 0; $i < count(array_keys(unserialize(QUESTION_LIST_SQL_COLUMNS))); $i++)
    $_SESSION['questionShowColumns'] += 1 << $i;
  $_SESSION['questionSortColumn'] = QUESTION_LIST_COLUMN_NUMBER;
  $_SESSION['questionSortDescending'] = FALSE;
  $_SESSION['questionPageNumber'] = 1;

  // Question archive Display session vars
  $_SESSION['archiveShowColumns'] = 0;
  for ($i = 0; $i < count(array_keys(unserialize(QUESTION_LIST_SQL_COLUMNS))); $i++)
    $_SESSION['archiveShowColumns'] += 1 << $i;
  $_SESSION['archiveSortColumn'] = QUESTION_LIST_COLUMN_NUMBER;
  $_SESSION['archiveSortDescending'] = FALSE;
  $_SESSION['archivePageNumber'] = 1;

  // Track Assignment display session vars
  $_SESSION['assignmentShowColumns'] = 0;
  for ($i = 0; $i < count(array_keys(unserialize(QUEUE_LIST_LABELS))); $i++)
    $_SESSION['assignmentShowColumns'] += 1 << $i;
  $_SESSION['assignmentSortColumn'] = QUEUE_LIST_COLUMN_SEQUENCE;
  $_SESSION['assignmentSortDescending'] = FALSE;
  $_SESSION['assignmentPageNumber'] = 1;
  $_SESSION['assignmentShowAll'] = FALSE;

  // Track Assignment display session vars
  $_SESSION['summaryShowColumns'] = 0;
  for ($i = 0; $i < count(array_keys(unserialize(SUMMARY_LIST_LABELS))); $i++) {
    if (1 << $i == SUMMARY_LIST_COLUMN_USER_ID && $config->GetValue(CONFIG_SHOW_USER_THAT_ANSWERED) == 0)
      continue;
    $_SESSION['summaryShowColumns'] += 1 << $i;
  }
  $_SESSION['summarySortColumn'] = SUMMARY_LIST_COLUMN_COMPLETION_DATE;
  $_SESSION['summarySortDescending'] = TRUE;
  $_SESSION['summaryPageNumber'] = 1;
  $_SESSION['summaryShowAll'] = FALSE;

  // Track display session vars
  $_SESSION['progressShowColumns'] = 0;
  $progressListMask = ASSIGNMENT_COLUMN_USER_ID |
    ASSIGNMENT_COLUMN_QUESTION_NUMBER |
    ASSIGNMENT_COLUMN_DELIVERY_DATE |
    ASSIGNMENT_COLUMN_COMPLETION_DATE |
    ASSIGNMENT_COLUMN_EXIT_INFO;
  for ($i = 0; $i < count(array_keys(unserialize(ASSIGNMENT_SQL_COLUMNS))); $i++)
    if ((1 << $i) & $progressListMask)
      $_SESSION['progressShowColumns'] += 1 << $i;
  $_SESSION['progressSortColumn'] = ASSIGNMENT_COLUMN_COMPLETION_DATE;
  $_SESSION['progressSortDescending'] = TRUE;
  $_SESSION['progressPageNumber'] = 1;

  $_SESSION['initSession'] = TRUE;
  return;
}


function SetupMotifDisplay(&$smarty, &$config) {

  if ($config->GetValue(CONFIG_USE_DEFAULT_MOTIF)) {
    $theme = $config->GetValue(CONFIG_UI_THEME);
    if (empty($theme))
      $theme = 'Default';
    $prefix = URL_ROOT."/themes/$theme/presentation";
    $smarty->assign('topLeftBack', "$prefix/header_middle.gif");
    $smarty->assign('topLeftBackRepeat', 1);
    $smarty->assign('topLeftFront', "$prefix/wtp_logo.gif");
    $smarty->assign('topMiddleBack', "$prefix/header_middle.gif");
    $smarty->assign('topMiddleBackRepeat', 1);
    $smarty->assign('topMiddleFront', '');
    $smarty->assign('topRightBack', "$prefix/header_right.gif");
    $smarty->assign('topRightBackRepeat', 0);
    $smarty->assign('topRightFront', "$prefix/hrtools_logo.gif");
  } else {
    $prefix = URL_ROOT.'/organizations/'.$config->GetValue(CONFIG_ORG_DIRECTORY);
    if ($config->GetValue(CONFIG_TOP_LEFT_BACK))
      $smarty->assign('topLeftBack', $prefix.$config->GetValue(CONFIG_TOP_LEFT_BACK));
    else
      $smarty->assign('topLeftBack', '');
    $smarty->assign('topLeftBackRepeat', $config->GetValue(CONFIG_TOP_LEFT_BACK_REPEAT));
    if ($config->GetValue(CONFIG_TOP_LEFT_FRONT))
      $smarty->assign('topLeftFront', $prefix.$config->GetValue(CONFIG_TOP_LEFT_FRONT));
    else
      $smarty->assign('topLeftFront', '');
    if ($config->GetValue(CONFIG_TOP_MIDDLE_BACK))
      $smarty->assign('topMiddleBack', $prefix.$config->GetValue(CONFIG_TOP_MIDDLE_BACK));
    else
      $smarty->assign('topMiddleBack', '');
    $smarty->assign('topMiddleBackRepeat', $config->GetValue(CONFIG_TOP_MIDDLE_BACK_REPEAT));
    if ($config->GetValue(CONFIG_TOP_MIDDLE_FRONT))
      $smarty->assign('topMiddleFront', $prefix.$config->GetValue(CONFIG_TOP_MIDDLE_FRONT));
    else
      $smarty->assign('topMiddleFront', '');
    if ($config->GetValue(CONFIG_TOP_RIGHT_BACK))
      $smarty->assign('topRightBack', $prefix.$config->GetValue(CONFIG_TOP_RIGHT_BACK));
    else
      $smarty->assign('topRightBack', '');
    $smarty->assign('topRightBackRepeat', $config->GetValue(CONFIG_TOP_RIGHT_BACK_REPEAT));
    if ($config->GetValue(CONFIG_TOP_RIGHT_FRONT))
      $smarty->assign('topRightFront', $prefix.$config->GetValue(CONFIG_TOP_RIGHT_FRONT));
    else
      $smarty->assign('topRightFront', '');
  }

} // end function SetupMotifDisplay()


function CheckCustomizeMotif(&$config, &$fm, $requestParam, $configParam, &$refreshMotif, $repeatParam = FALSE) {

  if ($repeatParam) {
    if (isset($_REQUEST[$requestParam]) && $_REQUEST[$requestParam] &&
	$config->GetValue($configParam) == 0) {
      $config->SetValue($configParam, 1);
      $refreshMotif = TRUE;
    } elseif ((!isset($_REQUEST[$requestParam]) || !$_REQUEST[$requestParam]) &&
	      $config->GetValue($configParam) == 1) {
      $config->SetValue($configParam, 0);
      $refreshMotif = TRUE;
    }
  } else {
    if (isset($_REQUEST[$requestParam]) && $_REQUEST[$requestParam] &&
	$config->GetValue($configParam) != $_REQUEST['path']) {
      $config->SetValue($configParam, $fm->FilterPath($_REQUEST['path']));
      $refreshMotif = TRUE;
    } elseif ((!isset($_REQUEST[$requestParam]) || !$_REQUEST[$requestParam]) &&
	      $config->GetValue($configParam) == $_REQUEST['path']) {
      $config->SetValue($configParam, '');
      $refreshMotif = TRUE;
    }
  }
}


function AssignContactInfo(&$smarty, $orgContactUserId, $user, $assignmentTrackId=0) {

  $orgContactUser = new User($_SESSION['orgId'], $orgContactUserId);
  $trackContacts = array();
  $tmpTrackObj = NULL;

  if ($user->useSupervisor) {
    if ((int)$assignmentTrackId) {
      $tmpTrackObj = new Track($_SESSION['orgId'], (int)$assignmentTrackId);
      if (!$tmpTrackObj->isAb1825) {
	$myContactUser = new User($_SESSION['orgId'], $tmpTrackObj->supervisorId);
	$smarty->assign('myContactFullName', $myContactUser->GetFullName(FALSE));
	if ($myContactUser->email) {
	  $smarty->assign('myContactEmail', $myContactUser->email);
	  $smarty->assign('myContactEmailEncoded', urlencode($myContactUser->email));
	}
	$smarty->assign('myContactPhone', $myContactUser->phone);
	$smarty->assign('myContactFullNameEncoded', urlencode($myContactUser->firstName.' '.$myContactUser->lastName));
      }
    } else {
      $tracks = $user->GetParticipantTracks();
      foreach ($tracks as $trackId => $trackName) {
	$tmpTrackObj = new Track($_SESSION['orgId'], $trackId);
	$trackContacts[] = array($trackName, new User($_SESSION['orgId'], $tmpTrackObj->supervisorId));
      }
      $smarty->assign('trackContacts', $trackContacts);
    }
  } else {
    $myContactUser = new User($_SESSION['orgId'], $user->contactId);
    $smarty->assign('myContactFullName', $myContactUser->GetFullName(FALSE));
    if ($myContactUser->email) {
      $smarty->assign('myContactEmail', $myContactUser->email);
      $smarty->assign('myContactEmailEncoded', urlencode($myContactUser->email));
    }
    $smarty->assign('myContactPhone', $myContactUser->phone);
    $smarty->assign('myContactFullNameEncoded', urlencode($myContactUser->firstName.' '.$myContactUser->lastName));
  }
  
  if ($orgContactUser) {
    $smarty->assign('corpFullName', $orgContactUser->GetFullName(FALSE));
    if ($orgContactUser->email) {
      $smarty->assign('corpEmail', $orgContactUser->email);
      $smarty->assign('corpEmailEncoded', urlencode($orgContactUser->email));
    }
    $smarty->assign('corpPhone', $orgContactUser->phone);
    $smarty->assign('corpFullNameEncoded', urlencode($orgContactUser->firstName.' '.$orgContactUser->lastName));
  }

  if ((int)$assignmentTrackId) {
    if (!$tmpTrackObj)
      $tmpTrackObj = new Track($_SESSION['orgId'], (int)$assignmentTrackId);
    if ($tmpTrackObj->isAb1825) {
      $smarty->assign('myContactFullName', AB1825_CONTACT_FULL_NAME);
      $smarty->assign('myContactEmail', AB1825_CONTACT_EMAIL);
      $smarty->assign('myContactEmailEncoded', urlencode(AB1825_CONTACT_EMAIL));
      $smarty->assign('myContactPhone', AB1825_CONTACT_PHONE);
      $smarty->assign('myContactFullNameEncoded', URLENCODE(AB1825_CONTACT_FULL_NAME));
    }
  }

}


function LinkToFile($linkFromDir, $linkToDir, $linkToFile) {

  $cwd = getcwd();
  $msg = '';

  if (!chdir($linkFromDir)) {
    error_log("Changing dir to: $linkFromDir failed, likely does not exist or permission denied");
    $msg .= "The organization directory $linkFromDir probably does not exist, this must be fixed before proceeding<BR>\n";
    return RC_OPEN_FILE_FAILED;
  }

  if (!empty($linkToDir)) {
    if (!file_exists($linkToDir.'/'.escapeshellcmd($linkToFile))) {
      error_log("Lib|LinkToFile(linkFromDir=$linkFromDir,linkToDir=$linkToDir,linkToFile=$linkToFile): Media file($linkToDir/".
		escapeshellCmd($linkToFile).") does not exist when looking for single source dir, cwd=".getcwd());
      $msg .= "Media file specified does not exist in master location: $linkToDir/".
	escapeshellCmd($linkToFile)."<BR>\n";
      chdir($cwd);
      return RC_OPEN_FILE_FAILED;
    }
  }
  
  if (empty($linkFromDir)) {
    error_log("Lib|LinkToFile(linkFromDir=$linkFromDir,linkToDir=$linkToDir,linkToFile=$linkToFile), ".
	      "linkFromDir=$linkFromDir): Internal application error, media file directory must not be empty but it was");
    $msg .= "Internal application error, please report this error message to support.<BR>\n";
    return RC_MEDIA_UNSPECIFIED;
  } elseif (empty($linkToDir) && !file_exists($linkFromDir.'/'.escapeshellcmd($linkToFile))) {
    error_log("Lib|LinkToFile(linkFromDir=$linkFromDir,linkToDir=$linkToDir,linkToFile=$linkToFile): Media file($linkFromDir/".
	      escapeshellCmd($linkToFile).") does not exist in organization motif dir, cwd=".getcwd());
    $msg .= "Media file specified does not exist in organization motif folder: $linkFromDir/".
      escapeshellCmd($linkToFile)."<BR>\n";
    return RC_OPEN_FILE_FAILED;
  }
  chdir($cwd);
	  
  // NOTE ******************
  // This should be the very last test performed since it creates a symbolic link on success
  // and you do not want any stragglers hanging around if any other tests fail
  // ***************************************************************
  // If this is supposed to be a sym link from a single source of content
  // then create the symbolic link in the appropriate organization directory
  if (!empty($linkToDir)) {
    chdir($linkFromDir);
    if (!file_exists(escapeshellcmd($linkToFile))) {
      if (!symlink($linkToDir.'/'.escapeshellcmd($linkToFile), escapeshellcmd($linkToFile))) {
	error_log("Lib|LinkToFile(linkFromDir=$linkFromDir,linkToDir=$linkToDir,linkToFile=$linkToFile): ".
		  "Failed to create symbolic link from ($linkFromDir) to ($linkToDir/".
		  escapeshellcmd($linkToFile).')');;
	$msg .= "Failed to create symbolic link from ($linkFromDir) to ($linkToDir/".
	  escapeshellcmd($linkToFile).")<BR>\n";
	return RC_OPEN_FILE_FAILED;
      }
    }
    // cd back to original current working directory for the sake of consistency
    chdir($cwd);
  }

  return RC_OK;
}


function CheckFileUpload($formElementName) {

  switch($_FILES[$formElementName]['error']) {
  case UPLOAD_ERR_OK:
    return RC_OK;
    break;
  case UPLOAD_ERR_INI_SIZE:
    return RC_UPLOAD_EXCEEDED_MAX_INI_SIZE;
    break;
  case UPLOAD_ERR_FORM_SIZE:
    return RC_UPLOAD_EXCEEDED_MAX_FORM_SIZE;
    break;
  case UPLOAD_ERR_PARTIAL:
    return RC_UPLOAD_INCOMPLETE;
    break;
  case UPLOAD_ERR_NO_FILE:
    return RC_UPLOAD_NO_FILE;
    break;
  case UPLOAD_ERR_NO_TMP_DIR:
    return RC_UPLOAD_NO_TMP_DIR;
    break;
  case UPLOAD_ERR_CANT_WRITE:
    return RC_UPLOAD_WRITE_FAILED;
    break;
  case UPLOAD_ERR_EXTENSION:
    return RC_UPLOAD_EXTENSION_STOP;
    break;
  default:
    return RC_UPLOAD_ERR_UNKNOWN;;
    break;
  }
}


function RcToText($errorCode, $errAppend='') {
    
  $errMsg = '';
  
  switch($errorCode) {
  case RC_OK:
    $errMsg = 'OK';
    break;
  case RC_ORG_SQL_FILE_NOT_FOUND:
    $errMsg = 'Organization SQL Template File not found';
    break;
  case RC_CREATE_DB_FAILED:
    $errMsg = 'Failed to create new database for organization';
    break;
  case RC_ORG_DIRECTORY_EXISTS:
    $errMsg = 'The organization directory specified already exists';
    break;
  case RC_ORG_NAME_EXISTS:
    $errMsg = 'The organization name you have chosed is already in use';
    break;
  case RC_CREATE_ID_FAILED:
    $errMsg = 'Failed to obtain a new Unique ID for this organization';
    break;
  case RC_CREATE_DIRECTORY_FAILED:
    $errMsg = 'Failed to create new directory structure for this organization';
    break;
  case RC_UPDATE_DB_NAME_FAILED:
    $errMsg = 'Failed to update database name for new organization';
    break;
  case RC_MYSQL_CLI_NOT_FOUND:
    $errMsg = 'Failed to locate mysql client binary, could not import db structure';
    break;
  case RC_MYSQL_SCRIPT_FAILED:
    $errMsg = 'Failed executing '.ORG_SQL_FILE.' ($this->rc): $this->mysqlOutput'; 
    break;
  case RC_ORG_INIT_DATA_FAILED:
    $errMsg = 'Failed during insert of organization initialization data';
    break;
  case RC_DELETE_ORG_FAILED:
    $errMsg = 'Unable to complete all operations in removing directory:';
    break;
  case RC_SELECT_DB_FAILED:
    $errMsg = 'Select db in create organization failed';
    break;
  case RC_UPDATE_INFO_FAILED:
    $errMsg = 'Failed to update organization information in both places';
    break;
  case RC_INVALID_DATA_FORMAT:
    $errMsg = 'Data in invalid format, could not proceed';
    break;
  case RC_AUTH_DB_CONNECT_FAILED:
    $errMsg = 'Failed to connect to the database, please contact support personnel';
    break;
  case RC_ORG_DB_CONNECT_FAILED:
    $errMsg = 'Failed to connect to the database for your organization, please contact support personnel';
    break;
  case RC_DUPLICATE_LOGIN:
    $errMsg = 'The login specified is a duplicate of one already in the system.  Please try again after changing this users login.';
    break;
  case RC_INSERT_FAILED:
    $errMsg = 'The system attempted to create a new record and this operation has failed.';
    break;
  case RC_DELETE_FAILED:
    $errMsg = 'The system attempted to delete a record from the database and this operation has failed.';
    break;
  case RC_PASSWORD_REQUIRED:
    $errMsg = 'You must enter a password to login to this system.';
    break;
  case RC_QUERY_FAILED:
    $errMsg = 'The database is currently unavailable, please try again later.';
    break;
  case RC_USER_UNKNOWN:
    $errMsg = 'The specified user is unknown making the credentials supplied invalid.  Please check your Username and Password.';
    break;
  case RC_INCORRECT_PASSWORD:
    $errMsg = 'Login failed, supplied credentials are invalid.  Please check your username and password!';
    break;
  case RC_LOGIN_FAILED:
    $errMsg = 'Login failed, supplied credentials are invalid.  Please check your username and password.';
    break;
  case RC_INVALID_ARG:
    $errMsg = 'Invalid argument passed to function.';
    break;
  case RC_NO_MORE_NUMBERS:
    $errMsg = 'The function was unable to identify any unused identifiers within the range specified.';
    break;
  case RC_UPDATE_FAILED:
    $errMsg = 'Database update failed.';
    break;
  case RC_INVALID_SEQUENCE:
    $errMsg = 'Negative sequences, sequences equal to 0, or greater than the last sequence in the track are invalid.';
    break;
  case RC_INVALID_GROUP_ID:
    $errMsg = 'Valid group values are only \'New\', \'Unassign\', or an existing group number.';
    break;
  case RC_OPEN_FILE_FAILED:
    $errMsg = 'Open file failed.';
    break;
  case RC_CSV_READ_FAILED:
    $errMsg = 'Failure while reading CSV formatted file input.';
    break;
  case RC_CSV_INVALID_QUESTION_NUMBER:
    $errMsg = 'Invalid Question Number on CSV question import.';
    break;
  case RC_CSV_INVALID_DATA_FORMAT:
    $errMsg = 'Minimum required set of columns in import file is missing.';
    break;
  case RC_ACCOUNT_INACTIVE:
    $errMsg = 'Your account has been terminated, contact your administrator.';
    break;
  case RC_OBJECT_INSTANCE_REQUIRED:
    $errMsg = 'An instance of this object is required in order perform requested operation.';
    break;
  case RC_ADD_QUESTION_FAILED:
    $errMsg = 'Unable to migrate question from authoritative DB('.AUTHORITATIVE_QUESTION_DB.')';
    break;
  case RC_ORG_DIRECTORY_EMPTY:
    $errMsg = 'The specified org directory value is empty, unable to proceed. This is likely a bug and should be reported.';
    break;
  case RC_FILE_NOT_FOUND:
    $errMsg = 'The specified file does not exist.  Please either upload a file or double check the path.';
    break;
  case RC_MEDIA_UNSPECIFIED:
    $errMsg = 'A media file was not provided to an internal function.  This has been logged and is a critical internal app failure.';
    break;
  case RC_PARENT_QUESTION_NOEXIST:
    $errMsg = 'At least one parent question referenced does not exist in the system.  Please double check the data.';
    break;
  case RC_CLASS_CREATE_NO_AB1825_CONTENT:
    $errMsg = 'The AB1825 track could not be created because no AB1825 questions (50000-54999) exist.';
    break;
  case RC_COPY_EMAIL_TEMPLATES_FAILED:
    $errMsg = 'There was a problem copying the email templates into the organization directory.  Please try copying default templates from the file manager.';
    break;
  case RC_INVALID_LOGIN_ORG:
    $errMsg = 'Please select a valid organization to login with or contact your site administrator for assistance if unable to select an organization.';
    break;
  case RC_ORG_DIR_WHITESPACE:
    $errMsg = 'The organization directory cannot contain any white space characters in it.';
    break;
  case RC_UPLOAD_EXCEEDED_MAX_INI_SIZE:
    $errMsg = 'The uploaded file exceeds the maximum file size allowed.';
    break;
  case RC_UPLOAD_EXCEEDED_MAX_FORM_SIZE:
    $errMsg = 'The uploaded file exceeds the maximum file size allowed on form.';
    break;
  case RC_UPLOAD_INCOMPLETE:
    $errMsg = 'The upload was interrupted and only partially transferred, please try again.';
    break;
  case RC_UPLOAD_NO_FILE:
    $errMsg = 'No file was specified, please select a file to upload and try again.';
    break;
  case RC_UPLOAD_NO_TMP_DIR:
    $errMsg = 'The temporary directory configured for file uploads does not exist.';
    break;
  case RC_UPLOAD_WRITE_FAILED:
    $errMsg = 'The system was unable to write the uploaded file to the temporary directory.';
    break;
  case RC_UPLOAD_EXTENSION_STOP:
    $errMsg = 'One of the extensions installed prevented the file from being uploaded.';
    break;
  case RC_UPLOAD_ERR_UNKNOWN:
    $errMsg = 'An unknown error occurred while attempting to upload file.';
    break;
  default:
    $errMsg = "Unknown return code: $errorCode";
    break;
  }
  
  return $errMsg.$errAppend;
}

?>
