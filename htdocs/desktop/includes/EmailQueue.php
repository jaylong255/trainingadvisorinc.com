<?php

require_once('DatabaseObject.php');
require_once('Config.php');
require_once('RecurrenceObject.php');
require_once('Organization.php');
require_once('agent/agent.php');

class EmailQueue extends DatabaseObject {

  var $emailQueueListSqlColumns = array();
  var $emailQueueListLabelColumns = array();
  //var $trackSqlColumns = array();
  var $emailQueueSqlColumns = array();
  var $emailQueueId = 0;
  var $dbName = '';
  var $orgId = 0;
  var $trackId = 0;
  var $userId = 0; // The user who submitted the request
  var $fromAddress = '';
  var $subject = '';
  var $body = '';
  var $submittedDate = '';
  var $scheduledDate = '';
  var $progressDate = '';
  var $processedDate = '';
  var $status = '';
  var $recipientCnt = 0;
  var $deliveredCnt = 0;
  var $lastRecipient = '';
  var $rc = 0;
  //  var $myqslOutput = '';
  //  var $assignmentTrackId = 0;

  function EmailQueue($orgId, $newEmailQueueId = 0) {

    $this->emailQueueSqlColumns = unserialize(EMAIL_QUEUE_SQL_COLUMNS);
    $this->emailQueueLabels = unserialize(EMAIL_QUEUE_LABELS);
    //$this->trackSqlColumns = unserialize(TRACK_SQL_COLUMNS);
    //$this->emailQueueSqlColumns = unserialize(EMAILQUEUE_SQL_COLUMNS);
    $this->orgId = (int)$orgId;

    $this->DatabaseObject(ORG_DB_NAME_PREFIX.$this->orgId);
    
    if (!$this->initialized)
      return FALSE;

    if (is_numeric($newEmailQueueId) && $newEmailQueueId)
      $this->SetEmailQueueId($newEmailQueueId);
  }


  function SetEmailQueueId($newEmailQueueId) {
    
    //$values = array();
    if ($this->emailQueueId != $newEmailQueueId && $newEmailQueueId > 0) {
      $this->emailQueueId = $newEmailQueueId;
      // Get DB name from HR_Tools_Track db or table
      $oldDbName = $this->dbName;
      if (!$this->Open(AUTH_DB_NAME, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST))
	return RC_AUTH_DB_CONNECT_FAILED;
      $this->Select('*', EMAIL_QUEUE_TABLE);
      $this->Where($this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_ID].'='.$newEmailQueueId);
      $values = $this->Query(SINGLE_ROW, KEYS_FORMAT);
      $this->trackId = $values[$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_TRACK_ID]];
      $this->userId = $values[$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_USER_ID]];
      $this->fromAddress = $values[$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_FROM_ADDRESS]];
      $this->subject = $values[$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_SUBJECT]];
      $this->body = $values[$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_BODY]];
      $this->submittedDate = $values[$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_SUBMITTED_DATE]];
      $this->scheduledDate = $values[$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_SCHEDULED_DATE]];
      $this->progressDate = $values[$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_PROGRESS_DATE]];
      $this->processedDate = $values[$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_PROCESSED_DATE]];
      $this->status = $values[$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_STATUS]];
      $this->recipientCnt = $values[$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_RECIPIENT_COUNT]];
      $this->deliveredCnt = $values[$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_DELIVERED_COUNT]];
      $this->lastRecipient = $values[$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_LAST_RECIPIENT]];
      if (!$this->Open($oldDbName))
	return RC_ORG_DB_CONNECT_FAILED;
    }

    return RC_OK;
  }


  static public function GetQueueList($orgId, $trackId) {
    $orgId = (int)$orgId;

    if (!$orgId) {
      error_log("EmailQueue::GetQueueList(orgId=$orgId, trackId=$trackId): Invalid organization ID");
      return FALSE;
    }

    $emailQueueSqlColumns = unserialize(EMAIL_QUEUE_SQL_COLUMNS);
    $db = new DatabaseObject(AUTH_DB_NAME, ORG_ADMIN_USER, ORG_ADMIN_PASS);
    if (!($db)) {
      error_log("EmailQueue::GetQueueList(orgId=$orgId,trackId=$trackId): Failed to open database ".AUTH_DB_NAME);
      return FALSE;
    }
    $db->Select(array($emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_ID],
		      $emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_FROM_ADDRESS],
		      $emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_SUBJECT],
		      'DATE('.$emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_SUBMITTED_DATE].')',
		      'DATE('.$emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_PROCESSED_DATE].')'),
		EMAIL_QUEUE_TABLE);
    $db->Where($emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_ORGANIZATION_ID]."=$orgId");
    $db->Where($emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_TRACK_ID]."=$trackId");
    $db->Where($emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_STATUS]."!='".EMAIL_QUEUE_STATUS_SENT."'");
    $db->Where('('.$emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_SCHEDULED_DATE].'=0 OR NOW() > '.
	       $emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_SCHEDULED_DATE].')');

    if (($rows = $db->Query(NAME_VALUES, KEYS_FORMAT)) === FALSE) {
      error_log("EmailQueue::GetQueueList(orgId=$orgId,trackId=$trackId): Failed to get queue list");
      return FALSE;
    }

    return $rows;
  }


  function GetDatePart($dateAndTime) {
    $dateParts = explode(' ', $dateAndTime);
    return $dateParts[0];
  }


  function DeleteEmailQueue() {
    
    // Validate the queue id
    if (!is_numeric($this->emailQueueId) || $this->emailQueueId == 0) {
      error_log("EmailQueue.UpdateEmailQueue(): Invalid non-numeric queueId '$this->emailQueueId'");
      return FALSE;
    }
    
    // add to the email queue
    $oldDbName = $this->dbName;
    if (!$this->Open(AUTH_DB_NAME, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST))
      return RC_AUTH_DB_CONNECT_FAILED;
    
    $this->Where($this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_ID].'='.$this->emailQueueId);
    $this->Delete(EMAIL_QUEUE_TABLE);
    error_log("########################################");
    if (!$this->Open($oldDbName))
      return RC_ORG_DB_CONNECT_FAILED;

  }


  function SetRecipientCount($cnt) {
    
    $oldDbName = '';

    // Validate the queue id
    if (!is_numeric($cnt)) {
      error_log("EmailQueue.SetRecipientCount(): Invalid non-numeric recipient count $cnt");
      return FALSE;
    }

    // add to the email queue
    if ($this->dbName != AUTH_DB_NAME) {
      $oldDbName = $this->dbName;
      if (!$this->Open(AUTH_DB_NAME, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST))
	return RC_AUTH_DB_CONNECT_FAILED;
    }
    $this->Where($this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_ID].'='.$this->emailQueueId);
    $rc = $this->Update(array($this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_RECIPIENT_COUNT] => $cnt,
			      $this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_STATUS] => EMAIL_QUEUE_STATUS_PROCESSING),
			EMAIL_QUEUE_TABLE);    
    if (!$rc) {
      $this->cacheError = $this->GetErrorMessage();
      return RC_UPDATE_INFO_FAILED;
    }
    $this->recipientCnt = $cnt;

    if (!empty($oldDbName) && !$this->Open($oldDbName))
      return RC_ORG_DB_CONNECT_FAILED;

  }


  function UpdateLastRecipient($recipAddress) {
    
    $oldDbName = '';

    // add to the email queue
    if ($this->dbName != AUTH_DB_NAME) {
      $oldDbName = $this->dbName;
      if (!$this->Open(AUTH_DB_NAME, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST))
	return RC_AUTH_DB_CONNECT_FAILED;
    }

    $dateTime = date('Y-m-d H:i:s');

    $this->deliveredCount++;
    $this->Where($this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_ID].'='.$this->emailQueueId);
    $rc = $this->Update(array($this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_DELIVERED_COUNT] => $this->deliveredCnt+1,
			      $this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_LAST_RECIPIENT] => $recipAddress,
			      $this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_PROGRESS_DATE] => $dateTime),
			EMAIL_QUEUE_TABLE);
    if (!$rc) {
      $this->cacheError = $this->GetErrorMessage();
      return RC_UPDATE_INFO_FAILED;
    }
    $this->deliveredCnt++;

    if (!empty($oldDbName) && !$this->Open($oldDbName))
      return RC_ORG_DB_CONNECT_FAILED;

  }


  function DeliveriesCompleted() {
    
    $oldDbName = '';

    // add to the email queue
    if ($this->dbName != AUTH_DB_NAME) {
      $oldDbName = $this->dbName;
      if (!$this->Open(AUTH_DB_NAME, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST))
	return RC_AUTH_DB_CONNECT_FAILED;
    }

    $dateTime = date('Y-m-d H:i:s');

    $this->deliveredCount++;
    $this->Where($this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_ID].'='.$this->emailQueueId);
    $rc = $this->Update(array($this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_PROCESSED_DATE] => $dateTime,
			      $this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_LAST_RECIPIENT] => '',
			      $this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_STATUS] => EMAIL_QUEUE_STATUS_SENT),
			EMAIL_QUEUE_TABLE);
    
    if (!$rc) {
      $this->cacheError = $this->GetErrorMessage();
      return RC_UPDATE_INFO_FAILED;
    }
    $this->deliveredCnt++;

    if (!empty($oldDbName) && !$this->Open($oldDbName))
      return RC_ORG_DB_CONNECT_FAILED;

  }


  function UpdateEmailQueue() {

    $oldDbName = '';

    // Validate the queue id
    if (!is_numeric($this->emailQueueId)) {
      error_log("EmailQueue.UpdateEmailQueue(): Invalid non-numeric queueId '$this->emailQueueId'");
      return FALSE;
    }

    // add to the email queue
    if ($this->dbName != AUTH_DB_NAME) {
      $oldDbName = $this->dbName;
      if (!$this->Open(AUTH_DB_NAME, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST))
	return RC_AUTH_DB_CONNECT_FAILED;
    }
    
    // This is also used to create new queue messages.  Anytime the queueId is zero
    // a new record will be inserted
    if ($this->emailQueueId == 0) {

      $this->emailQueueId = $this->Insert(array($this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_ID] => $this->emailQueueId,
						$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_ORGANIZATION_ID] => $this->orgId,
						$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_TRACK_ID] => $this->trackId,
						$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_USER_ID] => $this->userId,
						$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_FROM_ADDRESS] => $this->fromAddress,
						$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_SUBJECT] => $this->subject,
						$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_BODY] => $this->body,
						$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_SUBMITTED_DATE] => date('Y-m-d'),
						$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_SCHEDULED_DATE] => $this->scheduledDate,
						$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_PROGRESS_DATE] => 0,
						$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_PROCESSED_DATE] => 0,
						$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_STATUS] => EMAIL_QUEUE_STATUS_PENDING,
						$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_RECIPIENT_COUNT] => 0,
						$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_DELIVERED_COUNT] => 0),
					  EMAIL_QUEUE_TABLE);
      if (!$this->emailQueueId) {
	$this->emailQueueId = 0;
	return RC_INSERT_FAILED;
      }
      
    } else {

      $this->Where($this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_ID].'='.$this->emailQueueId);
      $rc = $this->Update(array($this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_ORGANIZATION_ID] => $this->orgId,
				$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_TRACK_ID] => $this->trackId,
				$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_USER_ID] => $this->userId,
				$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_FROM_ADDRESS] => $this->fromAddress,
				$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_SUBJECT] => $this->subject,
				$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_BODY] => $this->body,
				$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_SUBMITTED_DATE] => $this->submittedDate,
				$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_SCHEDULED_DATE] => $this->scheduledDate,
				$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_PROGRESS_DATE] => $this->progressDate,
				$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_PROCESSED_DATE] => $this->processedDate,
				$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_STATUS] => EMAIL_QUEUE_STATUS_PENDING,
				$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_RECIPIENT_COUNT] => $this->recipientCnt,
				$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_DELIVERED_COUNT] => $this->deliveredCnt,
				$this->emailQueueSqlColumns[EMAIL_QUEUE_COLUMN_LAST_RECIPIENT] => $this->lastRecipient),
			  EMAIL_QUEUE_TABLE);
      if (!$rc) {
	$this->cacheError = $this->GetErrorMessage();
	return RC_UPDATE_INFO_FAILED;
      }

    }

    if (!empty($oldDbName) && !$this->Open($oldDbName))
      return RC_ORG_DB_CONNECT_FAILED;

    return RC_OK;
  }


  function Dump() {
    echo "\n\temailQueueId=\t".$this->emailQueueId."\n";
    echo "\tdbName=\t".$this->dbName."\n";
    echo "\torgId=\t".$this->orgId."\n";
    echo "\ttrackId=\t".$this->trackId."\n";
    echo "\tuserId=\t".$this->userId."\n";
    echo "\tfromAddress=\t".$this->fromAddress."\n";
    echo "\tsubject=\t".$this->subject."\n";
    echo "\tbody=\t".$this->body."\n";
    echo "\tsubmittedDate=\t".$this->submittedDate."\n";
    echo "\tscheduledDate=\t".$this->scheduledDate."\n";
    echo "\tprogressDate=\t".$this->progressDate."\n";
    echo "\tprocessedDate=\t".$this->processedDate."\n";
    echo "\tstatus=\t".$this->status."\n";
    echo "\trecipientCnt=\t".$this->recipientCnt."\n";
    echo "\tdeliveredCnt=\t".$this->deliveredCnt."\n";
    echo "\tlastRecipient=\t".$this->lastRecipient."\n\n";
  }

}
