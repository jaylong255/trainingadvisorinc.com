<?php

require_once('Report.php');


class ReportAb1825CompletionByTrack extends Report {

  var $allTracks = array();
  var $reportData = array();
  var $selectColumns = array();
  var $onlyIncomplete = FALSE;
  var $userSqlColumns = array();


  // Returns an array structured like the following:
  // (<track_id> => (
  //                 (<track_name>, <user name>, <assigned>, <completed>, <met>, <remaining time>),
  //                 (...
  //
  function ReportAb1825CompletionByTrack($orgId, $trackIds, $lOnlyIncomplete=FALSE) {

    $this->Report($orgId);
    $allTracks = $this->GetDistinctAb1825Tracks();
    $selectColumns = array();
    $idx = 0;
    $this->onlyIncomplete = $lOnlyIncomplete;
    $this->userSqlColumns = unserialize(USER_SQL_COLUMNS);

    foreach($trackIds as $trackId) {
      $idx = array_push($this->reportData, array(0 => array($trackId, $allTracks[$trackId])));

      $this->Select('DISTINCT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID].')', ASSIGNMENT_TABLE);
      if ($this->onlyIncomplete)
	$this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETED_SECONDS].'<7200');
      $this->ReportConstraints($trackId, 0);
      $this->SortBy($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID]);
      //      $this->LeftJoinUsing(QUESTION_TABLE, $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER]);
      //      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DELIVERY_DATE].">='".MIGRATION_DATE);
      $results = $this->Query(MANY_VALUES, VALUES_FORMAT);
      foreach($results as $userId) {
	$this->Select(array($this->userSqlColumns[USER_COLUMN_FIRST_NAME],
			    $this->userSqlColumns[USER_COLUMN_LAST_NAME]),
		      USER_TABLE);
	$this->Where($this->userSqlColumns[USER_COLUMN_ID].'='.$userId);
	$nameParts = $this->Query(MANY_VALUES, VALUES_FORMAT);
	$this->reportData[$idx-1][] = array((int)$userId, $nameParts[0].' '.$nameParts[1],
					    $this->GetAssigned($trackId, $userId),
					    $this->GetCompleted($trackId, $userId),
					    $this->GetUnderstood($trackId, $userId),
					    $this->GetTimeCompleted($trackId, $userId));
      }
    }

    return $this->reportData;
  }


  function GetDistinctAb1825Tracks() {

    $trackColumns = unserialize(TRACK_SQL_COLUMNS);

    $this->Select(array($trackColumns[TRACK_COLUMN_ID], $trackColumns[TRACK_COLUMN_NAME]),
			    TRACK_TABLE);
    $this->Where($trackColumns[TRACK_COLUMN_IS_AB1825]);
    return $this->Query(NAME_VALUES, BOTH_FORMAT);
  }


  function GetAssigned($trackId, $userId) {

    $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'=0');
    $this->ReportConstraints($trackId, $userId);
    return $this->Query(SINGLE_VALUE, VALUES_FORMAT);

  }


  function GetCompleted($trackId, $userId) {

    $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].'!=0');
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'=0');
    $this->ReportConstraints($trackId, $userId);
    return $this->Query(SINGLE_VALUE, VALUES_FORMAT);

  }

  
  function GetUnderstood($trackId, $userId) {

    $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->ReportConstraints($trackId, $userId);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO].'='.EXIT_INFO_UNDERSTOOD);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'=0');
    return $this->Query(SINGLE_VALUE, VALUES_FORMAT);

  }


  function GetTimeCompleted($trackId, $userId) {

    $this->Select('SUM('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETED_SECONDS].')', ASSIGNMENT_TABLE);
    $this->ReportConstraints($trackId, $userId);
    return $this->Query(SINGLE_VALUE, VALUES_FORMAT);

  }


  function ReportConstraints($trackId, $userId) {

    if (!$trackId) {
      error_log("ReportAb1825CompletionByTrack::ReportConstraints(trackId=$trackId,userId=$userId): ".
		"Invalid track ID passed to function, track ID must be specified and non-zero");
      return FALSE;
    }

    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID].'='.$trackId);
    if ($userId)
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID].'='.$userId);
  }


  function GetReportData() {
    return $this->reportData;
  }


  function CreatePdf($results) {
    // new PDF document with landscape orientation, inche units
    // format is last argument and hopefully letter is the default
    $html = '';
    $resultFormatNumbers = TRUE;
    $resultFormatPercentages = FALSE;

    $pdf = new tcpdf('L');
    $pdf->SetCreator('Training Advisor');
    $pdf->SetAuthor('Training Advisor System');
    $pdf->SetSubject('Question Accuracy By Class or Date Range');
    $pdf->SetTitle('Training Advisor Report');
    $pdf->SetKeywords('report, question, accuracy, date range');
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetHeaderData(PDF_HEADER_LOGO, 50, 'Training Advisor Report:', 'Question Accuracy By Class or Date Range');
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetMargins(PDF_MARGIN_LEFT, 23, PDF_MARGIN_RIGHT);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    //    $pdf->UseTableHeader();
    //$pdf->UseCSS();
    //$pdf->SetFont('Arial','',12);

    foreach ($results as $trackData) {
      $html = '<span class="OrgInfoText">Report for Class: '.$trackData[0][1].'</span><BR>
  <table border="0" align="center" bgcolor="#FFFFFF">
    <tr bgcolor="#DDDDDD">
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 0.6em; font-weight: bold;" align="left">User ID</td>
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 1.5em; font-weight: bold;" align="left">Full Name</td>
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 0.9em; font-weight: bold;">Assigned</td>
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 0.9em; font-weight: bold;">Completed</td>
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 0.9em; font-weight: bold;">Understood</td>
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 1.1em; font-weight: bold;">Time Completed</td>
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 1.1em; font-weight: bold;">Time Remaining</td>
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 1.1em; font-weight: bold;">Met Requirement</td>
    </tr>';
      for($i = 1; $i < count($trackData); $i++) {
	$compHrs = (int)($trackData[$i][5]/60/60);
	$compMts = (int)(($trackData[$i][5]/60)%60);
	$remHrs = (int)((7200-$trackData[$i][5])/60/60);
	$remMts = (int)(((7200-$trackData[$i][5])/60)%60);
	$html .= '<tr bgcolor="#FFFFFF">
      <td style="font-size: 10pt; width: 0.6em;" align="left">'.$trackData[$i][0].'</td>
      <td style="font-size: 10pt; width: 1.5em;" align="left">'.$trackData[$i][1].'</td>
      <td style="font-size: 10pt; width: 0.9em;" align="center">&nbsp;'.$trackData[$i][2].'</td>
      <td style="font-size: 10pt; width: 0.9em;" align="center">'.$trackData[$i][3].'</td>
      <td style="font-size: 10pt; width: 0.9em;" align="center">'.$trackData[$i][4].'</td>
      <td style="font-size: 10pt; width: 1.1em;" align="center">';
	if ($resultFormatNumbers)
	  $html .= $compHrs.' Hr(s), '.$compMts.' Mt(s)';
	if ($resultFormatPercentages) {
	  if ($resultFormatNumbers)
	    $html .= ' (';
	  $html .= sprintf("%.1f", $trackData[$i][5] / $trackData[$i][3] * 100.0).'%';
	  if ($resultFormatNumbers)
	    $html .= ')';
	}
	$html .= '      </td>
      <td style="font-size: 10pt; width: 1.1em" align="center">';
	if ($resultFormatNumbers)
	  $html .= $remHrs.' Hr(s), '.$remMts.' Mt(s)';
	if ($resultFormatPercentages) {
	  if ($resultFormatNumbers)
	    $html .= ' (';
	  $html .= sprintf("%.1f", $trackData[$i][5] / $trackData[$i][5] * 100.0).'%';
	  if ($resultFormatNumbers)
	    $html .= ')';
	}
	$html .= '      </td>
      <td style="font-size: 10pt; width: 1.1em" align="center">';
	if ($trackData[$i][5] < 7200)
	  $html .= 'No';
	else
	  $html .= 'Yes';
	$html .= '      </td>
    </tr>';
      }
      $html .= ' </table>
';
      $pdf->AddPage('L');
      $pdf->WriteHTML($html);
    }

    $pdf->Output('Class_Accuracy_Report.pdf', 'D');
  }


  function CreateCsv($results) {

    if (empty($results)) {
      error_log("Report::CreateCsv(): empty results data passed to CSV function");
      return;
    }

    $outputArr = array();
    //$fp = fopen('php://output', 'w');
    $csv = new CustomCsv('php://output', 'w');
    $resultFormatNumbers = TRUE;
    $resultFormatPercentages = FALSE;

    // Output headers
    $csv->WriteHeaders('Class_AB1825_Completion.csv');

    // Output CSV header information:
    $outputArr = array('Class', 'User ID', 'Full Name', 'Assigned', 'Completed',
		       'Understood', 'Time Completed', 'Time Remaining', 'Met Requirement');
    $csv->WriteCsv($outputArr);

    foreach ($results as $trackData) {
      for($i = 1; $i < count($trackData); $i++) {
	$compHrs = (int)($trackData[$i][5]/60/60);
	$compMts = (int)(($trackData[$i][5]/60)%60);
	$remHrs = (int)((7200-$trackData[$i][5])/60/60);
	$remMts = (int)(((7200-$trackData[$i][5])/60)%60);
	$outputArr = array($trackData[0][1], $trackData[$i][0], $trackData[$i][1], $trackData[$i][2], $trackData[$i][3]);
	$outputArr[] = $compHrs.' Hr(s), '.$compMts.' Mt(s)';
	$outputArr[] = $remHrs.' Hr(s), '.$remMts.' Mt(s)';
	if ($trackData[$i][5] < 7200)
	  $outputArr[] = 'No';
	else
	  $outputArr[] = 'Yes';
	$csv->WriteCsv($outputArr);
      }
    }

    $csv->CloseCsv();

    return;
  }


}