<?php


define ('MANY_ROWS', 1);
define ('SINGLE_ROW', 2);
define ('MANY_VALUES', 3);
define ('SINGLE_VALUE', 4);
define ('NAME_VALUES', 5);

define ('KEYS_FORMAT', MYSQL_ASSOC);
define ('VALUES_FORMAT', MYSQL_NUM);
define ('BOTH_FORMAT', MYSQL_BOTH);

define ('RC_OPEN_DB_FAILED', 100);


class DatabaseObject {

  var $firstRow = 0;
  var $lastRow = 0;
  var $numRows = -1;
  //  var $currentPage = 0;
  //  var $numPages = 0;
  //  var $rowsPerPage = 0;

  var $dbName = '';
  var $columns = '';
  var $tables = '';
  var $distinct = FALSE;
  var $searchColumn = '';
  var $searchStateOperator = '';
  var $searchData = '';
  var $group = '';
  var $orderColumns = '';
  var $orderDescending = FALSE;
  var $order = '';
  var $join = '';
  var $where = '';
  var $limit = '';
  var $selectColumns = array();
  var $dbLink = NULL;
  var $initialized = TRUE;
  var $sql = '';
  var $stage1Sql = '';
  var $stage1SqlNoLimit = '';
  var $stage1Op = '';
  var $stage1OrderColumns = '';
  var $stage1SortBy = '';
  var $stage1Limit = '';
  var $stage1LeftJoinUsing = array();
  var $orderedJoinUsing = array();
  var $leftJoinUsing = array();
  var $rightJoinUsing = array();
  var $innerJoinUsing = array();
  var $cacheError = '';


  function DatabaseObject($dbName = '', $dbUser = DB_USER, $dbPass = DB_PASS, $dbHost = DB_HOST) {

    $this->dbLink = mysql_connect($dbHost, $dbUser, $dbPass);
    if (!$this->dbLink) {
      error_log("**** DBCONNECT FAILED for host=$dbHost, db=$dbName, user=$dbUser, pass=$dbPass ****: ".mysql_error());
      //   or die('Unable to connect to mysql.'.mysql_error()."<BR>\n");
      return FALSE;
    }
    
    if ($dbName) {
      if (!mysql_select_db($dbName, $this->dbLink)) {
	error_log($this->GetErrorMessage());
	return FALSE;
      }
      $this->dbName = $dbName;
    }

    if (!$this->dbLink) {
      $this->initialized = FALSE;
      return FALSE;
    }
    
    return TRUE;
  }


  function Open($dbName = '', $dbUser = DB_USER, $dbPass = DB_PASS, $dbHost = DB_HOST) {

    if (DEBUG & DEBUG_SQL)
      error_log("DatabaseObject.Open(DBName = $dbName, DBUser = $dbUser, DBPass = $dbPass, DBHost = $dbHost);");

    if ($this->dbLink)
      $this->Close();

    $this->dbLink = mysql_connect($dbHost, $dbUser, $dbPass)
      or die('Unable to connect to mysql.'.mysql_error()."<BR>\n");
    
    if ($dbName) {
      $this->dbName = $dbName;

      if (!mysql_select_db($this->dbName, $this->dbLink)) {
	error_log("DatabaseObject.Open($dbName,$dbUser,$dbPass,$dbHost): ".$this->GetErrorMessage());
	$this->initialized = FALSE;
	return FALSE;
      }

      return $this->dbLink;
    }

    return FALSE;
  }


  function SetDb($dbName) {

    if (!$this->dbLink)
      return FALSE;

    $res = mysql_select_db($dbName, $this->dbLink);
    if (!$res)
      return FALSE;

    $this->dbName = $dbName;
    return TRUE;
  }


  function Select($columnNames, $tableNames, $distinct = FALSE) {

    $this->ReInit();

    if (gettype($columnNames) == 'array') {
      $this->columns = implode(", ", array_map('esql', $columnNames));
    } else {
      $this->columns = $columnNames;
    }

    if (gettype($tableNames) == 'array') {
      $this->tables = implode(", ", array_map('esql', $tableNames));
    } else {
      $this->tables = $tableNames;
    }
    
    $this->distinct = $distinct;
  }


  function Union($columnNames, $tableNames, $distinct = FALSE, $all = TRUE) {

    $this->stage1Sql = $this->GetSql();
    $this->stage1SqlNoLimit = $this->GetSql(FALSE);
    if ($all)
      $this->stage1Op = ' UNION ALL ';
    else
      $this->stage1Op = ' UNION ';

    // Clear the rest of the query for the rest of the params
    $this->ReInit();

    if (gettype($columnNames) == 'array') {
      $this->columns = implode(", ", array_map('esql', $columnNames));
    } else {
      $this->columns = $columnNames;
    }

    if (gettype($tableNames) == 'array') {
      $this->tables = implode(", ", array_map('esql', $tableNames));
    } else {
      $this->tables = $tableNames;
    }
    
    $this->distinct = $distinct;
  }


  function Join($clause) {
    $this->join = $clause;
  }


  // Takes arguments 3 different ways (trust me, I wish I could use function overloading
  // but given this is a very loosely typed language that probably is not going to happen
  // 1: first and only argument is: array('table_name' => array(table1.field, op, table2.field))
  // 2: table name, array(table1.field, op, table2.field)
  // 3: table name, usingcolumnname
  function OrderedJoinUsing($joinTable, $usingColumn = '') {
    //    error_log("DatabaseObject.LeftJoinUsing($joinTable, $usingColumn)");
    if (gettype($joinTable) == 'array' && $usingColumn == '')
      $this->orderedJoinUsing = $joinTable;
    else // default to simple left join
      array_push($this->orderedJoinUsing, array(LEFT_JOIN, $joinTable, $usingColumn));
    //error_log("ORDERED JOIN USING ARRAY: ".print_r($this->orderedJoinUsing, TRUE));
  }


  // Takes arguments 3 different ways (trust me, I wish I could use function overloading
  // but given this is a very loosely typed language that probably is not going to happen
  // 1: first and only argument is: array('table_name' => array(table1.field, op, table2.field))
  // 2: table name, array(table1.field, op, table2.field)
  // 3: table name, usingcolumnname
  function LeftJoinUsing($joinTable, $usingColumn = '') {
    //    error_log("DatabaseObject.LeftJoinUsing($joinTable, $usingColumn)");
    if (gettype($joinTable) == 'array' && $usingColumn == '')
      $this->leftJoinUsing = $joinTable;
    else
      $this->leftJoinUsing[$joinTable] = $usingColumn;
    //error_log("LEFT JOIN USING ARRAY: ".print_r($this->leftJoinUsing, TRUE));
  }


  // Takes arguments 3 different ways (trust me, I wish I could use function overloading
  // but given this is a very loosely typed language that probably is not going to happen
  // 1: first and only argument is: array('table_name' => array(table1.field, op, table2.field))
  // 2: table name, array(table1.field, op, table2.field)
  // 3: table name, usingcolumnname
  function UnionLeftJoinUsing($joinTable, $usingColumn = '') {
    //    error_log("DatabaseObject.LeftJoinUsing($joinTable, $usingColumn)");
    if (gettype($joinTable) == 'array' && $usingColumn == '')
      $this->stage1LeftJoinUsing = $joinTable;
    else
      $this->stage1LeftJoinUsing[$joinTable] = $usingColumn;
    //error_log("LEFT JOIN USING ARRAY: ".print_r($this->leftJoinUsing, TRUE));
  }


  // Takes arguments 3 different ways (trust me, I wish I could use function overloading
  // but given this is a very loosely typed language that probably is not going to happen
  // 1: first and only argument is: array('table_name' => array(table1.field, op, table2.field))
  // 2: table name, array(table1.field, op, table2.field)
  // 3: table name, usingcolumnname
  function RightJoinUsing($joinTable, $usingColumn = '') {
    //    error_log("DatabaseObject.RightJoinUsing($joinTable, $usingColumn)");
    if (gettype($joinTable) == 'array' && $usingColumn == '')
      $this->rightJoinUsing = $joinTable;
    else
      $this->rightJoinUsing[$joinTable] = $usingColumn;
    //error_log("RIGHT JOIN USING ARRAY: ".print_r($this->rightJoinUsing, TRUE));
  }


  // Takes arguments 3 different ways (trust me, I wish I could use function overloading
  // but given this is a very loosely typed language that probably is not going to happen
  // 1: first and only argument is: array('table_name' => array(table1.field, op, table2.field))
  // 2: table name, array(table1.field, op, table2.field)
  // 3: table name, usingcolumnname
  function InnerJoinUsing($joinTable, $usingColumn = '') {
    //    error_log("DatabaseObject.InnerJoinUsing($joinTable, $usingColumn)");
    if (gettype($joinTable) == 'array' && $usingColumn == '')
      $this->innerJoinUsing = $joinTable;
    else
      $this->innerJoinUsing[$joinTable] = $usingColumn;
    //error_log("INNER JOIN USING ARRAY: ".print_r($this->innerJoinUsing, TRUE));
  }


  function Where($clause, $cond = 'AND') {
    // Ensure proper spacing and save backward compatability
    $cond = ' '.trim($cond).' ';

    if ($this->where)
      $this->where .= $cond.$clause;
    else
      $this->where = $clause;
  }


  function SearchByOp($op = 'AND') {
    $this->searchStateOperator = $op;
  }


  function SearchBy($columnName, $newSearchData, $exact = FALSE, $op = ' = ') {

    $this->searchColumn = $columnName;
    $this->searchData = $newSearchData;

    if ($this->searchStateOperator) {
      $this->where .= " $this->searchStateOperator ";
      $this->searchStateOperator = '';
    } elseif ($this->where)
      $this->where .= ' AND ';

    if ($exact) {
      if (is_numeric($this->searchData))
	$this->where .= $this->Escape($this->searchColumn).$op.$this->Escape($this->searchData);
      else
	$this->where .= $this->Escape($this->searchColumn)." $op '".$this->Escape($this->searchData)."'";
    } else {
      $this->where .= $this->Escape($this->searchColumn)." LIKE '%".$this->Escape($this->searchData)."%'";
    }
  }


  function SortBy($columnNames, $descending = FALSE, $orderIndex = 0) {

    if (gettype($columnNames) == 'array') {
      if ($descending) {
	if ($orderIndex >= 0 && $orderIndex < count($columnNames))
	  $columnNames[$orderIndex] .= " DESC";
	else
	  $columnNames[0] .= " DESC";
      }
      $this->orderColumns = implode(", ", array_map('esql', $columnNames));
    } else {
      $this->orderColumns = $columnNames;
      if ($descending)
	$this->orderColumns .= " DESC";
    }

    $this->order = " ORDER BY ".$this->orderColumns;
    //$this->orderDescending = $descending;
    //    if ($descending) {
    //      $this->order .= " DESC";
    //    }
  }


  function UnionSortBy($columnNames, $descending = FALSE, $orderIndex = 0) {

    if (gettype($columnNames) == 'array') {
      if ($descending) {
	if ($orderIndex >= 0 && $orderIndex < count($columnNames))
	  $columnNames[$orderIndex] .= ' DESC';
	else
	  $columnNames[0] .= ' DESC';
      }
      $this->stage1OrderColumns = implode(", ", array_map('esql', $columnNames));
    } else {
      $this->stage1OrderColumns = $columnNames;
      if ($descending)
	$this->stage1OrderColumns .= ' DESC';
    }

    $this->stage1SortBy = ' ORDER BY '.$this->stage1OrderColumns;
    //$this->orderDescending = $descending;
    //    if ($descending) {
    //      $this->order .= " DESC";
    //    }
  }


  function SetPage($pageNumber, $rowsPerPage) {

    $offset = 0;

    if (!is_numeric($pageNumber) || !is_numeric($rowsPerPage) ||
	$pageNumber < 0 || $rowsPerPage < 0)
      return;

    $this->firstRow = ($pageNumber - 1) * $rowsPerPage;
    $this->lastRow = $this->firstRow + $rowsPerPage - 1;
    if ($this->stage1Op)
      $this->stage1Limit = "LIMIT $this->firstRow, $rowsPerPage";
    else
      $this->limit = "LIMIT $this->firstRow, $rowsPerPage";
  }


  /*
  function UnionSetPage($pageNumber, $rowsPerPage) {

    $offset = 0;

    if (!is_numeric($pageNumber) || !is_numeric($rowsPerPage) ||
	$pageNumber < 0 || $rowsPerPage < 0)
      return;

    $this->stage1FirstRow = ($pageNumber - 1) * $rowsPerPage;
    $this->stage1LastRow = $this->stage1FirstRow + $rowsPerPage - 1;
    $this->stage1Limit = "LIMIT $this->stage1FirstRow, $rowsPerPage";
  }
  */


  function Limit($limitParam1, $limitParam2 = 0) {

    if ($this->limit) {
      error_log("DatabaseObject::Limit(limitParam1=$limitParam1,limitParams2=$limitParam2): ".
		"Limit already defined as ".$this->limit.", not setting limit params");
      return FALSE;
    }

    if ($limitParam2)
      $this->limit = "LIMIT $limitParam1, $limitParam2";
    else
      $this->limit = "LIMIT $limitParam1";
      
    return TRUE;
  }


  function GroupBy($field) {
    $this->group = esql($field);
  }


  function GetRowCounts(&$getFirstRow, &$getLastRow, &$getNumRows) {
    //    error_log("GetRowCounts(): numRows.before = $this->numRows");
    if ($this->numRows < 0) {
      //      error_log("Calling QueryCount()...");
      if ($this->QueryCount() === FALSE) {
	error_log("DatabaseObject.GetRowCounts: ".$this->GetErrorMessage());
      }
    }

    if ($this->lastRow > $this->numRows && $this->numRows > 0)
      $this->lastRow = $this->numRows - 1;
    $getFirstRow = $this->firstRow;
    $getLastRow = $this->lastRow;
    $getNumRows = $this->numRows;
  }

  
  function GetPageCount($rowsPerPage) {

    if ($this->numRows < 0) {
      //error_log("Calling QueryCount()...");
      if ($this->QueryCount() === FALSE) {
	error_log("DatabaseObject.GetPageCount(rowsPerPage=$rowsPerPage): ".$this->GetErrorMessage());
      }
    }

    if ($this->numRows == 0)
      return 0;

    $remainder = ($this->numRows % $rowsPerPage);
    if ($remainder) {
      return (int)($this->numRows / $rowsPerPage) + 1;
    } else {
      return $this->numRows / $rowsPerPage;
    }
  }


  function GetSql($includeLimit = TRUE) {

    if ($this->distinct)
      $sqlStr = "SELECT DISTINCT $this->columns FROM $this->tables";
    else
      $sqlStr = "SELECT $this->columns FROM $this->tables";

    
    //    if (DEBUG & DEBUG_CLASS_FUNCTIONS && !empty($this->leftJoinUsing))
    //      error_log(print_r($this->leftJoinUsing, TRUE));
    foreach($this->orderedJoinUsing as $joinEntry) {
      $sqlStr .= " $joinEntry[0] $joinEntry[1] ";
      if (gettype($joinEntry[2]) == 'array') {
	$sqlStr .= " ON (";
	foreach($joinEntry[2] as $cn)
	  $sqlStr .= " $cn ";
	$sqlStr .= ')';
      } else
	$sqlStr .= " USING ($joinEntry[2])";
    }

    
    //    if (DEBUG & DEBUG_CLASS_FUNCTIONS && !empty($this->leftJoinUsing))
    //      error_log(print_r($this->leftJoinUsing, TRUE));
    foreach($this->leftJoinUsing as $tableName => $columnName) {
      if (gettype($columnName) == 'array') {
	$sqlStr .= " LEFT JOIN $tableName ON (";
	foreach($columnName as $cn)
	  $sqlStr .= " $cn ";
	$sqlStr .= ')';
      } else
	$sqlStr .= " LEFT JOIN $tableName USING ($columnName)";
    }

    
    //    if (DEBUG & DEBUG_CLASS_FUNCTIONS && !empty($this->rightJoinUsing))
    //      error_log(print_r($this->rightJoinUsing, TRUE));
    foreach($this->rightJoinUsing as $tableName => $columnName) {
      if (gettype($columnName) == 'array') {
	$sqlStr .= " RIGHT JOIN $tableName ON (";
	foreach($columnName as $cn)
	  $sqlStr .= " $cn ";
	$sqlStr .= ')';
      } else
	$sqlStr .= " RIGHT JOIN $tableName USING ($columnName)";
    }

    
    //    if (DEBUG & DEBUG_CLASS_FUNCTIONS && !empty($this->innerJoinUsing))
    //      error_log(print_r($this->innerJoinUsing, TRUE));
    foreach($this->innerJoinUsing as $tableName => $columnName) {
      if (gettype($columnName) == 'array')
	$sqlStr .= " INNER JOIN $tableName ON $columnName[0] $columnName[1] $columnName[2]";
      else
	$sqlStr .= " INNER JOIN $tableName USING ($columnName)";
	
    }
    
    if ($this->join) {
      $sqlStr .= " $this->join";
    }

    if ($this->where) {
      $sqlStr .= " WHERE $this->where";
    }
    
    if ($this->group) {
      $sqlStr .= " GROUP BY ".$this->group;
    }

    if ($this->order) {
      //$tempArr = explode(', ', $this->orderColumns);
      //      if ($this->orderDescending && !strstr($tempArr[0], "DESC"))
      //	$tempArr[0] .= " DESC";
      //$this->orderColumns = implode(', ', $tempArr);
      //$sqlStr .= " ORDER BY $this->orderColumns";
      $sqlStr .= $this->order;
    }

    if ($includeLimit && $this->limit) {
      $sqlStr .= " $this->limit";
    }

    return $sqlStr;
  }


  function Query($mode = SINGLE_ROW, $resultType = BOTH_FORMAT) {

    $rows = array();
    if ($this->stage1Op) {
      $this->sql = "($this->stage1SqlNoLimit) $this->stage1Op (".$this->GetSql(FALSE).") $this->stage1SortBy $this->stage1Limit";

      foreach($this->stage1LeftJoinUsing as $tableName => $columnName) {
	if (gettype($columnName) == 'array') {
	  $sqlStr .= " LEFT JOIN $tableName ON (";
	  foreach($columnName as $cn)
	    $sqlStr .= " $cn ";
	  $sqlStr .= ')';
	} else
	  $sqlStr .= " LEFT JOIN $tableName USING ($columnName)";
      }
      if ($this->group)
	$sqlStr .= " GROUP BY ".$this->group;
    } else
      $this->sql = $this->GetSql();

    if (DEBUG & DEBUG_SQL)
      error_log('#####| '.$this->sql.' |#####');

    $result = mysql_query($this->sql, $this->dbLink);

    if (!$result) {
      error_log("** FAILED ** ($this->sql) failed: ERR(".mysql_errno().") ".mysql_error());
      $this->ReInit();
      return FALSE;
    }

    //    $this->numRows = mysql_num_rows($result);

    switch($mode) {
    case MANY_ROWS:
      while ($row = mysql_fetch_array($result, $resultType)) {
	array_push($rows, $row);
      }
      break;
    case SINGLE_ROW:
      $rows = mysql_fetch_array($result, $resultType);
      break;
    case MANY_VALUES:
      if ($resultType == BOTH_FORMAT || $resultType == KEYS_FORMAT) {
	while ($row = mysql_fetch_assoc($result)) {
	  array_push($rows, $row);
	}
      } else {
	while ($row = mysql_fetch_array($result, $resultType)) {
	  for ($i = 0; $i < mysql_num_fields($result); $i++) {
	    array_push($rows, $row[$i]);
	  }
	}
      }
      break;
    case SINGLE_VALUE:
      if ($resultType == BOTH_FORMAT || $resultType == KEYS_FORMAT) {
	$rows = mysql_fetch_array($result, $resultType);
      } else {
	$row = mysql_fetch_array($result);
	$rows = $row[0];
      }
      break;
    case NAME_VALUES:
      while ($row = mysql_fetch_array($result, VALUES_FORMAT)) {
	if (count($row) <= 0)
	  break;
	if (count($row) == 1) {
	  $rows[$row[0]] = NULL;
	} elseif (count($row) == 2) {
	  $rows[$row[0]] = $row[1];
	} else {
	  $rows[$row[0]] = array();
	  for ($i = 1; $i < count($row); $i++) {
	    array_push($rows[$row[0]], $row[$i]);
	  }
	}
	//	print_r($rows);
	//	exit(0);
      }
      break;   
    default:
      break;
    }

    // The Where clause is the one thing that must get reset
    // on a query because it is cumulative even when other post
    // query information is kept
    $this->where = '';

    mysql_free_result($result);

    return $rows;
  }


  function Update($columnsAndValues, $table, $force = FALSE) {

    $column = '';
    $value = '';

    if (empty($columnsAndValues)) {
      error_log("DatabaseObject.Update(): columns and values empty passed to update, returning FALSE...");
      $this->ReInit();
      return FALSE;
    }

    if (empty($table)) {
      error_log("DatabaseObject.Update(): empty table value passed, returning FALSE...");
      $this->ReInit();
      return FALSE;
    }

    if (empty($this->where) && !$force) {
      error_log("DatabaseObject::Update(table=$table,force=FALSE): Update called without where clause or force enabled; "
		."refusing to execute dangerous query");
      $this->ReInit();
      return FALSE;
    }

    $this->sql = "UPDATE $table SET ";
    // Handle the case when arrays are used
    if (is_array($columnsAndValues)) {
      foreach ($columnsAndValues as $column => $value) {
	if ($value == 'NULL' || $value == 'NOW()' ||
	    (is_numeric($value) && substr($value, 0, 1) != "0") ||
	    (!is_numeric($value) && strpos($value, $column) === 0))
	  $this->sql .= "$column = $value,";
	else
	  $this->sql .= "$column = '".esql($value)."',";
      }
      // Remove the last comma added in previous construct loop
      $this->sql = substr_replace($this->sql, '', -1);
    } else { // If not an array, assume a simple string.
      $parts = explode('=', $columnsAndValues);
      if (count($parts) != 2) {
	error_log("DatabaseObject::Update(columnsAndValues=$columnsAndValues,table=$table): Update called with invalid name=value param");
	$this->ReInit();
	return FALSE;
      }
      // Trim whitespace
      $parts[1] = trim($parts[1]);
      // Remove any leading or training quote marks
      if (substr($parts[1], 0, 1) == "'" || substr($parts[1], 0, 1) == '"')
	$parts[1] = substr($parts[1], 1);
      if (substr($parts[1], -1, 1) == "'" || substr($parts[1], -1, 1) == '"')
	$parts[1] = substr($parts[1], 0, -1);
      $parts[1] = "'".esql($parts[1])."'";
      $this->sql .= implode('=', $parts);
    }

    if ($this->where)
      $this->sql .= " WHERE $this->where";
    
    return $this->Execute($this->sql);
  }

  
  function Insert($columnsAndValues, $table) {
    
    $column = '';
    $value = '';

    if (count($columnsAndValues) == 0) {
      $this->ReInit();
      return RC_INVALID_ARG;
    }

    if (!$table) {
      $this->ReInit();
      return RC_INVALID_ARG;
    }

    $this->sql = "INSERT INTO $table ("
      .implode(", ", array_keys($columnsAndValues)).") VALUES ('"
      .implode("', '", array_map('esql', array_values($columnsAndValues)))."')";

    $rc = $this->Execute($this->sql);

    if (!$rc)
      return FALSE;

    $insId = mysql_insert_id($this->dbLink);

    if ($insId)
      return $insId;

    return TRUE;
  }


  function Replace($columnsAndValues, $table) {
    
    $column = '';
    $value = '';

    if (count($columnsAndValues) == 0) {
      $this->ReInit();
      return FALSE;
    }

    if (!$table) {
      $this->ReInit();
      return FALSE;
    }

    $this->sql = "REPLACE INTO $table ("
      .implode(", ", array_keys($columnsAndValues)).") VALUES ('"
      .implode("', '", array_map('esql', array_values($columnsAndValues)))."')";
      //.implode("', '", array_values($columnsAndValues))."')";
    $this->sql = str_replace("'NOW()'", 'NOW()', $this->sql);

    if ($this->where) {
      $this->sql .= " WHERE ".$this->where;
    }

    if ($this->Execute($this->sql))
      return mysql_affected_rows($this->dbLink);
    
    return FALSE;
  }


  function Delete($table, $force = FALSE) {
    
    if (!$table) {
      $this->ReInit();
      return FALSE;
    }

    $this->sql = "DELETE FROM $table";

    if (!$this->where && !$force) {
      error_log("DatabaseObject->Delete(table=$table,force=FALSE): Delete called without where clause or force enabled; "
		."refusing to execute dangerous query");
      $this->ReInit();
      return FALSE;
    }

    if ($this->where) {
      $this->sql .= " WHERE $this->where";
    }

    return $this->Execute($this->sql);
  }


  function GetRows() {

    return mysql_num_rows();
  }


  function GetUpdatedRows() {
    
    return mysql_affected_rows();
  }


  function QueryCount() {

    if ($this->stage1Op) {
      $this->sql = '('.$this->stage1SqlNoLimit.')'.$this->stage1Op.'('.$this->GetSql(FALSE).')';

      foreach($this->stage1LeftJoinUsing as $tableName => $columnName) {
	if (gettype($columnName) == 'array') {
	  $sqlStr .= " LEFT JOIN $tableName ON (";
	  foreach($columnName as $cn)
	    $sqlStr .= " $cn ";
	  $sqlStr .= ')';
	} else
	  $sqlStr .= " LEFT JOIN $tableName USING ($columnName)";
      }
      
    } else
      $this->sql = $this->GetSql(FALSE);

    if (DEBUG & DEBUG_SQL)
      error_log("DatabaseObject.QueryCount(): ".$this->sql);
    $result = mysql_query($this->sql, $this->dbLink);

    if ($result === FALSE)
      return FALSE;

    $this->numRows = mysql_num_rows($result);
    mysql_free_result($result);

    return $this->numRows;
  }


  function Execute($sqlStr) {

    $rows = array();
    $row = array();

    if (DEBUG & DEBUG_SQL) {
      error_log($sqlStr);
    }

    $result = mysql_query($sqlStr, $this->dbLink);

    // 1008 is failed to drop database because it does not exist
    // That is OK for us.  We were wanting to drop it anyway!
    if ($result === FALSE) {
      if (mysql_errno($this->dbLink) != 1008) {
	error_log("Query ($sqlStr) failed: ERR(".mysql_errno($this->dbLink).") ".mysql_error($this->dbLink));
	$this->ReInit();
	return FALSE;
      } else {
	error_log("DatabaseObject.Execute($sqlStr): Nonexistant database deletion attempted...");
	$this->ReInit();
	return TRUE;
      }
    }

    // mysql_query returns exactly TRUE on queries that do not return results
    if ($result === TRUE) {
      $this->ReInit();
      $this->stage1Op = '';
      $this->stage1Sql = '';
      $this->stage1SqlNoLimit = '';
      $this->stage1SortBy = '';
      $this->stage1OrderColumns = '';
      $this->stage1Limit = '';
      return TRUE;
    }

    // Treat it as though results were returned from a querty that produces them
    while ($row = mysql_fetch_assoc($result)) {
      array_push($rows, $row);
    }
    mysql_free_result($result);

    $this->ReInit();
    return $rows;
  }


  function CreateDb($dbName) {
    
    return $this->Execute("CREATE DATABASE ".esql($dbName));
  }


  function DropDb($dbName) {
    
    return $this->Execute("DROP DATABASE ".esql($dbName));
  }

  function Close() {
 
    if ($this->dbLink)
      mysql_close($this->dbLink);
    
    $this->ReInit();
    $this->dbLink = NULL;
    $this->initialized = FALSE;
  }


  function ReInit() {

    $this->firstRow = 0;
    $this->lastRow = 0;
    $this->numRows = -1;

    $this->columns = '';
    $this->tables = '';
    $this->searchColumn = '';
    $this->searchData = '';
    $this->orderColumns = '';
    $this->orderDescending = FALSE;
    $this->group = '';
    $this->order = '';
    $this->join = '';
    $this->where = '';
    $this->limit = '';
    $this->selectColumns = array();
    $this->leftJoinUsing = array();
    $this->rightJoinUsing = array();
    $this->innerJoinUsing = array();
    //    $this->sql = '';
  }


  function GetErrorMessage() {
    if ($this->dbLink)
      return "Database Error (".mysql_errno($this->dbLink)."): ".mysql_error($this->dbLink)." ==> ".$this->sql;
    else
      return '';
  }

  
  function GetErrorNumber() {
    if ($this->dbLink)
      return mysql_errno($this->dbLink);
    else
      return FALSE;
  }


  function Escape($str) {
    if ((bool)ini_get('magic_quotes_gpc'))
      return $str;
    else
      return mysql_escape_string($str);
  }
}


?>