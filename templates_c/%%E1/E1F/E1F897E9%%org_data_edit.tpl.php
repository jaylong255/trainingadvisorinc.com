<?php /* Smarty version 2.6.28, created on 2016-12-03 00:55:14
         compiled from admin/org_data_edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'admin/org_data_edit.tpl', 16, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<BODY class="ContentFrameBody" style="overflow:hidden;">

<form name="AdminForm" id="AdminForm" method="POST" action="org_data_edit.php">
<table border="0" class="PageText" width="<?php if ($this->_tpl_vars['currentDataEditFrame'] == 7): ?>680<?php else: ?>350<?php endif; ?>" align="left" valign="top">

  <tr>
    <td align="left">
      <div class="OrgInfoText">Select <?php echo $this->_tpl_vars['dataElement']; ?>
:</div>
    </td>
    <td>
      <?php if (isset ( $this->_tpl_vars['dataElementValues'] ) && $this->_tpl_vars['dataElementValues']): ?>
      <select name="dataElementValues" onchange="javascript:OrgDataChangeItem('addOrgDataDiv');" class="FormValue">
        <option></option>
        <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['dataElementValues'],'selected' => $this->_tpl_vars['dataElementSelected']), $this);?>

      </select>
      <?php else: ?>
	None defined
      <?php endif; ?>
    </td>
  </tr>


  <tr>
    <td align="left">
      <div class="OrgInfoText">Name:</div>
    </td>
    <td>
      <input type="text" name="elementValue" value="<?php echo $this->_tpl_vars['elementValue']; ?>
" size="20" maxlength="50"
	 class="FormValue" onKeyUp="OrgDataEditKeyPress(event, 'addOrgDataDiv');">
    </td>
  </tr>


  <tr>
    <td align="left">
      <div class="OrgInfoText">&nbsp;</div>
    </td>
    <td>
      <!-- The OrgDataEditSubmit method called below updates the value of this hidden field before submitting form -->
      <input type="hidden" name="operation" value="update">
      <a id="addOrgDataDiv" name="addOrgDataDiv" href="javascript:OrgDataEditSubmit('add');" class="OrgInfoText"<?php if (isset ( $this->_tpl_vars['hideAddDiv'] ) && $this->_tpl_vars['hideAddDiv']): ?> style="visibility:hidden;"<?php endif; ?>>Add</a>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <?php if ($this->_tpl_vars['dataElementValues']): ?><a href="javascript:OrgDataEditSubmit('update');" class="OrgInfoText">Update</a><?php endif; ?>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <?php if ($this->_tpl_vars['dataElementValues']): ?><a href="javascript:OrgDataEditSubmit('delete');" class="OrgInfoText">Delete</a><?php endif; ?>
    </td>
  </tr>
</table>
</form>
</BODY>
</HTML>