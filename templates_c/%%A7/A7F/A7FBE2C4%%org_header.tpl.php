<?php /* Smarty version 2.6.28, created on 2016-12-03 00:05:08
         compiled from admin/org_header.tpl */ ?>
<BODY class="ContentFrameBody"
      onLoad="<?php if (isset ( $this->_tpl_vars['create'] ) && $this->_tpl_vars['create']): ?>parent.AddOrg();<?php endif; ?>
      MM_preloadImages('../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_update_up.gif','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_update_down.gif','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_back_down.gif');
      <?php if (isset ( $this->_tpl_vars['currentDataEditFrame'] )): ?>ChangePointer(<?php echo $this->_tpl_vars['currentDataEditFrame']; ?>
);<?php endif; ?>
      <?php if ($this->_tpl_vars['currentSubTab'] == 0): ?>document.search_form.searchFor.focus();<?php else: ?>parent.SetTab(1, <?php echo $this->_tpl_vars['currentSubTab']; ?>
);<?php endif; ?>">

<?php if ($this->_tpl_vars['currentSubTab'] > 0): ?>
<table border="0" width="680" align="top" cellpadding="2" cellspacing="3">
  <tr>
    <?php if ($this->_tpl_vars['superUser']): ?>
      <td width="23">
	<a href="org_list.php" onMouseOut="MM_swapImgRestore()"
		 onMouseOver="MM_swapImage('Back','','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_back_down.gif',1)"
		 onClick="javascript:parent.SetTab(<?php echo $this->_tpl_vars['currentTab']; ?>
,0); return CheckSave();">
	    <img name="Back" border="0" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_back_up.gif" width="23" height="21" align="center">
      </td>
      <td align="left">
	<a class="OrgInfoText" href="org_list.php" onClick="javascript:parent.SetTab(<?php echo $this->_tpl_vars['currentTab']; ?>
, 0);">
		Return to Organization List</a>
      </td>
    <?php else: ?>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    <?php endif; ?>
    <td align="right">
      <?php if ($this->_tpl_vars['currentSubTab'] < 3): ?>
        <?php if ($this->_tpl_vars['create']): ?>
	  <!--div id="AddDiv" style="position:absolute; left:589px; top:9px; width:67px;
		   height:25px; z-index:1;"-->
	  <a href="javascript:ValidateOrgForm(<?php echo $this->_tpl_vars['currentSubTab']; ?>
);" onMouseOut="MM_swapImgRestore()"
	    onMouseOver="MM_swapImage('AddBtn','','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_add_down.gif',1)"
	    onClick="document.getElementById('Cover').style.visibility='visible';">
	  <img name="AddBtn" border="0" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_add_up.gif" width="56" height="22"></a>
	  <!--/div-->
        <?php else: ?>
	  <!--div id="UpdateDiv" style="position:absolute; left:589px; top:9px;
		   width:67px; height:25px; z-index:1"-->
	  <a href="javascript:ValidateOrgForm(<?php echo $this->_tpl_vars['currentSubTab']; ?>
);" onMouseOut="MM_swapImgRestore()"
	    onMouseOver="MM_swapImage('Update','','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_update_down.gif',1)">
	  <img name="Update" border="0" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_update_up.gif" width="56" height="22"></a>
	  <!--/div-->
        <?php endif; ?>
      <?php endif; ?>
    </td>
  </tr>
  <?php if (isset ( $this->_tpl_vars['errMsg'] ) && $this->_tpl_vars['errMsg']): ?>
  <tr>
    <td colspan="3" class="error">
      <?php echo $this->_tpl_vars['errMsg']; ?>

    </td>
  </tr>
  <?php endif; ?>
  <tr>
    <td colspan="3"><hr width="100%"></td>
  </tr>
</table>
<?php endif; ?>