<?php /* Smarty version 2.6.28, created on 2016-12-03 06:47:24
         compiled from presentation/sendmail.tpl */ ?>
<div id="msgFormContent" name="msgFormContent" class="FeedbackContent">

  <img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/contact_header.jpg" align="top">

  <div class="Feedback">
    To: <span id="msgTxtToName" name="msgTxtToName">&nbsp;</span> &lt;<span id="msgTxtToAddr" name="msgTxtToAddr">&nbsp;</span>&gt;
  </div>
  
  <div class="Feedback">
  Subject:
  <input class="Feedback" type="text" id="msgFormSubject" name="msgFormSubject" alt="Message Subject" size="45" maxlength="128" value="">
  </div>

  <div class="Feedback">
  Message:<BR>
  <textarea id="msgFormBody" name="msgFormBody" alt="Message Body" rows="10" cols="54" value=""><?php if ($this->_tpl_vars['preview']): ?>Because you are previewing this question, any text entered here will be discarded and no message will be delivered to the contact you selected.  This is here only so that you may see exactly what someone answering this question will see.  Someone taking this training will not see the text you are currently reading.<?php endif; ?></textarea>
  </div>

  <div class="Feedback">
    Thanks for completing this training question.  After clicking the 'Send Message' button below, your message will be sent and you will be taken back to the portal home page where you may answer any remaining questions you have waiting for you to complete.
  </div>

  <p style="text-align:center;">
    <a href="javascript:<?php if ($this->_tpl_vars['preview']): ?>window.close();<?php else: ?>HideMailForm(); SubmitAnswer();<?php endif; ?>"
	 onmouseout="document.images.btnMsgCloseImage.src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/send_msg_up.jpg'"
	 onmouseover="document.images.btnMsgCloseImage.src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/send_msg_down.jpg'">
      <img name="btnContactCloseImage" id="btnMsgCloseImage" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/send_msg_up.jpg" alt="Send Message" border="0">
    </a>
  </p>

</div>