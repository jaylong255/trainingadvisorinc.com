<?php /* Smarty version 2.6.28, created on 2016-12-03 06:47:24
         compiled from presentation/feedback.tpl */ ?>
<!-- This holds all content necessary for displaying the feedback window, yeah, no more iframes -->
<div id="feedbackVeil" name="feedbackVeil" class="translucent"
        onclick="return false" onmousedown="return false" onmousemove="return false"
        onmouseup="return false" ondblclick="return false">
        &nbsp;</div>

<div id="feedbackContent" name="feedbackContent" class="FeedbackContent">

  <img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/feedback_header.jpg"><BR>

  <!--div id="btnClose" style="position:absolute; left:406px; top:16px; width:98px; height:19px; z-index:22;">
    <a href="javascript:window.close();" onmouseout="document.images.btnCloseImage.src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_popup_close_up.gif'"
       onmouseover="document.images.btnCloseImage.src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_popup_close_down.gif'">
      <img name="btnCloseImage" id="btnCloseImage" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_popup_close_up.gif" alt="" border="0" />
    </a>
  </div-->

  <div id="headerFeedback" name="headerFeedback" class="HeaderFeedback">&nbsp;</div>

  <p class="Feedback">
    <img id="imageFeedbackCorrect" align="left" name="imageFeedbackCorrect" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/correct_check.jpg" class="HideByDefault">
    <img id="imageFeedbackIncorrect" align="left" name="imageFeedbackIncorrect" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/incorrect_x.jpg" class="HideByDefault">

    <span id="foilAnswer" name="foilAnswer">&nbsp;</span>
  </p>

  <p class="Feedback">
    <b>Explanation:</b><BR>
    <span id="answerFeedback" name="answerFeedback">&nbsp;</span>
  </p>

  <p class="Feedback">
    <span id="correctFeedback" name="correctFeedback">&nbsp;</span>
  </p>

  <p style="text-align:center; margin-left:auto; margin-right:auto;">
    <div id="divImgFeedbackClose" name="divImgFeedbackClose" style="text-align:center;">
    <img id="imgFeedbackTryAgain" name="imgFeedbackClose" class="HideByDefault"
         src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_try_again_up.png" type="button" name="closeFeedbackWindow" id="closeFeedbackWindow"
         onmouseout="MM_swapImage('closeFeedbackWindow', '', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_try_again_up.png', 1);"
         onmouseover="MM_swapImage('closeFeedbackWindow', '', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_try_again_down.png', 1);"
    	 onclick="CloseFeedbackWindow(uiTheme);">
    </div>
  </p>

</div>