<?php /* Smarty version 2.6.28, created on 2016-12-03 00:05:13
         compiled from admin/file_man_edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'admin/file_man_edit.tpl', 85, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<BODY class="ContentFrameBody" onLoad="<?php if ($this->_tpl_vars['reloadFrame']): ?>parent.frames[0].location.reload();<?php endif; ?>
   <?php if (isset ( $this->_tpl_vars['refreshMotif'] ) && $this->_tpl_vars['refreshMotif']): ?>
        ChangeOrg('<?php echo $this->_tpl_vars['orgName']; ?>
', '<?php echo $this->_tpl_vars['topLeftBack']; ?>
', '<?php echo $this->_tpl_vars['topLeftFront']; ?>
', '<?php echo $this->_tpl_vars['topLeftBackRepeat']; ?>
', '<?php echo $this->_tpl_vars['topMiddleBack']; ?>
',
		  '<?php echo $this->_tpl_vars['topMiddleFront']; ?>
', '<?php echo $this->_tpl_vars['topMiddleBackRepeat']; ?>
', '<?php echo $this->_tpl_vars['topRightBack']; ?>
', '<?php echo $this->_tpl_vars['topRightFront']; ?>
', '<?php echo $this->_tpl_vars['topRightBackRepeat']; ?>
', parent.parent);
   <?php endif; ?>">

<form enctype="multipart/form-data" class="CustomizeForm" name="FileEditForm" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>
">

<?php if (isset ( $this->_tpl_vars['errMsg'] ) && $this->_tpl_vars['errMsg']): ?>
  <span class="error"><?php echo $this->_tpl_vars['errMsg']; ?>
</span>
<?php endif; ?>


<?php if (isset ( $this->_tpl_vars['path'] )): ?>
  <input type="hidden" name="path" value="<?php echo $this->_tpl_vars['path']; ?>
">
  <p class="highlighted" style="width: 90%;"><span class="Large">File/Folder Selected: <?php echo $this->_tpl_vars['path']; ?>
</span></p>
<?php endif; ?>

<?php if (isset ( $this->_tpl_vars['isDir'] ) && $this->_tpl_vars['isDir']): ?>
<!--  If this is a directory -->
<span class="OrgInfoText" align="left">Upload File to Selected Folder</span><BR>
  <input type="hidden" name="MAX_FILE_SIZE" value="20971520">
  <input type="file" name="sendFile" size="85" maxlen="128">
  <input type="submit" name="sendSubmit" value="Upload File"><BR>
  <hr width="90%" align="left">
<span class="OrgInfoText" align="left">Create New Folder Named</span><BR>
  <input type="text" name="createFolder" size="85" maxlen="128">
  <input type="submit" name="createFolderSubmit" value="Create Folder"><BR>
  <hr width="90%" align="left">
<?php endif; ?>

<?php if (isset ( $this->_tpl_vars['isRoot'] ) && ! $this->_tpl_vars['isRoot']): ?>
<span class="OrgInfoText" align="left">Rename <?php if (isset ( $this->_tpl_vars['isDir'] ) && $this->_tpl_vars['isDir']): ?>Folder<?php else: ?>File<?php endif; ?></span><BR>
  <input type="text" name="renameFile" size="85" maxlen="128" value="<?php echo $this->_tpl_vars['basepath']; ?>
">
  <input type="submit" name="renameSubmit" value="Rename"><BR>
  <hr width="90%" align="left">
<?php endif; ?>

<?php if (isset ( $this->_tpl_vars['path'] ) && ! empty ( $this->_tpl_vars['path'] ) && ! $this->_tpl_vars['isRoot']): ?>
  <table>
    <tr>
      <td>
        <input type="checkbox" name="deleteFile" value="1" align="left">
      </td>
      <td valign="top">
        <span class="OrgInfoText">
	  Delete Selected <?php if (isset ( $this->_tpl_vars['isDir'] ) && $this->_tpl_vars['isDir']): ?>Folder And All Files Or Folders Within This Folder<?php else: ?>File<?php endif; ?>
	</span>
	<input type="submit" name="deleteSubmit" value="Confirm Delete">
      </td>
    </tr>
  </table>
  <hr width="90%" align="left">

  <table>
    <tr>
      <td valign="top">
        <span class="OrgInfoText">
	  <input type="radio" name="copyMove" value="copy">Copy OR
	  <input type="radio" name="copyMove" value="move">Move Selected <?php if (isset ( $this->_tpl_vars['isDir'] ) && $this->_tpl_vars['isDir']): ?>Folder<?php else: ?>File<?php endif; ?> To:
	</span>
        <select name="copyMoveDest">
           <option value="/">/</option>
        <?php unset($this->_sections['nodeIndex']);
$this->_sections['nodeIndex']['name'] = 'nodeIndex';
$this->_sections['nodeIndex']['loop'] = is_array($_loop=$this->_tpl_vars['nodes']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['nodeIndex']['show'] = true;
$this->_sections['nodeIndex']['max'] = $this->_sections['nodeIndex']['loop'];
$this->_sections['nodeIndex']['step'] = 1;
$this->_sections['nodeIndex']['start'] = $this->_sections['nodeIndex']['step'] > 0 ? 0 : $this->_sections['nodeIndex']['loop']-1;
if ($this->_sections['nodeIndex']['show']) {
    $this->_sections['nodeIndex']['total'] = $this->_sections['nodeIndex']['loop'];
    if ($this->_sections['nodeIndex']['total'] == 0)
        $this->_sections['nodeIndex']['show'] = false;
} else
    $this->_sections['nodeIndex']['total'] = 0;
if ($this->_sections['nodeIndex']['show']):

            for ($this->_sections['nodeIndex']['index'] = $this->_sections['nodeIndex']['start'], $this->_sections['nodeIndex']['iteration'] = 1;
                 $this->_sections['nodeIndex']['iteration'] <= $this->_sections['nodeIndex']['total'];
                 $this->_sections['nodeIndex']['index'] += $this->_sections['nodeIndex']['step'], $this->_sections['nodeIndex']['iteration']++):
$this->_sections['nodeIndex']['rownum'] = $this->_sections['nodeIndex']['iteration'];
$this->_sections['nodeIndex']['index_prev'] = $this->_sections['nodeIndex']['index'] - $this->_sections['nodeIndex']['step'];
$this->_sections['nodeIndex']['index_next'] = $this->_sections['nodeIndex']['index'] + $this->_sections['nodeIndex']['step'];
$this->_sections['nodeIndex']['first']      = ($this->_sections['nodeIndex']['iteration'] == 1);
$this->_sections['nodeIndex']['last']       = ($this->_sections['nodeIndex']['iteration'] == $this->_sections['nodeIndex']['total']);
?>
           <?php if ($this->_tpl_vars['nodes'][$this->_sections['nodeIndex']['index']]['isDir'] && $this->_tpl_vars['nodes'][$this->_sections['nodeIndex']['index']]['path'] != $this->_tpl_vars['path']): ?>
	     <option value="<?php echo $this->_tpl_vars['nodes'][$this->_sections['nodeIndex']['index']]['path']; ?>
"><?php echo $this->_tpl_vars['nodes'][$this->_sections['nodeIndex']['index']]['path']; ?>
</option>
	   <?php endif; ?>
        <?php endfor; endif; ?>
	</select>
	<input type="submit" name="copyMoveSubmit" value="Execute">
      </td>
    </tr>
  </table>
  <hr width="90%" align="left">

<?php endif; ?>
  <!--hr width="90%" align="left"-->
<?php if (isset ( $this->_tpl_vars['isEmail'] ) && $this->_tpl_vars['isEmail']): ?>
<span class="OrgInfoText" align="left">
  Send test email message to the following recipient(s):</span><BR>
  Select contact: <select name="testEmailUser" class="FormValue">
	<option></option>
	<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['contactList']), $this);?>

	</select> and/or enter an email address: <input type="text" name="testEmailAddr" size="35">
  <input type="submit" name="testEmailSubmit" value="Send"><BR>
  <hr width="90%" align="left">
<?php endif; ?>

<?php if (isset ( $this->_tpl_vars['isEditable'] ) && $this->_tpl_vars['isEditable'] && ! ( isset ( $this->_tpl_vars['isDir'] ) || $this->_tpl_vars['isDir'] )): ?>
<span class="OrgInfoText" align="left">
  Edit File</span><BR>
  <textarea name="editFile" rows="10" cols="50"><?php echo $this->_tpl_vars['fileContent']; ?>
</textarea><BR>
  <input type="submit" name="editSubmit" value="Rename"><BR>
  <hr width="90%" align="left">
<?php endif; ?>

<?php if (isset ( $this->_tpl_vars['showNewsOptions'] ) && $this->_tpl_vars['showNewsOptions']): ?>
  <table>
    <tr>
      <td>
        <input type="checkbox" name="makeNewsFile" value="1" align="left"<?php if ($this->_tpl_vars['publishSelected']): ?> checked<?php endif; ?>>
      </td>
      <td valign="top">
        <span class="OrgInfoText">
	  Publish contents of selected file to news
	</span>
	<input type="submit" name="submitNewsFile" value="Confirm Publish">
      </td>
    </tr>
  </table>
  <hr width="90%" align="left">
<?php endif; ?>

<?php if (isset ( $this->_tpl_vars['showEdit'] ) && $this->_tpl_vars['showEdit']): ?>
  <span class="OrgInfoText">Edit File: <?php echo $this->_tpl_vars['path']; ?>
</span><BR>
  <textarea name="fileContents" rows="15" cols="80"><?php echo $this->_tpl_vars['fileContents']; ?>
</textarea><BR>
  <input type="submit" name="editSubmit" value="Save File">
  <hr width="90%" align="left">
<?php endif; ?>

<?php if (isset ( $this->_tpl_vars['imageUrlPath'] )): ?>
  <span class="OrgInfoText">Below is the image file you selected (images restricted to a height of 97 pixels):</span><BR>
  <img src="<?php echo $this->_tpl_vars['imageUrlPath']; ?>
" border="0" name="imageUrlPath"><BR>
  <span class="OrgInfoText">
    If you would like to use this image as part of the motif, select the region(s) in which you would like it to appear:<BR>
    <input type="checkbox" name="topLeftBack" value="1"<?php if ($this->_tpl_vars['configTopLeftBack'] == $this->_tpl_vars['path']): ?> checked<?php endif; ?>>Left Background Image<BR>
    &nbsp;&nbsp;&nbsp;<input type="checkbox" name="topLeftBackRepeat" id="topLeftBackRepeat" value="1"<?php if ($this->_tpl_vars['topLeftBackRepeat']): ?> checked<?php endif; ?>>Repeat left background image across the display area<BR>
    <input type="checkbox" name="topLeftFront" value="1"<?php if ($this->_tpl_vars['configTopLeftFront'] == $this->_tpl_vars['path']): ?> checked<?php endif; ?>>Left Foreground Image<BR>
    <input type="checkbox" name="topMiddleBack" value="1"<?php if ($this->_tpl_vars['configTopMiddleBack'] == $this->_tpl_vars['path']): ?> checked<?php endif; ?>>Middle Background Image<BR>
    &nbsp;&nbsp;&nbsp;<input type="checkbox" name="topMiddleBackRepeat" value="1"<?php if ($this->_tpl_vars['topMiddleBackRepeat']): ?> checked<?php endif; ?>>Repeat middle background image across the display area<BR>
    <input type="checkbox" name="topMiddleFront" value="1"<?php if ($this->_tpl_vars['configTopMiddleFront'] == $this->_tpl_vars['path']): ?> checked<?php endif; ?>>Middle Foreground Image<BR>
    <input type="checkbox" name="topRightBack" value="1"<?php if ($this->_tpl_vars['configTopRightBack'] == $this->_tpl_vars['path']): ?> checked<?php endif; ?>>Right Background Image<BR>
    &nbsp;&nbsp;&nbsp;<input type="checkbox" name="topRightBackRepeat" value="1"<?php if ($this->_tpl_vars['topRightBackRepeat']): ?> checked<?php endif; ?>>Repeat right background image across the display area<BR>
    <input type="checkbox" name="topRightFront" value="1"<?php if ($this->_tpl_vars['configTopRightFront'] == $this->_tpl_vars['path']): ?> checked<?php endif; ?>>Right Foreground Image<BR>
    <input type="checkbox" name="useDefaultMotif" value="1"<?php if (isset ( $this->_tpl_vars['useDefaultMotif'] ) && $this->_tpl_vars['useDefaultMotif']): ?> checked<?php endif; ?>>Use the default Motif<BR>
	<input type="submit" name="publishMotif" value="Publish Changes"><BR>
  </span>

  <hr width="90%" align="left">
<?php elseif (isset ( $this->_tpl_vars['motifFolderSelected'] ) && $this->_tpl_vars['motifFolderSelected']): ?>
  <?php if (isset ( $this->_tpl_vars['useDefaultMotif'] ) && $this->_tpl_vars['useDefaultMotif']): ?>
    <input type="submit" name="publishMotif" value="Disable Default Motif">
  <?php else: ?>
    <input type="submit" name="publishMotif" value="Publish Default Motif">
  <?php endif; ?>
  <BR>
  <hr width="90%" align="left">
<?php endif; ?>


</form>

</body>
</html>