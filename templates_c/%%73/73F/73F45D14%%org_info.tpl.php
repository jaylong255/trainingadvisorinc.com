<?php /* Smarty version 2.6.28, created on 2016-12-03 00:48:00
         compiled from admin/org_info.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/org_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<form name="OrgForm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
">

  <?php if (! $this->_tpl_vars['create']): ?>
    <input type="hidden" name="create" value="0">
  <?php else: ?>
    <input type="hidden" name="create" value="1">
  <?php endif; ?>
  <input type="hidden" name="saveMe" value="1">

  <table border="0" align="top">
  <?php if (! $this->_tpl_vars['create']): ?>
    <tr>
      <td class="OrgInfoText"> Organization ID: &nbsp;&nbsp;</td>
      <td><?php echo $this->_tpl_vars['orgId']; ?>
&nbsp;</td>
    </tr>
  <?php endif; ?>
    <tr>
      <td class="OrgInfoText"> Organization Name: &nbsp;&nbsp;</td>
      <td>
        <input class="FormValue" type="text" name="orgName" id="orgName" maxlength="45" size="70"
		value="<?php echo $this->_tpl_vars['orgName']; ?>
" onchange="javascript:SetFlag();">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Organization Directory:</td>
      <td>
        <input <?php if ($this->_tpl_vars['create']): ?>class="FormValue"<?php else: ?>class="FormDisabled" readonly<?php endif; ?>
		 type="text" name="orgDirectory" id="orgDirectory" maxlength="255" size="70"
		 onchange="javascript:SetFlag();" value="<?php echo $this->_tpl_vars['orgDirectory']; ?>
">
        </td>
    </tr>
    <?php if ($this->_tpl_vars['licenseAb1825']): ?>
    <tr>
      <td class="OrgInfoText" valign="top">Policies:</td>
      <td>
	<textarea class="FormValue" name="orgPolicies" id="orgPolicies" rows="10" cols="50"
		onChange="javascript:SetFlag();"><?php echo $this->_tpl_vars['orgPolicies']; ?>
</textarea>
      </td>
    </tr>
    <?php endif; ?>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" class="OrgInfoText">
	News URL and Motif selections have been moved to the customize tab.<BR>
        Click on that tab to configure the items previously available on this page.
	<?php if ($this->_tpl_vars['create']): ?>
	<!--p>Note: If creating an organization, the customize tab will not be available<BR>
	until after the organization has been successfully added to the system.<BR>
	</p-->
	<?php endif; ?>
      </td>
    </tr>
  </table>

</form>


<div id="Cover" class="WaitText" style="position:absolute; left:0px; top:0px;
	 width:716px; height:341px; z-index:25; background-color: #FFFBAF;
	 /* layer-background-color: #FFFBAF; */ border: 1px solid #000000; visibility: hidden">
  <table width="100%" width="716" height="341" valign="center">
    <tr><td align="center" valign="middle" class="WaitText">Please Wait...</td><tr>
    <tr><td align="center" valign="middle" class="WaitText">Creating a new Organization Database.</td><tr>
    <tr><td align="center" valign="middle" class="WaitText">This may take several minutes.</td><tr>
  </table>
</div>

</BODY>
</HTML>