<?php /* Smarty version 2.6.28, created on 2016-12-03 06:47:24
         compiled from presentation/assignments.tpl */ ?>
<div style="visibility:hidden" name="divAssignmentList" id="divAssignmentList">
<?php if (empty ( $this->_tpl_vars['assignments'] ) && empty ( $this->_tpl_vars['divAssignments'] ) && empty ( $this->_tpl_vars['deptAssignments'] )): ?>
  <p class="Black">There are no questions currently due at this time.</p>
<?php else: ?>
  <?php if (! empty ( $this->_tpl_vars['divAssignments'] ) && ! empty ( $this->_tpl_vars['deptAssignments'] )): ?>
  Questions from your organization:<BR><hr width="100%">
  <?php endif; ?>
  <?php unset($this->_sections['assignmentIdx']);
$this->_sections['assignmentIdx']['name'] = 'assignmentIdx';
$this->_sections['assignmentIdx']['loop'] = is_array($_loop=$this->_tpl_vars['assignments']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['assignmentIdx']['show'] = true;
$this->_sections['assignmentIdx']['max'] = $this->_sections['assignmentIdx']['loop'];
$this->_sections['assignmentIdx']['step'] = 1;
$this->_sections['assignmentIdx']['start'] = $this->_sections['assignmentIdx']['step'] > 0 ? 0 : $this->_sections['assignmentIdx']['loop']-1;
if ($this->_sections['assignmentIdx']['show']) {
    $this->_sections['assignmentIdx']['total'] = $this->_sections['assignmentIdx']['loop'];
    if ($this->_sections['assignmentIdx']['total'] == 0)
        $this->_sections['assignmentIdx']['show'] = false;
} else
    $this->_sections['assignmentIdx']['total'] = 0;
if ($this->_sections['assignmentIdx']['show']):

            for ($this->_sections['assignmentIdx']['index'] = $this->_sections['assignmentIdx']['start'], $this->_sections['assignmentIdx']['iteration'] = 1;
                 $this->_sections['assignmentIdx']['iteration'] <= $this->_sections['assignmentIdx']['total'];
                 $this->_sections['assignmentIdx']['index'] += $this->_sections['assignmentIdx']['step'], $this->_sections['assignmentIdx']['iteration']++):
$this->_sections['assignmentIdx']['rownum'] = $this->_sections['assignmentIdx']['iteration'];
$this->_sections['assignmentIdx']['index_prev'] = $this->_sections['assignmentIdx']['index'] - $this->_sections['assignmentIdx']['step'];
$this->_sections['assignmentIdx']['index_next'] = $this->_sections['assignmentIdx']['index'] + $this->_sections['assignmentIdx']['step'];
$this->_sections['assignmentIdx']['first']      = ($this->_sections['assignmentIdx']['iteration'] == 1);
$this->_sections['assignmentIdx']['last']       = ($this->_sections['assignmentIdx']['iteration'] == $this->_sections['assignmentIdx']['total']);
?>
    <?php if (isset ( $this->_tpl_vars['printFeatureEnabled'] ) && $this->_tpl_vars['printFeatureEnabled']): ?>
      <?php if ($this->_sections['assignmentIdx']['iteration'] == 1): ?>
  <form method="POST" action="print_questions.php" name="printQuestionsForm" id="printQuestionsForm">
  <table width="100%" border="0">
    <tr>
      <td align="left">
        <a href="javascript:ToggleCheckBoxes('questionsToPrint', <?php echo $this->_sections['assignmentIdx']['total']; ?>
, true);">Select All</a> | 
        <a href="javascript:ToggleCheckBoxes('questionsToPrint', <?php echo $this->_sections['assignmentIdx']['total']; ?>
, false);">Unselect All</a><BR>
      </td>
      <td align="right">
        <a href="javascript:PrepareSelectedQuestions('print_questions.php', 'questionsToPrint',
						     <?php echo $this->_sections['assignmentIdx']['start']; ?>
,
						     <?php echo $this->_sections['assignmentIdx']['max']; ?>
);">
		Print Selected Question(s)</a>
	<!--input type="submit" name="submit" value="Print Selected Questions"-->
      </td>
    </tr>
  </table>
    <?php endif; ?>

  <input type="checkbox" id="questionsToPrint<?php echo $this->_sections['assignmentIdx']['iteration']; ?>
"
  	 name="questionsToPrint[]" value="<?php echo $this->_tpl_vars['assignments'][$this->_sections['assignmentIdx']['index']]['Assignment_ID']; ?>
">
  <?php endif; ?>
  <a class="bottomLinks" target="_parent" href="q_type.php?assignmentId=<?php echo $this->_tpl_vars['assignments'][$this->_sections['assignmentIdx']['index']]['Assignment_ID']; ?>
<?php if (isset ( $this->_tpl_vars['review'] ) && $this->_tpl_vars['review']): ?>&review=<?php echo $this->_tpl_vars['review']; ?>
<?php endif; ?>"><?php echo $this->_tpl_vars['assignments'][$this->_sections['assignmentIdx']['index']]['Title']; ?>
</a><BR>
  <?php endfor; endif; ?>


  <?php if (! empty ( $this->_tpl_vars['divAssignments'] )): ?>
  Questions from your division:<BR><hr width="100%">
  <?php endif; ?>
  <?php unset($this->_sections['assignmentIdx']);
$this->_sections['assignmentIdx']['name'] = 'assignmentIdx';
$this->_sections['assignmentIdx']['loop'] = is_array($_loop=$this->_tpl_vars['divAssignments']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['assignmentIdx']['show'] = true;
$this->_sections['assignmentIdx']['max'] = $this->_sections['assignmentIdx']['loop'];
$this->_sections['assignmentIdx']['step'] = 1;
$this->_sections['assignmentIdx']['start'] = $this->_sections['assignmentIdx']['step'] > 0 ? 0 : $this->_sections['assignmentIdx']['loop']-1;
if ($this->_sections['assignmentIdx']['show']) {
    $this->_sections['assignmentIdx']['total'] = $this->_sections['assignmentIdx']['loop'];
    if ($this->_sections['assignmentIdx']['total'] == 0)
        $this->_sections['assignmentIdx']['show'] = false;
} else
    $this->_sections['assignmentIdx']['total'] = 0;
if ($this->_sections['assignmentIdx']['show']):

            for ($this->_sections['assignmentIdx']['index'] = $this->_sections['assignmentIdx']['start'], $this->_sections['assignmentIdx']['iteration'] = 1;
                 $this->_sections['assignmentIdx']['iteration'] <= $this->_sections['assignmentIdx']['total'];
                 $this->_sections['assignmentIdx']['index'] += $this->_sections['assignmentIdx']['step'], $this->_sections['assignmentIdx']['iteration']++):
$this->_sections['assignmentIdx']['rownum'] = $this->_sections['assignmentIdx']['iteration'];
$this->_sections['assignmentIdx']['index_prev'] = $this->_sections['assignmentIdx']['index'] - $this->_sections['assignmentIdx']['step'];
$this->_sections['assignmentIdx']['index_next'] = $this->_sections['assignmentIdx']['index'] + $this->_sections['assignmentIdx']['step'];
$this->_sections['assignmentIdx']['first']      = ($this->_sections['assignmentIdx']['iteration'] == 1);
$this->_sections['assignmentIdx']['last']       = ($this->_sections['assignmentIdx']['iteration'] == $this->_sections['assignmentIdx']['total']);
?>
    <?php if (isset ( $this->_tpl_vars['printFeatureEnabled'] ) && $this->_tpl_vars['printFeatureEnabled']): ?>
      <?php if ($this->_sections['assignmentIdx']['iteration'] == 1): ?>
  <form method="POST" action="print_questions.php" name="printQuestionsForm" id="printQuestionsForm">
  <table width="100%" border="0">
    <tr>
      <td align="left">
        <a href="javascript:ToggleCheckBoxes('questionsToPrint', <?php echo $this->_sections['assignmentIdx']['total']; ?>
, true);">Select All</a> | 
        <a href="javascript:ToggleCheckBoxes('questionsToPrint', <?php echo $this->_sections['assignmentIdx']['total']; ?>
, false);">Unselect All</a><BR>
      </td>
      <td align="right">
        <a href="javascript:PrepareSelectedQuestions('print_questions.php', 'questionsToPrint',
						     <?php echo $this->_sections['assignmentIdx']['start']; ?>
,
						     <?php echo $this->_sections['assignmentIdx']['max']; ?>
);">
		Print Selected Question(s)</a>
	<!--input type="submit" name="submit" value="Print Selected Questions"-->
      </td>
    </tr>
  </table>
    <?php endif; ?>

  <input type="checkbox" id="questionsToPrint<?php echo $this->_sections['assignmentIdx']['iteration']; ?>
"
  	 name="questionsToPrint[]" value="<?php echo $this->_tpl_vars['divAssignments'][$this->_sections['assignmentIdx']['index']]['Assignment_ID']; ?>
">
  <?php endif; ?>
  <a class="bottomLinks" target="_parent" href="q_type.php?assignmentId=<?php echo $this->_tpl_vars['divAssignments'][$this->_sections['assignmentIdx']['index']]['Assignment_ID']; ?>
<?php if (isset ( $this->_tpl_vars['review'] ) && $this->_tpl_vars['review']): ?>&review=<?php echo $this->_tpl_vars['review']; ?>
<?php endif; ?>"><?php echo $this->_tpl_vars['divAssignments'][$this->_sections['assignmentIdx']['index']]['Title']; ?>
</a><BR>
  <?php endfor; endif; ?>



  <?php if (! empty ( $this->_tpl_vars['deptAssignments'] )): ?>
  Questions from your department:<BR><hr width="100%">
  <?php endif; ?>
  <?php unset($this->_sections['assignmentIdx']);
$this->_sections['assignmentIdx']['name'] = 'assignmentIdx';
$this->_sections['assignmentIdx']['loop'] = is_array($_loop=$this->_tpl_vars['deptAssignments']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['assignmentIdx']['show'] = true;
$this->_sections['assignmentIdx']['max'] = $this->_sections['assignmentIdx']['loop'];
$this->_sections['assignmentIdx']['step'] = 1;
$this->_sections['assignmentIdx']['start'] = $this->_sections['assignmentIdx']['step'] > 0 ? 0 : $this->_sections['assignmentIdx']['loop']-1;
if ($this->_sections['assignmentIdx']['show']) {
    $this->_sections['assignmentIdx']['total'] = $this->_sections['assignmentIdx']['loop'];
    if ($this->_sections['assignmentIdx']['total'] == 0)
        $this->_sections['assignmentIdx']['show'] = false;
} else
    $this->_sections['assignmentIdx']['total'] = 0;
if ($this->_sections['assignmentIdx']['show']):

            for ($this->_sections['assignmentIdx']['index'] = $this->_sections['assignmentIdx']['start'], $this->_sections['assignmentIdx']['iteration'] = 1;
                 $this->_sections['assignmentIdx']['iteration'] <= $this->_sections['assignmentIdx']['total'];
                 $this->_sections['assignmentIdx']['index'] += $this->_sections['assignmentIdx']['step'], $this->_sections['assignmentIdx']['iteration']++):
$this->_sections['assignmentIdx']['rownum'] = $this->_sections['assignmentIdx']['iteration'];
$this->_sections['assignmentIdx']['index_prev'] = $this->_sections['assignmentIdx']['index'] - $this->_sections['assignmentIdx']['step'];
$this->_sections['assignmentIdx']['index_next'] = $this->_sections['assignmentIdx']['index'] + $this->_sections['assignmentIdx']['step'];
$this->_sections['assignmentIdx']['first']      = ($this->_sections['assignmentIdx']['iteration'] == 1);
$this->_sections['assignmentIdx']['last']       = ($this->_sections['assignmentIdx']['iteration'] == $this->_sections['assignmentIdx']['total']);
?>
    <?php if (isset ( $this->_tpl_vars['printFeatureEnabled'] ) && $this->_tpl_vars['printFeatureEnabled']): ?>
      <?php if ($this->_sections['assignmentIdx']['iteration'] == 1): ?>
  <form method="POST" action="print_questions.php" name="printQuestionsForm" id="printQuestionsForm">
  <table width="100%" border="0">
    <tr>
      <td align="left">
        <a href="javascript:ToggleCheckBoxes('questionsToPrint', <?php echo $this->_sections['assignmentIdx']['total']; ?>
, true);">Select All</a> | 
        <a href="javascript:ToggleCheckBoxes('questionsToPrint', <?php echo $this->_sections['assignmentIdx']['total']; ?>
, false);">Unselect All</a><BR>
      </td>
      <td align="right">
        <a href="javascript:PrepareSelectedQuestions('print_questions.php', 'questionsToPrint',
						     <?php echo $this->_sections['assignmentIdx']['start']; ?>
,
						     <?php echo $this->_sections['assignmentIdx']['max']; ?>
);">
		Print Selected Question(s)</a>
	<!--input type="submit" name="submit" value="Print Selected Questions"-->
      </td>
    </tr>
  </table>
    <?php endif; ?>

  <input type="checkbox" id="questionsToPrint<?php echo $this->_sections['assignmentIdx']['iteration']; ?>
"
  	 name="questionsToPrint[]" value="<?php echo $this->_tpl_vars['deptAssignments'][$this->_sections['assignmentIdx']['index']]['Assignment_ID']; ?>
">
  <?php endif; ?>
  <a class="bottomLinks" target="_parent" href="q_type.php?assignmentId=<?php echo $this->_tpl_vars['deptAssignments'][$this->_sections['assignmentIdx']['index']]['Assignment_ID']; ?>
"><?php echo $this->_tpl_vars['deptAssignments'][$this->_sections['assignmentIdx']['index']]['Title']; ?>
<?php if (isset ( $this->_tpl_vars['review'] ) && $this->_tpl_vars['review']): ?>&review=<?php echo $this->_tpl_vars['review']; ?>
<?php endif; ?></a><BR>
  <?php endfor; endif; ?>


</form>
<?php endif; ?>
</div>

<!--div style="visibility:hidden" name="divCompletedAssignmentList" id="divCompletedAssignmentList">
section name="completedAssignmentIdx" loop=completedAssignments
  <a class="bottomLinks" target="_parent" href="q_type.php?assignmentId=$completedAssignments[completedAssignmentIdx].Assignment_ID">$completedAssignments[completedAssignmentIdx].Title</a><BR>
sectionelse
  <p class="Black">There are no questions that you have completed.</p>
/section
</div-->