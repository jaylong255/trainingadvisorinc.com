<?php /* Smarty version 2.6.28, created on 2016-12-03 00:05:08
         compiled from admin/generic_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'admin/generic_list.tpl', 11, false),array('function', 'cycle', 'admin/generic_list.tpl', 178, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "admin/".($this->_tpl_vars['localHeader']).".tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Search Bar -->
<table border="0" cellpadding="0" cellspacing="0" width="99%">
  <tr>
    <form method="POST" id="search_form" name="search_form" action="<?php echo $_SERVER['PHP_SELF']; ?>
">
    <td>
      Search
      <select size="1" class="formvalue" name="searchBy">
	 <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['columnLabels'],'selected' => $this->_tpl_vars['searchBy']), $this);?>

      </select> for <input type="text" class="formvalue" name="searchFor" size="25">
      <a href="javascript:document.search_form.submit();" onMouseOut="MM_swapImgRestore()"
       	 onMouseOver="MM_swapImage('GoSearch','','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_go_down.gif',1)">
      <img name="GoSearch" border="0" align="absbottom" width="25" height="22" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_go_up.gif"></a>
    </td>
    </form>
    <form method="POST" id="row_form" name="row_form" action="../login/change_prefs.php">
    <td align="right">
      Show&nbsp;<input type="text" class="formvalue" name="showRows" size="3" value="<?php echo $this->_tpl_vars['rowsPerPage']; ?>
">
      &nbsp;items per page&nbsp;
      <a href="javascript:document.row_form.submit();" onMouseOut="MM_swapImgRestore()"
         onMouseOver="MM_swapImage('GoMaxRows','','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_go_down.gif',1)">
      <img name="GoMaxRows" border="0" align="absbottom" width="25" height="22" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_go_up.gif"></a>
      <input type="hidden" name="returnUrl" value="<?php echo $_SERVER['PHP_SELF']; ?>
">
    </td>
      </form>
  </tr>
  <?php if (FALSE && $this->_tpl_vars['currentTab'] == 4 && ( isset ( $this->_tpl_vars['currentSubTab'] ) && ( $this->_tpl_vars['currentSubTab'] == 1 || $this->_tpl_vars['currentSubTab'] == 2 ) )): ?><!--questions tab-->
  <tr>
    <td>
      Category: <select size="1" class="formvalue" name="displayCategories">
	<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['displayCategories'],'selected' => $this->_tpl_vars['displayCategory']), $this);?>

      </select>
    </td>
    <td align="right">
      <?php if ($this->_tpl_vars['currentSubTab'] == 1): ?>
      <input type="button" value="Release Questions" OnClick="BranchTo('question_release.php')">
      <?php else: ?>
      &nbsp;
      <?php endif; ?>
    </td>
  </tr>
  <?php endif; ?>
</table>
<hr align="left" width="99%">



<!-- Links -->
<table border="0" cellpadding="0" cellspacing="0" width="99%">
  <tr>
    <td>
      <a id="ColumnLink" href="javascript:ShowPopupMenu('ColumnMenu', 'ColumnLink', true);">Select columns</a>
      <?php if (isset ( $this->_tpl_vars['topLinkList'] )): ?>
	<?php if (gettype ( $this->_tpl_vars['topLinkList'] ) == 'array'): ?>
          <?php unset($this->_sections['topLinkIdx']);
$this->_sections['topLinkIdx']['name'] = 'topLinkIdx';
$this->_sections['topLinkIdx']['loop'] = is_array($_loop=$this->_tpl_vars['topLinkList']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['topLinkIdx']['show'] = true;
$this->_sections['topLinkIdx']['max'] = $this->_sections['topLinkIdx']['loop'];
$this->_sections['topLinkIdx']['step'] = 1;
$this->_sections['topLinkIdx']['start'] = $this->_sections['topLinkIdx']['step'] > 0 ? 0 : $this->_sections['topLinkIdx']['loop']-1;
if ($this->_sections['topLinkIdx']['show']) {
    $this->_sections['topLinkIdx']['total'] = $this->_sections['topLinkIdx']['loop'];
    if ($this->_sections['topLinkIdx']['total'] == 0)
        $this->_sections['topLinkIdx']['show'] = false;
} else
    $this->_sections['topLinkIdx']['total'] = 0;
if ($this->_sections['topLinkIdx']['show']):

            for ($this->_sections['topLinkIdx']['index'] = $this->_sections['topLinkIdx']['start'], $this->_sections['topLinkIdx']['iteration'] = 1;
                 $this->_sections['topLinkIdx']['iteration'] <= $this->_sections['topLinkIdx']['total'];
                 $this->_sections['topLinkIdx']['index'] += $this->_sections['topLinkIdx']['step'], $this->_sections['topLinkIdx']['iteration']++):
$this->_sections['topLinkIdx']['rownum'] = $this->_sections['topLinkIdx']['iteration'];
$this->_sections['topLinkIdx']['index_prev'] = $this->_sections['topLinkIdx']['index'] - $this->_sections['topLinkIdx']['step'];
$this->_sections['topLinkIdx']['index_next'] = $this->_sections['topLinkIdx']['index'] + $this->_sections['topLinkIdx']['step'];
$this->_sections['topLinkIdx']['first']      = ($this->_sections['topLinkIdx']['iteration'] == 1);
$this->_sections['topLinkIdx']['last']       = ($this->_sections['topLinkIdx']['iteration'] == $this->_sections['topLinkIdx']['total']);
?>
         | <a href="<?php echo $this->_tpl_vars['topLinkList'][$this->_sections['topLinkIdx']['index']]->href; ?>
<?php if ($this->_tpl_vars['topLinkList'][$this->_sections['topLinkIdx']['index']]->params): ?>?<?php echo $this->_tpl_vars['topLinkList'][$this->_sections['topLinkIdx']['index']]->params; ?>
<?php endif; ?>"
	      <?php if ($this->_tpl_vars['topLinkList'][$this->_sections['topLinkIdx']['index']]->target): ?>target="<?php echo $this->_tpl_vars['topLinkList'][$this->_sections['topLinkIdx']['index']]->target; ?>
"<?php endif; ?>
	      <?php if ($this->_tpl_vars['topLinkList'][$this->_sections['topLinkIdx']['index']]->class): ?>class="<?php echo $this->_tpl_vars['topLinkList'][$this->_sections['topLinkIdx']['index']]->class; ?>
"<?php endif; ?>
	      <?php if ($this->_tpl_vars['topLinkList'][$this->_sections['topLinkIdx']['index']]->style): ?>style="<?php echo $this->_tpl_vars['topLinkList'][$this->_sections['topLinkIdx']['index']]->style; ?>
"<?php endif; ?>
	      <?php if ($this->_tpl_vars['topLinkList'][$this->_sections['topLinkIdx']['index']]->onClick): ?>onClick="javascript:<?php echo $this->_tpl_vars['topLinkList'][$this->_sections['topLinkIdx']['index']]->onClick; ?>
;"<?php endif; ?>
	      <?php if ($this->_tpl_vars['topLinkList'][$this->_sections['topLinkIdx']['index']]->alt): ?>alt="<?php echo $this->_tpl_vars['topLinkList'][$this->_sections['topLinkIdx']['index']]->alt; ?>
"<?php endif; ?>><?php echo $this->_tpl_vars['topLinkList'][$this->_sections['topLinkIdx']['index']]->caption; ?>
</a>
          <?php endfor; endif; ?>
        <?php elseif ($this->_tpl_vars['topLinkList']): ?>
         | <a href="<?php echo $this->_tpl_vars['topLinkList']->href; ?>
<?php if ($this->_tpl_vars['topLinkList']->params): ?>?<?php echo $this->_tpl_vars['topLinkList']->params; ?>
<?php endif; ?>"
	      <?php if ($this->_tpl_vars['topLinkList']->target): ?>target="<?php echo $this->_tpl_vars['topLinkList']->target; ?>
"<?php endif; ?>
	      <?php if ($this->_tpl_vars['topLinkList']->class): ?>class="<?php echo $this->_tpl_vars['topLinkList']->class; ?>
"<?php endif; ?>
	      <?php if ($this->_tpl_vars['topLinkList']->style): ?>style="<?php echo $this->_tpl_vars['topLinkList']->style; ?>
"<?php endif; ?>
	      <?php if ($this->_tpl_vars['topLinkList']->onClick): ?>onClick="javascript:<?php echo $this->_tpl_vars['topLinkList']->onClick; ?>
"<?php endif; ?>
	      <?php if ($this->_tpl_vars['topLinkList']->alt): ?>alt="<?php echo $this->_tpl_vars['topLinkList']->alt; ?>
"<?php endif; ?>><?php echo $this->_tpl_vars['topLinkList']->caption; ?>
</a>
        <?php endif; ?>
      <?php endif; ?>
    </td>
    <td width="50%" align="right">
      <?php if ($this->_tpl_vars['pageNumber'] > 1): ?>
        <a href="../login/change_prefs.php?<?php echo $this->_tpl_vars['pageNumberParamName']; ?>
=<?php echo $this->_tpl_vars['pageNumber']-1; ?>
&returnUrl=<?php echo $_SERVER['PHP_SELF']; ?>
">Previous</a>
      <?php endif; ?>
      &nbsp;&nbsp;&nbsp;
      <?php if ($this->_tpl_vars['pageNumber'] < $this->_tpl_vars['numPages']): ?>
        <a href="../login/change_prefs.php?<?php echo $this->_tpl_vars['pageNumberParamName']; ?>
=<?php echo $this->_tpl_vars['pageNumber']+1; ?>
&returnUrl=<?php echo $_SERVER['PHP_SELF']; ?>
">Next</a>
      <?php endif; ?>
    </td>
  </tr>
</table>


<div id="ColumnMenu" style="visibility:hidden; position:absolute; left:0px; top:0px; z-index:10">
<table border="0" class="table_popup" cellpadding="2" cellspacing="0">
<?php $_from = $this->_tpl_vars['columnLabels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['selectColumns'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['selectColumns']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['columnId'] => $this->_tpl_vars['columnLabel']):
        $this->_foreach['selectColumns']['iteration']++;
?>
  <tr>
    <td><?php if ($this->_tpl_vars['columnId'] & $this->_tpl_vars['showColumns']): ?>
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/check_mark.gif"><?php else: ?>&nbsp;<?php endif; ?></td>
    <td><a class="popup" href="../login/change_prefs.php?returnUrl=<?php echo $_SERVER['PHP_SELF']; ?>
&<?php echo $this->_tpl_vars['toggleParamName']; ?>
=<?php echo $this->_tpl_vars['columnId']; ?>
">
	<?php echo $this->_tpl_vars['columnLabel']; ?>
</a></td>
  </tr>
<?php endforeach; endif; unset($_from); ?>
</table>
</div>


<!-- Formerly DisplayPageInfo() Includes page viewing and display info -->
<table border="0" cellpadding="0" cellspacing="0" width="99%">
  <tr>
    <td width="50%">&nbsp;</td><td width="50%">&nbsp;</td>
  </tr>
  <tr>
    <td width="50%">
      <?php if ($this->_tpl_vars['numRows']): ?>
        Viewing <span id="first_row" name="first_row"><?php echo $this->_tpl_vars['firstRow']+1; ?>
</span> - <span id="last_row" name="last_row"><?php echo $this->_tpl_vars['lastRow']+1; ?>
</span> of <span id="num_rows" name="num_rows"><?php echo $this->_tpl_vars['numRows']; ?>
</span>
      <?php else: ?>
        <font color="#FF0000">No matching records found</font>
      <?php endif; ?>
    </td>
    <td width="50%" align="right">
      <?php if ($this->_tpl_vars['numRows']): ?>
        Page <?php echo $this->_tpl_vars['pageNumber']; ?>
 of <?php echo $this->_tpl_vars['numPages']; ?>

      <?php else: ?>
        &nbsp;
      <?php endif; ?>
    </td>
  </tr>
  <tr>
    <td width="50%">&nbsp;</td>
    <td width="50%">&nbsp;</td>
  </tr>
</table>

<!-- end of DisplayPageInfo -->






<?php if ($this->_tpl_vars['numRows']): ?>
<!-- heders and data -->
<table id="dataTable" name="dataTable" border="0" cellpadding="3" cellspacing="0" width="99%" class="table_list">
  <tr>
    <?php if (isset ( $this->_tpl_vars['showSelectedColumn'] ) && $this->_tpl_vars['showSelectedColumn']): ?>
      <td class="table_header" width="20">&nbsp;</td>
    <?php endif; ?>
    <?php $_from = $this->_tpl_vars['columnLabels']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['columnHeaders'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['columnHeaders']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['columnId'] => $this->_tpl_vars['columnLabel']):
        $this->_foreach['columnHeaders']['iteration']++;
?>
      <?php if ($this->_tpl_vars['columnId'] & $this->_tpl_vars['showColumns']): ?>
        <td class="table_header">
        <?php if ($this->_tpl_vars['columnId'] == $this->_tpl_vars['sortColumn']): ?>
	  <?php if ($this->_tpl_vars['sortDescending']): ?>
           <a style="color:#FFFFFF"
	      href="../login/change_prefs.php?returnUrl=<?php echo $_SERVER['PHP_SELF']; ?>
&<?php echo $this->_tpl_vars['orderParamName']; ?>
=0">
	      <?php echo $this->_tpl_vars['columnLabel']; ?>
</a>
	   <a href="../login/change_prefs.php?returnUrl=<?php echo $_SERVER['PHP_SELF']; ?>
&<?php echo $this->_tpl_vars['orderParamName']; ?>
=0">
	      <img border="0" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/order_descend.gif"></a>
	  <?php else: ?>
           <a style="color:#FFFFFF"
	      href="../login/change_prefs.php?returnUrl=<?php echo $_SERVER['PHP_SELF']; ?>
&<?php echo $this->_tpl_vars['orderParamName']; ?>
=1">
	      <?php echo $this->_tpl_vars['columnLabel']; ?>
</a>
	   <a href="../login/change_prefs.php?returnUrl=<?php echo $_SERVER['PHP_SELF']; ?>
&<?php echo $this->_tpl_vars['orderParamName']; ?>
=1">
	      <img border="0" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/order_ascend.gif"</a>
	  <?php endif; ?>
        <?php else: ?>
         <a style="color:#FFFFFF" href="../login/change_prefs.php?returnUrl=<?php echo $_SERVER['PHP_SELF']; ?>
&<?php echo $this->_tpl_vars['sortParamName']; ?>
=<?php echo $this->_tpl_vars['columnId']; ?>
">
	    <?php echo $this->_tpl_vars['columnLabel']; ?>
</a>
        <?php endif; ?>
        </td>
      <?php endif; ?>
    <?php endforeach; endif; unset($_from); ?>
    <?php if (! isset ( $this->_tpl_vars['hideActionHeader'] )): ?>
    <td class="table_header" width="100">
      Actions
    </td>
    <?php endif; ?>
  </tr>



  <!-- This section loops through the list of data provided-->
  <?php unset($this->_sections['recordListIndex']);
$this->_sections['recordListIndex']['name'] = 'recordListIndex';
$this->_sections['recordListIndex']['loop'] = is_array($_loop=$this->_tpl_vars['recordList']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['recordListIndex']['show'] = true;
$this->_sections['recordListIndex']['max'] = $this->_sections['recordListIndex']['loop'];
$this->_sections['recordListIndex']['step'] = 1;
$this->_sections['recordListIndex']['start'] = $this->_sections['recordListIndex']['step'] > 0 ? 0 : $this->_sections['recordListIndex']['loop']-1;
if ($this->_sections['recordListIndex']['show']) {
    $this->_sections['recordListIndex']['total'] = $this->_sections['recordListIndex']['loop'];
    if ($this->_sections['recordListIndex']['total'] == 0)
        $this->_sections['recordListIndex']['show'] = false;
} else
    $this->_sections['recordListIndex']['total'] = 0;
if ($this->_sections['recordListIndex']['show']):

            for ($this->_sections['recordListIndex']['index'] = $this->_sections['recordListIndex']['start'], $this->_sections['recordListIndex']['iteration'] = 1;
                 $this->_sections['recordListIndex']['iteration'] <= $this->_sections['recordListIndex']['total'];
                 $this->_sections['recordListIndex']['index'] += $this->_sections['recordListIndex']['step'], $this->_sections['recordListIndex']['iteration']++):
$this->_sections['recordListIndex']['rownum'] = $this->_sections['recordListIndex']['iteration'];
$this->_sections['recordListIndex']['index_prev'] = $this->_sections['recordListIndex']['index'] - $this->_sections['recordListIndex']['step'];
$this->_sections['recordListIndex']['index_next'] = $this->_sections['recordListIndex']['index'] + $this->_sections['recordListIndex']['step'];
$this->_sections['recordListIndex']['first']      = ($this->_sections['recordListIndex']['iteration'] == 1);
$this->_sections['recordListIndex']['last']       = ($this->_sections['recordListIndex']['iteration'] == $this->_sections['recordListIndex']['total']);
?>
  <?php echo smarty_function_cycle(array('name' => 'rowClass','assign' => 'trStyle','values' => "trDefault,trDefaultAlt"), $this);?>

  <tr<?php if (isset ( $this->_tpl_vars['recordList'][$this->_sections['recordListIndex']['index']]['rowClass'] )): ?> class="<?php echo $this->_tpl_vars['recordList'][$this->_sections['recordListIndex']['index']]['rowClass']; ?>
"<?php else: ?> class="<?php echo $this->_tpl_vars['trStyle']; ?>
"<?php endif; ?>>
    <?php if (isset ( $this->_tpl_vars['showSelectedColumn'] ) && $this->_tpl_vars['showSelectedColumn']): ?>
    <td width="20">
      <?php if (isset ( $this->_tpl_vars['recordId'] ) && $this->_tpl_vars['recordIds'][$this->_sections['recordListIndex']['index']] == $this->_tpl_vars['recordId']): ?>
        <img id="<?php echo $this->_tpl_vars['recordId']; ?>
" border="0" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/check_mark.gif">
      <?php else: ?>
        &nbsp;
      <?php endif; ?>
    </td>
    <?php endif; ?>
    <?php $_from = $this->_tpl_vars['recordList'][$this->_sections['recordListIndex']['index']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['org'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['org']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['orgColumnName'] => $this->_tpl_vars['orgColumnValue']):
        $this->_foreach['org']['iteration']++;
?>
    <?php if (is_numeric ( $this->_tpl_vars['orgColumnName'] )): ?>
    <td>
      <?php if (gettype ( $this->_tpl_vars['orgColumnValue'] ) == 'array'): ?>
        <?php unset($this->_sections['actionIdx']);
$this->_sections['actionIdx']['name'] = 'actionIdx';
$this->_sections['actionIdx']['loop'] = is_array($_loop=$this->_tpl_vars['orgColumnValue']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['actionIdx']['show'] = true;
$this->_sections['actionIdx']['max'] = $this->_sections['actionIdx']['loop'];
$this->_sections['actionIdx']['step'] = 1;
$this->_sections['actionIdx']['start'] = $this->_sections['actionIdx']['step'] > 0 ? 0 : $this->_sections['actionIdx']['loop']-1;
if ($this->_sections['actionIdx']['show']) {
    $this->_sections['actionIdx']['total'] = $this->_sections['actionIdx']['loop'];
    if ($this->_sections['actionIdx']['total'] == 0)
        $this->_sections['actionIdx']['show'] = false;
} else
    $this->_sections['actionIdx']['total'] = 0;
if ($this->_sections['actionIdx']['show']):

            for ($this->_sections['actionIdx']['index'] = $this->_sections['actionIdx']['start'], $this->_sections['actionIdx']['iteration'] = 1;
                 $this->_sections['actionIdx']['iteration'] <= $this->_sections['actionIdx']['total'];
                 $this->_sections['actionIdx']['index'] += $this->_sections['actionIdx']['step'], $this->_sections['actionIdx']['iteration']++):
$this->_sections['actionIdx']['rownum'] = $this->_sections['actionIdx']['iteration'];
$this->_sections['actionIdx']['index_prev'] = $this->_sections['actionIdx']['index'] - $this->_sections['actionIdx']['step'];
$this->_sections['actionIdx']['index_next'] = $this->_sections['actionIdx']['index'] + $this->_sections['actionIdx']['step'];
$this->_sections['actionIdx']['first']      = ($this->_sections['actionIdx']['iteration'] == 1);
$this->_sections['actionIdx']['last']       = ($this->_sections['actionIdx']['iteration'] == $this->_sections['actionIdx']['total']);
?>
	  <?php if (! $this->_sections['actionIdx']['first']): ?>&nbsp;&nbsp;&nbsp;<?php endif; ?>
	  <?php if (gettype ( $this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']] ) == 'object'): ?>
          <a href="<?php echo $this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->href; ?>
<?php if (( strpos ( $this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->href , 'javascript' ) !== 0 && strpos ( $this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->href , '#' ) !== 0 && $this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->isService == FALSE )): ?>?<?php if (isset ( $this->_tpl_vars['idParamName'] )): ?><?php echo $this->_tpl_vars['idParamName']; ?>
=<?php echo $this->_tpl_vars['recordIds'][$this->_sections['recordListIndex']['index']]; ?>
<?php endif; ?><?php endif; ?><?php if ($this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->params): ?><?php if (isset ( $this->_tpl_vars['idParamName'] )): ?>&<?php endif; ?><?php echo $this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->params; ?>
<?php endif; ?>"
	    <?php if ($this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->id): ?>id="<?php echo $this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->id; ?>
"<?php else: ?>id="action<?php echo $this->_sections['actionIdx']['index']; ?>
-<?php echo $this->_sections['recordListIndex']['index']; ?>
"<?php endif; ?>
	    <?php if ($this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->target): ?>target="<?php echo $this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->target; ?>
"<?php endif; ?>
	    <?php if ($this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->class): ?>class="<?php echo $this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->class; ?>
"<?php endif; ?>
	    <?php if ($this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->style): ?>style="<?php echo $this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->style; ?>
"<?php endif; ?>
	    <?php if ($this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->onClick): ?>onClick="return <?php echo $this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->onClick; ?>
;"<?php endif; ?>
	    <?php if ($this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->alt): ?>
	    alt="<?php echo $this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->alt; ?>
"<?php endif; ?>><?php echo $this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]->caption; ?>
</a>
          <?php else: ?>
	    <?php if ($this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]): ?><?php echo $this->_tpl_vars['orgColumnValue'][$this->_sections['actionIdx']['index']]; ?>
<?php else: ?><span class="DisabledText">Not Set</span><?php endif; ?>
	  <?php endif; ?>
	<?php endfor; endif; ?>
      <?php else: ?>
	<?php if (gettype ( $this->_tpl_vars['orgColumnValue'] ) == 'object'): ?>
          <a href="<?php echo $this->_tpl_vars['orgColumnValue']->href; ?>
<?php if (( strpos ( $this->_tpl_vars['orgColumnValue']->href , 'javascript' ) !== 0 && strpos ( $this->_tpl_vars['orgColumnValue']->href , '#' ) !== 0 && $this->_tpl_vars['orgColumnValue']->isService == FALSE )): ?>?<?php if (isset ( $this->_tpl_vars['idParamName'] )): ?><?php echo $this->_tpl_vars['idParamName']; ?>
=<?php echo $this->_tpl_vars['recordIds'][$this->_sections['recordListIndex']['index']]; ?>
<?php endif; ?><?php endif; ?><?php if ($this->_tpl_vars['orgColumnValue']->params): ?><?php if (isset ( $this->_tpl_vars['idParamName'] )): ?>&<?php endif; ?><?php echo $this->_tpl_vars['orgColumnValue']->params; ?>
<?php endif; ?>"
	  <?php if ($this->_tpl_vars['orgColumnValue']->id): ?>id="<?php echo $this->_tpl_vars['orgColumnValue']->id; ?>
"<?php else: ?>id="action0-<?php echo $this->_sections['recordListIndex']['index']; ?>
"<?php endif; ?>
	  <?php if ($this->_tpl_vars['orgColumnValue']->target): ?>target="<?php echo $this->_tpl_vars['orgColumnValue']->target; ?>
"<?php endif; ?>
	  <?php if ($this->_tpl_vars['orgColumnValue']->class): ?>class="<?php echo $this->_tpl_vars['orgColumnValue']->class; ?>
"<?php endif; ?>
	  <?php if ($this->_tpl_vars['orgColumnValue']->style): ?>style="<?php echo $this->_tpl_vars['orgColumnValue']->style; ?>
"<?php endif; ?>
	  <?php if ($this->_tpl_vars['orgColumnValue']->onClick): ?>onClick="return <?php echo $this->_tpl_vars['orgColumnValue']->onClick; ?>
;"<?php endif; ?>
	  <?php if ($this->_tpl_vars['orgColumnValue']->alt): ?>
	      alt="<?php echo $this->_tpl_vars['orgColumnValue']['alt']; ?>
"<?php endif; ?>><?php echo $this->_tpl_vars['orgColumnValue']->caption; ?>
</a>
        <?php else: ?>
	  <?php if ($this->_tpl_vars['orgColumnValue']): ?><?php echo $this->_tpl_vars['orgColumnValue']; ?>
<?php else: ?><span class="DisabledText">Not Set</span><?php endif; ?>
	<?php endif; ?>
      <?php endif; ?>
    </td>
    <?php endif; ?>
    <?php endforeach; endif; unset($_from); ?>
  </tr>
  <?php endfor; endif; ?>
</table>
      

<!-- Display page footer with previous and next links at bottom -->
<table border="0" cellpadding="0" cellspacing="0" width="99%">
  <tr>
    <td width="50%">&nbsp;</td><td width="50%">&nbsp;</td>
  </tr>
  <tr>
    <td width="50%">&nbsp;</td>
    <td width="50%" align="right">
      <?php if ($this->_tpl_vars['pageNumber'] > 1): ?>
        <a href="../login/change_prefs.php?returnUrl=<?php echo $_SERVER['PHP_SELF']; ?>
&<?php echo $this->_tpl_vars['pageNumberParamName']; ?>
=<?php echo $this->_tpl_vars['pageNumber']-1; ?>
">Previous</a>
      <?php endif; ?>
      &nbsp;&nbsp;&nbsp;
      <?php if ($this->_tpl_vars['pageNumber'] < $this->_tpl_vars['numPages']): ?>
        <a href="../login/change_prefs.php?returnUrl=<?php echo $_SERVER['PHP_SELF']; ?>
&<?php echo $this->_tpl_vars['pageNumberParamName']; ?>
=<?php echo $this->_tpl_vars['pageNumber']+1; ?>
">Next</a>
      <?php endif; ?>
    </td>
  </tr>
  <tr>
    <td width="50%">&nbsp;</td>
    <td width="50%">&nbsp;</td>
  </tr>
</table>
<?php endif; ?>

<?php if (isset ( $this->_tpl_vars['includeSequenceMenu'] ) && $this->_tpl_vars['includeSequenceMenu']): ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/sequence_menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['includeGroupMenu'] ) && $this->_tpl_vars['includeGroupMenu']): ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/group_menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?><?php endif; ?>

</body>
</html>