<?php /* Smarty version 2.6.28, created on 2016-12-03 00:49:45
         compiled from admin/org_reports.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/org_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<BODY>

<table border="0">
  <tr>
    <td class="OrgInfoText" height="20">
      <ul>
        <li><a href="org_report_accuracy_by_track.php">Question Accuracy By Class or Date Range</a></li>
	<?php if ($this->_tpl_vars['showAb1825Reports']): ?>
        <li><a href="org_report_ab1825_completion_by_track.php">Completion of AB1825 Training By Class</a></li>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['showIndividualReport']): ?>
	<li><a href="org_report_individual_by_track.php">Individual Assignment Report</a></li>
	<?php endif; ?>
      </ul>
    </td>
    <td class="OrgInfoText" height="20">&nbsp;</td>
  </tr>
</table>

</body>
</html>