<?php /* Smarty version 2.6.28, created on 2016-12-03 06:47:24
         compiled from presentation/contacts.tpl */ ?>
<div id="contactsContent" name="contactsContent" class="FeedbackContent">

  <img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/contact_header.jpg" align="top">

  <?php if (( isset ( $this->_tpl_vars['corpEmail'] ) && $this->_tpl_vars['corpEmail'] ) || ( isset ( $this->_tpl_vars['myContactEmail'] ) && $this->_tpl_vars['myContactEmail'] )): ?>
    <div class="Feedback" style="text-align:center;">To send an email to a contact, click the contact's email address below:</div>
  <?php endif; ?>
  
  <?php if (! isset ( $this->_tpl_vars['corpFullName'] ) && ! isset ( $this->_tpl_vars['corpEmail'] )): ?>
    <p class="HeaderFeedback">No Organization Contact Designated.</p>
  <?php else: ?>
    <p> <div class="HeaderFeedback">Organization Contact:</div>
	<div class="Feedback" style="text-align:center;"><?php echo $this->_tpl_vars['corpFullName']; ?>
<br><?php echo $this->_tpl_vars['corpPhone']; ?>
<br>
        <?php if (isset ( $this->_tpl_vars['corpEmail'] ) && $this->_tpl_vars['corpEmail']): ?>
	  <a href="javascript:DisplayMailForm('<?php echo $this->_tpl_vars['corpFullName']; ?>
', '<?php echo $this->_tpl_vars['corpEmailEncoded']; ?>
');"><?php echo $this->_tpl_vars['corpEmail']; ?>
</a>
	<?php endif; ?>
        </div>
    </p>
  <?php endif; ?>

  <?php if (isset ( $this->_tpl_vars['trackContacts'] )): ?>
    <p class="Feedback" style="text-align:center;">Below are the contacts of each track in which you are currently participating:</p>
    <?php unset($this->_sections['trackContactsIdx']);
$this->_sections['trackContactsIdx']['name'] = 'trackContactsIdx';
$this->_sections['trackContactsIdx']['loop'] = is_array($_loop=$this->_tpl_vars['trackContacts']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['trackContactsIdx']['show'] = true;
$this->_sections['trackContactsIdx']['max'] = $this->_sections['trackContactsIdx']['loop'];
$this->_sections['trackContactsIdx']['step'] = 1;
$this->_sections['trackContactsIdx']['start'] = $this->_sections['trackContactsIdx']['step'] > 0 ? 0 : $this->_sections['trackContactsIdx']['loop']-1;
if ($this->_sections['trackContactsIdx']['show']) {
    $this->_sections['trackContactsIdx']['total'] = $this->_sections['trackContactsIdx']['loop'];
    if ($this->_sections['trackContactsIdx']['total'] == 0)
        $this->_sections['trackContactsIdx']['show'] = false;
} else
    $this->_sections['trackContactsIdx']['total'] = 0;
if ($this->_sections['trackContactsIdx']['show']):

            for ($this->_sections['trackContactsIdx']['index'] = $this->_sections['trackContactsIdx']['start'], $this->_sections['trackContactsIdx']['iteration'] = 1;
                 $this->_sections['trackContactsIdx']['iteration'] <= $this->_sections['trackContactsIdx']['total'];
                 $this->_sections['trackContactsIdx']['index'] += $this->_sections['trackContactsIdx']['step'], $this->_sections['trackContactsIdx']['iteration']++):
$this->_sections['trackContactsIdx']['rownum'] = $this->_sections['trackContactsIdx']['iteration'];
$this->_sections['trackContactsIdx']['index_prev'] = $this->_sections['trackContactsIdx']['index'] - $this->_sections['trackContactsIdx']['step'];
$this->_sections['trackContactsIdx']['index_next'] = $this->_sections['trackContactsIdx']['index'] + $this->_sections['trackContactsIdx']['step'];
$this->_sections['trackContactsIdx']['first']      = ($this->_sections['trackContactsIdx']['iteration'] == 1);
$this->_sections['trackContactsIdx']['last']       = ($this->_sections['trackContactsIdx']['iteration'] == $this->_sections['trackContactsIdx']['total']);
?>
    <p> <div class="HeaderFeedback"> Track: <?php echo $this->_tpl_vars['trackContacts'][$this->_sections['trackContactsIdx']['index']][0]; ?>
</div>
      <div class="Feedback" style="text-align:center;"><?php echo $this->_tpl_vars['trackContacts'][$this->_sections['trackContactsIdx']['index']][1]->firstName; ?>
 <?php echo $this->_tpl_vars['trackContacts'][$this->_sections['trackContactsIdx']['index']][1]->lastName; ?>
<br><?php echo $this->_tpl_vars['trackContacts'][$this->_sections['trackContactsIdx']['index']][1]->phone; ?>
<br>
      <?php if (isset ( $this->_tpl_vars['trackContacts'][$this->_sections['trackContactsIdx']['index']][1]->email ) && $this->_tpl_vars['trackContacts'][$this->_sections['trackContactsIdx']['index']][1]->email): ?>
	<a href="javascript:DisplayMailForm('<?php echo $this->_tpl_vars['trackContacts'][$this->_sections['trackContactsIdx']['index']][1]->firstName; ?>
 <?php echo $this->_tpl_vars['trackContacts'][$this->_sections['trackContactsIdx']['index']][1]->lastName; ?>
', '<?php echo $this->_tpl_vars['trackContacts'][$this->_sections['trackContactsIdx']['index']][1]->email; ?>
');"><?php echo $this->_tpl_vars['trackContacts'][$this->_sections['trackContactsIdx']['index']][1]->email; ?>
</a>
      <?php endif; ?>
      </div>
    </p>
    <?php endfor; endif; ?>
  <?php else: ?>
    <?php if (! isset ( $this->_tpl_vars['myContactFullName'] ) && ! isset ( $this->_tpl_vars['myContactEmail'] )): ?>
    <p class="HeaderFeedback">No Class Contact(s) Designated.</p>
    <?php else: ?>
    <p> <div class="HeaderFeedback"><?php if (isset ( $this->_tpl_vars['a'] ) && $this->_tpl_vars['a']->categoryId == 118): ?>Training Advisor Contact<?php else: ?>Your Class Contact<?php endif; ?>:</div>
      <div class="Feedback" style="text-align:center;"><?php echo $this->_tpl_vars['myContactFullName']; ?>
<br><?php echo $this->_tpl_vars['myContactPhone']; ?>
<br>
      <?php if (isset ( $this->_tpl_vars['myContactEmail'] ) && $this->_tpl_vars['myContactEmail']): ?>
	<a href="javascript:DisplayMailForm('<?php echo $this->_tpl_vars['myContactFullName']; ?>
', '<?php echo $this->_tpl_vars['myContactEmail']; ?>
');"><?php echo $this->_tpl_vars['myContactEmail']; ?>
</a>
      <?php endif; ?>
      </div>
    </p>
    <?php endif; ?>
  <?php endif; ?>

  <p style="text-align:center;">
    <a href="javascript:<?php if ($this->_tpl_vars['preview']): ?>window.close();<?php else: ?>HideContacts(); if (!review) SubmitAnswer(); else window.location.href='portal.php';<?php endif; ?>"
	 onmouseout="document.images.btnContactCloseImage.src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_desktop.png'"
	 onmouseover="document.images.btnContactCloseImage.src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_desktop-mo.png'">
      <img name="btnContactCloseImage" id="btnContactCloseImage" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_desktop.png" alt="Desktop" border="0">
    </a>
  </p>

</div>

<!-- Content for mail form displayed if user clicks to send a message to a contact -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'presentation/sendmail.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>