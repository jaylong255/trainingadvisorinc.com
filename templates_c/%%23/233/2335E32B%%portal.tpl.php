<?php /* Smarty version 2.6.28, created on 2016-12-03 06:47:24
         compiled from presentation/portal.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'math', 'presentation/portal.tpl', 3, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'presentation/presentation_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<BODY marginwidth='0' marginheight='0' topmargin='0' leftmargin='0' onload="javascript:ChangeUserTabs(<?php echo smarty_function_math(array('equation' => ($this->_tpl_vars['currentTab'])." - 100"), $this);?>
, '<?php echo $this->_tpl_vars['uiTheme']; ?>
');">

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'common/motif.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $this->assign('tabsVPos', 267); ?>
<?php $this->assign('tabsHPos', 5); ?>


<table border='0' cellpadding='10' cellspacing='0' width='100%' height='50'>
  <tr>
    <td valign='middle' width='40%' nowrap>
      <b><font face="Arial, Helvetica, sans-serif" size="+1">Welcome <?php echo $this->_tpl_vars['firstName']; ?>
 <?php echo $this->_tpl_vars['lastName']; ?>
</font></b>
    </td>
    <td valign='middle' width='15%' nowrap>
      <?php if ($this->_tpl_vars['superUser'] || $this->_tpl_vars['role'] != ROLE_END_USER): ?><a class="Links" href="/desktop/admin/main.php?init=1">Admin</a><?php endif; ?>
    </td>
    <td valign='middle' width='15%' nowrap>
      <A class="Links" HREF="logout.php">Logout</A>
    </td>
    <td valign='middle' align='left' width='15%' nowrap colspan="2">
      <A class="Links" HREF="javascript:review=1; preview=0; DisplayContacts();">Contact</A><!--contacts.php?displayHeader=1-->
    </td>
  </tr>
</table>



<div id="Tab1" style="position:absolute; top:<?php echo $this->_tpl_vars['tabsVPos']; ?>
; left:<?php echo $this->_tpl_vars['tabsHPos']; ?>
;">
  <a href="menu1.php" target="QMenu" onclick="javascript:ChangeUserTabs(1, '<?php echo $this->_tpl_vars['uiTheme']; ?>
');">
    <img name="imgTab1" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/assigned_off.gif" border="0"></a>
</div>
<div id="Tab2" style="position:absolute; top:<?php echo $this->_tpl_vars['tabsVPos']; ?>
; left:<?php echo $this->_tpl_vars['tabsHPos']+113; ?>
;">
  <a href="menu2.php" target="QMenu" onclick="javascript:ChangeUserTabs(2, '<?php echo $this->_tpl_vars['uiTheme']; ?>
');">
    <img name="imgTab2" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/completed_off.gif" border="0"></a>
</div>
<div id="Tab3" style="position:absolute; top:<?php echo $this->_tpl_vars['tabsVPos']; ?>
; left:<?php echo $this->_tpl_vars['tabsHPos']+226; ?>
;">
  <a href="menu3.php" target="QMenu" onclick="javascript:ChangeUserTabs(3, '<?php echo $this->_tpl_vars['uiTheme']; ?>
');">
    <img name="imgTab3" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/faq_off.gif" border="0"></a>
</div>
<div id="Tab4" style="position:absolute; top:<?php echo $this->_tpl_vars['tabsVPos']; ?>
; left:<?php echo $this->_tpl_vars['tabsHPos']+297; ?>
;">
  <a href="menu4.php" target="QMenu" onclick="javascript:ChangeUserTabs(4, '<?php echo $this->_tpl_vars['uiTheme']; ?>
');">
    <img name="imgTab4" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/scorecard_off.jpg" border="0"></a>
</div>






<table border='0' cellpadding='0' cellspacing='0' width='100%' height='100%'>
  <tr>
    <td valign='top' width='80%'>
      <table border='0' cellpadding='0' cellspacing='0' width='100%' background='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/bg_question.gif'
         style='background-repeat:repeat-x;'>
        <tr>
          <td rowspan="2"><img src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/question_left.gif' border='0'></td>
	  <td valign='top' height="40"><img src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/today_question.gif' border='0' align='left'><br></td>
          <td align='right' rowspan="2"><img src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/bg_question_right.gif' border='0'></td>
        </tr>
        <tr>
          <td valign='top' height="50"><!--img src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/transparent.gif' width='70' height='1' border='0' align='left'-->
	      <?php if (isset ( $this->_tpl_vars['errMsg'] ) && $this->_tpl_vars['errMsg']): ?>
	        <div class="error"><?php echo $this->_tpl_vars['errMsg']; ?>
</div>
	      <?php elseif (count ( $this->_tpl_vars['assignments'] ) > 0): ?>
	     	<div class="CurrentQuestionTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		Current Topic: <a class="Links" target="_parent" href="q_type.php?assignmentId=<?php echo $this->_tpl_vars['assignments'][0]['Assignment_ID']; ?>
"><?php echo $this->_tpl_vars['assignments'][0]['Title']; ?>
</a></div>
	      <?php else: ?>
	     	<div class="CurrentQuestionTitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;There are currently no questions for today</div>
	      <?php endif; ?>
          </td>
        </tr>
      </table>
      <BR>
      <table border='0' cellpadding='0' cellspacing='0' width='100%' bgcolor='#FFFFFF'>
        <tr>
          <td valign='bottom' colspan='3' height="32">&nbsp;</td>
	</tr>
	<tr>
	  <td width='10' height='8'><img src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/body_top_left.gif' border='0'></td>
	  <td background='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/body_top.gif' style='background-repeat:repeat-x;'></td>
	  <td width='15' height='8'><img src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/body_top_right.gif' border='0'></td>
	</tr>
	<tr>
	  <td width='10' background='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/body_left.gif' style='background-repeat:repeat-y;'></td>
	  <td valign='top' class="stdbg">
	    <table border='0' cellpadding='4' cellspacing='0' class="stdbg">
	      <tr>
		<td>
		  <div id="Instructions" class="Instructions"></div>
		</td>
	      </tr>
	      <tr>
		<td valign='top'>
		  <iframe name="QMenu" id="QMenu" src="menu<?php echo smarty_function_math(array('equation' => ($this->_tpl_vars['currentTab'])." - 100"), $this);?>
.php" marginheight="0"
			  marginwidth="0" width="575" height="300" scrolling="auto" frameborder="0"></iframe>
		</td>
	     </tr>
	   </table>
	  </td>
	  <td width='15' background='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/body_right.gif' style='background-repeat:repeat-y;'></td>
        </tr>
        <tr>
	  <td width='10' height='18' align='right'><img src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/body_bottom_left.gif' border='0'></td>
	  <td align='right' background='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/body_bottom.gif' style='background-repeat:repeat-x;'></td>
	  <td align='right' width='15' height='18'><img src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/body_bottom_right.gif' border='0'></td>
        </tr>
      </table>
      <table border='0' cellpadding='10' cellspacing='0' width='100%'>
		<tr>
			<td valign='middle' width='33%' nowrap>
			<?php if (isset ( $this->_tpl_vars['showPoweredBy'] ) && $this->_tpl_vars['showPoweredBy']): ?>
			<div id="Powered">
				<A class="bottomLinks" TARGET="new" HREF="http://www.trainingadvisorinc.com"><i>Powered by Training Advisor, Inc.</i></A>
			</div>
			<?php else: ?>
			&nbsp;
			<?php endif; ?>
			</td>
			<td valign='middle' width='33%' nowrap>
			<div id="ChangePassword">
				<A class="bottomLinks" HREF="setpass.php"><i>Change Password</i></A>
			</div>
			</td>
			<td valign='middle' width='33%' nowrap>
			<div id="Disclaimer">
				<A class="bottomLinks" HREF="disclaimer.php"><i>Disclaimer & Credits</i></A>
			</div>
			</td>
		</tr>
	</table>
	</td>
	<td valign='top' width='20%' height='80%'>
	<table border='0' cellpadding='0' cellspacing='0' width='240' height='90%'>
		<tr>
			<td valign='top' height='52'><img src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/news_top.png' border='0'></td>
		</tr>
		<tr>
			<td align='center' valign='top' background='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/news_middle.gif' style='background-repeat:repeat-y;'>
			<iframe name="News" id="News" src="<?php echo $this->_tpl_vars['newsUrl']; ?>
" marginheight="0" marginwidth="0"
				width="200" height='310' scrolling="auto" frameborder="0"></iframe>
			</td>
		</tr>
		<tr>
			<td valign='bottom' height='18'><img src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/news_bottom.gif' border='0'></td>
		</tr>
	</table>
	</td>
</tr>
</table>

<!-- Content for feedback window displayed when a user selects an answer to a question -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'presentation/feedback.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Content for contacts window displayed when a user selects the Contact button on the thank you page -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'presentation/contacts.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Content for the display of assignments -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'presentation/assignments.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Content for the display of Faq answers -->
<div id="faqContent" name="faqContent" class="FeedbackContent">
  <img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/faq_answer_header.jpg" align="top">
  <p>
    <div class="Feedback" style="text-align:center;" name="answerContent" id="answerContent">&nbsp;</div>
  </p>
  <p style="text-align:center;">
    <a href="javascript:HideFaq();"
	 onmouseout="document.images.btnContactCloseImage.src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_desktop.png'"
	 onmouseover="document.images.btnContactCloseImage.src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_desktop-mo.png'">
      <img name="btnContactCloseImage" id="btnContactCloseImage" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_desktop.png" alt="Desktop" border="0">
    </a>
  </p>
</div>


</BODY>
</HTML>