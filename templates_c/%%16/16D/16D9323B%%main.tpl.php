<?php /* Smarty version 2.6.28, created on 2016-12-03 00:05:08
         compiled from admin/main.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<BODY class="noscroll" bgcolor="#FFFFFF" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" width="100%"
      onResize="javascript:GetSize();" onLoad="GetSize(); MM_preloadImages('../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_help_down.gif');
                SetupImgDesk('<?php echo $this->_tpl_vars['uiTheme']; ?>
');">

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'common/motif.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $this->assign('tabsVPos', 124); ?>
<?php $this->assign('tabsHPos', 26); ?>
<?php $this->assign('subtabsVPos', 148); ?>

<!-- Organization Name -->
<div id="Title" style="position:absolute; left:25px; top:100px; width:600px; height:26px; text-align:left;" class="Title">
	<?php echo $this->_tpl_vars['orgName']; ?>
</div>

<!-- To Desktop button -->
<div id="ToDesktop" style="position:absolute; left:565px; top:100px;">
      <a href="/desktop/login/change_prefs.php?currentTab=101&returnUrl=/desktop/presentation/portal.php"
	 onMouseOver="swap(toDesk,imgDeskb);"
	 onMouseOut="swap(toDesk,imgDesk);">
      <img name="toDesk" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/to_desktop_up.png" border="0"></a>
</div>

<!-- Top level tabs -->
<div id="Tab1Grey" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']; ?>
px; top:<?php echo $this->_tpl_vars['tabsVPos']; ?>
px; z-index:2">
	<?php if ($this->_tpl_vars['superUser'] && ! isset ( $this->_tpl_vars['orgId'] )): ?>
	  <a href="javascript:BranchTab('org_list.php');" target="FormWindow" onclick="javascript:SetTab(1, 0);">
	  <img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/builder_org_behind.png" border="0" id="OrgTab"></a>
	<?php elseif ($this->_tpl_vars['role'] == ROLE_ORG_ADMIN): ?>
	  <a href="javascript:BranchTab('org_info.php');" target="FormWindow" onclick="javascript:SetTab(1, 1);">
	  <img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/builder_org_behind.png" border="0" id="OrgTab"></a>
	<?php else: ?>
	  <a href="javascript:BranchTab('org_reports.php');" target="FormWindow" onclick="javascript:SetTab(1, 5);">
	  <img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/builder_org_behind.png" border="0" id="OrgTab"></a>
        <?php endif; ?>
	</div>
<div id="Tab2Grey" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+100; ?>
px; top:<?php echo $this->_tpl_vars['tabsVPos']; ?>
px; z-index:2">
	<?php if (( $this->_tpl_vars['superUser'] || $this->_tpl_vars['role'] != ROLE_CLASS_CONTACT_SUPERVISOR )): ?>
	  <a href="javascript:BranchTab('user_list.php');" target="FormWindow" onclick="javascript:SetTab(2, 0);">
	  <img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/builder_employee_behind.png" border="0" id="EmployeeTab"></a>
	<?php endif; ?>
	</div>
<div id="Tab3Grey" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+200; ?>
px; top:<?php echo $this->_tpl_vars['tabsVPos']; ?>
px; width:107px; height:26px; z-index:2">
	  <a href="javascript:BranchTab('track_list.php');" target="FormWindow" onclick="javascript:SetTab(3, 0);">
	  <img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/builder_tracks_behind.png" border="0" id="TracksTab"></a>
	</div>
<div id="Tab4Grey" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+301; ?>
px; top:<?php echo $this->_tpl_vars['tabsVPos']; ?>
px; width:107px; height:26px; z-index:2">
	<?php if (( $this->_tpl_vars['superUser'] || $this->_tpl_vars['role'] != ROLE_CLASS_CONTACT_SUPERVISOR )): ?>
	  <a href="javascript:BranchTab('question_list.php');" target="FormWindow" onclick="javascript:SetTab(4, 0);">
	  <img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/builder_questions_behind.png" border="0" id="QuestionsTab"></a>
	<?php endif; ?>
	</div>
<div id="Tab5Grey" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+400; ?>
px; top:<?php echo $this->_tpl_vars['tabsVPos']; ?>
px; width:107px; height:26px; z-index:2">
	<?php if (( $this->_tpl_vars['superUser'] || $this->_tpl_vars['role'] != ROLE_CLASS_CONTACT_SUPERVISOR )): ?>
	  <a href="javascript:BranchTab('file_man.php');" target="FormWindow" onclick="javascript:SetTab(5, 0);">
	  <img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/builder_customize_back.png" border="0" id="CustomizeTab"></a>
	<?php endif; ?>
	</div>


<div id="Tab1" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']; ?>
px; top:<?php echo $this->_tpl_vars['tabsVPos']; ?>
px; width:107px; height:26px; z-index:3; border:1px none #000000;
	<?php if ($this->_tpl_vars['currentTab'] == 1 && ( $this->_tpl_vars['superUser'] || $this->_tpl_vars['role'] != ROLE_END_USER )): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/builder_org_forward.png" border="0" id="OrgTab"></div>
<div id="Tab2" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+100; ?>
px; top:<?php echo $this->_tpl_vars['tabsVPos']; ?>
px; width:107px; height:26px; z-index:3;
	<?php if ($this->_tpl_vars['currentTab'] == 2): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/builder_employee_forward.png" border="0" id="EmployeeTab"></div>
<div id="Tab3" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+200; ?>
px; top:<?php echo $this->_tpl_vars['tabsVPos']; ?>
px; width:107px; height:26px; z-index:3;
	<?php if ($this->_tpl_vars['currentTab'] == 3): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/builder_tracks_forward.png" border="0" id="TracksTab"></div>
<div id="Tab4" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+300; ?>
px; top:<?php echo $this->_tpl_vars['tabsVPos']; ?>
px; width:107px; height:26px; z-index:3;
	<?php if ($this->_tpl_vars['currentTab'] == 4): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/builder_questions_forward.png" border="0" id="QuestionsTab"></div>
<div id="Tab5" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+400; ?>
px; top:<?php echo $this->_tpl_vars['tabsVPos']; ?>
px; width:107px; height:26px; z-index:3;
	<?php if ($this->_tpl_vars['currentTab'] == 5): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/builder_customize_forward.png" border="0" id="CustomizeTab"></div>


<!-- These div tags make up the backgrounds of the primary and secondary tab folders -->
<div id="AdminOuterFolder" class="AdminOuterFolder" style="position:absolute; left:0px; top:<?php echo $this->_tpl_vars['subtabsVPos']-1; ?>
px;"></div>

<div id="ManillaFolder" class="manillafolder" style="position:absolute; left:18px; top:<?php echo $this->_tpl_vars['tabsVPos']+52; ?>
px;">&nbsp;</div>


<div id="FormTab" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; width:211px; height:23px; z-index:11; border:1px none #000000;
	<?php if ($this->_tpl_vars['currentSubTab'] == 0): ?>visibility:visible;<?php else: ?>visibility:hidden;<?php endif; ?>">
	<!--img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/blank_up.png"--></div>


<!-- question subtabs -->
<div id="CurrentTab2" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:12;
	<?php if ($this->_tpl_vars['currentTab'] == 4 && $this->_tpl_vars['currentSubTab'] > 2 && $this->_tpl_vars['currentSubTab'] != 6): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="javascript:BranchTab('question_list.php');" target="FormWindow" onclick="javascript:SetQuestionSubTab(1);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/current_up.png" border="0"></a></div>

<div id="CurrentTab1" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:13;
	<?php if ($this->_tpl_vars['currentTab'] == 4 && ( $this->_tpl_vars['currentSubTab'] == 1 || $this->_tpl_vars['currentSubTab'] == 2 || $this->_tpl_vars['currentSubTab'] == 6 )): ?> visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/current_down.png" border="0"></div>

<div id="ArchiveTab2" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+66; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:14;
	<?php if ($this->_tpl_vars['currentTab'] == 4 && $this->_tpl_vars['currentSubTab'] != 0 && $this->_tpl_vars['currentSubTab'] != 3 && $this->_tpl_vars['currentSubTab'] != 7): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="javascript:BranchTab('archive_list.php');" target="FormWindow" onclick="javascript:SetQuestionSubTab(3);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/archived_up.png" border="0"></a></div>

<div id="ArchiveTab1" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+66; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:15;
	<?php if ($this->_tpl_vars['currentTab'] == 4 && $this->_tpl_vars['currentSubTab'] == 3 || $this->_tpl_vars['currentSubTab'] == 7): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/archived_down.png" border="0"></div>

<div id="SummaryTab2" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+138; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:16;
	<?php if ($this->_tpl_vars['currentTab'] == 4 && $this->_tpl_vars['currentSubTab'] != 0 && $this->_tpl_vars['currentSubTab'] != 4): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="javascript:BranchTab('question_summary.php');" target="FormWindow" onclick="javascript:SetQuestionSubTab(4);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/summary_up.png" border="0"></a></div>

<div id="SummaryTab1" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+138; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:17;
	<?php if ($this->_tpl_vars['currentTab'] == 4 && $this->_tpl_vars['currentSubTab'] == 4): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/summary_down.png" border="0"></div>

<div id="AnalysisTab2" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+217; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:18;
	<?php if ($this->_tpl_vars['currentTab'] == 4 && $this->_tpl_vars['currentSubTab'] != 0 && $this->_tpl_vars['currentSubTab'] != 5): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="javascript:BranchTab('question_analysis.php');" target="FormWindow" onclick="javascript:SetQuestionSubTab(5);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/analysis_up.png" border="0"></a></div>

<div id="AnalysisTab1" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+217; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:19;
	<?php if ($this->_tpl_vars['currentTab'] == 4 && $this->_tpl_vars['currentSubTab'] == 5): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/analysis_down.png" border="0"></div>

<div id="CategoriesUnselected" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+286; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:18;
	<?php if ($this->_tpl_vars['currentTab'] == 4 && $this->_tpl_vars['currentSubTab'] != 0 && $this->_tpl_vars['currentSubTab'] != 8): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="javascript:BranchTab('org_data_edit.php?form=3');" target="FormWindow" onclick="javascript:SetQuestionSubTab(8);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/categories_up.png" border="0"></a></div>

<div id="CategoriesSelected" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+286; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:19;
	<?php if ($this->_tpl_vars['currentTab'] == 4 && $this->_tpl_vars['currentSubTab'] == 8): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/categories_down.png" border="0"></div>


<!-- track subtabs -->
<div id="Track1g" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:20; border:1px none #000000;
	<?php if ($this->_tpl_vars['currentTab'] == 3 && $this->_tpl_vars['currentSubTab'] > 1): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="javascript:BranchTab('track_edit.php');" id="Track1Ref" target="FormWindow" onclick="javascript:SetTrackSubTab(1);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/info_up.png" border="0"></a></div>
<div id="Track1" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:21; border:1px none #000000;
	<?php if ($this->_tpl_vars['currentTab'] == 3 && $this->_tpl_vars['currentSubTab'] == 1): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/info_down.png" border="0"></div>
<div id="Track2g" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+48; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:22; border:1px none #000000;
	<?php if ($this->_tpl_vars['currentTab'] == 3 && $this->_tpl_vars['currentSubTab'] != 0 && $this->_tpl_vars['currentSubTab'] != 2): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="javascript:BranchTab('participant_list.php');" id="Track2Ref" target="FormWindow" onclick="javascript:SetTrackSubTab(2);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/participants_up.png" border="0"></a></div>
<div id="Track2" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+48; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:23; border:1px none #000000;
	<?php if ($this->_tpl_vars['currentTab'] == 3 && $this->_tpl_vars['currentSubTab'] == 2): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/participants_down.png" border="0"></div>
<div id="Track3g" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+136; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:24; border:1px none #000000;
	<?php if ($this->_tpl_vars['currentTab'] == 3 && $this->_tpl_vars['currentSubTab'] != 0 && $this->_tpl_vars['currentSubTab'] != 3): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="javascript:BranchTab('assignment_list.php');" id="Track3Ref" target="FormWindow" onclick="javascript:SetTrackSubTab(3);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/assignments_up.png" border="0"></a></div>
<div id="Track3" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+136; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:25; border:1px none #000000;
	<?php if ($this->_tpl_vars['currentTab'] == 3 && $this->_tpl_vars['currentSubTab'] == 3): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/assignments_down.png" border="0"></div>
<div id="Track4g" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+236; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:26; border:1px none #000000;
	<?php if ($this->_tpl_vars['currentTab'] == 3 && $this->_tpl_vars['currentSubTab'] != 0 && $this->_tpl_vars['currentSubTab'] != 4): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="javascript:BranchTab('faq_list.php');" id="Track4Ref" target="FormWindow" onclick="javascript:SetTrackSubTab(4);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/FAQs_up.png" border="0"></a></div>
<div id="Track4" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+236; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:27; border:1px none #000000;
	<?php if ($this->_tpl_vars['currentTab'] == 3 && $this->_tpl_vars['currentSubTab'] == 4): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/FAQs_down.png" border="0"></div>
<div id="Track5g" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+289; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:28; border:1px none #000000;
	<?php if ($this->_tpl_vars['currentTab'] == 3 && $this->_tpl_vars['currentSubTab'] != 0 && $this->_tpl_vars['currentSubTab'] != 5): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="javascript:BranchTab('progress_list.php');" id="Track5Ref" target="FormWindow" onclick="javascript:SetTrackSubTab(5);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/progress_up.png" border="0"></a></div>
<div id="Track5" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+289; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:29; border:1px none #000000;
	<?php if ($this->_tpl_vars['currentTab'] == 3 && $this->_tpl_vars['currentSubTab'] == 5): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/progress_down.png" border="0"></div>
<div id="Track6g" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+362; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:28; border:1px none #000000;
	<?php if ($this->_tpl_vars['currentTab'] == 3 && $this->_tpl_vars['currentSubTab'] != 0 && $this->_tpl_vars['currentSubTab'] != 6): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="javascript:BranchTab('class_email.php');" id="Track6Ref" target="FormWindow" onclick="javascript:SetTrackSubTab(6);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/builder_sendemail_up.png" border="0"></a></div>
<div id="Track6" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+362; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:29; border:1px none #000000;
	<?php if ($this->_tpl_vars['currentTab'] == 3 && $this->_tpl_vars['currentSubTab'] == 6): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/builder_sendemail_down.png" border="0"></div>


<!-- organization subtabs -->
<?php if ($this->_tpl_vars['role'] == ROLE_ORG_ADMIN): ?>
<div id="Org1g" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:30;
	<?php if ($this->_tpl_vars['currentTab'] == 1 && $this->_tpl_vars['currentSubTab'] > 1): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="org_info.php" id="Org1Ref" target="FormWindow" onclick="javascript:SetOrgSubTab(1);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/info_up.png" border="0"></a></div>
<div id="Org1" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:31;
	<?php if ($this->_tpl_vars['currentTab'] == 1 && $this->_tpl_vars['currentSubTab'] == 1): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/info_down.png" border="0"></div>
<div id="Org2g" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+48; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:32;
	<?php if ($this->_tpl_vars['currentTab'] == 1 && $this->_tpl_vars['currentSubTab'] != 0 && $this->_tpl_vars['currentSubTab'] != 2): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="org_contact.php" id="Org2Ref" target="FormWindow" onclick="javascript:SetOrgSubTab(2);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/contact_up.png" border="0"></a></div>
<div id="Org2" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+48; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:33;
	<?php if ($this->_tpl_vars['currentTab'] == 1 && $this->_tpl_vars['currentSubTab'] == 2): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/contact_down.png" border="0"></div>
<div id="Org3g" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+114; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:34;
	<?php if ($this->_tpl_vars['currentTab'] == 1 && $this->_tpl_vars['currentSubTab'] != 0 && $this->_tpl_vars['currentSubTab'] != 3): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="org_data.php" id="Org3Ref" target="FormWindow" onclick="javascript:SetOrgSubTab(3);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/data_up.png" border="0"></a></div>
<div id="Org3" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+114; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:35;
	<?php if ($this->_tpl_vars['currentTab'] == 1 && $this->_tpl_vars['currentSubTab'] == 3): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/data_down.png" border="0"></div>
<div id="Org4g" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+160; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:36;
	<?php if ($this->_tpl_vars['currentTab'] == 1 && $this->_tpl_vars['currentSubTab'] != 0 && $this->_tpl_vars['currentSubTab'] != 4): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="org_utilities.php" id="Org3Ref" target="FormWindow" onclick="javascript:SetOrgSubTab(4);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/utilities_up.png" border="0"></a></div>
<div id="Org4" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+160; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:37;
	<?php if ($this->_tpl_vars['currentTab'] == 1 && $this->_tpl_vars['currentSubTab'] == 4): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/utilities_down.png" border="0"></div>
<?php endif; ?>
<div id="Org5g" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+226; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:38;
	<?php if ($this->_tpl_vars['currentTab'] == 1 && $this->_tpl_vars['currentSubTab'] != 0 && $this->_tpl_vars['currentSubTab'] != 5): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="org_reports.php" id="Org3Ref" target="FormWindow"<?php if ($this->_tpl_vars['role'] == ROLE_ORG_ADMIN): ?> onclick="javascript:SetOrgSubTab(5);"<?php endif; ?>>
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/reports_up.png" border="0"></a></div>
<div id="Org5" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+226; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:39;
	<?php if ($this->_tpl_vars['currentTab'] == 1 && $this->_tpl_vars['currentSubTab'] == 5): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/reports_down.png" border="0"></div>


<!-- customize subtabs -->
<div id="FileMan2" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:12;
	<?php if ($this->_tpl_vars['currentTab'] == 5 && $this->_tpl_vars['currentSubTab'] > 1): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="javascript:BranchTab('file_man.php');" target="FormWindow" onclick="javascript:SetCustomizeSubTab(1);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/file_manager_up.png" border="0"></a></div>
<div id="FileMan1" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:13;
	<?php if ($this->_tpl_vars['currentTab'] == 5 && $this->_tpl_vars['currentSubTab'] == 1): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/file_manager_down.png" border="0"></div>
<?php if ($this->_tpl_vars['role'] == ROLE_ORG_ADMIN): ?>
<div id="CustMsg2" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+102; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:14;
	<?php if ($this->_tpl_vars['currentTab'] == 5 && $this->_tpl_vars['currentSubTab'] != 0 && $this->_tpl_vars['currentSubTab'] != 2): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<a href="javascript:BranchTab('cust_misc.php');" target="FormWindow" onclick="javascript:SetCustomizeSubTab(2);">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/misc_up.png" border="0"></a></div>
<div id="CustMsg1" style="position:absolute; left:<?php echo $this->_tpl_vars['tabsHPos']+102; ?>
px; top:<?php echo $this->_tpl_vars['subtabsVPos']; ?>
px; z-index:15;
	<?php if ($this->_tpl_vars['currentTab'] == 5 && $this->_tpl_vars['currentSubTab'] == 2): ?>visibility:visible<?php else: ?>visibility:hidden<?php endif; ?>">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/misc_down.png" border="0"></div>
<?php endif; ?>

<div id="HelpDiv" style="position:absolute; left:3px; top:<?php echo $this->_tpl_vars['tabsVPos']+27; ?>
px; width:24px; height:25px; z-index:51">
	<a href="javascript:Help();" onMouseOut="MM_swapImgRestore()"
           onMouseOver="MM_swapImage('Help','','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_help_down.gif',1)">
	<img name="Help" border="0" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_help_up.gif" width="22" height="22"
	     alt="Online Help is not available yet."></a></div>

<div id="FormDiv" style="position:absolute; left:25px; top:<?php echo $this->_tpl_vars['tabsVPos']+58; ?>
px; width:90%; height:90%; z-index:7; border:1px none #000000;">
  <IFRAME NAME="FormWindow" ID="FormWindow" SCROLLING="auto" WIDTH="100%" HEIGHT="100%"
	SRC="<?php echo $this->_tpl_vars['frameTarget']; ?>
"  FRAMEBORDER="0"></IFRAME>
</div>

</body>
</html>