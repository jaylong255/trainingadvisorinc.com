<?php /* Smarty version 2.6.28, created on 2016-12-03 00:05:10
         compiled from admin/user_header.tpl */ ?>
<BODY class="ContentFrameBody"
      onLoad="MM_preloadImages('../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_update_up.gif','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_update_down.gif',
				 '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_add_up.gif','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_add_down.gif',
				 '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_back_down.gif');
	<?php if (isset ( $this->_tpl_vars['user'] ) && ! $this->_tpl_vars['user']->password): ?>document.getElementById('password').value='';<?php endif; ?>">

<?php if ($this->_tpl_vars['currentSubTab'] > 0): ?>
<table border="0" width="680" align="top" cellpadding="2" cellspacing="3">
  <tr>
    <td width="23">
      <A href="user_list.php" onClick="return CheckSave();"
	 onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Back','','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_back_down.gif',1)">
        <img name="Back" border="0" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_back_up.gif" width="23" height="21"></a>
    </td>
    <td align="left">
      <a class="OrgInfoText" href="user_list.php">Return to Employee List</a>
    </td>
    <td align="left">
    <?php if ($this->_tpl_vars['user']->userId != 0): ?>
      <a href="user_edit.php?userIdAsTemplate=<?php echo $this->_tpl_vars['user']->userId; ?>
">Create User With Below As Template</a>
    <?php else: ?>&nbsp;<?php endif; ?>
    </td>
    <td align="right">
      <?php if ($this->_tpl_vars['user']->userId == 0): ?>
	<a href="javascript:AddRecordUserEdit();" onMouseOut="MM_swapImgRestore()"
	   onMouseOver="MM_swapImage('AddBtn','','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_add_down.gif',1)">
	<!--onClick="document.getElementById('Cover').style.visibility='visible';-->
	  <img name="Add" border="0" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_add_up.gif" width="56" height="22"></a>
      <?php else: ?>
	<a href="javascript:UpdateRecordUserEdit();" onMouseOut="MM_swapImgRestore()"
	   onMouseOver="MM_swapImage('Update','','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_update_down.gif',1)">
	  <img name="Update" border="0" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_update_up.gif" width="56" height="22"></a>
      <?php endif; ?>
    </td>
  </tr>
  <?php if (isset ( $this->_tpl_vars['errMsg'] ) && $this->_tpl_vars['errMsg']): ?>
  <tr>
    <td colspan="4" class="error">
      <?php echo $this->_tpl_vars['errMsg']; ?>

    </td>
  </tr>
  <?php endif; ?>
</table>

<hr width="680" align="left">
<?php endif; ?>