<?php /* Smarty version 2.6.28, created on 2016-12-03 00:51:35
         compiled from admin/cust_misc.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'admin/cust_misc.tpl', 31, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<BODY class="ContentFrameBody">

<form>
<?php if ($this->_tpl_vars['superUser']): ?>
<span class="FormText">Options available only to super user</span><BR>
<hr style="height:4px">

<span class="RecurrenceFormText">Features:</span><BR>
<input type="checkbox" name="enableQuestionPrinting" value="1"<?php if ($this->_tpl_vars['enableQuestionPrinting']): ?> checked<?php endif; ?>> Check this to allow folks in this organization to print questions.<BR>
<input type="checkbox" name="showUserThatAnswered" value="1"<?php if ($this->_tpl_vars['showUserThatAnswered']): ?> checked<?php endif; ?>> Check this to show the user that answered each question in the Summary report.<BR>

<hr>

<span class="RecurrenceFormText">Content Licensing:</span><BR>
<input type="checkbox" name="licenseHr" value="1"<?php if (isset ( $this->_tpl_vars['licenseHr'] ) && $this->_tpl_vars['licenseHr']): ?> checked<?php endif; ?>>This organization is licensed for HR content<BR>
<input type="checkbox" name="licenseAb1825" value="1"<?php if (isset ( $this->_tpl_vars['licenseAb1825'] ) && $this->_tpl_vars['licenseAb1825']): ?> checked<?php endif; ?>>This organization is licensed for AB1825 content<BR>
<input type="checkbox" name="licenseEdu" value="1"<?php if (isset ( $this->_tpl_vars['licenseEdu'] ) && $this->_tpl_vars['licenseEdu']): ?> checked<?php endif; ?>>This organization is licensed for School District content<BR>

<?php endif; ?>

<hr>

<BR><BR>
<span class="FormText">Options available to organization admins</span><BR>
<hr style="height:4px">

<span class="RecurrenceFormText">Application Theme:</span><BR>
<select name="uiTheme" onChange="javascript:parent.location.href='main.php?changeUiTheme='+this.options[this.selectedIndex].value">
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['uiThemes'],'selected' => $this->_tpl_vars['selectedTheme']), $this);?>

</select>

<hr>

<span class="RecurrenceFormText">Email Format:</span><BR>
Select the format for email messages delivered by the system to class participants:<BR>
<select name="emailFormat">
<?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['emailFormats'],'output' => $this->_tpl_vars['emailFormats'],'selected' => $this->_tpl_vars['selectedEmailFormat']), $this);?>

</select>

<hr>

<span class="RecurrenceFormText">Content Layout Options:</span><BR>
<input type="radio" name="displayChoices" value="right"<?php if ($this->_tpl_vars['displayChoices'] == 'right'): ?> checked<?php endif; ?>>Display choices to the right of the question (default)<BR>
<input type="radio" name="displayChoices" value="bottom"<?php if ($this->_tpl_vars['displayChoices'] == 'bottom'): ?> checked<?php endif; ?>>Display choices below the question<BR>
<input type="checkbox" name="useQuestionSetting" value="1"<?php if (isset ( $this->_tpl_vars['useQuestionSetting'] ) && $this->_tpl_vars['useQuestionSetting']): ?> checked<?php endif; ?>>Use setting from the question but if question setting is not defined, use the above selection as default option<BR>

<hr>

<span class="RecurrenceFormText">Data Reporting Options:</span><BR>
<input type="checkbox" name="trackAdminSummaryDefault" value="1"<?php if (isset ( $this->_tpl_vars['trackAdminSummaryDefault'] ) && $this->_tpl_vars['trackAdminSummaryDefault']): ?> checked<?php endif; ?>> Limit summary data to class administrators be default.  Viewing all summary data will still be available.

<hr>

<input type="submit" name="submit" value="Set">

</form>


</BODY>
</HTML>