<?php /* Smarty version 2.6.28, created on 2016-12-03 00:04:57
         compiled from login/login.tpl */ ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
  <TITLE>Training Advisor Portal Login</TITLE>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <LINK rel="stylesheet" href="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/desktop.css" type="text/css">
  <SCRIPT language="javascript" type="text/javascript" src="../javascript/rpc.js"></script>
  <SCRIPT language="javascript" type="text/javascript" src="../javascript/desktop.js"></script>
</HEAD>
<BODY bgcolor="#FFFFFF" onLoad="MM_preloadImages('../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/login/btn_logon_dn.gif'); SetFocus(document.AdminForm.User_ID);<?php if (! isset ( $this->_tpl_vars['fromEmail'] )): ?> svcGetUserOrgs('User_ID');<?php endif; ?>">
	<div id="Layer1" style="position:absolute; left:0px; top:0px; z-index:1"><img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/login/logon_header.gif"></div>
	<?php if (isset ( $this->_tpl_vars['showTaLogo'] ) && $this->_tpl_vars['showTaLogo']): ?>
	  <div id="Layer2" style="position:absolute; left:629px; top:0px; z-index:2"><img src="../organizations/hr_tools/motif/hrtools_logo.gif"></div>
	<?php endif; ?>
	<!--div id="Layer3" style="position:absolute; left:213px; top:87px; z-index:3"><img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/login/head_note.gif"></div-->
	<div id="Layer4" style="position:absolute; left:92px; top:201px; z-index:4"><img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/login/redbar.gif"></div>
	<div id="Layer5" class="FormText" style="position:absolute; left:80px; top:163px; width:6290px; height:25px; z-index:5">Welcome!
	  Please enter your <b>User ID</b> and <b>Password</b>, then click the <b>Login</b>
	  button.</div>
	<div id="Layer8" style="position:absolute; left:260px; top:284px; width:114px; height:43px; z-index:8">
	  <div align="right" class="FormText1">Password:</div>
	</div>
	<div id="Layer9" style="position:absolute; left:260px; top:324px; width:114px; height:43px; z-index:10;">
	  <div <?php if (! $this->_tpl_vars['showOrgIdSelect']): ?>style="visibility:hidden;" <?php endif; ?>id="lbl_Org" align="right" class="FormText1">Organization:</div>
	</div>
	<?php if (isset ( $this->_tpl_vars['showPoweredBy'] ) && $this->_tpl_vars['showPoweredBy']): ?>
	<div id="Layer11" style="position:absolute; left:342px; top:427px; width:183px; height:21px; z-index:11">
	  <a href="http://www.trainingadvisorinc.com" class="CRText">Powered by Training Advisor, Inc.</a></div>
	<?php endif; ?>
	<div id="Layer12" class="CRText" style="position:absolute; left:371px; top:443px; width:131px; height:23px; z-index:12">Copyright <?php echo $this->_tpl_vars['copyrightYear']; ?>
</div>
	<div id="Layer6" style="position:absolute; left:297px; top:245px; width:77px; height:26px; z-index:6">
	<div align="right" class="FormText1">User ID:</div>
	</div>

<form name="AdminForm" method="POST" action="javascript:SubmitForm();">
	<div style="position:absolute; left:378px; top:244px; width:298px; height:32px; z-index:7">
		<input id="User_ID" name="User_ID" type="text" size="30" maxlength="45" value="<?php echo $this->_tpl_vars['User_ID']; ?>
"<?php if (! isset ( $this->_tpl_vars['fromEmail'] )): ?> onChange="javascript:svcGetUserOrgs('User_ID');"<?php endif; ?>>
	</div>
	<div style="position:absolute; left:378px; top:284px; width:298px; height:34px; z-index:9">
		<input id="UPassword" name="UPassword" type="password" size="30" maxlength="30" value="">
	</div>
	<div id="orgSelect" style="position:absolute; left:378px; top:324px; width:298px; height:34px; z-index:11;<?php if (! $this->_tpl_vars['showOrgIdSelect']): ?> visibility:hidden;<?php endif; ?>">
		<select class="login" style="width:201px;" id="Org_ID" name="Org_ID" size="1">
		  <?php if (isset ( $this->_tpl_vars['fromEmail'] )): ?><option value="<?php echo $this->_tpl_vars['Org_ID']; ?>
"><?php echo $this->_tpl_vars['Org_ID']; ?>
</option><?php endif; ?>
		</select>
		<BR>
		<input type="checkbox" name="saveOrg" id="saveOrg">
		Save my organization selection.
	</div>
	<div id="Layer10" style="position:absolute; left:278px; top:414px; width:259px; height:55px; z-index:12">
		<input type="image" border="0" name="imageField" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/login/btn_logon_up.gif" width="259" height="55">
	</div>
</form>

<div style="position:absolute; left:119px; top:375px; width:627px; height:32px; z-index:12">
	<font face="Arial, Helvetica, sans-serif">
	  <span class='red'><?php echo $this->_tpl_vars['ErrMsg']; ?>
</span>
	</font>
	</div>

</body>
</html>