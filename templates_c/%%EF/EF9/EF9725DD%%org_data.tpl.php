<?php /* Smarty version 2.6.28, created on 2016-12-03 00:55:14
         compiled from admin/org_data.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/org_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="DivOrgForm" class="orgForm">
<p>
<div id="Page1" class="OrgInfoText">Select an item below to add, modify, or delete values for that item.</div>
</p>

<!-- Table for displaying the menu selection on the left and the edit subwindow on the right -->
<table border="0" width="575">
  <?php unset($this->_sections['label']);
$this->_sections['label']['name'] = 'label';
$this->_sections['label']['loop'] = is_array($_loop=$this->_tpl_vars['editLabels']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['label']['show'] = true;
$this->_sections['label']['max'] = $this->_sections['label']['loop'];
$this->_sections['label']['step'] = 1;
$this->_sections['label']['start'] = $this->_sections['label']['step'] > 0 ? 0 : $this->_sections['label']['loop']-1;
if ($this->_sections['label']['show']) {
    $this->_sections['label']['total'] = $this->_sections['label']['loop'];
    if ($this->_sections['label']['total'] == 0)
        $this->_sections['label']['show'] = false;
} else
    $this->_sections['label']['total'] = 0;
if ($this->_sections['label']['show']):

            for ($this->_sections['label']['index'] = $this->_sections['label']['start'], $this->_sections['label']['iteration'] = 1;
                 $this->_sections['label']['iteration'] <= $this->_sections['label']['total'];
                 $this->_sections['label']['index'] += $this->_sections['label']['step'], $this->_sections['label']['iteration']++):
$this->_sections['label']['rownum'] = $this->_sections['label']['iteration'];
$this->_sections['label']['index_prev'] = $this->_sections['label']['index'] - $this->_sections['label']['step'];
$this->_sections['label']['index_next'] = $this->_sections['label']['index'] + $this->_sections['label']['step'];
$this->_sections['label']['first']      = ($this->_sections['label']['iteration'] == 1);
$this->_sections['label']['last']       = ($this->_sections['label']['iteration'] == $this->_sections['label']['total']);
?>
    <tr>
      <td width="15" align="left">
        <img id="pointer<?php echo $this->_sections['label']['iteration']-1; ?>
" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/pointer.gif"
	     width="12" height="11" style="visibility: <?php if ($this->_sections['label']['first']): ?>visible<?php else: ?>hidden<?php endif; ?>">
      </td>
      <td width="185" align="left">
        <a class="OrgInfoText" href="org_data_edit.php?form=<?php echo $this->_sections['label']['iteration']-1; ?>
"
	   target="DataEditFrame"
	   onclick="javascript:ChangePointer(<?php echo $this->_sections['label']['iteration']-1; ?>
);"><?php echo $this->_tpl_vars['editLabels'][$this->_sections['label']['index']]; ?>
:</a>
      </td>
      <?php if ($this->_sections['label']['first']): ?>
      <td rowspan="8" width="375">
        <div id="IFrameDiv">
          <IFRAME NAME="DataEditFrame" ID="DataEditFrameId" SCROLLING="auto" WIDTH="100%" HEIGHT="100%"
		 FRAMEBORDER="<?php echo $this->_tpl_vars['frameBoarderSize']; ?>
" SRC="org_data_edit.php?form=<?php echo $this->_tpl_vars['currentDataEditFrame']; ?>
"></IFRAME>
        </div>
      </td>
      <?php endif; ?>
    </tr>
  <?php endfor; endif; ?>
    <tr>
      <td width="15" align="left">
        &nbsp;
      </td>
      <td width="185" align="left">
        &nbsp;
      </td>
    </tr>
</table>
  
<!--p>

<div id="Text2" class="PageText">
 * Optional fields that give you the ability to further categorize your employees. <BR>
 These categories might include location (other than division or department), job title, shift, etc.
</div>

</p-->

</div>
</BODY>
</HTML>